﻿=====================================
Gamemode_Slayer Modding Documentation
=====================================

Slayer is highly extensible and can be used to easily create your next game mode.

.. contents::
   :backlinks: top
.. sectnum::
   :suffix: .

The Basics
==========

Slayer can be used by your game mode in two ways:

-	*Embedded:* Embed Slayer in your "`Blockland Game Mode`_", with no coding required.
-	*Custom:* Code your own game mode using the Slayer Game Mode API.

.. _`Blockland Game Mode`: http://forum.blockland.us/index.php?topic=203327.0

Embedding Slayer in Your Game Mode
==================================

Slayer can be embedded in a standard Blockland game mode. This requires no coding experience.

Simply follow the steps below:

1.	While using the Custom game mode, create a Slayer minigame and set it up the way you want it to be for your game mode.
2.	End the minigame.
3.	Navigate to "config/server/Slayer" and find the files called "last.mgame.csv" and "last.teams.csv".
4.	Rename the files to "slayer.mgame.csv" and "slayer.teams.csv" and place them in your game mode's folder.
5.	Add the line "ADDON Gamemode_Slayer" to your GameMode.txt file.
6.	Ensure that "$Minigame::Enabled 1" is not in your gameMode.txt file.

After following these steps and loading your game mode, a Slayer minigame should automatically be created with the settings you chose in step 1.

Creating a Custom Game Mode
===========================

Coding experience is necessary to create a custom Slayer game mode. However, Slayer exposes a large variety of useful functions and APIs to make this process much easier.

Getting Started: An Example
---------------------------

Every Slayer game mode is defined using a template that contains information such as the name, default preferences, and teams.

Here is an example game mode definition::

	new ScriptGroup(Slayer_GameModeTemplateSG)
	{
		// Game mode settings
		className = "ExampleGameMode";
		uiName = "Example Mode";
		useTeams = true;
		// disable_teamCreation = true;

		// Team settings
		// teams_minTeams = 2;
		// teams_maxTeams = 4;

		// Default minigame settings
		default_title = "Slayer Example";
		default_weaponDamage = false;
		
		// Locked minigame settings
		locked_playerDatablock = nameToID("PlayerStandardArmor");
		locked_lives = 10;
		locked_weaponDamage = true;
		locked_teams_allySameColors = false;
		locked_chat_enableTeamChat = false;
		
		// Teams
		new ScriptObject()
		{
			// Prevent team from being deleted
			disable_delete = true;
			disable_edit = false;
			
			// Default team settings
			default_botFillLimit = 5;
			
			// Locked team settings
			locked_name = "Red";
			locked_lives = 5;
			locked_color = 0;
			locked_sort = true;
		};
		
		new ScriptObject()
		{
			// Prevent team from being deleted
			disable_delete = false;
			disable_edit = false;
			
			// Default team settings
			default_name = "Blue";
			
			// Locked team settings
			locked_color = 3;
			locked_sort = false;
			locked_syncLoadout = true;
		};
		
		new ScriptObject()
		{
			disable_edit = true;
			disable_delete = true;
			default_name = "Green";
			default_color = 2;
		};
	};

Let's walk through the above example. The first line specifies that this object is a ScriptGroup of class "Slayer_GameModeTemplateSG".

Next, we see some "game mode settings". These fields are required for every game mode.

-	'className' is a unique class name that will be used in callbacks for this game mode.
-	'uiName' is the name that players will see.
-	'useTeams' specifies whether this is a team-based game mode.

Default minigame settings can be set using "default_*" fields. These settings are set the given values when the game mode starts, but a user may change them at any time.

Locked minigame settings, denoted by "locked_*" are the same as default settings, except that they cannot be changed by the user. **It is recommended that locked settings be used sparingly - give the user as much flexibility as possible!**

Default teams can be created by creating ScriptObjects inside the game mode template. Team preferences follow the same conventions as game mode preferences.

Additional minigame preferences may be found at `Appendix B: Minigame Preferences List`_, and team preferences may be found at `Appendix C: Team Preferences List`_.

Game Mode Callbacks
-------------------

Slayer contains many callbacks that allow game modes to alter game-play. To view a list of callbacks, see the "gamemode-callbacks.html" file in this folder.

The basic structure of a callback looks like this::

	// This function will be called when the game mode starts.
	function YourGameModeClassName::onGameModeStart(%this)
	{
		// %this is the game mode object, used for storing game mode information.
		// Each instance of the game mode will have a separate game mode object.
		// Get the minigame:
		%minigame = %this.minigame;
		echo("Started gamemode" SPC %this SPC "in minigame" SPC %minigame @ ".");
	}

Refer to `Appendix A: Game Mode Callbacks Reference`_ to fully customize game-play.

Creating Custom Preferences
===========================

The preference API is very similar to the game mode API.

Here is an example::

	new ScriptObject(Slayer_PrefSO)
	{
		category = "Example Category";
		title = "Example Title";
		defaultValue = 42;
		permissionLevel = $Slayer::PermissionLevel["Any"];
		variable = "%mini.myVariable";
		type = "int";
		int_minValue = 0;
		int_maxValue = 100;
		callback = "myCallback(%1, %2);";
		notifyPlayersOnChange = true;
		requiresMiniGameReset = false;
		requiresServerRestart = false;
		priority = 1000;
	};

This will create an integer preference called "Example Title" with a default value of 42. It cannot be set below 0 or above 100. Other aspects of this example preference can be determined using the table below.
Possible preference fields are listed below:

+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
|  Field                | Required         | Default Value                    | Description                                                                                                                  |
+=======================+==================+==================================+==============================================================================================================================+
| callback              | No               |                                  | A function to be called when this preference is changed. myCallback(%1,%2); where %1 is new and %2 is old value.             |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| category              | Yes              |                                  | The category for this preference.                                                                                            |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| defaultValue          | Yes              |                                  | This will be the initial setting of the preference.                                                                          |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| doNotReset            | No               |                                  | If true, this preference will never be reset to its defaultValue.                                                            |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| doNotSendToClients    | No               |                                  | If true, this preference will not be transmitted over the network and cannot be modified by clients.                         |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| guiTag                | No               |                                  | Used by the GUI for display grouping purposes.                                                                               |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| notifyPlayersOnChange | No               | true                             | Whether to alert players that this preference was changed.                                                                   |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| permissionLevel       | Yes              | $Slayer::PermissionLevel["Any"]  | The minimum permission level required to change this setting. See `Appendix D: Permission Levels`_ for more information.     |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| priority              | Yes              | 1000                             | Affects the order that this preference is saved and transmitted over the network. Lower numbers have higher priority.        |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| requiresMiniGameReset | No               |                                  | If true, the minigame will automatically reset when this preference is changed.                                              |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| requiresServerRestart | No               |                                  | If true, the server must be restarted after changing this variable.                                                          |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| title                 | Yes              |                                  | The name of this preference.                                                                                                 |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| type                  | Yes              |                                  | The type of the variable that this preference modifies. [bool|int|string|slide|object|list]                                  |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| variable              | Yes              |                                  | The variable that this preference will modify.                                                                               |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| int_maxValue          | If type="int"    |                                  | The maximum integer allowed.                                                                                                 |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| int_minValue          | If type="int"    |                                  | The minimum integer allowed.                                                                                                 |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| list_items            | If type="list"   |                                  | A list of items in the format                                                                                                |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| object_cannotBeNull   | If type="object" |                                  | Whether this variable can be null.                                                                                           |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| object_class          | If type="object" |                                  | The class of objects this preference represents.                                                                             |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| slide_maxValue        | If type="slide"  |                                  | The maximum integer allowed.                                                                                                 |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| slide_minValue        | If type="slide"  |                                  | The minimum integer allowed.                                                                                                 |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| slide_numTicks        | If type="slide"  |                                  | The number of ticks to show in the GUI.                                                                                      |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| slide_snapToTicks     | If type="slide"  |                                  | Whether to snap to tick marks in the GUI.                                                                                    |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| string_maxLength      | If type="string" |                                  | The maximum allowed string size.                                                                                             |
+-----------------------+------------------+----------------------------------+------------------------------------------------------------------------------------------------------------------------------+

Slayer 3 Compatibility
======================

Most Slayer 3 APIs and callbacks will still function in Slayer 4. However, new projects should use Slayer 4 and existing projects should be converted if possible.

Appendices
==========

The follow appendices are for reference purposes.

Appendix A: Game Mode Callbacks Reference
-----------------------------------------

This is a list of all included game mode callbacks and their arguments. More detail, including descriptions and return values, is available in the "doc" folder of Gamemode_Slayer.zip. This table is meant to be a quick reference, not a complete guide.

+---------------------------+------------------------------------------------------------------------------------------+
|  Callback Name            | Arguments                                                                                |
+===========================+==========================================================================================+
| assignBotObjectives       | %bot                                                                                     |
+---------------------------+------------------------------------------------------------------------------------------+
| miniGameCanDamage         | %objA, %objB                                                                             |
+---------------------------+------------------------------------------------------------------------------------------+
| miniGameClientCanDamage   | %clientA, %clientB                                                                       |
+---------------------------+------------------------------------------------------------------------------------------+
| miniGameCanUse            | %objA, %objB                                                                             |
+---------------------------+------------------------------------------------------------------------------------------+
| onClientJoinGame          | %client                                                                                  |
+---------------------------+------------------------------------------------------------------------------------------+
| onClientLeaveGame         | %client                                                                                  |
+---------------------------+------------------------------------------------------------------------------------------+
| onClientChat              | %client, %msg                                                                            |
+---------------------------+------------------------------------------------------------------------------------------+
| onClientDisplayRules      | %client                                                                                  |
+---------------------------+------------------------------------------------------------------------------------------+
| onClientSetScore          | %client, %score                                                                          |
+---------------------------+------------------------------------------------------------------------------------------+
| onClientSpectateFree      | %client                                                                                  |
+---------------------------+------------------------------------------------------------------------------------------+
| onClientSpectateTarget    | %client, %target                                                                         |
+---------------------------+------------------------------------------------------------------------------------------+
| onCPReset                 | %capturePoint, %color, %oldColor, %client                                                |
+---------------------------+------------------------------------------------------------------------------------------+
| onCPCapture               | %capturePoint, %color, %oldColor, %client                                                |
+---------------------------+------------------------------------------------------------------------------------------+
| onCreateDeathMessage      | %client, %killer, %value                                                                 |
+---------------------------+------------------------------------------------------------------------------------------+
| onGameModeStart           |                                                                                          |
+---------------------------+------------------------------------------------------------------------------------------+
| onGameModeEnd             |                                                                                          |
+---------------------------+------------------------------------------------------------------------------------------+
| onMinigameReset           | %client                                                                                  |
+---------------------------+------------------------------------------------------------------------------------------+
| preMinigameReset          | %client                                                                                  |
+---------------------------+------------------------------------------------------------------------------------------+
| onMiniGameBrickAdded      | %brick, %type                                                                            |
+---------------------------+------------------------------------------------------------------------------------------+
| onMiniGameBrickRemoved    | %brick, %type                                                                            |
+---------------------------+------------------------------------------------------------------------------------------+
| onObserverTrigger         | %observer, %camera, %button, %state, %client                                             |
+---------------------------+------------------------------------------------------------------------------------------+
| onPlayerSpawn             | %client                                                                                  |
+---------------------------+------------------------------------------------------------------------------------------+
| onPlayerDeath             | %client, %obj, %killer, %type, %area                                                     |
+---------------------------+------------------------------------------------------------------------------------------+
| prePlayerDeath            | %client, %obj, %killer, %type, %area                                                     |
+---------------------------+------------------------------------------------------------------------------------------+
| onPreRoundCountdownTick   | %totalTicks, %remainingTicks                                                             |
+---------------------------+------------------------------------------------------------------------------------------+
| onRoundStart              |                                                                                          |
+---------------------------+------------------------------------------------------------------------------------------+
| preRoundEnd               | %winner, %nameList                                                                       |
+---------------------------+------------------------------------------------------------------------------------------+
| onRoundEnd                | %winner, %nameList                                                                       |
+---------------------------+------------------------------------------------------------------------------------------+
| onTeamAdd                 | %team                                                                                    |
+---------------------------+------------------------------------------------------------------------------------------+
| onTeamRemove              | %team                                                                                    |
+---------------------------+------------------------------------------------------------------------------------------+
| onTeamShuffle             | %teamHandler, %shuffleMode, %doNotRespawn                                                |
+---------------------------+------------------------------------------------------------------------------------------+
| onTeamSwap                | %teamHandler, %clientA, %clientB                                                         |
+---------------------------+------------------------------------------------------------------------------------------+
| onTeamChat                | %client, %msg                                                                            |
+---------------------------+------------------------------------------------------------------------------------------+
| onClientJoinTeam          | %team, %client                                                                           |
+---------------------------+------------------------------------------------------------------------------------------+
| onClientLeaveTeam         | %team, %client                                                                           |
+---------------------------+------------------------------------------------------------------------------------------+
| pickPlayerSpawnPoint      | %client                                                                                  |
+---------------------------+------------------------------------------------------------------------------------------+
| scoreListInit             | %header, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9                   |
+---------------------------+------------------------------------------------------------------------------------------+
| scoreListCheckSendTeams   |                                                                                          |
+---------------------------+------------------------------------------------------------------------------------------+
| scoreListCheckSendPlayers |                                                                                          |
+---------------------------+------------------------------------------------------------------------------------------+
| scoreListAdd              | %object, %line, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9            |
+---------------------------+------------------------------------------------------------------------------------------+
| victoryCheck_lives        |                                                                                          |
+---------------------------+------------------------------------------------------------------------------------------+
| victoryCheck_points       |                                                                                          |
+---------------------------+------------------------------------------------------------------------------------------+
| victoryCheck_time         |                                                                                          |
+---------------------------+------------------------------------------------------------------------------------------+

Appendix B: Minigame Preferences List
-------------------------------------

This is a list of all default Slayer preferences. Variables that are not prefixed by a dollar sign ($) are minigame member fields.

+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Category        | Title                         | Variable                                       | Type   | Default Value                     |
+=================+===============================+================================================+========+===================================+
|  Chat           | Allow Dead Talking            | chat_deadChatMode                              | list   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Chat            | Death Message Mode            | deathMsgMode                                   | list   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Chat            | Enable Team Chat              | chat_enableTeamChat                            | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Chat            | Team Display Mode             | chat_teamDisplayMode                           | list   | 2                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Chat            | Disable All Slayer Messages   | disableSlayerMessages                          | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Damage          | Bot                           | botDamage                                      | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Damage          | Brick                         | brickDamage                                    | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Damage          | Falling                       | fallingDamage                                  | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Damage          | Self                          | selfDamage                                     | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Damage          | Vehicle                       | vehicleDamage                                  | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Damage          | Weapon                        | weaponDamage                                   | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| EoRR            | Display End of Round Report   | eorrEnable                                     | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| EoRR            | Display Team Scores           | eorrDisplayTeamScores                          | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| EoRR            | Display Victory/Defeat        | eorrDisplayVictory                             | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Allow Movement During Reset   | allowMoveWhileResetting                        | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Auto Start With Server        | $Pref::Slayer::Server::AutoStartMiniGame       | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Clear Scores on Reset         | clearScores                                    | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Clear Stats on Reset          | clearStats                                     | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Color                         | colorIdx                                       | int    | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Custom Rule                   | customRule                                     | string |                                   |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Default Minigame              | isDefaultMinigame                              | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Enable Building               | enableBuilding                                 | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Enable Painting               | enablePainting                                 | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Enable Wand                   | enableWand                                     | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Game Mode                     | gameModeTemplate                               | object | Slayer_DefaultGameModeTemplateSG  |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Host Name                     | hostName                                       | string | HOST                              |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Invite-Only                   | inviteOnly                                     | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Late Join Time                | lateJoinTime                                   | slide  | -1                                |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Players Use Own Bricks        | playersUseOwnBricks                            | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Pre-Round Countdown           | preRoundSeconds                                | int    | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Reset When Empty              | resetOnEmpty                                   | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Spawn Brick Color             | spawnBrickColor                                | int    | -1                                |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Time Between Rounds           | timeBetweenRounds                              | int    | 10                                |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Title                         | Title                                          | string | Slayer                            |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Use All Players' Bricks       | useAllPlayersBricks                            | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Minigame        | Use Spawn Bricks              | useSpawnBricks                                 | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Spectators      | Enable Auto-Camera Mode       | spectateAutoCam                                | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Spectators      | Spectate Capture Points       | spectateCapturePoints                          | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Permissions     | Create Minigame Rights        | $Pref::Slayer::Server::MiniGameCreationRights  | list   | 3                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Permissions     | Edit Rights                   | editRights                                     | list   | 3                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Permissions     | Leave Rights                  | leaveRights                                    | list   | 2                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Permissions     | Reset Rights                  | resetRights                                    | list   | 3                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Player          | Enable Lights                 | enablePlayerLights                             | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Player          | Enable Suicide                | enablePlayerSuicide                            | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Player          | Name Distance                 | nameDistance                                   | slide  | 140                               |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Player          | Playertype                    | playerDatablock                                | object | 38                                |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Player          | Starting Equipment 0          | startEquip0                                    | object | 65                                |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Player          | Starting Equipment 1          | startEquip1                                    | object | 75                                |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Player          | Starting Equipment 2          | startEquip2                                    | object | 383                               |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Player          | Starting Equipment 3          | startEquip3                                    | object | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Player          | Starting Equipment 4          | startEquip4                                    | object | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Points          | Break Brick                   | points_breakBrick                              | int    | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Points          | CP Capture                    | points_CP                                      | int    | 10                                |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Points          | Die                           | points_die                                     | int    | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Points          | Friendly Fire                 | points_friendlyFire                            | int    | -5                                |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Points          | Kill Bot                      | points_killBot                                 | int    | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Points          | Kill Player                   | points_killPlayer                              | int    | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Points          | Plant Brick                   | points_plantBrick                              | int    | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Points          | Suicide                       | points_killSelf                                | int    | -1                                |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Respawn Time    | Bot                           | respawnTime_bot                                | int    | 5                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Respawn Time    | Brick                         | respawnTime_brick                              | int    | 30                                |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Respawn Time    | Progressive FF Penalty        | friendlyFireProgressivePenalty                 | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Respawn Time    | Friendly Fire Penalty         | respawnPenalty_FriendlyFire                    | int    | 5                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Respawn Time    | Player                        | respawnTime_player                             | int    | 5                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Respawn Time    | Vehicle                       | respawnTime_vehicle                            | int    | 10                                |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Victory Method  | Lives                         | lives                                          | int    | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Victory Method  | Points                        | points                                         | int    | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Victory Method  | Time                          | time                                           | int    | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Server          | Announcements                 | announcements                                  | string |                                   |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Server          | Save Settings on Update       | $Pref::Slayer::Server::SavePrefsOnUpdate       | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Ally Same Colors              | teams_allySameColors                           | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Auto Sort                     | teams_autoSort                                 | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Balance New Teams             | teams_balanceOnNew                             | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Balance Teams                 | teams_balanceTeams                             | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Display Join/Leave Messages   | teams_notifyMemberChanges                      | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Friendly Fire                 | teams_friendlyFire                             | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Lock Teams                    | teams_lock                                     | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Punish Friendly Fire          | teams_punishFF                                 | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Display Friendly Fire Warning | friendlyFireDisplayWarning                     | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Rounds Between Shuffle        | teams_roundsBetweenShuffles                    | int    | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Shuffle Teams                 | teams_shuffleTeams                             | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Shuffle Mode                  | teams_shuffleMode                              | list   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Swap/Join Mode                | teams_swapMode                                 | list   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Team-Only DeadCam             | teams_teamOnlyDeadCam                          | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Teams           | Max Teams                     | $Pref::Server::Slayer::Teams::maxTeams         | int    | 100                               |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Capture Points  | Tick Time                     | $Pref::Slayer::Server::CPTriggerTickMS         | int    | 150                               |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Capture Points  | Use Transitional Colors       | CPTransitionColors                             | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Events          | Max Team Events               | $Pref::Slayer::Server::Teams::maxEvents        | int    | 6                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Events          | Restrict Output Events        | restrictOutputEvents                           | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Bricks          | Create Team Bot Holes         | $Pref::Slayer::Server::CreateTeamBotHoles      | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Bricks          | Place Bricks in               | $Pref::Slayer::Server::CreateBrickUISlayerTab  | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Server          | Old TDM Mod Compatible        | $Pref::Slayer::Server::LoadTDMCompatibility    | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Fly-Through Cam | Play At Beginning of Round    | flyCam_playOnReset                             | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Fly-Through Cam | Countdown During Fly-Thru     | flyCam_playDuringCountdown                     | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Fly-Through Cam | Rounds Between Fly-Thrus      | flyCam_roundsBetweenFlyThroughs                | bool   | 0                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Bonus Kills     | Enable                        | bKills_enable                                  | bool   | 1                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Bonus Kills     | Kill Spree Start              | bKills_kSpree                                  | int    | 4                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Bonus Kills     | Bonus Points per Kill Spree   | bKills_pointsKillSpree                         | int    | 4                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+
| Bonus Kills     | Bonus Points per Multi-Kill   | bKills_pointsMultiKill                         | int    | 4                                 |
+-----------------+-------------------------------+------------------------------------------------+--------+-----------------------------------+

Appendix C: Team Preferences List
---------------------------------

This is a list of all default team preferences.

+-----------+--------------------------+--------------------------+---------+-----------------------+
|  Category | Title                    | Variable                 | Type    | Default Value         |
+===========+==========================+==========================+=========+=======================+
| Bots      | Preferred Player Count   | botFillLimit             | int     | -1                    |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Captain   | Playertype               | captain_playerDatablock  | object  | 38                    |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Captain   | Start Equip 0            | captain_startEquip0      | object  | 65                    |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Captain   | Start Equip 1            | captain_startEquip1      | object  | 75                    |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Captain   | Start Equip 2            | captain_startEquip2      | object  | 383                   |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Captain   | Start Equip 3            | captain_startEquip3      | object  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Captain   | Start Equip 4            | captain_startEquip4      | object  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Chat      | Enable Team Chat         | enableTeamChat           | bool    | 1                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Chat      | Hide Kills               | hideKillMsgs             | bool    | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Chat      | Name Color               | chatColor                | string  |                       |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Auto Sort                | sort                     | bool    | 1                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Auto Sort Weight         | sortWeight               | int     | 1                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Color                    | color                    | colorID | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Lives                    | lives                    | int     | -1                    |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Lock Team                | lock                     | bool    | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Max Players              | maxPlayers               | int     | -1                    |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Name                     | name                     | string  | New Team              |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Player Scale             | playerScale              | slide   | 1                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Playertype               | playerDatablock          | object  | 38                    |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Respawn Time             | respawnTime_Player       | int     | -1                    |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Spectator Team           | spectate                 | bool    | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Start Equip 0            | startEquip0              | object  | 65                    |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Start Equip 1            | startEquip1              | object  | 75                    |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Start Equip 2            | startEquip2              | object  | 383                   |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Start Equip 3            | startEquip3              | object  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Start Equip 4            | startEquip4              | object  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Sync w/ Minigame Loadout | syncLoadout              | bool    | 1                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Template                 | template                 | object  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Uni Update Trigger       | uni_updateTrigger        | int     | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Uniform                  | uniform                  | list    | 2                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Team      | Win on Time Up           | winOnTimeUp              | bool    | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | Accent                   | uni_accent               | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | AccentColor              | uni_accentColor          | string  | TEAMCOLOR             |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | Chest                    | uni_chest                | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | ChestColor               | uni_chestColor           | string  |                       |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | DecalColor               | uni_decalColor           | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | DecalName                | uni_decalName            | string  | AAA-None              |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | FaceColor                | uni_faceColor            | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | FaceName                 | uni_faceName             | string  | smiley                |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | Hat                      | uni_hat                  | string  | 6                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | HatColor                 | uni_hatColor             | string  | TEAMCOLOR             |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | HeadColor                | uni_headColor            | string  | 1 0.878431 0.611765 1 |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | Hip                      | uni_hip                  | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | HipColor                 | uni_hipColor             | string  | TEAMCOLOR             |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | LArm                     | uni_lArm                 | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | LArmColor                | uni_lArmColor            | string  | TEAMCOLOR             |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | LHand                    | uni_lHand                | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | LHandColor               | uni_lHandColor           | string  | 1 0.878431 0.611765 1 |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | LLeg                     | uni_lLeg                 | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | LLegColor                | uni_lLegColor            | string  | TEAMCOLOR             |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | Pack                     | uni_pack                 | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | PackColor                | uni_packColor            | string  | TEAMCOLOR             |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | RArm                     | uni_rArm                 | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | RArmColor                | uni_rArmColor            | string  | TEAMCOLOR             |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | RHand                    | uni_rHand                | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | RHandColor               | uni_rHandColor           | string  | 1 0.878431 0.611765 1 |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | RLeg                     | uni_rLeg                 | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | RLegColor                | uni_rLegColor            | string  | TEAMCOLOR             |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | SecondPack               | uni_secondPack           | string  | 0                     |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | SecondPackColor          | uni_secondPackColor      | string  | TEAMCOLOR             |
+-----------+--------------------------+--------------------------+---------+-----------------------+
| Uniform   | TorsoColor               | uni_torsoColor           | string  | TEAMCOLOR             |
+-----------+--------------------------+--------------------------+---------+-----------------------+

Appendix D: Permission Levels
-----------------------------

Slayer permission levels are enumerated by "$Slayer::PermissionLevel*" variables. Possible permission levels include:

-	Slayer::PermissionLevel["Host"] - Only the host.
-	Slayer::PermissionLevel["SuperAdmin"] - Super admins and above.
-	Slayer::PermissionLevel["Admin"] - Admins and above.
-	Slayer::PermissionLevel["Owner"] - The owner of the minigame and above.
-	Slayer::PermissionLevel["OwnerFullTrust"] - Those with the owner's full trust and above.
-	Slayer::PermissionLevel["OwnerBuildTrust"] - Those with the owner's build trust and above.
-	Slayer::PermissionLevel["Any"] - Any client with minigame edit permissions.
