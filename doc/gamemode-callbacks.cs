
//Gives objectives to a bot.
//@param	AiController bot
//@return	int	-1 if default objectives should not be assigned
//@see	Slayer_AiController::addObjective
function ExampleMode::assignBotObjectives(%this, %bot) {}

//Determines whether an object can be damaged.
//@param	SimObject objA
//@param	SimObject objB
//@return	bool	Whether the object can be damaged.
function ExampleMode::miniGameCanDamage(%this, %objA, %objB) {}

//Determines whether an object belonging to a client can be damaged by that of another client.
//@param	GameConnection clientA
//@param	GameConnection clientB
//@return	bool	Whether the object can be damaged.
function ExampleMode::miniGameClientCanDamage(%this, %clientA, %clientB) {}

//Determines whether an object can be used.
//@param	SimObject objA
//@param	SimObject objB
//@return	bool	Whether the object can be used.
function ExampleMode::miniGameCanUse(%this, %objA, %objB) {}

//Called when a client joins the minigame.
//@param	GameConnection client
function ExampleMode::onClientJoinGame(%this, %client) {}

//Called when a client leaves the minigame.
//@param	GameConnection client
function ExampleMode::onClientLeaveGame(%this, %client) {}

//Called when the client talks.
//@param	GameConnection client
//@param	string msg
function ExampleMode::onClientChat(%this, %client, %msg) {}

//Called when minigame rules are displayed to the client. 
//Can be used to display additional game mode rules or information.
//@param	GameConnection client
function ExampleMode::onClientDisplayRules(%this, %client) {}

//Called when the client's score changes.
//@param	GameConnection client
//@param	int score
function ExampleMode::onClientSetScore(%this, %client, %score) {}

//Called when the client enters free-spectate mode.
//@param	GameConnection client
function ExampleMode::onClientSpectateFree(%this, %client) {}

//Called when the client spectates another client.
//@param	GameConnection client
//@param	GameConnection target
function ExampleMode::onClientSpectateTarget(%this, %client, %target) {}

//Called when a capture point is reset.
//@param	FxDtsBrick capturePoint
//@param	int color
//@param	int oldColor
//@param	GameConnection client
function ExampleMode::onCPReset(%this, %capturePoint, %color, %oldColor, %client) {}

//Called when a capture point is captured.
//@param	FxDtsBrick capturePoint
//@param	int color
//@param	int oldColor
//@param	GameConnection client
function ExampleMode::onCPCapture(%this, %capturePoint, %color, %oldColor, %client) {}

//Called when a player's death message is being created.
//@param	GameConnection client
//@param	GameConnection killer
//@param	string value	The current death message.
//@return	string	A modified death message in Support_SpecialKills notation.
function ExampleMode::onCreateDeathMessage(%this, %client, %killer, %value) {}

//Called when the game mode begins.
function ExampleMode::onGameModeStart(%this) {}

//Called when the game mode ends.
function ExampleMode::onGameModeEnd(%this) {}

//Called after the minigame has been reset.
//@param	GameConnection client	The client that reset the minigame.
//@see	ExampleMode::preMinigameReset
function ExampleMode::onMinigameReset(%this, %client) {}

//Called before the minigame has been reset.
//@param	GameConnection client	The client that reset the minigame.
//@see	ExampleMode::onMinigameReset
function ExampleMode::preMinigameReset(%this, %client) {}

//Called when a Slayer brick, such as a team spawn, is added.
//@param	FxDtsBrick brick
//@param	string type	The Slayer brick type designation.
//@return	string	Message to display when the brick is planted.
function ExampleMode::onMiniGameBrickAdded(%this, %brick, %type) {}

//Called when a Slayer brick, such as a team spawn, is removed.
//@param	FxDtsBrick brick
//@param	string type	The Slayer brick type designation.
function ExampleMode::onMiniGameBrickRemoved(%this, %brick, %type) {}

//Called when a client presses a button in spectator mode.
//@param	Observer observer
//@param	Camera camera
//@param	int button
//@param	bool state
//@param	GameConnection client
function ExampleMode::onObserverTrigger(%this, %observer, %camera, %button, %state, %client) {}

//@param	GameConnection client
function ExampleMode::onPlayerSpawn(%this, %client) {}

//Called after the death message has displayed.
//@param	GameConnection client
//@param	GameConnection killer
//@return	int	-1 will prevent minigame resetting when out of lives - deprecated
function ExampleMode::onPlayerDeath(%this, %client, %obj, %killer, %type, %area) {}

//Called before the death message has displayed.
//@param	GameConnection client
//@param	GameConnection killer
function ExampleMode::prePlayerDeath(%this, %client, %obj, %killer, %type, %area) {}

//@param	int totalTicks
//@param	int remainingTicks
function ExampleMode::onPreRoundCountdownTick(%this, %totalTicks, %remainingTicks) {}

//Called when a new round begins.
function ExampleMode::onRoundStart(%this) {}

//Called before the end-of-round announcement.
//@param	string winner	A tab-delimited list of winners.
//@param	string nameList	A human-readable version of %winner.
function ExampleMode::preRoundEnd(%this, %winner, %nameList) {}

//Called after the end-of-round announcement.
//@param	string winner	A tab-delimited list of winners.
//@param	string nameList	A human-readable version of %winner.
function ExampleMode::onRoundEnd(%this, %winner, %nameList) {}

//Called when a team is created.
//@param	Slayer_TeamSO team
function ExampleMode::onTeamAdd(%this, %team) {}

//Called when a team is removed.
//@param	Slayer_TeamSO team
function ExampleMode::onTeamRemove(%this, %team) {}

//Called when teams are shuffled.
//@param	Slayer_TeamHandlerSG teamHandler	The team group.
//@param	int shuffleMode
//@param	bool doNotRespawn
function ExampleMode::onTeamShuffle(%this, %teamHandler, %shuffleMode, %doNotRespawn) {}

//Called when clients swap teams.
//@param	Slayer_TeamHandlerSG teamHandler	The team group.
//@param	GameConnection clientA
//@param	GameConnection clientB
function ExampleMode::onTeamSwap(%this, %teamHandler, %clientA, %clientB) {}

//Called when the client uses team chat.
//@param	GameConnection client
//@param	string msg
function ExampleMode::onTeamChat(%this, %client, %msg) {}

//Called when a client joins the team.
//@param	Slayer_TeamSO team
//@param	GameConnection client
function ExampleMode::onClientJoinTeam(%this, %team, %client) {}

//Called when a client leaves the team.
//@param	Slayer_TeamSO team
//@param	GameConnection client
function ExampleMode::onClientLeaveTeam(%this, %team, %client) {}

//Chooses a spawn point for the player.
//@param	GameConnection client
//@return	vector3F	The position for the player to spawn at.
function ExampleMode::pickPlayerSpawnPoint(%this, %client) {}

//Creates headers for the end of round score list.
//@param	string header	The default tagged header string. Default: '<color:ffffff><tab:150, 250, 350, 450><font:arial:16>%1<h2>%2\t%3\t%4\t%5\t%6</h2><br>'
//@param	string var1	Value that corresponds to tag in header.
//@param	string var2	Value that corresponds to tag in header.
//@param	string var3	Value that corresponds to tag in header.
//@param	string var4	Value that corresponds to tag in header.
//@param	string var5	Value that corresponds to tag in header.
//@param	string var6	Value that corresponds to tag in header.
//@param	string var7	Value that corresponds to tag in header.
//@param	string var8	Value that corresponds to tag in header.
//@param	string var9	Value that corresponds to tag in header.
//@return	string	To change headers, return: true TAB 'headerstring' TAB "headerVal1" TAB "headerVal2"...
//@see	ExampleMode::scoreListAdd
//@see	ExampleMode::scoreListCheckSendPlayers
//@see	ExampleMode::scoreListCheckSendTeams
function ExampleMode::scoreListInit(%this, %header, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9) {}

//Determines whether to include teams in the score list.
//@return	bool	Whether to display team scores in the list.
function ExampleMode::scoreListCheckSendTeams(%this) {}

//Determines whether to include players in the score list.
//@return	bool	Whether to display player scores in the list.
function ExampleMode::scoreListCheckSendPlayers(%this) {}

//Adds an item to the score list.
//@param	GameConnection object	The player/team to add.
//@param	Slayer_TeamSO object	The player/team to add.
//@param	string line	The default tagged string. Default: '<color:%1><b>%2</b></color>\t%3\t%4\t%5\t%6<br>'
//@param	string var1	Value that corresponds to tag in line.
//@param	string var2	Value that corresponds to tag in line.
//@param	string var3	Value that corresponds to tag in line.
//@param	string var4	Value that corresponds to tag in line.
//@param	string var5	Value that corresponds to tag in line.
//@param	string var6	Value that corresponds to tag in line.
//@param	string var7	Value that corresponds to tag in line.
//@param	string var8	Value that corresponds to tag in line.
//@param	string var9	Value that corresponds to tag in line.
//@return	string	To change line, return: true TAB 'line' TAB "lineVal1" TAB "lineVal2"...
function ExampleMode::scoreListAdd(%this, %object, %line, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9) {}


//Checks whether there is a last man/team standing.
//@return	SimObject	The winner, a GameConnection or Slayer_TeamSO.
function ExampleMode::victoryCheck_lives(%this) {}

//Finds the man/team with the most points.
//@return	SimObject	The winner, a GameConnection or Slayer_TeamSO.
function ExampleMode::victoryCheck_points(%this) {}

//Checks whether time is up and determines a winner.
//@return	SimObject	The winner, a GameConnection or Slayer_TeamSO.
function ExampleMode::victoryCheck_time(%this) {}
