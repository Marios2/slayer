// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function SlayerClient_ServerPrefSO::setValue(%this, %value)
{
	%proof = %this.idiotProof(%value);
	if(getField(%proof, 0))
		%value = getField(%proof, 1);
	else
		return false;
	$Slayer::Client::ServerPref[%this.clientVariable] = %value;
	return true;
}

function SlayerClient_ServerPrefSO::getValue(%this)
{
	return $Slayer::Client::ServerPref[%this.clientVariable];
}

function SlayerClient_ServerPrefSO::idiotProof(%this, %value)
{
	return Slayer_PrefSO::idiotProof(%this, %value, true);
}

function SlayerClient_ServerPrefSO::getDisplayValue(%this, %value)
{
	return Slayer_PrefSO::getDisplayValue(%this, %value);
}