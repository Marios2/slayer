// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

//@param	SlayerClient_GameModeTemplateSG gameMode
function SlayerClient_GameModeHandlerSG::updateGameMode(%this, %gameMode)
{
	SlayerClient.ServerPrefs.clearRules("ALL");
	SlayerClient.ServerPrefs.updateDynamicRules(%gameMode);
	Slayer_Main_Tabs.lockTab(Slayer_Teams, !%gameMode.useTeams);
	
	if(%gameMode != %this.currentGameMode)
	{
		%this.currentGameMode = %gameMode;
		%gameMode.applyDefaultPreferences();
		if(%gameMode.getCount() > 0)
		{
			SlayerClient.teams.clearTeams(true);
			%gameMode.createDefaultTeams();
		}
		else
		{
			for(%i = SlayerClient.teams.getCount() - 1; %i >= 0; %i --)
			{
				%team = SlayerClient.teams.getObject(%i);
				if(isObject(%team._template))
					SlayerClient.teams.removeTeam(%team);
			}
		}
		SlayerClient.serverPrefs.updateGUIValue(Slayer_Main, true, Slayer_General_Mode_Selector);
	}
}

//Add game mode when it is received from server.
function onClientReceivedObject_SlayerClient_GameModeTemplateSG(%object)
{
	SlayerClient.GameModes.add(%object);
	Slayer_General_Mode_Selector.add(%object.uiName, %object._serverID);
}

function clientCmdSlayer_getGamemodes_Start()
{
	SlayerClient.GameModes.deleteAll();
	Slayer_General_Mode_Selector.clear();
}

function clientCmdSlayer_getGamemodes_End()
{
	Slayer_General_Mode_Selector.sort();
}