// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function SlayerClient_ServerPrefHandlerSG::addPref(%this, %pref)
{
	%this.add(%pref);
	%pref.clientVariable = "::" @ getSafeVariableName(%pref.category) @
		"::" @ getSafeVariableName(%pref.title);
	
	//This makes lookups by variable much easier. More memory but less CPU.
	%var = "$Slayer::Client::ServerPref" @ %pref.clientVariable;
	$Slayer::Client::PrefObj[%var] = %pref;
	
	if(%pref.guiTag $= "advanced" && Slayer_Advanced_CategoryFilter.findText(%pref.category) < 0)
		Slayer_Advanced_CategoryFilter.add(%pref.category, %pref);
	return %pref;
}

function SlayerClient_ServerPrefHandlerSG::getPrefSO(%this, %category, %title)
{
	%count = %this.getCount();
	for(%i = 0; %i < %count; %i ++)
	{
		%p = %this.getObject(%i);
		if(%p.category $= %category && %p.title $= %title)
		{
			return %p;
		}
	}
	
	return 0;
}

function SlayerClient_ServerPrefHandlerSG::clearPrefs(%this)
{
	%this.deleteAll();
	deleteVariables("$Slayer::Client::ServerPref::*");
	deleteVariables("$Slayer::Client::PrefObj::*");
	Slayer_Advanced_Selector.clear();
	Slayer_Advanced_CategoryFilter.clear();
	Slayer_Main_Announcements.clearMessages("SERVER");
}

function SlayerClient_ServerPrefHandlerSG::displayRule(%this, %category, %pref)
{
	%gui = "Slayer_DynamicPrefs_" @ %category;
	if(!isObject(%gui))
		return;

	%variable = "$Slayer::Client::ServerPref" @ %pref.clientVariable;
	%value = %pref.getValue();

	if(%gui.getCount() <= 0)
		%pos = "3 3";
	else
		%pos = 3 SPC getWord(%gui.getObject(%gui.getCount()-1).position, 1) + getWord(%gui.getObject(%gui.getCount()-1).extent, 1) + 3;
	%ext = getWord(%gui.extent, 0)-6 SPC 30;

	%swatch = new GuiSwatchCtrl()
	{
		profile = "GuiDefaultProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = %pos;
		extent = %ext;
		minExtent = "8 2";
		visible = "1";
		color = "50 50 50 50";
	};

	%txt = new GuiMLTextCtrl()
	{
		profile = "Slayer_TextProfile";
		horizSizing = "right";
		vertSizing = "center";
		position = "3 1"; //this is properly set below
		extent = (getWord(%ext, 0) / 3) - 10 SPC "14";
		minExtent = "8 2";
		visible = "1";
		lineSpacing = "2";
		allowColorChars = "0";
		maxChars = "-1";
		text = %pref.title;
		maxBitmapHeight = "-1";
		selectable = "1";
	};
	%swatch.add(%txt);

	%extX = getWord(%swatch.extent, 0) - getWord(%txt.extent, 0);
	%pos = getWord(%swatch.extent, 0) - %extX + 10 SPC 0;
	%extX -= 15;

	switch$(%pref.type)
	{
		case "string":
			%ctrl = new GuiTextEditCtrl()
			{
				profile = "GuiTextEditProfile";
				horizSizing = "right";
				vertSizing = "center";
				position = %pos;
				extent = %extX SPC 18;
				minExtent = "8 2";
				visible = "1";
				variable = %variable;
				maxLength = %pref.string_maxLength;
				historySize = "0";
				password = "0";
				tabComplete = "0";
				sinkAllKeyEvents = "0";
			};
			
		case "int":
			%ctrl = new GuiTextEditCtrl()
			{
				profile = "GuiTextEditProfile";
				horizSizing = "right";
				vertSizing = "center";
				position = %pos;
				extent = %extX SPC 18;
				minExtent = "8 2";
				visible = "1";
				variable = %variable;
				maxLength = "50";
				command = "$ThisControl.restrictNumberInput();";
				minValue = %pref.int_minValue;
				maxValue = %pref.int_maxValue;
				historySize = "0";
				password = "0";
				tabComplete = "0";
				sinkAllKeyEvents = "0";
			};

		case "bool":
			%ctrl = new GuiCheckBoxCtrl()
			{
				profile = "Slayer_CheckBoxProfile";
				horizSizing = "right";
				vertSizing = "center";
				position = %pos;
				extent = %extX SPC 30;
				minExtent = "8 2";
				visible = "1";
				variable = %variable;
				text = "";
				groupNum = "-1";
				buttonType = "ToggleButton";
			};

		case "slide":
			%ctrl = new GuiSliderCtrl()
			{
				profile = "GuiSliderProfile";
				horizSizing = "right";
				vertSizing = "center";
				position = %pos;
				extent = %extX SPC 31;
				minExtent = "8 2";
				visible = "1";
				variable = %variable;
				range = %pref.slide_minValue SPC %pref.slide_maxValue;
				ticks = %pref.slide_numTicks;
				value = "0.5";
				snap = %pref.slide_snapToTicks;
			};
			%ctrl.setValue(%value);
			//slide controls add .000000 to the end of every number...
			%ctrl.command = %variable SPC "=" SPC "Slayer_Support::stripTrailingZeros(" @ %ctrl @ ".getValue());";
			schedule(50, 0, eval, %ctrl.command);

		case "list":
			%ctrl = new GuiPopUpMenuCtrl()
			{
				profile = "Slayer_PopUpMenuProfile";
				horizSizing = "right";
				vertSizing = "center";
				position = %pos;
				extent = %extX SPC 22;
				minExtent = "8 2";
				visible = "1";
				maxLength = "255";
				maxPopupHeight = "200";
				popupVariable = %variable;
			};

			%count = getRecordCount(%pref.list_items);
			for(%i = 0; %i < %count; %i ++)
			{
				%item = getRecord(%pref.list_items, %i);
				%ctrl.add(restWords(%item), firstWord(%item));
			}
			%ctrl.setSelected(%value);

		case "object":
			%ctrl = new GuiPopUpMenuCtrl()
			{
				profile = "Slayer_PopUpMenuProfile";
				horizSizing = "right";
				vertSizing = "center";
				position = %pos;
				extent = %extX SPC 22;
				minExtent = "8 2";
				visible = "1";
				maxLength = "255";
				maxPopupHeight = "200";
				popupVariable = %variable;
			};
			
			if(!$UINameTableCreated)
				createUINameTable();

			%class = %pref.object_class;
			%count = $ExtUINameTableCount[%class];
			for(%i = 0; %i < %count; %i ++)
			{
				%name = $ExtUINameTableName[%class, %i];
				%id = $ExtUINameTableID[%class, %i];
				%ctrl.add(%name, %id);
			}
			%ctrl.sort();
			if(!%pref.object_cannotBeNull)
				%ctrl.addFront("NONE", 0);
			%ctrl.setSelected(%value);

		default:
			%swatch.delete();
			return;
	}
	%swatch.add(%ctrl);
	%ctrl.setCenteredY();

	%guiExtY = getWord(%gui.extent, 1);
	%swatchExtY = getWord(%swatch.extent, 1);
	%swatchPosY = getWord(%swatch.position, 1);

	%extX = getWord(%gui.extent, 0);
	%extY = %guiExtY + %swatchExtY + 3;
	%gui.extent = %extX SPC %extY;

	%gui.add(%swatch);

	%txt.forceReflow();
	%txt.setCenteredY();

	%gui.setVisible(true); //refresh so that the scroll box works

	return %swatch;
}

function SlayerClient_ServerPrefHandlerSG::updateDynamicRules(%this, %gameMode)
{
	SlayerClient_Support::Debug(2, "Updating dynamic preferences");

	%count = %this.getCount();
	for(%i = 0; %i < %count; %i ++)
	{
		%pref = %this.getObject(%i);
		%guiTag = %pref.guiTag;

		if(getWord(%guiTag, 0) $= "rules")
		{
			%condition = getWord(%guiTag, 1);
			if(%condition $= %gameMode.className ||
				%condition $= "ALL" ||
				%condition $= "Teams" && %gameMode.useTeams ||
				%condition $= "!Teams" && !%gameMode.useTeams)
			{
				%this.displayRule(getWord(%pref.guiTag, 2), %pref);
			}
		}
	}
}

function SlayerClient_ServerPrefHandlerSG::clearRules(%this, %category)
{
	if(%category $= "ALL")
	{
		%this.clearRules("Mode");
		%this.clearRules("Player");
		%this.clearRules("Respawn");
		%this.clearRules("Points");
	}
	else
	{
		%gui = "Slayer_DynamicPrefs_" @ %category;
		if(!isObject(%gui))
			return;
		%gui.deleteAll();
		%gui.extent = getWord(%gui.extent, 0) SPC 3;
		%gui.setVisible(true); //refresh so that the scroll box works
	}
}

function SlayerClient_ServerPrefHandlerSG::sendPreferenceValues(%this, %reset)
{
	%heap = HeapQueue("", $COMPARE::SCORES);
	%count = 0;
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %this.getObject(%i);
		if(%pref.getValue() !$= %pref.origValue)
		{
			%count ++;
			%heap.push(%pref, %pref.priority);
		}
	}
	for(%i = SlayerClient.Teams.getCount() - 1; %i >= 0; %i --)
	{
		%team = SlayerClient.Teams.getObject(%i);
		if(%team._serverID < 0)
		{
			%count ++;
			break;
		}
		for(%e = SlayerClient.TeamPrefs.getCount() - 1; %e >= 0; %e --)
		{
			%p = SlayerClient.TeamPrefs.getObject(%e);
			if(%p.origValue !$= %p.getValue(%team))
			{
				%changed = true;
				break;
			}
		}
		if(%changed)
		{
			%count ++;
			break;
		}
	}
	%count += getFieldCount(SlayerClient.Teams.removeTeamList);

	SlayerClient_Support::Debug(1, "Sending Preferences", %count);

	//BEGIN PREF TRANSFER
	commandToServer('Slayer_getPrefs_Start', %reset, %count, $Pref::Slayer::Client::NotifyPlayersOnUpdate);

	//PREF TRANSFER TICK
	while(isObject(%pref = %heap.pop()))
	{
		%val = %pref.getValue();
		if(%val !$= %pref.origValue)
		{
			SlayerClient_Support::Debug(2, "Sending pref", %pref._serverID TAB %val);
			commandToServer('Slayer_getPrefs_Tick', %pref._serverID, %val);
		}
	}

	//SEND TEAMS
	SlayerClient.Teams.SendTeams();

	//END PREF TRANSFER
	commandToServer('Slayer_getPrefs_End');
}

//Exports all minigame preference settings to a file.
//@param	string path
//@param	Slayer_MinigameSO mini
function SlayerClient_ServerPrefHandlerSG::exportMinigamePreferences(%this, %path)
{
	if(!isWriteableFileName(%path))
	{
		Slayer_Support::error("SlayerClient_ServerPrefHandlerSG::exportMinigamePreferences", "Invalid file path" SPC %path);
		return;
	}
	
	%file = new FileObject();
	%file.openForWrite(%path);
	%file.writeLine("SLAYERMINIGAMECONFIG 1.0");
	%file.writeLine("");
	
	%heap = HeapQueue("", $COMPARE::SCORES);
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %this.getObject(%i);
		%heap.push(%pref, %pref.priority);
	}
	while(isObject(%pref = %heap.pop()))
	{
		if(!%pref.isMinigameVar)
			continue;
		%value = %pref.getValue();
		if(%pref.type $= "object")
			%value = isObject(%value) ? %value.uiName : "";
		%file.writeLine(
			"\"" @ expandEscape(%pref.category) @ "\"," @
			"\"" @ expandEscape(%pref.title) @ "\"," @
			"\"" @ expandEscape(%value) @ "\"");
	}
	%heap.delete();
	
	%file.close();
	%file.delete();
}

//Imports minigame preference settings from a file.
//@param	string path
function SlayerClient_ServerPrefHandlerSG::importMinigamePreferences(%this, %path)
{
	if(!isFile(%path))
	{
		Slayer_Support::error("SlayerClient_ServerPrefHandlerSG::importMinigamePreferences", "Invalid file path" SPC %path);
		return;
	}
	
	%file = new FileObject();
	%file.openForRead(%path);
	
	if(%file.readLine() $= "SLAYERMINIGAMECONFIG 1.0")
	{
		%csv = CSVReader(",");
		%file.readLine(); //blank line
		while(!%file.isEOF())
		{
			%line = %file.readLine();
			%csv.setDataString(%line);
			%category = collapseEscape(%csv.readNextValue());
			%title = collapseEscape(%csv.readNextValue());
			%value = collapseEscape(%csv.readNextValue());
			%pref = %this.getPrefSO(%category, %title);
			if(isObject(%pref) && !%csv.hasNextValue())
			{
				if(%pref.type $= "object")
				{
					if(strLen(%value))
						%value = SlayerClient_Support::getIDFromUiName(%value, %pref.object_class);
					else
						%value = 0;
				}
				%pref.setValue(%value);
			}
			else
				Slayer_Support::error("Invalid config file line", %line);
		}
	}
	else
	{
		Slayer_Support::error("SlayerClient_ServerPrefHandlerSG::importMinigamePreferences", "Incompatible preference file" SPC %path);	
	}
	
	%file.close();
	%file.delete();
}

//Update the displayed value to match the actual value.
//@param	GuiControl ctrl
//@param	bool recursive	Whether to affect all children of this control as well.
//@param	GuiControl exclude	A control to exclude.
function SlayerClient_ServerPrefHandlerSG::updateGUIValue(%this, %ctrl, %recursive, %exclude)
{
	if(nameToID(%ctrl) == nameToID(%exclude))
		return;
	
	if(strPos(%var = %ctrl.variable, "$Slayer::Client::ServerPref::") == 0 ||
		strPos(%var = %ctrl.popupVariable, "$Slayer::Client::ServerPref::") == 0)
	{
		//Update values.
		%value = Slayer_Support::getDynamicVariable(%var);
		%class = %ctrl.getClassName();
		switch$(%class)
		{
			case "GuiPopUpMenuCtrl":
				%ctrl.setSelected(%value);
			case "GuiSliderCtrl":
				%ctrl.setValue(%value);
				Slayer_Support::setDynamicVariable(%var, Slayer_Support::stripTrailingZeros(%value));
			default:
				%ctrl.setValue(%value);
		}
		
		//Disable locked settings.
		%gameMode = SlayerClient.Gamemodes.currentGameMode;
		if(isObject(%gameMode))
		{
			%pref = $Slayer::Client::PrefObj[%var];
			if(isObject(%pref) && %pref.isMinigameVar)
			{
				%enabled = strLen(%gameMode.locked_[%pref.variable]) == 0;
				%ctrl.setActive(%enabled);
				%ctrl.enabled = %enabled;
			}
		}
	}
	
	if(%recursive)
	{
		for(%i = %ctrl.getCount() - 1; %i >= 0; %i --)
		{
			%this.updateGUIValue(%ctrl.getObject(%i), true, %exclude);
		}
	}
}

function clientCmdSlayer_getPrefData_Start(%expectedCount)
{
	SlayerClient.ServerPrefs.clearPrefs();
	SlayerClient.ServerPrefs.expectedCount = %expectedCount;
	SlayerClient_Support::Debug(2, "Receiving Preference Data...");
	Slayer_Main_Content_Loading_Status.setText("Receiving Preference Data");
	Slayer_Main_Content_Loading_Progress.setValue(0);
}

function onClientReceivedObject_SlayerClient_ServerPrefSO(%pref)
{
	SlayerClient.serverPrefs.addPref(%pref);
	Slayer_Main_Content_Loading_Progress.setValue(
		SlayerClient.ServerPrefs.getCount() / SlayerClient.ServerPrefs.expectedCount);
}

function clientCmdSlayer_getPrefData_End(%prefCount)
{
	SlayerClient_Support::Debug(1, "Preferences Received", %prefCount);

	SlayerClient.ServerPrefs.expectedCount = "";
	Slayer_Main_Content_Loading_Status.setText("");
	Slayer_Main_Content_Loading_Progress.setValue(0);

	Slayer_Advanced_CategoryFilter.sort();
	Slayer_Advanced_CategoryFilter.addFront("All", -1);
}

function clientCmdSlayer_getPrefValues_Start(%expectedCount)
{
	SlayerClient.ServerPrefs.expectedCount = %expectedCount;
	SlayerClient_Support::Debug(2, "Receiving Preference Values...");
	Slayer_Main_Content_Loading_Status.setText("Receiving Preferences");
	Slayer_Main_Content_Loading_Progress.setValue(0);
}

function clientCmdSlayer_getPrefValues_Tick(%prefID, %value, %canEdit)
{
	%pref = nameToID("ServerObject" @ %prefID);
	%pref.setValue(%value);
	%pref.origValue = %value;
	%pref.canEdit = %canEdit;
	Slayer_Main_Content_Loading_Progress.setValue(
		SlayerClient.ServerPrefs.getCount() / SlayerClient.ServerPrefs.expectedCount);
}

function clientCmdSlayer_getPrefValues_End()
{
	SlayerClient_Support::Debug(1, "Preferences Received");
	SlayerClient.ServerPrefs.expectedCount = "";
	Slayer_Main_Content_Loading_Status.setText("");
	Slayer_Main_Content_Loading_Progress.setValue(0);
}

//----------------------------------------------------------------------
// Deprecated Functions - DO NOT USE
//----------------------------------------------------------------------

//@DEPRECATED
function SlayerClient_ServerPrefHandlerSG::exportPrefs(%this, %path)
{
	%file = new fileObject();
	%file.openForWrite(%path);

	%file.writeLine("//Slayer -----" SPC getDateTime());
	%file.writeLine("//By Greek2me, Blockland ID 11902");
	%file.writeLine("");
	%file.writeLine("//Version:" SPC $Slayer::Client::Version);
	%file.writeLine("//Debug Mode:" SPC $Slayer::Client::Debug);
	%file.writeLine("//Core Directory:" SPC $Slayer::Client::Directory);
	%file.writeLine("//Config Directory:" SPC $Slayer::Client::ConfigDir);
	%file.writeLine("");

	%file.writeLine("//PREFERENCES //USAGE: (category,title,value)");
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%p = %this.getObject(%i);
		%v = %p.getValue();

		if(%p.objectClassName $= "")
			%val = "\"" @ %v @ "\"";
		else
		{
			%val = strReplace(%v.uiName, "\'","\\'");
			%val = "SlayerClient_Support::getIDFromUiName(\"" @ %val @ "\",\"" @ %p.objectClassName @ "\")";
		}

		%file.writeLine("SlayerClient.ServerPrefs.setPref(\"" @ %p.category @ "\",\"" @ %p.title @ "\"," @ %val @ ");");
	}

	//TEAMS
	%numNewTeams = 0;

	%file.writeLine("");
	%file.writeLine("//TEAMS");
	%file.writeLine("SlayerClient.Teams.clearTeams(1);");
	for(%i=0; %i < SlayerClient.Teams.getCount(); %i++)
	{
		%t = SlayerClient.Teams.getObject(%i);

		%file.writeLine("%team = SlayerClient.Teams.addTeam(\"" @ -%numNewTeams - 2 @ "\",\"" @ %t.Team_Name @ "\");");
		for(%e=0; %e < SlayerClient.TeamPrefs.getCount(); %e++)
		{
			%p = SlayerClient.TeamPrefs.getObject(%e);
			%v = %p.getValue(%t);

			if(%p.objectClassName $= "")
				%val = "\"" @ %v @ "\"";
			else
			{
				%val = strReplace(%v.uiName, "\'","\\'");
				%val = "SlayerClient_Support::getIDFromUiName(\"" @ %val @ "\",\"" @ %p.objectClassName @ "\")";
			}

			%file.writeLine("\tSlayerClient.TeamPrefs.setPref(%team,\"" @ %p.category @ "\",\"" @ %p.title @ "\"," @ %val @ ");");
		}

		%numNewTeams ++;
	}

	%file.writeLine("");
	%file.writeLine("SlayerClient_Support::Debug(1,\"Preferences Loaded\");");

	%file.close();
	%file.delete();

	SlayerClient_Support::Debug(1, "Exporting Preferences", %path);
}