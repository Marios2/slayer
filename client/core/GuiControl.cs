// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

$Slayer::Client::GUI::RestrictInputTimeout = 1000;

$Slayer::Client::GUI::Help["about"] = "<just:left><bitmap:" @ $Slayer::Client::Path @ "/resources/images/logo_icon.png>  <color:aa0000><h1>Gamemode_Slayer</h1></color>Version" SPC $Slayer::Client::Version @ " (<a:" @ $Slayer::URL::Respository @ ">Repository</a>)\n\n    <b>Do you like Slayer?</b> <a:goo.gl/c0mDkz>Donate!</a> *\n\n\nCreated by <a:forum.blockland.us/index.php?action=profile;u=22331>Greek2me</a>.\n\n<b>Special Thanks To:</b><ul><li>Pecon7</li><li>Jetz</li><li>Boom</li><li>Brian Smith</li>\n<li>KINEX</li><li>Gizmo</li><li>Racerboy</li></ul>\n<just:right><size:10>*Donating supports the website, <a:greek2me.us>www.greek2me.us</a>.</size></just>";
$Slayer::Client::GUI::HelpIcon["about"] = "information";
$Slayer::Client::GUI::HelpTitle["about"] = "Slayer | About";
$Slayer::Client::GUI::Help["autoSortWeight"] = "<h1>Auto-Sort Weight</h1>This acts as a ratio for the number of players per team. For example, setting this team\'s weight to 1 and another team\'s weight to 2 will cause that team to have twice as many players as this team.";
$Slayer::Client::GUI::Help["maxPlayers"] = "<h1>Maximum Players</h1>When set to -1, an infinite amount of players may join the team. When set to 0 or greater, the team will not exceed that player count.\n\nThis only limits the number of human players on a team, <b>not</b> the number of bots.";
$Slayer::Client::GUI::Help["preferredPlayers"] = "<h1>Preferred Players</h1>When set to a number greater than 0, Slayer will add bots to the team to ensure that it always has that number of players.\n\n\n<b>Advanced:</b> Setting the \"Preferred Players\" higher than the \"Maximum Players\" will still cause bots to fill the empty slots.";
$Slayer::Client::GUI::Help["shuffleTeams"] = "<h1>Shuffle Teams</h1>When enabled, players will randomly be assigned to a new team every specified number of rounds.\n\n\n<b>Advanced</b>: The mechanics of this can be changed in the Advanced tab using the [Teams | Shuffle Mode] preference.\n<ul><li><b>New Team Every Time</b> - Designed for use with <i>more than two teams</i>. A player will never be on the same team twice in a row.</li><li><b>Random</b> - The default mode, this works best for games with only two teams.</li></ul>";
$Slayer::Client::GUI::Help["spectatorTeam"] = "<h1>Spectator Team</h1>If enabled, this team will not be able to interact with objects in the minigame, cannot be damaged, and cannot win. They can only walk around and watch.";
$Slayer::Client::GUI::Help["teamLoadout"] = "<h1>Team Loadout</h1>Select what items this team will carry into battle.\n\n\n<b>Sync</b> - When \"Sync\" is enabled, the team\'s loadout will match the overall minigame loadout.";

//Display a dialog box.
//@param	string type
//@param	string icon
//@param	string title
//@param	string text
//@param	string cmd
//@param	bool fade
function showDialogBox(%type, %icon, %title, %text, %cmd, %fade)
{
	%swatch = new GuiSwatchCtrl()
	{
		profile = "GuiDefaultProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "0 0";
		extent = canvas.getExtent();
		minExtent = "8 2";
		visible = "1";
		color = (%fade == 1 ? "200 200 200 200" : "0 0 0 0");
	};

	%window = new GuiWindowCtrl()
	{
		profile = "Slayer_WindowProfile";
		horizSizing = "center";
		vertSizing = "center";
		position = "125 125";
		extent = "300 100";
		minExtent = "200 100";
		visible = "1";
		text = "     " @ %title;
		maxLength = "255";
		resizeWidth = "0";
		resizeHeight = "0";
		canMove = "1";
		canClose = "0";
		canMinimize = "0";
		canMaximize = "0";
		minSize = "50 50";
	};
	%swatch.add(%window);

	%bitmap = new GuiBitmapCtrl()
	{
		profile = "GuiDefaultProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "5 4";
		extent = "16 16";
		minExtent = "8 2";
		visible = "1";
		bitmap = $Slayer::Client::Path @ "/resources/images/" @ %icon;
		wrap = "0";
		lockAspectRatio = "0";
		alignLeft = "0";
		overflowImage = "0";
		keepCached = "0";
	};
	%window.add(%bitmap);

	%textCtrl = new GuiMLTextCtrl()
	{
		profile = "Slayer_TextProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "10 29";
		extent = "280 14";
		minExtent = "8 2";
		visible = "1";
		lineSpacing = "2";
		allowColorChars = "1";
		maxChars = "-1";
		maxBitmapHeight = "-1";
		selectable = "1";
	};
	%window.add(%textCtrl);

	%text = parseCustomTML(%text, %textCtrl);
	%textCtrl.setText(%text);

	%confirm = new GuiBitmapButtonCtrl()
	{
		profile = "Slayer_ButtonProfile";
		horizSizing = "left";
		vertSizing = "top";
		position = "255 69";
		extent = "38 25";
		minExtent = "8 2";
		visible = "1";
		command = %swatch @ ".delete();" SPC %cmd;
		accelerator = "return";
		text = "OK";
		groupNum = "-1";
		buttonType = "PushButton";
		bitmap = $Slayer::Client::GUI::ResourcePath @ "/button2";
		lockAspectRatio = "0";
		alignLeft = "0";
		overflowImage = "0";
		mKeepCached = "0";
		mColor = "255 255 255 255";
	};
	%window.add(%confirm);

	if(%type $= "YESNO")
	{
		%confirm.text = "YES";

		%deny = new GuiBitmapButtonCtrl()
		{
			profile = "Slayer_ButtonProfile";
			horizSizing = "left";
			vertSizing = "top";
			position = "215 69";
			extent = "38 25";
			minExtent = "8 2";
			visible = "1";
			command = %swatch @ ".delete();";
			accelerator = "escape";
			text = "NO";
			groupNum = "-1";
			buttonType = "PushButton";
			bitmap = $Slayer::Client::GUI::ResourcePath @ "/button2";
			lockAspectRatio = "0";
			alignLeft = "0";
			overflowImage = "0";
			mKeepCached = "0";
			mColor = "255 255 255 255";
		};
		%window.add(%deny);
	}
	else
	{
		// %window.command = %swatch @ ".delete();" SPC %cmd;
		// %window.accelerator = "escape";
	}

	canvas.pushDialog(%swatch);

	//window resizing...
	%textCtrl.forceReflow();
	%posY = getWord(%textCtrl.getPosition(), 1);
	%extY = getWord(%textCtrl.getExtent(), 1);
	%confPosY = getWord(%confirm.getPosition(), 1);
	%winExtX = getWord(%window.getExtent(), 0);
	%winExtY = getWord(%window.getExtent(), 1);
	%extentY = %extY + %posY + %winExtY - %confPosY + 5;
	%window.resize(0, 0, %winExtX, %extentY);
	%window.setCentered(%swatch);
}

//Display a help dialog about a specific topic.
//@param	string topic
function SlayerClient_showHelpDialog(%topic)
{
	%icon = $Slayer::Client::GUI::HelpIcon[%topic] $= "" ?
		"help" : $Slayer::Client::GUI::HelpIcon[%topic];
	%title = $Slayer::Client::GUI::HelpTitle[%topic] $= "" ?
		"Slayer | Help" : $Slayer::Client::GUI::HelpTitle[%topic];
	showDialogBox("OK", %icon, %title, $Slayer::Client::GUI::Help[%topic]);
}

//Find the bottom-most point of one of this control's children.
//@return	int
function GuiControl::getLowestChildPoint(%this)
{
	%lowest = 0;

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%obj = %this.getObject(%obj);
		%low = getWord(%obj.position, 1) + getWord(%obj.extent, 1);
		if(%low > %lowest)
			%lowest = %low;
	}

	return %lowest;
}

//Center this control on the Y-axis.
function GuiControl::setCenteredY(%this)
{
	%parent = %this.getGroup();
	if(!isObject(%parent))
		return;

	%parExtY = getWord(%parent.getExtent(), 1);
	%extY = getWord(%this.getExtent(), 1);
	%posY = (%parExtY / 2) - (%extY / 2);

	%this.position = getWord(%this.getPosition(), 0) SPC %posY;
}

//Center this control on the X-axis.
function GuiControl::setCenteredX(%this)
{
	%parent = %this.getGroup();
	if(!isObject(%parent))
		return;

	%parExtX = getWord(%parent.getExtent(), 0);
	%extX = getWord(%this.getExtent(), 0);
	%posX = (%parExtX / 2) - (%extX / 2);

	%this.position = %posX SPC getWord(%this.getPosition(), 1);
}

//Center this control on both axes. 
function GuiControl::setCentered(%this)
{
	%this.setCenteredY();
	%this.setCenteredX();
}

//Set this as the "command" of a GuiTextEditCtrl to restrict input between ctrl.minValue and ctrl.maxValue.
function GuiTextEditCtrl::restrictNumberInput(%this)
{
	%val = %this.getValue();
	if(Slayer_Support::isFloat(%val))
		%value = mClampF(%val, %this.minValue, %this.maxValue);
	else
		%value = %this.minValue;
	cancel(%this.restrictInputTimer);
	%this.restrictInputTimer = %this.schedule($Slayer::Client::GUI::RestrictInputTimeout, "setText", %value);
}

function SlayerClient_pushMain(%state) 
{
	if(%state)
		SlayerClient.editMinigame();
}

function SlayerClient_pushOptions(%state) 
{
	if(%state)
		canvas.pushDialog(Slayer_Options);
}

function clientCmdSlayer_ForceGUI(%gui, %state)
{
	if(%gui $= "ALL")
	{
		clientCmdSlayer_ForceGUI("Slayer_Main", %state);
		clientCmdSlayer_ForceGUI("Slayer_Options", %state);
		clientCmdSlayer_ForceGUI("Slayer_CtrDisplay", %state);
	}
	else if(isObject(%gui))
	{
		if(%gui.isHUD)
		{
			if(%state && !$Pref::Slayer::Client::Disable_[%gui])
				playGui.add(%gui);
			else if(playGui.isMember(%gui))
				playGui.remove(%gui);
		}
		else
		{
			if(%state && !$Pref::Slayer::Client::Disable_[%gui])
				canvas.pushDialog(%gui);
			else
				canvas.popDialog(%gui);
		}
	}
}

function clientCmdSlayer_setGUIVisible(%gui, %flag)
{
	if(isObject(%gui))
		%gui.setVisible(%flag);
}

//Fix cases where default functions aren't defined.
if(!isFunction(GuiPopUpMenuCtrl, onSelect))
	eval("function GuiPopUpMenuCtrl::onSelect(){}");
if(!isFunction(GuiMLTextCtrl, onAdd))
	eval("function GuiMLTextCtrl::onAdd(){}");

package SlayerClient_GuiControl
{
	function GuiPopUpMenuCtrl::onSelect(%this, %id)
	{
		if(%this.popupVariable !$= "")
			Slayer_Support::setDynamicVariable(%this.popupVariable, %id);
		return parent::onSelect(%this, %id);
	}

	function GuiMLTextCtrl::onAdd(%this)
	{
		parent::onAdd(%this);
		if(isObject(%this.profile) && %this.profile.getName() $= "Slayer_TextProfile")
			%this.setText(parseCustomTML(%this.text, %this, %this.TML_parserFunction));
	}

	function GuiMLTextCtrl::onURL(%this, %url)
	{
		%firstChar = getSubStr(%url, 0, 1);
		if(%firstChar $= "#")
		{
			%restChars = getSubStr(%url, 1, strLen(%url));
			if(!strLen(%restChars))
				return parent::onURL(%this, %url);

			%parentObj = %this.getGroup();
			if(isObject(%parentObj) && isFunction(%parentObj.getClassName(), "scrollToBottom"))
				%parentObj.scrollToBottom();

			%this.scrollToTag(%restChars);
		}
		else
		{
			return parent::onURL(%this, %url);
		}
	}

	function NewPlayerListGui::onWake(%this)
	{
		parent::onWake(%this);
		commandToServer('Slayer_SendMinigameState');
	}

	function NewPlayerListGui::clickList(%this)
	{
		parent::clickList(%this);
		if(SlayerClient.minigameState == $Slayer::MinigameState["Edit"])
		{
			NPL_MiniGameInviteBlocker.setVisible(false);
			NPL_MiniGameRemoveBlocker.setVisible(false);
		}
	}
};
activatePackage(SlayerClient_GuiControl);