// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

$Slayer::Client::GUI::ResourcePath = "base/client/ui";

new GuiControlProfile(Slayer_ButtonProfile : blockButtonProfile)
{
	fontColor = "30 30 30 255";
	fontType = "Arial Bold";
	fontSize = "16";
	justify = "Center";
	fontColors[1] = "100 100 100";
	fontColors[2] = "30 30 30 150";  
	fontColors[3] = "0 0 255"; 
	fontColors[4] = "255 255 0"; 
	fontColorLink = "60 60 60 255";
	fontColorLinkHL = "0 0 0 255";
};

new GuiControlProfile(Slayer_TabTextProfile)
{
	fontColor = "30 30 30 255";
	fontType = "Arial";
	fontSize = "17";
	justify = "Left";
	fontColors[1] = "100 100 100";
	fontColors[2] = "0 255 0";  
	fontColors[3] = "0 0 255"; 
	fontColors[4] = "255 255 0"; 
	fontColorLink = "60 60 60 255";
	fontColorLinkHL = "0 0 0 255";
};

new GuiControlProfile(Slayer_TabBlueTextProfile)
{
	fontColor = "0 0 255 255";
	fontType = "Arial";
	fontSize = "17";
	justify = "Left";
	fontColors[1] = "100 100 100";
	fontColors[2] = "0 255 0";  
	fontColors[3] = "0 0 255"; 
	fontColors[4] = "255 255 0"; 
	fontColorLink = "60 60 60 255";
	fontColorLinkHL = "0 0 0 255";
};

new GuiControlProfile(Slayer_TextProfile)
{
	fontColor = "30 30 30 255";
	fontType = "Arial";
	fontSize = "15";
	justify = "Left";
	fontColors[1] = "100 100 100";
	fontColors[2] = "0 255 0";  
	fontColors[3] = "0 0 255"; 
	fontColors[4] = "255 255 0"; 
	fontColorLink = "60 60 60 255";
	fontColorLinkHL = "0 0 0 255";
};

new GuiControlProfile(Slayer_CheckBoxProfile : GuiCheckBoxProfile)
{
	bitmap = $Slayer::Client::GUI::ResourcePath @ "/torqueCheck.png";
	fontColor = "30 30 30 255";
	fontType = "Arial";
	fontSize = "15";
};

new GuiControlProfile(Slayer_RadioProfile : GuiRadioProfile)
{
	bitmap = $Slayer::Client::GUI::ResourcePath @ "/torqueRadio.png";
	fontColor = "30 30 30 255";
	fontType = "Arial";
	fontSize = "15";
};

new GuiControlProfile(Slayer_ScrollProfile : GuiScrollProfile)
{
	bitmap = $Slayer::Client::GUI::ResourcePath @ "/blockScroll.png";
};

new GuiControlProfile(Slayer_HalfScrollProfile : colorScrollProfile)
{
	bitmap = $Slayer::Client::GUI::ResourcePath @ "/halfScroll.png";
};

new GuiControlProfile(Slayer_PopUpMenuProfile : GuiPopupMenuProfile)
{
	fontSize = "15";
};

new GuiControlProfile(Slayer_ProgressProfile : GuiProgressProfile)
{
	border = 0;
	fillColor = "90 230 70 255";
};

new GuiControlProfile(Slayer_WindowProfile : GuiWindowProfile)
{
	bitmap = $Slayer::Client::GUI::ResourcePath @ "/blockWindow.png";
	fontType = "Arial Bold";
	fontSize = "18";
	textOffset = "5 3";
};