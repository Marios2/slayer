// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function SlayerClient_PrefSO::onAdd(%this)
{
	%this.scheduleNoQuota(0, onAdded);
}

function SlayerClient_PrefSO::onAdded(%this)
{
	SlayerClient.prefs.add(%this);
	if(%this.getValue() $= "")
		%this.setValue(%this.defaultValue);
}

function SlayerClient_PrefSO::setValue(%this, %value)
{
	%proof = %this.idiotProof(%value);
	if(getField(%proof, 0))
		%value = getField(%proof, 1);
	else
		return false;
	Slayer_Support::setDynamicVariable(%this.variable, %value);
	return true;
}

function SlayerClient_PrefSO::getValue(%this)
{
	return Slayer_Support::getDynamicVariable(%this.variable);
}

function SlayerClient_PrefSO::idiotProof(%this, %value)
{
	return Slayer_PrefSO::idiotProof(%this, %value);
}

function SlayerClient_PrefSO::getDisplayValue(%this, %value)
{
	return Slayer_PrefSO::getDisplayValue(%this, %value);
}