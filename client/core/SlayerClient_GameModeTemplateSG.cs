// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

//Applies preferences from the game mode template.
function SlayerClient_GameModeTemplateSG::applyDefaultPreferences(%this)
{
	%prefs = SlayerClient.serverPrefs;
	for(%i = %prefs.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %prefs.getObject(%i);
		if(%pref.isMinigameVar)
		{
			%default = (strLen(%this.locked_[%pref.variable]) ?
				%this.locked_[%pref.variable] : %this.default_[%pref.variable]);
			if(strLen(%default))
			{
				%pref.setValue(%default);
			}
		}
	}
}

//Applies preferences from the game mode template to a specific team.
//@param	SlayerClient_TeamSO team
//@param	ScriptObject teamTemplate
function SlayerClient_GameModeTemplateSG::applyDefaultTeamPreferences(%this, %team, %teamTemplate)
{
	%prefs = SlayerClient.teamPrefs;
	for(%i = %prefs.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %prefs.getObject(%i);
		%default = (strLen(%teamTemplate.locked_[%pref.variable]) ?
			%teamTemplate.locked_[%pref.variable] : %teamTemplate.default_[%pref.variable]);
		if(strLen(%default))
		{
			%pref.setValue(%default, %team);
		}
	}
}

//Create built-in teams for the game mode.
function SlayerClient_GameModeTemplateSG::createDefaultTeams(%this)
{
	%count = %this.getCount();
	for(%i = 0; %i < %count; %i ++)
	{
		%template = %this.getObject(%i);
		%team = SlayerClient.teams.createTeam(true);
		%this.applyDefaultTeamPreferences(%team, %template);
		%team.template = %template._serverID;
		%team._template = %template;
		SlayerClient.teams.addTeam(%team);
	}
}