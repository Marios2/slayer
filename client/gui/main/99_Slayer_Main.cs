// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_Main::onWake(%this)
{
	//WELCOME SCREEN
	if(!SlayerClient.serverHasSlayer || (!$Slayer::Client::startScreenDisplayed &&
		($Slayer::Client::FirstRun || $Pref::Slayer::Client::showStartScreen)))
	{
		$Slayer::Client::startScreenDisplayed = 1;
		Slayer_Main_Tabs.setTab(Slayer_Main_Content_StartScreen, 1);
	}
	else if(SlayerClient.DataReceived)
	{
		Slayer_Main_TabList.setSelectedById(0);
	}
	else
	{
		Slayer_Main_Tabs.setTab(Slayer_Main_Content_Loading, 1);
	}
}

function Slayer_Main::onSleep(%this)
{
	SlayerClient.DataReceived = false;
}

function Slayer_Main::apply(%this, %reset)
{
	//check that we have enough teams
	if((%minTeams = SlayerClient.GameModes.currentGameMode.teams_minTeams) > 0 &&
		SlayerClient.teams.getCount() < %minTeams)
	{
		showDialogBox
		(
			"OK",
			"information",
			"Oops!",
			"This game mode requires that you create at least" SPC %minTeams SPC
				(%minTeams == 1 ? "team" : "teams") @ "."
		);
		Slayer_Main_Tabs.setTab(Slayer_Teams);
		return;
	}

	//cleanup, make sure they clicked ok on everything	
	Slayer_Teams.clickApply();
	Slayer_Teams_Advanced.apply(Slayer_Teams_Advanced_Selector.getSelectedID());
	Slayer_Advanced.apply(Slayer_Advanced_Selector.getSelectedID()); 

	//SEND DATA TO SERVER
	SlayerClient.SendData(%reset);

	//CLOSE GUI
	canvas.popDialog(%this);
}

function Slayer_Main::refresh(%this)
{
	SlayerClient.serverPrefs.updateGUIValue(%this, true);
	Slayer_Teams.refresh();
	Slayer_Advanced.refresh();
}

function Slayer_Main::refreshFromServer(%this)
{
	Slayer_Main_Tabs.setTab(Slayer_Main_Content_Loading, true);
	commandToServer('Slayer', "edit");
}

function Slayer_Main_Tabs::addTab(%this, %title, %gui, %minigameRequired, %image)
{
	%numTabs = Slayer_Main_TabList.rowCount();
	for(%i = 0; %i < %numTabs; %i ++)
	{
		%t = Slayer_Main_TabList.getRowText(%i);
		if(getField(%t, 0) $= %title && getField(%t, 1) $= %gui)
			return;
	}

	Slayer_Main_TabList.addRow(%numTabs,
		%title TAB %gui TAB %minigameRequired TAB %teamOnly);

	if(isFile(%image))
	{
		%pos = 2 SPC %numTabs * 19 + 1;
		%ctrl = new GuiBitmapCtrl()
		{
			profile = "GuiDefaultProfile";
			horizSizing = "center";
			vertSizing = "bottom";
			position = %pos;
			extent = "16 16";
			minExtent = "8 2";
			visible = "1";
			bitmap = %image;
			wrap = "0";
			lockAspectRatio = "0";
			alignLeft = "0";
			overflowImage = "0";
			keepCached = "0";
		};
		Slayer_Main_TabIcons.add(%ctrl);
	}
}

function Slayer_Main_Tabs::removeTab(%this, %title)
{
	if(%title $= "")
		return;
	%row = -1;
	for(%i = 0; %i < Slayer_Main_TabList.rowCount(); %i ++)
	{
		%text = Slayer_Main_TabList.getRowText(%i);
		%t = getField(%text, 0);

		if(%t $= %title)
		{
			%row = %i;
			%rowText = %text;
			break;
		}
	}
	if(%row < 0)
		return;
	Slayer_Main_TabList.removeRow(%row);

	%iconPosY = %row * 19 + 1;
	%iconPos = 2 SPC %iconPosY;
	for(%i = 0; %i < Slayer_Main_TabIcons.getCount(); %i ++)
	{
		%obj = Slayer_Main_TabIcons.getObject(%i);
		if(%obj.getPosition() $= %iconPos)
		{
			%icon = %obj;
			break;
		}
	}
	if(isObject(%icon))
		%icon.delete();

	for(%i = 0; %i < Slayer_Main_TabIcons.getCount(); %i ++)
	{
		%obj = Slayer_Main_TabIcons.getObject(%i);
		%pos = %obj.getPosition();
		%posY = getWord(%pos, 1);
		if(%posY > %iconPosY)
		{
			%obj.position = 2 SPC (%posY - 19);
		}
	}
}

function Slayer_Main_Tabs::setTab(%this, %tab, %clearSelection)
{
	if(!isObject(%tab))
		return;

	if(%clearSelection)
		Slayer_Main_TabList.clearSelection();

	if(Slayer_Main_Content.isMember(%tab))
	{
		for(%i = Slayer_Main_Content.getCount() - 1; %i >= 0; %i --)
			Slayer_Main_Content.getObject(%i).setVisible(false);
		%tab.setVisible(true);
		if(isFunction(%tab, "onWake"))
			%tab.onWake(true);
	}
	else
	{
		canvas.pushDialog(%tab);
		%oldID = Slayer_Main_TabList.lastSelectedID;
		%curID = Slayer_Main_TabList.getSelectedID();
		if(%oldID >= 0 && %oldID != %curID)
			Slayer_Main_TabList.setSelectedById(Slayer_Main_TabList.lastSelectedID);
		else if(%curID >= 0)
			Slayer_Main_TabList.clearSelection();
	}
}

function Slayer_Main_Tabs::lockTab(%this, %tab, %lock)
{
	%this.tabLocked[%tab] = !!%lock;
}

function Slayer_Main_TabList::setSelectedByTab(%this, %tab)
{
	if(!isObject(%tab))
		return;

	for(%i = %this.rowCount() - 1; %i >= 0; %i --)
	{
		%row = %this.getRowTextByID(%i);
		%gui = getField(%row, 1);
		if(!isObject(%gui))
			continue;
		if(%gui == %tab)
		{
			%this.setSelectedByID(%i);
			break;
		}
	}
}

function Slayer_Main_TabList::onSelect(%this, %id)
{
	%text = %this.getRowTextByID(%id);
	%tab = getField(%text, 1);
	if(!isObject(%tab))
		return;
	if(SlayerClient.minigameState $= $Slayer::MinigameState["None"])
	{
		if(getField(%text, 2))
		{
			showDialogBox
			(
				"OK",
				"exclamation",
				"Access Denied",
				"You don't have permission to create a new minigame or edit " @
				"your current one. Contact the host for assistance."
			);
			Slayer_Main_Tabs.setTab(Slayer_Main_Content_StartScreen, 1);
			return;
		}
	}
	else if(Slayer_Main_Tabs.tabLocked[%tab])
	{
		showDialogBox
		(
			"OK",
			"information",
			"Oops",
			"This tab is locked. Please select a different " @
			"game mode in the General tab, under <i>Game Mode</i>.",
			"Slayer_Main_TabList.setSelectedByID(0);"
		);
		return;
	}
	Slayer_Main_Tabs.setTab(%tab);
	%this.lastSelectedID = %id;
}

function Slayer_Main_Announcements::onWake(%this)
{
	%this.startScrolling();
}

function Slayer_Main_Announcements::onSleep(%this)
{
	cancel(%this.timer);
}

function Slayer_Main_Announcements::startScrolling(%this)
{
	cancel(%this.timer);
	%this.currentChar = 0;
	%this.setText("");
	%this.currentMsg = -1;
	%this.scrollText();
}

function Slayer_Main_Announcements::scrollText(%this)
{
	%text = stripMLControlChars(%this.getText());
	if(trim(%text) $= "")
	{
		%text = "";
		for(%i = 0; %i < %this.maxChars-1; %i ++)
			%text = %text @ " ";

		%this.currentChar = 0;

		if(%this.currentMsg >= %this.numMessages-1)
			%this.currentMsg = 0;
		else
			%this.currentMsg ++;

		%type = %this.messageType[%this.currentMsg];
		if(%type $= "SERVER")
			%this.profile = Slayer_TabBlueTextProfile;
		else
			%this.profile = Slayer_TabTextProfile;
	}

	%msg = %this.message[%this.currentMsg];
	%cur = %this.currentChar;
	%text = getSubStr(%text, 1, strLen(%text));
	%text = %text @ getSubStr(%msg, %cur, 1);
	%this.currentChar ++;
	%this.setText(%text);

	cancel(%this.timer);
	%this.timer = %this.schedule(125, scrollText);
}

function Slayer_Main_Announcements::addMessage(%this, %msg, %type)
{
	if(%msg $= "")
		return;
	%this.message[%this.numMessages] = %msg;
	%this.messageType[%this.numMessages] = %type;
	%this.numMessages ++;
}

function Slayer_Main_Announcements::removeMessage(%this, %id)
{
	%msg = %this.message[%id];
	%type = %this.messageType[%id];
	if(%msg $= "")
		return;
	for(%i = %id + 1; %i < %this.numMessages; %i ++)
	{
		%this.message[%i-1] = %this.message[%i];
		%this.messageType[%i-1] = %this.messageType[%i];
	}
	%this.message[%this.numMessages-1] = "";
	%this.messageType[%this.numMessages-1] = "";
	%this.numMessages --;
}

function Slayer_Main_Announcements::clearMessages(%this, %type)
{
	for(%i = %this.numMessages - 1; %i >= 0; %i --)
	{
		if(%this.messageType[%i] $= %type || %type $= "")
			%this.removeMessage(%i);
	}
}

function Slayer_Main_Favs::onWake(%this)
{
	%this.updateFavs();
	Slayer_Main_Favs_Helper.setVisible(false);
	Slayer_Main_Window.bringToFront(%this);
}

function Slayer_Main_Favs::setFavs(%this)
{
	%visible = Slayer_Main_Favs_Helper.isVisible();
	if(%visible)
		Slayer_Main_Window.bringToFront(%this);
	else
		Slayer_Main_Window.pushToBack(%this);
	Slayer_Main_Favs_Helper.setVisible(!%visible);
}

function Slayer_Main_Favs::clickFav(%this, %flag)
{
	%prefix = $Slayer::Client::ConfigDir @ "/config_saved/fav" @ %flag;
	%mgameFile = %prefix @ ".mgame.csv";
	%teamsFile = %prefix @ ".teams.csv";

	if(Slayer_Main_Favs_Helper.isVisible())
	{
		Slayer_Main_Favs_Helper.setVisible(false);
		SlayerClient.ServerPrefs.exportMinigamePreferences(%mgameFile);
		SlayerClient.TeamPrefs.exportTeamPreferences(%teamsFile);
		%this.updateFavs();
	}
	else if(isFile(%mgameFile))
	{
		SlayerClient.ServerPrefs.importMinigamePreferences(%mgameFile);
		SlayerClient.teams.clearTeams(true);
		if(isFile(%teamsFile))
			SlayerClient.TeamPrefs.importTeamPreferences(%teamsFile);
		Slayer_Main.refresh();
	}
}

function Slayer_Main_Favs::updateFavs(%this)
{
	for(%i = 0; %i < 10; %i ++)
	{
		%file = $Slayer::Client::ConfigDir @ "/config_saved/fav" @ %i @ ".mgame.csv";
		%ctrl = nameToID("Slayer_Main_Favs_" @ %i);
		if(isFile(%file))
			%ctrl.mColor = "255 255 255 255";
		else
			%ctrl.mColor = "255 255 255 128";
	}
}