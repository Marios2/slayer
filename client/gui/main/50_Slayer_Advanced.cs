// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_Advanced::onWake(%this, %tabSet)
{
	if(%tabSet)
	{
		Slayer_Advanced_bool.setVisible(false);
		Slayer_Advanced_list.setVisible(false);
		Slayer_Advanced_string.setVisible(false);
		Slayer_Advanced_int.setVisible(false);
		Slayer_Advanced_slide.setVisible(false);
		Slayer_Advanced_Apply.setVisible(false);

		%selected = Slayer_Advanced_CategoryFilter.getSelected();
		if(%selected != 0)
		{
			%category = Slayer_Advanced_CategoryFilter.getTextByID(%selected);
			%this.refresh(%category);
		}
		else
			Slayer_Advanced_CategoryFilter.setSelected(-1);
	}
}

function Slayer_Advanced::refresh(%this, %category)
{
	Slayer_Advanced_Selector.clear();
	for(%i = SlayerClient.ServerPrefs.getCount() - 1; %i >= 0; %i --)
	{
		%pref = SlayerClient.ServerPrefs.getObject(%i);
		if(%pref.guiTag !$= "advanced")
			continue;
		if(%category !$= "" && %category !$= "All" && %category !$= %pref.category)
			continue;
		%type = %pref.type;
		%value = %pref.getValue();
		%display = %pref.getDisplayValue(%value);
		Slayer_Advanced_Selector.addRow(%pref, %pref.category TAB %pref.title TAB %display);
	}
	Slayer_Advanced_Selector.sort(0);
}

function Slayer_Advanced_CategoryFilter::onSelect(%this, %id)
{
	%category = %this.getTextByID(%id);
	Slayer_Advanced.refresh(%category);
}

function Slayer_Advanced_Selector::onSelect(%this, %pref)
{
	Slayer_Advanced_bool.setVisible(false);
	Slayer_Advanced_list.setVisible(false);
	Slayer_Advanced_string.setVisible(false);
	Slayer_Advanced_int.setVisible(false);
	Slayer_Advanced_slide.setVisible(false);

	if(!%pref.canEdit)
	{
		showDialogBox
		(
			"OK",
			"exclamation",
			"Access Denied",
			"You don't have permission to edit that setting. " @
			"Please contact the host for assistance."
		);
		Slayer_Advanced_Apply.setVisible(false);
		Slayer_Advanced_Selector.clearSelection();
		return;
	}
	
	%gameMode = SlayerClient.gameModes.currentGameMode;
	if(%pref.isMinigameVar && strLen(%gameMode.locked_[%pref.variable]) > 0)
	{
		showDialogBox
		(
			"OK",
			"error",
			"Game Mode",
			"The current game mode is preventing you from " @
			"changing this setting."
		);
		Slayer_Advanced_Apply.setVisible(false);
		Slayer_Advanced_Selector.clearSelection();
		return;
	}

	Slayer_Advanced_Apply.setVisible(true);

	if(%pref.type $= "object")
		%control = Slayer_Advanced_list;
	else
		%control = "Slayer_Advanced_" @ %pref.type;
	if(!isObject(%control))
		return;
	%control.setVisible(true);

	switch$(%pref.type)
	{
		case "bool":
			%control.setText(%pref.title);
			%control.setValue(%pref.getValue());

		case "string":
			%control.maxLength = %pref.string_maxLength;
			%control.setValue(%pref.getValue());

		case "int":
			%control.minValue = %pref.int_minValue;
			%control.maxValue = %pref.int_maxValue;
			%control.setValue(%pref.getValue());

		case "slide":
			%control.range = %pref.slide_minValue SPC %pref.slide_maxValue;
			%control.ticks = %pref.slide_numTicks;
			%control.snap = %pref.slide_snapToTicks;
			%control.setValue(%pref.getValue());

		case "list":
			%control.clear();
			%count = getRecordCount(%pref.list_items);
			for(%i = 0; %i < %count; %i ++)
			{
				%f = getRecord(%pref.list_items, %i);
				%control.add(restWords(%f), firstWord(%f));
			}
			%control.setSelected(%pref.getValue());

		case "object":
			%control.clear();
			if(!$UINameTableCreated)
				createUINameTable();
			%class = %pref.object_class;
			%count = $ExtUINameTableCount[%class];
			for(%i = 0; %i < %count; %i ++)
			{
				%name = $ExtUINameTableName[%class, %i];
				%id = $ExtUINameTableID[%class, %i];
				%control.add(%name, %id);
			}
			%control.sort();
			if(!%pref.object_cannotBeNull)
				%control.addFront("NONE", 0);
			%control.setSelected(%pref.getValue());
	}
}

function Slayer_Advanced::apply(%this, %pref)
{
	Slayer_Advanced_Apply.setVisible(false);
	if(%pref.type $= "object")
		%control = Slayer_Advanced_list;
	else
		%control = "Slayer_Advanced_" @ %pref.type;
	if(!isObject(%control))
		return;
	%control.setVisible(false);

	switch$(%pref.type)
	{
		case "list" or "object":
			%value = %control.getSelected();

		default:
			%value = %control.getValue();
	}
	%pref.setValue(%value);
	%entry = %pref.category TAB %pref.title TAB %pref.getDisplayValue(%value);
	Slayer_Advanced_Selector.setRowByID(%pref, %entry);
	Slayer_Advanced_Selector.clearSelection();
}