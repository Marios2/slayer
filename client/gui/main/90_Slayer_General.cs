// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_General::onWake(%this, %tabSet)
{
	if(!%tabSet)
	{
		//reset the scroll bar to top
		Slayer_General_Scroll.scrollToTop();

		//minigame colors
		deleteVariables("$Slayer::Client::MinigameColor*");
		if(isObject(Slayer_General_Basic_Color.colorMenu))
			Slayer_General_Basic_Color.colorMenu.delete();
		commandToServer('requestSlayerMiniGameColorList');
	}
}

function Slayer_General_Mode_Selector::onCancel(%this)
{
	%gameModeID = %this.getSelected();
	%gameMode = nameToID("ServerObject" @ %gameModeID);
	if(!isObject(%gameMode) && %this.displayNextInvalid)
	{
		%this.displayNextInvalid = false;
		showDialogBox("OK",
			"error",
			"Invalid Game Mode",
			"The selected game mode is not installed or is broken."
		);
		%this.forceOnAction();
	}
}

function Slayer_General_Mode_Selector::onSelect(%this, %gameModeID)
{
	parent::onSelect(%this, %gameModeID);
	%this.displayNextInvalid = true;
	%gameMode = nameToID("ServerObject" @ %gameModeID);
	if(!isObject(%gameMode))
	{
		showDialogBox("OK",
			"error",
			"Invalid Game Mode",
			"The selected game mode is not installed or is broken."
		);
		return;
	}
	SlayerClient.Gamemodes.updateGameMode(%gameMode);
}

function Slayer_General_Basic_Color::createColorMenu(%this)
{
	if(isObject(%this.colorMenu))
	{
		%this.colorMenu.delete();
		return;
	}

	%this.colorMenu = new GuiScrollCtrl()
	{
		profile = "Slayer_HalfScrollProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "310 41";
		extent = "204 224";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "0";
		willFirstRespond = "0";
		hScrollBar = "alwaysOff";
		vScrollBar = "alwaysOn";
		constantThumbHeight = "0";
		childMargin = "0 0";
		rowHeight = "40";
		columnWidth = "30";

		new GuiSwatchCtrl()
		{
			profile = "GuiDefaultProfile";
			horizSizing = "right";
			vertSizing = "bottom";
			position = "0 0";
			extent = "192 224";
			minExtent = "8 2";
			enabled = "1";
			visible = "1";
			clipToParent = "1";
			color = "0 0 0 255";
		};
	};
	%swatch = %this.colorMenu.getObject(0);

	%posX = 0;
	%posY = 0;
	%count = 0;
	%size = 18;
	%maxColumns = 6;
	for(%i = 0; %i < $Slayer::Client::MinigameColorCount; %i ++)
	{
		if($Slayer::Client::MinigameColor[%i] !$= "")
		{
			%s = new GuiSwatchCtrl()
			{
				profile = "GuiDefaultProfile";
				horizSizing = "right";
				vertSizing = "bottom";
				position = %posX SPC %posY;
				extent = %size SPC %size;
				minExtent = "8 2";
				enabled = "1";
				visible = "1";
				clipToParent = "1";
				color = $Slayer::Client::MinigameColor[%i];
			};
			%b = new GuiBitmapButtonCtrl()
			{
				profile = "GuiDefaultProfile";
				horizSizing = "right";
				vertSizing = "bottom";
				position = %posX SPC %posY;
				extent = %size SPC %size;
				minExtent = "8 2";
				enabled = "1";
				visible = "1";
				clipToParent = "1";
				command = "Slayer_General_Basic_Color.clickColor(" @ %i @ ");";
				text = " ";
				groupNum = "-1";
				buttonType = "PushButton";
				bitmap = "base/client/ui/btnColor";
				lockAspectRatio = "0";
				alignLeft = "0";
				alignTop = "0";
				overflowImage = "0";
				mKeepCached = "0";
				mColor = "255 255 255 255";
			};
			%swatch.add(%s);
			%swatch.add(%b);

			%count ++;
			if(%count % %maxColumns == 0)
			{
				%posX = 0;
				%posY += %size;
			}
			else
				%posX += %size;
		}
	}

	%extX = %maxColumns * %size + 12; //12 is the size of the scroll bar
	%extY = mClampF(mCeil(%count / %maxColumns) * %size, %size, 100);

	%p = %this.getPosition();
	%pX = getWord(%p, 0);
	%pY = getWord(%p, 1);

	%e = %this.getExtent();
	%eX = getWord(%e, 0);

	%swatch.extent = %extX SPC %extY;
	%this.colorMenu.resize(%pX+%eX, %pY, %extX, %extY);
	%this.getGroup().add(%this.colorMenu);
}

function Slayer_General_Basic_Color::clickColor(%this, %id)
{
	if(isObject(%this.colorMenu))
		%this.colorMenu.delete();

	%this.setColor($Slayer::Client::MinigameColor[%id] SPC 255);
	$Slayer::Client::ServerPref::Minigame::Color = %id;
}

function Slayer_General_Player::onPopUpMenuSelect(%this)
{
	for(%i = SlayerClient.Teams.getCount() - 1; %i >= 0; %i --)
	{
		%team = SlayerClient.Teams.getObject(%i);
		if(%team.syncLoadout)
		{
			//close the edit panel
			if(%team == Slayer_Teams_Selector.getSelectedId())
				Slayer_Teams.clickApply();

			%team.playerDatablock = $Slayer::Client::ServerPref::Player::Playertype;
			for(%e = 0; %e < 5; %e ++)
				%team.startEquip[%e] = $Slayer::Client::ServerPref::Player::Starting_Equipment_[%e];
		}
	}
}

function clientCmdAddSlayerMiniGameColor(%id, %name, %color)
{
	if($Slayer::Client::MinigameColorCount $= "")
		$Slayer::Client::MinigameColorCount = 0;

	$Slayer::Client::MinigameColorName[%id] = %name;
	$Slayer::Client::MinigameColor[%id] = %color;
	if(%id >= $Slayer::Client::MinigameColorCount)
		$Slayer::Client::MinigameColorCount = %id + 1;

	//This is the color of our minigame
	if($Slayer::Client::ServerPref::Minigame::Color == %id)
		Slayer_General_Basic_Color.setColor(%color SPC 255);
}