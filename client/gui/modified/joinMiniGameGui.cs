// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function JMG_List::onSelect(%this, %id)
{
	%rowText = %this.getRowTextById(%id);
	JMG_Slayer_Edit.setActive(getField(%rowText, 5));
}

function JMG_Slayer::onWake(%this)
{
	if(%this.isVisible())
	{
		JMG_Slayer_Create.setActive(
			SlayerClient.minigameState == $Slayer::MinigameState["Create"]);
		%id = JMG_List.getSelectedId();
		if(%id >= 0)
		{
			%rowText = JMG_List.getRowTextById(%id);
			JMG_Slayer_Edit.setActive(getField(%rowText, 5));
		}
		else
		{
			if(JMG_List.rowCount() > 0)
			{
				JMG_List.setSelectedRow(0);
			}
			JMG_Slayer_Edit.setActive(false);
		}
	}
}

function JMG_Slayer::createMinigame(%this)
{
	if(SlayerClient.minigameState !$= $Slayer::MinigameState["Create"])
	{
		showDialogBox
		(
			"OK",
			"exclamation",
			"Oops!",
			"You don't have permission to create a Slayer minigame.\n\n" @
			"Please contact the host for assistance.",
			"",
			true
		);
		return;
	}
	SlayerClient.editMinigame(-1);
	canvas.popDialog(joinMinigameGUI);
}

function JMG_Slayer::editMinigame(%this)
{
	%id = JMG_List.getSelectedId();
	%rowText = JMG_List.getRowTextById(%id);

	%isDefault = getField(%rowText, 4);
	%canEdit = getField(%rowText, 5);

	if(%id < 0)
	{
		showDialogBox
		(
			"OK",
			"exclamation",
			"Oops!",
			"Please select a minigame to edit.",
			"",
			true
		);
		return;
	}
	if(%isDefault $= "" && %canEdit $= "")
	{
		showDialogBox
		(
			"OK",
			"exclamation",
			"Oops!",
			"The minigame you have selected is not a Slayer minigame.\n\n" @
			"Please select a Slayer minigame to edit.",
			"",
			true
		);
		return;
	}
	if(!%canEdit)
	{
		showDialogBox
		(
			"OK",
			"exclamation",
			"Oops!",
			"You don't have permission to edit that minigame.\n\n" @
			"Please contact the owner for assistance.",
			"",
			true
		);
		return;
	}

	SlayerClient.editMinigame(%id);
	canvas.popDialog(joinMinigameGUI);
}

package SlayerClient_JoinMiniGameGUI
{
	function JoinMiniGameGui::onWake(%this)
	{
		commandToServer('Slayer_sendMinigameState');
		parent::onWake(%this);
	}
	
	function JoinMiniGameGui::clickLeave(%this)
	{
		parent::clickLeave(%this);
		commandToServer('Slayer_sendMinigameState');
	}
};
activatePackage(SlayerClient_JoinMiniGameGUI);