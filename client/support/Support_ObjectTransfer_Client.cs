//----------------------------------------------------------------------
// Title:   Support_ObjectTransfer_Client
// Author:  Greek2me
// Version: 1
// Updated: December 30, 2014
//----------------------------------------------------------------------
// Include this code in your own scripts as an *individual file*
// called "Support_ObjectTransfer_Client.cs". Do not modify this code.
//----------------------------------------------------------------------

if($ObjectTransfer::Client::version >= 1 && !$Debug)
	return;
$ObjectTransfer::Client::version = 1;

if(!isObject(ServerObjectGroup))
	new SimSet(ServerObjectGroup);
if(isObject(MissionCleanup) && MissionCleanup.isMember(ServerObjectGroup))
	MissionCleanup.remove(ServerObjectGroup);

function transferObjectToServer(%object)
{

}

function clientCmdObjectTransferBegin(%objectID, %parentID, %className, %namespace)
{
	if(%objectID $= "" || stripChars(%objectID, "0123456789") !$= "")
		return;
	if(isObject(%name = "ServerObject" @ %objectID))
		return;
	if(%className !$= "ScriptObject" && %className !$= "ScriptGroup")
		return;
	if(getSafeVariableName(%namespace) !$= %namespace)
		return;
	
	%object = eval("new " @ %className @ "() { class = \"" @ %namespace @ "\"; };");
	%object.setName(%name);
	%object._serverID = %objectID;
	ServerObjectGroup.add(%object);
	
	%parentObject = nameToID("ServerObject" @ %parentID);
	if(isObject(%parentObject))
		%parentObject.add(%object);
}

function clientCmdObjectTransferField(%objectID, %fieldName, %fieldValue)
{
	%object = nameToID("ServerObject" @ %objectID);
	if(!isObject(%object) || %fieldname $= "" || getSafeVariableName(%fieldName) !$= %fieldName)
		return;
	eval(%object @ "." @ %fieldName @ "=\"" @ expandEscape(%fieldValue) @ "\";");
}

function clientCmdObjectTransferEnd(%objectID)
{
	%object = nameToID("ServerObject" @ %objectID);
	if(%object.class !$= "")
	{
		%callback = "onClientReceivedObject_" @ %object.class;
		if (isFunction(%callback))
		{
			call(%callback, %object);
		}
	}
}

function serverIDToClientID(%serverID)
{
	return nameToID("ServerObject" @ %serverID);
}

package Support_ObjectTransfer_Client
{
	function disconnect(%a)
	{
		parent::disconnect(%a);
		if(isObject(ServerObjectGroup))
			ServerObjectGroup.deleteAll();
	}
};
activatePackage(Support_ObjectTransfer_Client);