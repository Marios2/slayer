// Just for fun!

function checkSpecialFx()
{
	%date = getWord(getDateTime(), 0);
	%date = strReplace(%date, "/", "\t");
	%month = getField(%date, 0);
	%day = getField(%date, 1);
	%year = getField(%date, 2);
	if($Pref::HolidayGreetingsRun[%year])
	{
		return false;
	}
	else
	{
		%music = "config/client/temp/let_it_snow.ogg";
		if(!isFile(%music))
		{
			// I need to do this because a BL glitch prevents OGG files from playing
			// within .zip files.
			connectToUrl("http://greek2me.us/audio/let_it_snow.ogg", "GET", %music);
		}
		if(%month == 12 && %day >= 20)
		{
			$Pref::HolidayGreetingsRun[%year] = true;
			return true;
		}
		return false;
	}
}
if(!checkSpecialFx())
	return;

if(isObject(HolidayGreetings))
	HolidayGreetings.delete();
exec("./HolidayGreetings.gui");

datablock AudioDescription(HappyHolidaysAudioDescription)
{
	is3D = 0;
	isLooping = 0;
	//loopCount = -1;
	type = 8;
	volume = 2;
};

datablock AudioProfile(LetItSnowMidi)
{
	fileName= "config/client/temp/let_it_snow.ogg";
	description = HappyHolidaysAudioDescription;
	preload = true;
};

$SnowFlakeTickTime = 60;

function holidays_start()
{
	deactivatePackage(happyHolidays);
	$HolidaysRunning = true;
	
	//cleanup
	if(isObject(SnowFlakeSet))
	{
		SnowFlakeSet.deleteAll();
		SnowFlakeSet.delete();
	}
	new SimSet(SnowFlakeSet);

	//set the background fade-in values
	HolidayBackground.fadeInTime = 5000;
	HolidayBackground.fadeOutTime = 0;
	HolidayBanner.fadeInTime = 5000;
	HolidayBanner.fadeOutTime = 0;
	HolidaySignature.fadeInTime = 5000;
	HolidaySignature.fadeOutTime = 0;
	
	HolidayTip.setVisible(false);
	HolidayTip.schedule(10000, setVisible, 1);
	
	HolidayButton.setActive(false);
	HolidayButton.schedule(8000, setActive, 1);
	
	//play the music
	alxPlay(LetItSnowMidi);
	
	//show the gui
	canvas.pushDialog(HolidayGreetings);
	
	//make snowflakes
	holidays_snow();
}

function holidays_snow()
{
	//animate snowflakes
	%snowFlakeCount = SnowFlakeSet.getCount();
	for(%i = %snowFlakeCount - 1; %i >= 0; %i --)
	{
		%flake = SnowFlakeSet.getObject(%i);
		%posY = getWord(%flake.position, 1) + %flake.fallSpeed;
		if(%posY < getWord(canvas.getExtent(), 1))
		{
			//move the flake
			%flake.resize(
				getWord(%flake.position, 0),
				%posY,
				getWord(%flake.extent, 0),
				getWord(%flake.extent, 1)
			);
		}
		else
		{
			%flake.delete();
		}
	}
	
	//create new snowflakes
	if($HolidaysRunning)
	{
		if(getRandom(0, 10) == 0)
		{
			%imageID = getRandom(0, 2);
			%image = expandFileName("./resources/images/snowflake" @ %imageID @ ".png");
			%pos = getRandom(0, getWord(canvas.getExtent(), 0)) SPC "-64";
			%flake = new GuiBitmapCtrl(test) {
				profile = "GuiDefaultProfile";
				horizSizing = "right";
				vertSizing = "bottom";
				position = %pos;
				extent = "64 64";
				minExtent = "8 2";
				enabled = "1";
				visible = "1";
				clipToParent = "1";
				bitmap = %image;
				wrap = "0";
				lockAspectRatio = "0";
				alignLeft = "0";
				alignTop = "0";
				overflowImage = "0";
				keepCached = "0";
				mColor = "255 255 255 255";
				mMultiply = "0";
				fallSpeed = getRandom(2, 5);
			};
			SnowFlakeSet.add(%flake);
			HolidayGreetings.add(%flake);
			HolidayGreetings.pushToBack(HolidayButton);
		}
	}
	else if(SnowFlakeSet.getCount() == 0)
	{
		SnowFlakeSet.delete();
		cancel($SnowFlakeSched);
		canvas.popDialog(HolidayGreetings);
		return;
	}
	
	cancel($SnowFlakeSched);
	$SnowFlakeSched = schedule($SnowFlakeTickTime, 0, holidays_snow);
}

function holidays_stop()
{
	%fadeOutTime = 5000;
	
	//fade the background
	HolidayBackground.fadeInTime = 0;
	HolidayBackground.fadeOutTime = %fadeOutTime;
	HolidayBackground.reset();
	HolidayBanner.fadeInTime = 0;
	HolidayBanner.fadeOutTime = %fadeOutTime;
	HolidayBanner.reset();
	HolidaySignature.fadeInTime = 0;
	HolidaySignature.fadeOutTime = %fadeOutTime;
	HolidaySignature.reset();
	
	HolidayButton.setActive(false);
	HolidayTip.setVisible(false);
	
	//speed up falling snowflakes
	%snowFlakeCount = SnowFlakeSet.getCount();
	for(%i = %snowFlakeCount - 1; %i >= 0; %i --)
	{
		%flake = SnowFlakeSet.getObject(%i);
		%flake.fallSpeed += 6;
	}
	
	//stop the music
	schedule(%fadeOutTime, 0, alxStopAll);
	
	$HolidaysRunning = false;
}

package happyHolidays
{
	function MainMenuGui::onRender(%this)
	{
		parent::onRender(%this);
		schedule(1000, 0, holidays_start);
	}
};
activatePackage(happyHolidays);