// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

new ScriptObject(SlayerClient_PrefSO)
{
	category = "Welcome Screen";
	title = "Display on Startup";
	defaultValue = false;
	type = "bool";
	variable = "$Pref::Slayer::Client::showStartScreen";
	guiTag = "options";
};

new ScriptObject(SlayerClient_PrefSO)
{
	category = "Minigames";
	title = "Notify Players on Update";
	defaultValue = true;
	type = "bool";
	variable = "$Pref::Slayer::Client::NotifyPlayersOnUpdate";
	guiTag = "options";
};

new ScriptObject(SlayerClient_PrefSO)
{
	category = "EoRR";
	title = "Disable End of Round Report";
	defaultValue = false;
	type = "bool";
	variable = "$Pref::Slayer::Client::Disable_Slayer_CtrDisplay";
	guiTag = "options";
};