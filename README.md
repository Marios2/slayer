﻿Gamemode_Slayer
===============

Slayer is a game mode for [Blockland][1]. Create teams and battle to the death or use one of Slayer's many game modes to create a custom game.

[Download Slayer][2]

[BLF Discussion Thread][3]

[Modding Documentation][4]

	+-------------------------------------------+
	|  ___   _     _____   __  __  ____   ____  |
	| | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
	| |__ | | |_  |  _  |   \  /  | __|  | \/ / |
	| |___| |___| |_| |_|   |__|  |____| |_| \\ |
	|  Greek2me              Blockland ID 11902 |
	+-------------------------------------------+

[1]: http://blockland.us
[2]: http://mods.greek2me.us/storage/Gamemode_Slayer.zip
[3]: http://forum.blockland.us/index.php?topic=256245.0
[4]: https://bitbucket.org/Greek2me/slayer/wiki/Modding
