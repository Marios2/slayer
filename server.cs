// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

$Slayer::Server::Debug = 0;

//----------------------------------------------------------------------
// Define globals.
//----------------------------------------------------------------------

$Slayer::Version = "4.1.3";

$Slayer::Server::Version = $Slayer::Version;
$Slayer::Server::CompatibleVersion = "4.1.2";

$Slayer::Platform = ($Server::Dedicated ? "DedicatedServer" : "IntegratedServer");

$Slayer::Path = filePath(expandFileName("./server.cs"));
$Slayer::Common::Path = $Slayer::Path @ "/common";
$Slayer::Server::Path = $Slayer::Path @ "/server";

$Slayer::Common::ConfigDir = "config/common/Slayer";
$Slayer::Server::ConfigDir = "config/server/Slayer";

$Slayer::Server::GameModeArg = $Slayer::Path @ "/gamemode.txt";
$Slayer::Server::CurGameModeArg = $gameModeArg;

warn("Slayer Version" SPC $Slayer::Server::Version);
warn(" + Compatible Version:" SPC $Slayer::Server::CompatibleVersion);
warn(" + Platform:" SPC $Slayer::Platform);
warn(" + Core Directory:" SPC $Slayer::Server::Path);
warn(" + Config Directory:" SPC $Slayer::Server::ConfigDir);
warn(" + Debug Mode:" SPC $Slayer::Server::Debug);

//----------------------------------------------------------------------
// Load core components.
//----------------------------------------------------------------------

exec($Slayer::Path @ "/common.cs");
exec($Slayer::Server::Path @ "/support/Support.cs");
Slayer_Support::LoadFiles($Slayer::Server::Path @ "/support/*.cs", "Support_");
Slayer_Support::LoadFiles($Slayer::Server::Path @ "/core/*.cs");

//----------------------------------------------------------------------
// Initialize Slayer.
//----------------------------------------------------------------------

if(!isObject(Slayer))
{
	$Slayer::Server = new ScriptGroup(Slayer);
}

//----------------------------------------------------------------------
// Load preferences, optional modules, and compatibility scripts.
//----------------------------------------------------------------------

Slayer_Support::LoadFiles($Slayer::Server::Path @ "/defaults/*.cs");
Slayer_Support::LoadFiles($Slayer::Server::Path @ "/modules/*.cs");
Slayer_Support::LoadFiles($Slayer::Server::Path @ "/compatibility/*.cs");