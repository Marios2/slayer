// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

$Slayer::Client::Debug = 0;

//----------------------------------------------------------------------
// Define globals.
//----------------------------------------------------------------------

if(isObject(SlayerClient))
	SlayerClient.delete();

$Slayer::Version = "4.1.3";

$Slayer::Client::Version = $Slayer::Version;
$Slayer::Client::CompatibleVersion = "4.1.2";

$Slayer::Platform = "Client";

$Slayer::Path = filePath(expandFileName("./client.cs"));
$Slayer::Common::Path = $Slayer::Path @ "/common";
$Slayer::Client::Path = $Slayer::Path @ "/client";

$Slayer::Common::ConfigDir = "config/common/Slayer";
$Slayer::Client::ConfigDir = "config/client/Slayer";

$Slayer::Client::FirstRun = !isFile($Slayer::Client::ConfigDir @ "/debug_client.log");

warn("Slayer Version" SPC $Slayer::Client::Version);
warn(" + Compatible Version:" SPC $Slayer::Client::CompatibleVersion);
warn(" + Platform:" SPC $Slayer::Platform);
warn(" + Core Directory:" SPC $Slayer::Client::Path);
warn(" + Config Directory:" SPC $Slayer::Client::ConfigDir);
warn(" + Debug Mode:" SPC $Slayer::Client::Debug);

//----------------------------------------------------------------------
// Load core components.
//----------------------------------------------------------------------

exec($Slayer::Path @ "/common.cs");
exec($Slayer::Client::Path @ "/support/Support.cs");
Slayer_Support::loadFiles($Slayer::Client::Path @ "/support/*.cs", "Support_");
Slayer_Support::LoadFiles($Slayer::Client::Path @ "/core/*.cs");

//----------------------------------------------------------------------
// Initialize SlayerClient.
//----------------------------------------------------------------------

if(!isObject(SlayerClient))
{
	$Slayer::Client = new ScriptGroup(SlayerClient)
	{
		minigameState = $Slayer::MinigameState["None"];
	};
}

//----------------------------------------------------------------------
// Load preferences, optional modules, and GUIs.
//----------------------------------------------------------------------

Slayer_Support::LoadFiles($Slayer::Client::Path @ "/gui/*.cs");
if(!$Slayer::Client::GuiLoaded)
{
	Slayer_Support::LoadFiles($Slayer::Client::Path @ "/gui/*.gui");
	$Slayer::Client::GuiLoaded = true;
}
Slayer_Support::LoadFiles($Slayer::Client::Path @ "/defaults/*.cs");
Slayer_Support::LoadFiles($Slayer::Client::Path @ "/modules/*.cs");

//----------------------------------------------------------------------
// Register keybinds.
//----------------------------------------------------------------------

//remove the old keybind from pre v3.0
unRegisterKeyBind("Slayer", "Setup", "SlayerClient_pushSetup");

//add keybinds
registerKeyBind("Slayer", "Edit Minigame", "SlayerClient_pushMain");
registerKeyBind("Slayer", "Options", "SlayerClient_pushOptions");

//----------------------------------------------------------------------
// Third-party mods.
//----------------------------------------------------------------------

//Blockland Glass
if(isObject(BLG_DT))
{
	BLG_DT.registerImageIcon("Slayer Options", "canvas.pushDialog(Slayer_Options);", $Slayer::Client::Path @ "/resources/images/logo_icon.png");
}

//Nexus Escape Menu Overlay
if(isFile("Add-Ons/Client_NEO.zip"))
{
	if(!strLen($neoCount))
		$neoCount = 0;
	$neoName[$neoCount] = "Slayer";
	$neoCmd[$neoCount] = "SlayerClient_pushMain(1);";
	$neoIcon[$neoCount] = "Add-Ons/Client_NEO/ui/minigames.png";
	$neoPrefClose[$neoCount] = true;
	$neoCount ++;
}