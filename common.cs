// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

if(!$Slayer::Common)
{
	$Slayer::Common = true;
	exec($Slayer::Common::Path @ "/support/Support.cs");
	Slayer_Support::loadFiles($Slayer::Common::Path @ "/support/*.cs", "Support_");
	Slayer_Support::loadFiles($Slayer::Common::Path @ "/core/*.cs");
	Slayer_Support::loadFiles($Slayer::Common::Path @ "/defaults/*.cs");
}
Slayer_Support::loadFiles($Slayer::Common::Path @ "/modules/*.cs");