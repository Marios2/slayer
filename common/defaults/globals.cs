// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

$Slayer::PI = 3.1415926;

$Slayer::MinigameState["None"] = 0;
$Slayer::MinigameState["Edit"] = 1;
$Slayer::MinigameState["Create"] = 2;

$Slayer::PermissionLevel["Any"] = -1;
$Slayer::PermissionLevel["Host"] = 0;
$Slayer::PermissionLevel["SuperAdmin"] = 1;
$Slayer::PermissionLevel["Admin"] = 2;
$Slayer::PermissionLevel["Owner"] = 3;
$Slayer::PermissionLevel["OwnerFullTrust"] = 4;
$Slayer::PermissionLevel["OwnerBuildTrust"] = 5;

$Slayer::URL::BugReport = "bitbucket.org/Greek2me/slayer/issues/new";
$Slayer::URL::Download = "mods.greek2me.us/storage/Gamemode_Slayer.zip";
$Slayer::URL::Information = "forum.blockland.us/index.php?topic=256245.0";
$Slayer::URL::Respository = "bitbucket.org/Greek2me/slayer/";
$Slayer::URL::Wiki = "bitbucket.org/Greek2me/slayer/wiki/browse/";