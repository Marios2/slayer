//----------------------------------------------------------------------
// Title:   Support_CSVReader
// Author:  Greek2me
// Version: 1
// Updated: December 30, 2014
//----------------------------------------------------------------------
// Character-separated-value reader. Description goes here.
//----------------------------------------------------------------------
// Include this code in your own scripts as an *individual file*
// called "Support_CSVReader.cs". Do not modify this code.
//----------------------------------------------------------------------

if($Support_CSVReader::version >= 1 && !$Debug)
	return;
$Support_CSVReader::version = 1;

//Creates a new CSVReader object.
//@param	string delimiter	The delimiter to use. "\" is the escape character.
//@param	string dataString	The string to parse. Not required in constructor.
//@return	CSVReader
function CSVReader(%delimiter, %dataString)
{
	if(!strLen(%delimiter))
		%delimiter = ",";
	return new ScriptObject()
	{
		class = CSVReader;
		currentPosition = 0;
		delimiter = %delimiter;
		dataString = %dataString;
	};
}

//Gives the CSVReader a new string to parse.
//@param	string dataString
function CSVReader::setDataString(%this, %dataString)
{
	%this.currentPosition = 0;
	%this.dataString = %dataString;
}

//Reads data from a comma-separated-value string.
//@param	string csvData
//@return	string	The next value found.
function CSVReader::readNextValue(%this)
{
	%inQuotes = false;
	%quoteStart = -1;
	%quoteEnd = -1;
	%length = strLen(%this.dataString);
	for(%i = %this.currentPosition; %i <= %length; %i ++)
	{
		%char = getSubStr(%this.dataString, %i, 1);
		if(%char $= "\\") continue;
		if(%char $= "\"") 
		{
			if(%inQuotes)
				%quoteEnd = %i;
			else if(%quoteStart < 0)
				%quoteStart = %i + 1;
			else
				break;
			%inQuotes = !%inQuotes;
		}
		if((!%inQuotes && %char $= %this.delimiter) || %i >= %length)
		{
			if(%quoteStart < 0)
				%value = getSubStr(%this.dataString, %this.currentPosition, %i - %this.currentPosition);
			else if(%quoteEnd > %quoteStart)
				%value = getSubStr(%this.dataString, %quoteStart, %quoteEnd - %quoteStart);
			%this.currentPosition = %i + 1;
			break;
		}
	}
	return %value;
}

//Checks whether another value exists.
//@return	bool
function CSVReader::hasNextValue(%this)
{
	return %this.currentPosition < strLen(%this.dataString) - 1;
}