// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

//Checks that %value is within acceptable limits.
//@param	string value
//@param	bool skipObjectChecks	Used on the client when an object may be server-sided.
//@return	bool
function Slayer_PrefSO::idiotProof(%this, %value, %skipObjectChecks)
{
	switch$(%this.type)
	{
		case "bool":
			if(%value !$= false && %value !$= true)
				return false;

		case "string":
			%value = getSubStr(%value, 0, %this.string_maxLength);
		
		case "object":
			if(!%skipObjectChecks)
			{
				if(isObject(%value))
				{
					if(strLen(%this.object_class) && %value.getClassName() !$= %this.object_class)
						return false;
					%value = nameToID(%value);
				}
				else
				{
					if(%this.object_cannotBeNull)
						return false;
					%value = 0;
				}
			}

		case "int":
			if(!Slayer_Support::isFloat(%value))
				return false;
			%value = Slayer_Support::StripTrailingZeros(%value);
			%value = mClampF(%value, %this.int_minValue, %this.int_maxValue);

		case "slide":
			if(!Slayer_Support::isFloat(%value))
				return false;
			%value = Slayer_Support::StripTrailingZeros(%value);
			%value = mClampF(%value, %this.slide_minValue, %this.slide_maxValue);

		case "list":
			%count = getRecordCount(%type);
			if(%count > 1)
			{
				for(%i = 1; %i < %count; %i ++)
				{
					%f = getRecord(%type, %i);
					%w = firstWord(%f);
					if(%w == %value)
					{
						%ok = true;
						break;
					}
				}
				if(!%ok)
					return false;
			}
	}

	return true TAB %value;
}

//Returns a human-readable version of the value.
//@return	string
function Slayer_PrefSO::getDisplayValue(%this, %value)
{
	switch$(%this.type)
	{
		case "bool":
			return %value ? "True" : "False";

		case "list":
			for(%i = 0; %i < getRecordCount(%this.list_items); %i ++)
			{
				%f = getRecord(%this.list_items, %i);
				if(firstWord(%f) $= %value)
				{
					return restWords(%f);
				}
			}

		case "object":
			if(isObject(%value))
			{
				return %value.uiName;
			}
			else if(!%this.object_cannotBeNull)
			{
				return "None";
			}

		default:
			return %value;
	}
}