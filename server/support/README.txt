Support scripts are generic, modular scripts, similar to libraries. Feel free 
to use them, but give the original author credit. Authors of support scripts 
may vary; they should be named in each file.

Include support scripts in your add-ons as individual files. **Do not modify
the support scripts.**