//////////////////////////////////////////////////////////////////////////////////////////////////
//				     Support_SpecialKills.cs     				//
//Creator: Space Guy [ID 130]									//
//Allows you to create special extra messages added to damage types in minigames		//
//												//
//addSpecialDamageMsg(string name,		- unique name for the message			//
//			string murderMessage,	- message displayed on kill			//
//			string suicideMessage,	- message displayed on self-kill		//
//												//
//string isSpecialKill_name(%this,%sourceObject,%sourceClient,%mini)				//
// return "-1" to hide the message completely
// return "0" for no special message								//
// return "1" for special message								//
// return "2" TAB [value] for extra values							//
// return "3" - Same as 1, but it only displays the client that died, not the killer	//
// return "4" TAB [value] - Same as 2, but it only displays the client that died, not the killer	//
//												//
//In the messages:										//
// %1 is the killed player and any messages after the name					//
// %2 is the killer's name and any messages before the name					//
// %3 is any text between the names (e.g. CI bitmap)						//
// %4+ are special strings sent by returning "2" TAB [value] TAB [...] from the function	//
//////////////////////////////////////////////////////////////////////////////////////////////////

//CHANGE LOG
//+v2 (By Greek2me [ID 11902])
// - Added support for bots.
// - Added support for more than three extra values.
// - Can now return "3" or "4" to hide killer
// - Can now return "-1" to hide the message completely
// - Now uses 'MsgClientKilled'

if($SpaceMods::Server::SpecialKillsVersion > 2 && !$Debug)
   return;
$SpaceMods::Server::SpecialKillsVersion = 2;

function addSpecialDamageMsg(%name,%murderMsg,%suicideMsg)
{
	if($SpecialDamage_NumTypes $= "")
		$SpecialDamage_NumTypes = -1;
	
	if($SpecialDamage_TypeID[%name] $= "")
		%id = $SpecialDamage_NumTypes++;
	else
		%id = $SpecialDamage_TypeID[%name];
	
	$SpecialDamage_TypeID[%name] = %id;
	$SpecialDamage_TypeName[%id] = %name;
	$SpecialDamage_MurderMessage[%id] = %murderMsg;
	$SpecialDamage_SuicideMessage[%id] = %suicideMsg;
}

package SpecialKills
{
	function GameConnection::onDeath(%this,%sourceObject,%sourceClient,%damageType,%damageArea)
	{
		%so = %sourceObject.sourceObject;
		if(!isObject(%sourceClient) && %so.isHoleBot && !isObject(%so.client))
				%sourceClient = %so;
		%mini = (isObject(%this.minigame) ? %this.minigame : 0);
		%kill = (isObject(%sourceClient) && %sourceClient != %this);
		
		%curmsg = "%2%3%1";
		for(%i=0;$SpecialDamage_TypeName[%i] !$= "";%i++)
		{
			%f = "isSpecialKill_" @ $SpecialDamage_TypeName[%i];
			if(!isFunction(%f))
				continue;
			
			%val = call(%f,%this,%sourceObject,%sourceClient,%mini);
			%numFields = getFieldCount(%val);
			%num = getField(%val,0);
			
			if(%num == -1)
			{
				%damageType = 0;
				%this.player.lastDirectDamageType = 0;
				%specialMessage = 0;
				break;
			}
			
			if(!%num)
				continue;

			if(%num == 3)
			{
				%kill = 0;
				%num = 1;
			}
			else if(%num == 4)
			{
				%kill = 0;
				%num = 2;
			}
			
			if(%num == 2)
			{
				for(%e = 1; %e < %numFields; %e ++)
					%extra[%e] = getField(%val,%e);
			}
			else
			{
				for(%e = 1; %e < %numFields; %e ++)
					%extra[%e] = "";
			}
			
			if(%kill)
			{
				%msg = $SpecialDamage_MurderMessage[%i];
				
				%pos1 = strPos(%msg,"%2");
				%pos2 = strPos(%msg,"%1");
				%killerString = getSubStr(%msg,0,%pos1+2);
				%killedString = getSubStr(%msg,%pos2,100);
				%ciString = getSubStr(%msg,%pos1+2,%pos2-%pos1-2);
			}
			else
			{
				%msg = $SpecialDamage_SuicideMessage[%i];
				
				%pos1 = strPos(%msg,"%1");
				%killerString = "";
				%killedString = getSubStr(%msg,%pos1,100);
				%ciString = getSubStr(%msg,0,%pos1);
			}
			
			%curmsg = strReplace(%curmsg,"%1",%killedString);
			%curmsg = strReplace(%curmsg,"%2",%killerString);
			%curmsg = strReplace(%curmsg,"%3",%ciString);
			%e = 4;
			while(strPos(%curmsg,"%" @ %e) >= 0)
			{
				%curmsg = strReplace(%curmsg,"%" @ %e,%extra[%e - 3]);
				%e ++;
			}
			
			%specialMessage = 1;
		}
		
		if(%specialMessage)
		{
			if(%kill)
			{
				%msg = getTaggedString($DeathMessage_Murder[%damageType]);
				
				%pos1 = strPos(%msg,"%2");
				%pos2 = strPos(%msg,"%1");
				%killerString = getSubStr(%msg,0,%pos1+2);
				%killedString = getSubStr(%msg,%pos2,100);
				%ciString = getSubStr(%msg,%pos1+2,%pos2-%pos1-2);
			}
			else
			{
				%msg = getTaggedString($DeathMessage_Suicide[%damageType]);
				%pos1 = strPos(%msg,"%1");
				%killerString = "";
				%killedString = getSubStr(%msg,%pos1,100);
				%ciString = getSubStr(%msg,0,%pos1);
			}
			
			%msg = strReplace(%curmsg,"%1",%killedString);
			%msg = strReplace(%curmsg,"%2",%killerString);
			%msg = strReplace(%curmsg,"%3",%ciString);
			
			%clientName = (%this.getClassName() $= "GameConnection" ? %this.getPlayerName() : %this.hName);
			%msg = strReplace(%msg,"%1",%clientName);
			if(%kill)
			{
				%killerName = (%sourceClient.getClassName() $= "GameConnection" ? %sourceClient.getPlayerName() : %sourceClient.hName);
				%msg = strReplace(%msg,"%2",%killerName);
			}
			
			if(isObject(%mini))
				%mini.messageAll('MsgClientKilled',%msg);
			else
				messageAll('MsgClientKilled',%msg);
			
			%damageType = 0;
			%this.player.lastDirectDamageType = 0;
		}
		
		parent::onDeath(%this,%sourceObject,%sourceClient,%damageType,%damageArea);
	}
};
activatePackage(SpecialKills);