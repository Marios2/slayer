// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+


// +-------------------------+
// | Stuff for finding stuff |
// +-------------------------+

//Searches for an object given a UI name.
//@param	string uiName
//@param	string className	Specifies what class the object must be.
function Slayer_Support::getIDFromUiName(%uiName, %className)
{
	%group = DatablockGroup;
	switch$(%className)
	{
		case "fxDtsBrick": return $uiNameTable[%uiName];
		case "ItemData": return $uiNameTable_Items[%uiName];
		case "ParticleEmitterData": return $uiNameTable_Emitters[%uiName];
		case "WheeledVehicleData" or "FlyingWheeledVehicleData" or "VehicleData": return $uiNameTable_Vehicle[%uiName];
		case "ScriptGroup":
			for(%i = Slayer.gameModes.getCount() - 1; %i >= 0; %i --)
			{
				%obj = Slayer.gameModes.getObject(%i);
				if(%obj.uiName $= %uiName)
					return %obj;
			}
		case "":
			%count = %group.getCount();
			for(%i = 0; %i < %count; %i ++)
			{
				%obj = %group.getObject(%i);
				if(!strLen(%obj.uiName))
					continue;
				%cn = %obj.getClassName();
				if(%cn $= "MissionArea")
					break;
				if(%obj.uiName $= %uiName)
					return %obj;
			}
		default:
			%count = %group.getCount();
			for(%i = 0; %i < %count; %i ++)
			{
				%obj = %group.getObject(%i);
				if(!strLen(%obj.uiName))
					continue;
				%cn = %obj.getClassName();
				if(%cn $= "MissionArea")
					break;
				if(%obj.uiName $= %uiName && %cn $= %className)
					return %obj;
			}
	}
	return 0;
}

function Slayer_Support::getClientFromObject(%obj)
{
	if(!isObject(%obj))
		return -1;

	//generic stuff
	if(isObject(%obj.client))
		return %obj.client;
	if(isObject(%obj.sourceClient))
		return %obj.sourceClient;

	//special cases
	if(%obj.isHoleBot && !%obj.isSlayerBot)
		return %obj;
	%class = %obj.getClassName();
	if(%class $= "GameConnection" || %class $= "AiController")
		%client = %obj;
	else if(%class $= "fxDtsBrick")
		%client = %obj.getGroup().client;
	else if(Slayer_Support::isVehicle(%obj))
		%client = %obj.getControllingObject().client;

	if(isObject(%client))
		return %client;

	return -1;
}

function Slayer_Support::getBLIDFromObject(%obj)
{
	if(!isObject(%obj))
		return -1;

	if(%obj.bl_id !$= "")
		return %obj.bl_id;

	%class = %obj.getClassName();

	if(%class $= "GameConnection")
		return %obj.getBLID();
	if(%class $= "Player" && isObject(%obj.client))
		return %obj.client.getBLID();
	if(%class $= "fxDtsBrick" && isObject(%obj.getGroup()))
		return %obj.getGroup().bl_id;
	if(Slayer_Support::isVehicle(%obj) && isObject(%obj.brickGroup))
		return %obj.brickGroup.bl_id;
	if(%class $= "Item")
	{
		if(isObject(%obj.spawnBrick))
			return %obj.spawnBrick.getGroup().bl_id;
	}
	if(%obj.class $= "Slayer_MinigameSO")
		return %obj.creatorBLID;

	return -1;
}

function Slayer_Support::getBlocklandID()
{
	if($Server::LAN)
		return getLAN_BLID();
	else
		return getNumKeyID();
}

// +-------+
// | Debug |
// +-------+

function Slayer_Support::Debug(%level, %title, %value)
{
	if($Slayer::Server::Debug == -1)
		return;

	if(%value $= "")
		%val = %title;
	else
		%val = %title @ ":" SPC %value; 

	if(%level <= $Slayer::Server::Debug)
		echo("\c4Slayer (Server):" SPC %val);

	%path = $Slayer::Server::ConfigDir @ "/debug_server.log";
	if(isWriteableFileName(%path))
	{
		if(!isObject(Slayer.DebugFO))
		{
			Slayer.debugFO = new fileObject();
			Slayer.add(Slayer.debugFO);

			Slayer.debugFO.openForWrite(%path);
			Slayer.debugFO.writeLine("//Slayer Version" SPC $Slayer::Server::Version SPC "-----" SPC getDateTime());
			Slayer.debugFO.writeLine("//By Greek2me, Blockland ID 11902");
		}

		Slayer.debugFO.writeLine(%val);
	}
}

function Slayer_Support::Error(%title, %value)
{
	if(%value $= "")
		%val = %title;
	else
		%val = %title @ ":" SPC %value; 

	error("Slayer Error (Server):" SPC %val);

	%path = $Slayer::Server::ConfigDir @ "/debug_server.log";
	if(isWriteableFileName(%path))
	{
		if(!isObject(Slayer.DebugFO))
		{
			Slayer.debugFO = new fileObject();
			Slayer.add(Slayer.debugFO);

			Slayer.debugFO.openForWrite(%path);
			Slayer.debugFO.writeLine("//Slayer Version" SPC $Slayer::Server::Version SPC "-----" SPC getDateTime());
			Slayer.debugFO.writeLine("//By Greek2me, Blockland ID 11902");
		}

		Slayer.debugFO.writeLine("ERROR:" SPC %val);
	}
}

// +-------+
// | Other |
// +-------+

function Slayer_Support::isVehicle(%obj)
{
	if(!isObject(%obj))
		return 0;

	if(!isFunction(%obj.getClassName(), "getDatablock"))
		return 0;

	if((%obj.getType() & $TypeMasks::VehicleObjectType) || %obj.getDatablock().numMountPoints > 0)
		return 1;
	else
		return 0;
}