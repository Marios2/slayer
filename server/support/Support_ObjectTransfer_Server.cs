//----------------------------------------------------------------------
// Title:   Support_ObjectTransfer_Server
// Author:  Greek2me
// Version: 1
// Updated: December 30, 2014
//----------------------------------------------------------------------
// Include this code in your own scripts as an *individual file*
// called "Support_ObjectTransfer_Server.cs". Do not modify this code.
//----------------------------------------------------------------------

if($ObjectTransfer::Server::version >= 1 && !$Debug)
	return;
$ObjectTransfer::Server::version = 1;

//Sends a ScriptObject or ScriptGroup to a client.
//@param	GameConnection client
//@param	ScriptObject object
//@param	bool recursive	Whether to send child objects.
//@param	string namespace	The optional namespace used by this object on the client. *Not sent with child objects.*
function transferObjectToClient(%client, %object, %recursive, %namespace)
{
	if(!isObject(%object))
		return;
	%className = %object.getClassName();
	if(%className !$= "ScriptObject" && %className !$= "ScriptGroup")
	{
		error("ERROR: You may only send objects of class ScriptObject or ScriptGroup.");
		return;
	}

	// Send the identifying information.
	%id = %object.getID();
	%parent = nameToID(%object.getGroup());
	commandToClient(%client, 'ObjectTransferBegin', %id, %parent, %className, %namespace);

	// Send the tagged fields.
	%index = 0;
	while((%field = %object.getTaggedField(%index)) !$= "")
	{
		if(strPos(%field, "_") != 0)
		{
			%fieldName = getField(%field, 0);
			%fieldValue = getFields(%field, 1);
			commandToClient(%client, 'ObjectTransferField', %id, %fieldName, %fieldValue);
		}
		%index ++;
	}

	if(%recursive && isFunction(%className, "getCount"))
	{
		%count = %object.getCount();
		for(%i = 0; %i < %count; %i ++)
		{
			transferObjectToClient(%client, %object.getObject(%i), true, "");
		}
	}
	
	// Let the client know that they have the complete object.
	commandToClient(%client, 'ObjectTransferEnd', %id);
}