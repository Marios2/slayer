// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

new ScriptObject(Slayer_DefaultTeamPrefSO)
{
	//category = "";
	//title = "";
	//defaultValue = "";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	//variable = "";
	//type = "";
	//callback = "";
	//guiTag = "";
	notifyPlayersOnChange = true;
	// requiresMiniGameReset = false;
	// requiresServerRestart = false;
	//doNotSendToClients = false;
	priority = 1000;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Bots";
	title = "Preferred Player Count";
	defaultValue = -1;
	permissionLevel = $Slayer::PermissionLevel["Admin"];
	variable = "botFillLimit";
	type = "int";
	int_minValue = -1;
	int_maxValue = 99;
	callback = "%team.updateBotFillLimit(%1);";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Captain";
	title = "Playertype";
	defaultValue = nameToID(playerStandardArmor);
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "captain_playerDatablock";
	type = "object";
	object_class = "PlayerData";
	object_cannotBeNull = true;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Captain";
	title = "Start Equip 0";
	defaultValue = nameToID(hammerItem);
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "captain_startEquip0";
	type = "object";
	object_class = "ItemData";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Captain";
	title = "Start Equip 1";
	defaultValue = nameToID(wrenchItem);
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "captain_startEquip1";
	type = "object";
	object_class = "ItemData";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Captain";
	title = "Start Equip 2";
	defaultValue = nameToID(printGun);
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "captain_startEquip2";
	type = "object";
	object_class = "ItemData";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Captain";
	title = "Start Equip 3";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "captain_startEquip3";
	type = "object";
	object_class = "ItemData";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Captain";
	title = "Start Equip 4";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "captain_startEquip4";
	type = "object";
	object_class = "ItemData";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Chat";
	title = "Enable Team Chat";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "enableTeamChat";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Chat";
	title = "Hide Kills";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "hideKillMsgs";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Chat";
	title = "Name Color";
	defaultValue = "";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "chatColor";
	type = "string";
	string_maxLength = 6;
	guiTag = "Advanced";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Auto Sort Weight";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "sortWeight";
	type = "int";
	int_minValue = 0;
	int_maxValue = 99;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Auto Sort";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "sort";
	type = "bool";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Color";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "color";
	type = "colorID";
	notifyPlayersOnChange = false;
	callback = "%team.updateColor(%1);";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Lives";
	defaultValue = -1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "lives";
	type = "int";
	int_minValue = -1;
	int_maxValue = 999;
	guiTag = "Advanced";
	callback = "%team.updateLives(%1, %2);";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Lock Team";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "lock";
	type = "bool";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Max Players";
	defaultValue = -1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "maxPlayers";
	type = "int";
	int_minValue = -1;
	int_maxValue = 99;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Name";
	defaultValue = "New Team";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "name";
	type = "string";
	string_maxLength = 50;
	callback = "%team.updateName(%1);";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Player Scale";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "playerScale";
	type = "slide";
	slide_minValue = 0;
	slide_maxValue = 5;
	slide_numTicks = 4;
	slide_snapToTicks = 1;
	guiTag = "Advanced";
	callback = "%team.updatePlayerScale(%1);";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Playertype";
	defaultValue = nameToID(playerStandardArmor);
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "playerDatablock";
	type = "object";
	object_class = "PlayerData";
	object_cannotBeNull = true;
	notifyPlayersOnChange = false;
	callback = "%team.updateDatablock(%1);";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Respawn Time";
	defaultValue = -1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "respawnTime_Player";
	type = "int";
	int_minValue = -1;
	int_maxValue = 999;
	guiTag = "Advanced";
	callback = "%team.updateRespawnTime(player, %1, %2);";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Spectator Team";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "spectate";
	type = "bool";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Start Equip 0";
	defaultValue = nameToID(hammerItem);
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "startEquip0";
	type = "object";
	object_class = "ItemData";
	notifyPlayersOnChange = false;
	callback = "%team.updateEquip(0, %1, %2);";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Start Equip 1";
	defaultValue = nameToID(wrenchItem);
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "startEquip1";
	type = "object";
	object_class = "ItemData";
	notifyPlayersOnChange = false;
	callback = "%team.updateEquip(1, %1, %2);";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Start Equip 2";
	defaultValue = nameToID(printGun);
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "startEquip2";
	type = "object";
	object_class = "ItemData";
	notifyPlayersOnChange = false;
	callback = "%team.updateEquip(2, %1, %2);";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Start Equip 3";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "startEquip3";
	type = "object";
	object_class = "ItemData";
	notifyPlayersOnChange = false;
	callback = "%team.updateEquip(3, %1, %2);";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Start Equip 4";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "startEquip4";
	type = "object";
	object_class = "ItemData";
	notifyPlayersOnChange = false;
	callback = "%team.updateEquip(4, %1, %2);";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Sync w/ Minigame Loadout";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "syncLoadout";
	type = "bool";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Template";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "template";
	type = "object";
	notifyPlayersOnChange = false;
	priority = 100;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Uni Update Trigger";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_updateTrigger";
	type = "int";
	int_minValue = 0;
	int_maxValue = 999999;
	notifyPlayersOnChange = false;
	callback = "%team.updateUniform();";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Uniform";
	defaultValue = 2;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uniform";
	type = "list";
	list_items = "0 NONE" NL "1 Shirt Only" NL "2 Full" NL "3 Custom";
	callback = "%team.updateUniform();";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Team";
	title = "Win on Time Up";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "winOnTimeUp";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "Accent";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_accent";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "AccentColor";
	defaultValue = "TEAMCOLOR";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_accentColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "Chest";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_chest";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "ChestColor";
	defaultValue = "";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_chestColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "DecalColor";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_decalColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "DecalName";
	defaultValue = "AAA-None";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_decalName";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "FaceColor";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_faceColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "FaceName";
	defaultValue = "smiley";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_faceName";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "Hat";
	defaultValue = "6";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_hat";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "HatColor";
	defaultValue = "TEAMCOLOR";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_hatColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "HeadColor";
	defaultValue = "1 0.878431 0.611765 1";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_headColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "Hip";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_hip";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "HipColor";
	defaultValue = "TEAMCOLOR";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_hipColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "LArm";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_lArm";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "LArmColor";
	defaultValue = "TEAMCOLOR";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_lArmColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "LHand";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_lHand";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "LHandColor";
	defaultValue = "1 0.878431 0.611765 1";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_lHandColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "LLeg";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_lLeg";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "LLegColor";
	defaultValue = "TEAMCOLOR";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_lLegColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "Pack";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_pack";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "PackColor";
	defaultValue = "TEAMCOLOR";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_packColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "RArm";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_rArm";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "RArmColor";
	defaultValue = "TEAMCOLOR";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_rArmColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "RHand";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_rHand";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "RHandColor";
	defaultValue = "1 0.878431 0.611765 1";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_rHandColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "RLeg";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_rLeg";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "RLegColor";
	defaultValue = "TEAMCOLOR";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_rLegColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "SecondPack";
	defaultValue = "0";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_secondPack";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "SecondPackColor";
	defaultValue = "TEAMCOLOR";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_secondPackColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_TeamPrefSO : Slayer_DefaultTeamPrefSO)
{
	category = "Uniform";
	title = "TorsoColor";
	defaultValue = "TEAMCOLOR";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "uni_torsoColor";
	type = "string";
	string_maxLength = 100;
	notifyPlayersOnChange = false;
};