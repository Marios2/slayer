// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

new ScriptObject(Slayer_DefaultPrefSO)
{
	//category = "";
	//title = "";
	//defaultValue = "";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	//variable = "";
	//type = "";
	//callback = "";
	//guiTag = "";
	notifyPlayersOnChange = true;
	//requiresMiniGameReset = false;
	//requiresServerRestart = false;
	//doNotSendToClients = false;
	priority = 1000;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Chat";
	title = "Allow Dead Talking";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.chat_deadChatMode";
	type = "list";
	list_items = "0 Disabled" NL "1 Enabled" NL "2 Enabled - Global Only" NL "3 Enabled - Team Only";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Chat";
	title = "Death Message Mode";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.deathMsgMode";
	type = "list";
	list_items = "0 Do Not Display" NL "1 Colored Names" NL "2 Non-Colored Names";
	guiTag = "Advanced";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Chat";
	title = "Enable Team Chat";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Owner"];
	variable = "%mini.chat_enableTeamChat";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Chat";
	title = "Team Display Mode";
	defaultValue = 2;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.chat_teamDisplayMode";
	type = "list";
	list_items = "0 Disabled" NL "1 Color Tag" NL "2 Color Name" NL "3 Add Name to Tag";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Chat";
	title = "Disable All Slayer Messages";
	defaultValue = false;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.disableSlayerMessages";
	type = "bool";
	doNotSendToClients = true;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Damage";
	title = "Bot";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.botDamage";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Damage";
	title = "Brick";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.brickDamage";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Damage";
	title = "Falling";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.fallingDamage";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Damage";
	title = "Self";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.selfDamage";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Damage";
	title = "Vehicle";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.vehicleDamage";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Damage";
	title = "Weapon";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.weaponDamage";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "EoRR";
	title = "Display End of Round Report";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.eorrEnable";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "EoRR";
	title = "Display Team Scores";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.eorrDisplayTeamScores";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "EoRR";
	title = "Display Victory/Defeat";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.eorrDisplayVictory";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Allow Movement During Reset";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.allowMoveWhileResetting";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Auto Start With Server";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Host"];
	variable = "$Pref::Slayer::Server::AutoStartMiniGame";
	type = "bool";
	guiTag = "Advanced";
	notifyPlayersOnChange = false;
	doNotReset = true;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Clear Scores on Reset";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.clearScores";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Clear Stats on Reset";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.clearStats";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Color";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.colorIdx";
	type = "int";
	int_minValue = 0;
	int_maxValue = 10;
	callback = "%mini.updateColor(%1, %2);";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Custom Rule";
	defaultValue = "";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.customRule";
	type = "string";
	string_maxLength = 150;
	guiTag = "Advanced";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Default Minigame";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Admin"];
	variable = "%mini.isDefaultMinigame";
	type = "bool";
	callback = "%mini.updateDefaultMinigame(%1);";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Enable Building";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.enableBuilding";
	type = "bool";
	callback = "%mini.updateEnableBuilding();";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Enable Painting";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.enablePainting";
	type = "bool";
	callback = "%mini.updateEnablePainting();";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Enable Wand";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.enableWand";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Game Mode";
	defaultValue = "Slayer_DefaultGameModeTemplateSG";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.gameModeTemplate";
	type = "object";
	object_class = "ScriptGroup";
	object_cannotBeNull = true;
	callback = "%mini.updateGamemode(%1, %2);";
	requiresMiniGameReset = true;
	priority = 10; //cause this to be sent/received/saved first
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Host Name";
	defaultValue = "HOST";
	permissionLevel = $Slayer::PermissionLevel["Owner"];
	variable = "%mini.hostName";
	type = "string";
	string_maxLength = 25;
	guiTag = "Advanced";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Invite-Only";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["OwnerFullTrust"];
	variable = "%mini.inviteOnly";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Late Join Time";
	defaultValue = -1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.lateJoinTime";
	type = "slide";
	slide_minValue = -1;
	slide_maxValue = 5;
	slide_numTicks = 5;
	slide_snapToTicks = 1;
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Players Use Own Bricks";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.playersUseOwnBricks";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Pre-Round Countdown";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.preRoundSeconds";
	type = "int";
	int_minValue = 0;
	int_maxValue = 10;
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Reset When Empty";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Owner"];
	variable = "%mini.resetOnEmpty";
	type = "bool";
	guiTag = "Advanced";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Spawn Brick Color";
	defaultValue = -1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.spawnBrickColor";
	type = "int";
	int_minValue = -1;
	int_maxValue = 63;
	guiTag = "Advanced";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Time Between Rounds";
	defaultValue = 10;
	permissionLevel = $Slayer::PermissionLevel["OwnerFullTrust"];
	variable = "%mini.timeBetweenRounds";
	type = "int";
	int_minValue = 5;
	int_maxValue = 300;
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Title";
	defaultValue = "Slayer";
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.Title";
	type = "string";
	string_maxLength = 50;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Use All Players' Bricks";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.useAllPlayersBricks";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Minigame";
	title = "Use Spawn Bricks";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.useSpawnBricks";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Spectators";
	title = "Enable Auto-Camera Mode";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.spectateAutoCam";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Spectators";
	title = "Spectate Capture Points";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.spectateCapturePoints";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Permissions";
	title = "Create Minigame Rights";
	defaultValue = 3;
	permissionLevel = $Slayer::PermissionLevel["Host"];
	variable = "$Pref::Slayer::Server::MiniGameCreationRights";
	type = "list";
	list_items = "0 Host" NL "1 Super Admin" NL "2 Admin" NL "3 Everyone";
	guiTag = "Advanced";
	doNotReset = true;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Permissions";
	title = "Edit Rights";
	defaultValue = 3;
	permissionLevel = $Slayer::PermissionLevel["Owner"];
	variable = "%mini.editRights";
	type = "list";
	list_items = "3 Creator" NL "4 Full Trust" NL "5 Build Trust" NL "0 Host" NL "1 Super Admin" NL "2 Admin";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Permissions";
	title = "Leave Rights";
	defaultValue = 2;
	permissionLevel = $Slayer::PermissionLevel["Owner"];
	variable = "%mini.leaveRights";
	type = "list";
	list_items = "3 Creator" NL "4 Full Trust" NL "5 Build Trust" NL "0 Host" NL "1 Super Admin" NL "2 Admin" NL "6 Everyone";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Permissions";
	title = "Reset Rights";
	defaultValue = 3;
	permissionLevel = $Slayer::PermissionLevel["Owner"];
	variable = "%mini.resetRights";
	type = "list";
	list_items = "3 Creator" NL "4 Full Trust" NL "5 Build Trust" NL "0 Host" NL "1 Super Admin" NL "2 Admin";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Player";
	title = "Enable Lights";
	defaultValue = true;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.enablePlayerLights";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Player";
	title = "Enable Suicide";
	defaultValue = true;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.enablePlayerSuicide";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Player";
	title = "Name Distance";
	defaultValue = 140;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.nameDistance";
	type = "slide";
	slide_minValue = 0;
	slide_maxValue = 1000;
	slide_numTicks = 9;
	slide_snapToTicks = false;
	callback = "%mini.updateNameDistance(%1);";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Player";
	title = "Playertype";
	defaultValue = nameToID(playerStandardArmor);
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.playerDatablock";
	type = "object";
	object_class = "PlayerData";
	object_cannotBeNull = true;
	callback = "%mini.updatePlayerDatablock();";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Player";
	title = "Starting Equipment 0";
	defaultValue = nameToID(hammerItem);
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.startEquip0";
	type = "object";
	object_class = "ItemData";
	object_cannotBeNull = false;
	callback = "%mini.updateEquip(0, %1, %2);";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Player";
	title = "Starting Equipment 1";
	defaultValue = nameToID(wrenchItem);
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.startEquip1";
	type = "object";
	object_class = "ItemData";
	object_cannotBeNull = false;
	callback = "%mini.updateEquip(1, %1, %2);";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Player";
	title = "Starting Equipment 2";
	defaultValue = nameToID(printGun);
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.startEquip2";
	type = "object";
	object_class = "ItemData";
	object_cannotBeNull = false;
	callback = "%mini.updateEquip(2, %1, %2);";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Player";
	title = "Starting Equipment 3";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.startEquip3";
	type = "object";
	object_class = "ItemData";
	object_cannotBeNull = false;
	callback = "%mini.updateEquip(3, %1, %2);";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Player";
	title = "Starting Equipment 4";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.startEquip4";
	type = "object";
	object_class = "ItemData";
	object_cannotBeNull = false;
	callback = "%mini.updateEquip(4, %1, %2);";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Points";
	title = "Break Brick";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.points_breakBrick";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Points";
	title = "CP Capture";
	defaultValue = 10;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.points_CP";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
	guiTag = "Rules Teams Points";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Points";
	title = "Die";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.points_die";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Points";
	title = "Friendly Fire";
	defaultValue = -5;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.points_friendlyFire";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
	guiTag = "Rules Teams Points";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Points";
	title = "Kill Bot";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.points_killBot";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Points";
	title = "Kill Player";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.points_killPlayer";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Points";
	title = "Plant Brick";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.points_plantBrick";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Points";
	title = "Suicide";
	defaultValue = -1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.points_killSelf";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Respawn Time";
	title = "Bot";
	defaultValue = 5;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.respawnTime_bot";
	type = "int";
	int_minValue = -1;
	int_maxValue = 999;
	callback = "%mini.updateRespawnTime(\"bot\", %1, %2);";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Respawn Time";
	title = "Brick";
	defaultValue = 30;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.respawnTime_brick";
	type = "int";
	int_minValue = -1;
	int_maxValue = 999;
	callback = "%mini.updateRespawnTime(\"brick\", %1, %2);";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Respawn Time";
	title = "Progressive FF Penalty";
	defaultValue = true;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.friendlyFireProgressivePenalty";
	type = "bool";
	guiTag = "Rules Teams Respawn";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Respawn Time";
	title = "Friendly Fire Penalty";
	defaultValue = 5;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.respawnPenalty_FriendlyFire";
	type = "int";
	int_minValue = 0;
	int_maxValue = 999;
	guiTag = "Rules Teams Respawn";
	callback = "%mini.updateRespawnTime(\"penalty_FF\", %1, %2);";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Respawn Time";
	title = "Player";
	defaultValue = 5;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.respawnTime_player";
	type = "int";
	int_minValue = 1;
	int_maxValue = 999;
	callback = "%mini.updateRespawnTime(\"player\", %1, %2);";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Respawn Time";
	title = "Vehicle";
	defaultValue = 10;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.respawnTime_vehicle";
	type = "int";
	int_minValue = 0;
	int_maxValue = 999;
	callback = "%mini.updateRespawnTime(\"vehicle\", %1, %2);";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Victory Method";
	title = "Lives";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.lives";
	type = "int";
	int_minValue = 0;
	int_maxValue = 99;
	callback = "%mini.updateLives(%1, %2);";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Victory Method";
	title = "Points";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.points";
	type = "int";
	int_minValue = 0;
	int_maxValue = 2000;
	callback = "%mini.updatePoints(%1, %2);";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Victory Method";
	title = "Time";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.time";
	type = "int";
	int_minValue = 0;
	int_maxValue = 999999;
	callback = "%mini.updateTime(%1, %2);";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Server";
	title = "Announcements";
	defaultValue = "";
	permissionLevel = $Slayer::PermissionLevel["Owner"];
	variable = "%mini.announcements";
	type = "string";
	string_maxLength = 500;
	guiTag = "Advanced";
	notifyPlayersOnChange = false;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Server";
	title = "Save Settings on Update";
	defaultValue = false;
	permissionLevel = $Slayer::PermissionLevel["Host"];
	variable = "$Pref::Slayer::Server::SavePrefsOnUpdate";
	type = "bool";
	guiTag = "Advanced";
	notifyPlayersOnChange = false;
	doNotReset = true;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Ally Same Colors";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.teams_allySameColors";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Auto Sort";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.teams_autoSort";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Balance New Teams";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Host"];
	variable = "%mini.teams_balanceOnNew";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Balance Teams";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.teams_balanceTeams";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Display Join/Leave Messages";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.teams_notifyMemberChanges";
	type = "bool";
	guiTag = "Advanced";
	notifyPlayersOnChange = 0;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Friendly Fire";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.teams_friendlyFire";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Lock Teams";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.teams_lock";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Punish Friendly Fire";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Host"];
	variable = "%mini.teams_punishFF";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Display Friendly Fire Warning";
	defaultValue = true;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.friendlyFireDisplayWarning";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Rounds Between Shuffle";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.teams_roundsBetweenShuffles";
	type = "int";
	int_minValue = 1;
	int_maxValue = 999;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Shuffle Teams";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.teams_shuffleTeams";
	type = "bool";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Shuffle Mode";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.teams_shuffleMode";
	type = "list";
	list_items = "0 Random" NL "1 New Team Every Time";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Swap/Join Mode";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.teams_swapMode";
	type = "list";
	list_items = "0 Regulated" NL "1 Unregulated (always join)";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Team-Only DeadCam";
	defaultValue = 0;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.teams_teamOnlyDeadCam";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Teams";
	title = "Max Teams";
	defaultValue = 100;
	variable = "$Pref::Server::Slayer::Teams::maxTeams";
	type = "int";
	int_minValue = 0;
	int_maxValue = 1024;
	doNotSendToClients = true;
	doNotReset = true;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Capture Points";
	title = "Tick Time";
	defaultValue = 150;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "$Pref::Slayer::Server::CPTriggerTickMS";
	type = "int";
	int_minValue = 20;
	int_maxValue = 500;
	callback = "Slayer_CPTriggerData.tickPeriodMS = %1;";
	guiTag = "Advanced";
	notifyPlayersOnChange = true;
	doNotReset = true;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Capture Points";
	title = "Use Transitional Colors";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.CPTransitionColors";
	type = "bool";
	guiTag = "Advanced";
	notifyPlayersOnChange = true;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Events";
	title = "Max Team Events";
	defaultValue = 6;
	permissionLevel = $Slayer::PermissionLevel["Host"];
	variable = "$Pref::Slayer::Server::Teams::maxEvents";
	type = "int";
	int_minValue = 0;
	int_maxValue = 64;
	guiTag = "Advanced";
	notifyPlayersOnChange = false;
	requiresServerRestart = true;
	doNotReset = true;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Events";
	title = "Restrict Output Events";
	defaultValue = 1;
	permissionLevel = $Slayer::PermissionLevel["Host"];
	variable = "%mini.restrictOutputEvents";
	type = "bool";
	guiTag = "Advanced";
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Bricks";
	title = "Create Team Bot Holes";
	defaultValue = true;
	variable = "$Pref::Slayer::Server::CreateTeamBotHoles";
	type = "bool";
	doNotSendToClients = true;
	doNotReset = true;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Bricks";
	title = "Place Bricks in \"Slayer\" Tab";
	defaultValue = true;
	variable = "$Pref::Slayer::Server::CreateBrickUISlayerTab";
	type = "bool";
	doNotSendToClients = true;
	doNotReset = true;
};

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Server";
	title = "Old TDM Mod Compatible";
	defaultValue = false;
	permissionLevel = $Slayer::PermissionLevel["Host"];
	variable = "$Pref::Slayer::Server::LoadTDMCompatibility";
	type = "bool";
	guiTag = "Advanced";
	notifyPlayersOnChange = false;
	requiresServerRestart = true;
	doNotReset = true;
};

// new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
// {
	// category = "Bots";
	// title = "Complete Objectives En Route";
	// defaultValue = true;
	// permissionLevel = $Slayer::PermissionLevel["Any"];
	// variable = "%mini.bots_objectivesEnRoute";
	// type = "bool";
	// guiTag = "Advanced";
	// notifyPlayersOnChange = false;
// };

// new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
// {
	// category = "Bots";
	// title = "Automatic Set Type";
	// defaultValue = true;
	// permissionLevel = $Slayer::PermissionLevel["Any"];
	// variable = "%mini.bots_autoSetType";
	// type = "bool";
	// guiTag = "Advanced";
	// notifyPlayersOnChange = false;
// };

// new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
// {
	// category = "Bots";
	// title = "Preferred Player Count";
	// defaultValue = 0;
	// permissionLevel = $Slayer::PermissionLevel["Admin"];
	// variable = "%mini.botFillLimit";
	// type = "int";
	// int_minValue = 0;
	// int_maxValue = 99;
	// guiTag = "Rules ALL Mode";
// };