// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

if($AddOn__Gamemode_Zombie != 1)
	return;

$Slayer::Server::Compatibility::Gamemode_Zombie = 1;

//Gamemode_Zombie is so badly written that I'm not even going to try making it work with Slayer.
//I'll just make sure it doesn't break stuff.

// +--------------------+
// | Packaged Functions |
// +--------------------+
package Slayer_Compatibility_Gamemode_Zombie
{
	function Slayer_MinigameSO::onAdd(%this)
	{
		parent::onAdd(%this);

		%this.zombieFriendlyFire = 1;
		%this.playersCanRespawn = 0; //for some reason this is the inverse of what it should be
		%this.enableZombies = 0;
	}

	//allow players to respawn
	function Observer::onTrigger(%this, %cam, %btn, %state)
	{
		%client = %cam.getControllingClient();
		%mini = %client.minigame;

		if(!isObject(%client) || !isObject(%cam))
			return parent::onTrigger(%this, %cam, %btn, %state);

		if(!isSlayerMinigame(%mini))
			return parent::onTrigger(%this, %cam, %btn, %state);

		%mini.playersCanRespawn = 0;
		%client.noRespawn = 0;

		parent::onTrigger(%this, %cam, %btn, %state);
	}

	//prevent this thing from Resetting the minigame
	function zIsEveryOneDead(%mini)
	{
		if(!isSlayerMinigame(%mini))
			parent::zIsEveryOneDead(%mini);
	}
};
activatePackage(Slayer_Compatibility_Gamemode_Zombie);

Slayer_Support::Debug(2, "Compatibility File Loaded", "Gamemode_Zombie");