// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

if(!$Pref::Slayer::Server::LoadTDMCompatibility)
	return;

package Slayer_Compatibility_Gamemode_TeamDeathmatch
{
	function Slayer_MinigameSO::onAdd(%this)
	{
		%parent = parent::onAdd(%this);
		%this.tdmGeneralRule_DEADTALK = 1;
		return %parent;
	}

	function Slayer_TeamSO::onAdd(%this)
	{
		%parent = parent::onAdd(%this);
		%mini = %this.minigame;
		for(%i=0; %i < %mini.numMembers; %i++)
		{
			%cl = %mini.member[%i];
			%team = %cl.slyrTeam;
			if(isObject(%team) && isObject(%team.getGroup()))
				%cl.tdmTeam = %team.getGroup().indexOf(%team);
			else
				%cl.tdmTeam = -1;
		}
		return %parent;
	}

	function Slayer_TeamSO::onRemove(%this)
	{
		%mini = %this.minigame;
		if(isObject(%this.getGroup()))
			%mini.teamCol[%this.getGroup().indexOf(%team)] = "";
		%parent = parent::onRemove(%this);
		for(%i=0; %i < %mini.numMembers; %i++)
		{
			%cl = %mini.member[%i];
			%team = %cl.slyrTeam;
			if(isObject(%team) && isObject(%team.getGroup()))
				%cl.tdmTeam = %team.getGroup().indexOf(%team);
			else
				%cl.tdmTeam = -1;
		}
		return %parent;
	}

	function Slayer_TeamSO::addMember(%this, %client, %reason, %doNotRespawn)
	{
		%parent = parent::addMember(%this, %client, %reason, %doNotRespawn);
		if(%parent && isObject(%this.getGroup()))
			%client.tdmTeam = %this.getGroup().indexOf(%team);
		return %parent;
	}

	function Slayer_TeamSO::removeMember(%this, %client, %respawn)
	{
		%parent = parent::removeMember(%this, %client, %respawn);
		if(%parent)
			%client.tdmTeam = -1;
		return %parent;
	}

	function Slayer_TeamSO::updateColor(%this, %color)
	{
		%parent = parent::updateColor(%this, %color);
		%mini = %this.minigame;
		if(isObject(%this.getGroup()))
		%mini.teamCol[%this.getGroup().indexOf(%team)] = %color;
		return %parent;
	}
};
activatePackage(Slayer_Compatibility_Gamemode_TeamDeathmatch);