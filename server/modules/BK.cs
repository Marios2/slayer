// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Bonus Kills";
	title = "Enable";
	defaultValue = true;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.bKills_enable";
	type = "bool";
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Bonus Kills";
	title = "Kill Spree Start";
	defaultValue = 4;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.bKills_kSpree";
	type = "int";
	int_minValue = 2;
	int_maxValue = 30;
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Bonus Kills";
	title = "Bonus Points per Kill Spree";
	defaultValue = 4;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.bKills_pointsKillSpree";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
	guiTag = "advanced";
};
new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
{
	category = "Bonus Kills";
	title = "Bonus Points per Multi-Kill";
	defaultValue = 4;
	permissionLevel = $Slayer::PermissionLevel["Any"];
	variable = "%mini.bKills_pointsMultiKill";
	type = "int";
	int_minValue = -999;
	int_maxValue = 999;
	guiTag = "advanced";
};

$Slayer::Server::BK::KillSpree[0] = "\c3(Killing Spree | %1)";
$Slayer::Server::BK::KillSpree[100] = "\c3(Almost as good as Greek2me! | %1)";
$Slayer::Server::BK::KillSpree[200] = "\c3(Seriously, what the hell? | %1)";

$Slayer::Server::BK::QuickKillTimeOut = 1000;
$Slayer::Server::BK::QuickKill[0] = "\c3(Multi-Kill)";
$Slayer::Server::BK::QuickKill[2] = "\c3(Double-Kill)";
$Slayer::Server::BK::QuickKill[3] = "\c3(Triple-Kill)";
$Slayer::Server::BK::QuickKill[4] = "\c3(Quadruple-Kill)";
$Slayer::Server::BK::QuickKill[5] = "\c3(Quintuple-Kill)";
$Slayer::Server::BK::QuickKill[6] = "\c3(Sextuple-Kill)";
$Slayer::Server::BK::QuickKill[7] = "\c3(Septuple-Kill)";
$Slayer::Server::BK::QuickKill[8] = "\c3(Octuple-Kill)";
$Slayer::Server::BK::QuickKill[9] = "\c3(Nonuple-Kill)";
$Slayer::Server::BK::QuickKill[10] = "\c3(Decuple-Kill)";

if(isFile($Slayer::Server::ConfigDir @ "/bonusKills.cs"))
	exec($Slayer::Server::ConfigDir @ "/bonusKills.cs");
export("$Slayer::Server::BK::KillSpree*", $Slayer::Server::ConfigDir @ "/bonusKills.cs", false);
export("$Slayer::Server::BK::QuickKill*", $Slayer::Server::ConfigDir @ "/bonusKills.cs", true);

function isSpecialKill_Slayer_BK(%client, %sourceObject, %killer, %mini)
{
	//bonus kills
	if(%mini.bKills_enable)
	{
		if(%killer != %client && isObject(%killer))
		{
			if(%killer.slyrTeam != %client.slyrTeam || !isObject(%killer.slyrTeam) || !isObject(%client.slyrTeam))
			{
				%killerName = %killer.getPlayerName();
				%clientName = %client.getPlayerName();

				%player = %killer.player;

				//KILL SPREES
				%player.killSpree ++;
				if(%player.killSpree >= %mini.bKills_kSpree)
				{
					%kills = ($Slayer::Server::BK::KillSpree[%player.killSpree] $= "" ? 0 : %player.killSpree);
					%msg = strReplace($Slayer::Server::BK::KillSpree[%kills], "%1", %player.killSpree);
					%msg = strReplace(%msg, "%2", %killerName);
					%msg = strReplace(%msg, "%3", %clientName);

					//bonus points
					%killer.incScore(%mini.bKills_pointsKillSpree);
				}

				//QUICK KILLS
				%time = getSimTime();
				if(%time - %player.lastKillTime <= $Slayer::Server::BK::QuickKillTimeOut || !%player.lastKillTime)
					%player.quickKills ++;
				else
					%player.quickKills = 0;

				%player.lastKillTime = %time;

				if(%player.quickKills > 1)
				{
					%kills = ($Slayer::Server::BK::QuickKill[%player.quickKills] $= "" ? 0 : %player.quickKills);
					%qk = strReplace($Slayer::Server::BK::QuickKill[%kills], "%1", %player.quickKills);
					%qk = strReplace(%qk, "%2", %killerName);
					%qk = strReplace(%qk, "%3", %clientName);

					if(%msg $= "")
						%msg = %qk;
					else
						%msg = %msg SPC %qk;

					//bonus points
					%killer.incScore(%mini.bKills_pointsMultiKill);
				}
			}
		}
	}

	if(%msg $= "")
		return 0;
	else
		return 2 TAB %msg;
}
addSpecialDamageMsg("Slayer_BK", "%2%3%1 %4", "%3%1");