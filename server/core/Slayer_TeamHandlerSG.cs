// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_TeamHandlerSG::onAdd(%this)
{
	//team preferences
	%this.Prefs = Slayer.TeamPrefs;
}

//Creates a new team.
//@return	Slayer_TeamSO
function Slayer_TeamHandlerSG::addTeam(%this)
{
	if(%this.getCount() >= $Pref::Server::Slayer::Teams::maxTeams)
	{
		Slayer_Support::Error("Slayer_TeamHandlerSG::addTeam", "Team limit reached!");
		return;
	}

	%team = new ScriptObject()
	{
		class = Slayer_TeamSO;
		minigame = %this.minigame;
		numMembers = 0;
		score = 0;
		wins = 0;
	};
	%this.add(%team);

	Slayer.TeamPrefs.resetPreferences(%team);
	%this.minigame.gameMode.callback("onTeamAdd", %this, %team);
	return %team;
}

//Removes the specified team.
//@param	Slayer_TeamSO team
function Slayer_TeamHandlerSG::removeTeam(%this, %team)
{
	if(!isObject(%team))
		return;
	if(!%this.isMember(%team))
		return;

	%this.minigame.gameMode.callback("onTeamRemove", %this, %team);

	//We will delete all bots in Slayer_TeamSO::onRemove
	%team.removeAllMembers("GameConnection");

	%team.delete();
}

//Called when the minigame is started.
function Slayer_TeamHandlerSG::onMinigameStart(%this)
{
//	for(%i = 0; %i < %this.getCount(); %i ++)
//		%this.getObject(%i).botFillTeam();

	%this.shuffleTeams(%this.minigame.teams_shuffleMode, 1);
}

//Called when the round ends.
function Slayer_TeamHandlerSG::onMinigameRoundEnd(%this)
{
	//team shuffling
	if(%this.minigame.teams_shuffleTeams)
		%this.shuffleCount ++;
	else
		%this.shuffleCount = 0;
}

//Called when the minigame is reset.
function Slayer_TeamHandlerSG::onMinigameReset(%this)
{
	if(%this.minigame.teams_shuffleTeams)
	{
		if(%this.shuffleCount >= %this.minigame.teams_roundsBetweenShuffles)
		{
			%this.shuffleTeams(%this.minigame.teams_shuffleMode, 1);
			%this.shuffleCount = 0;
		}
	}

	for(%i = %this.getCount() - 1; %i >= 0; %i --)
		%this.getObject(%i).onMinigameReset();
}

//Called after a new team has been created using the Slayer GUI.
//@param	Slayer_TeamSO team
function Slayer_TeamHandlerSG::onNewTeam(%this, %team)
{
	%mini = %this.minigame;

	if(%mini.teams_balanceOnNew)
	{
		for(%i = 0; %i < %mini.numMembers["GameConnection"]; %i ++)
		{
			%cl = %mini.member["GameConnection", %i];
			%t = %cl.slyrTeam;
			if(!isObject(%t))
				%this.autoSort(%cl);
		}

		while(!%team.isTeamBalanced())
		{
			%team.balanceTeam(1); //forced team balance
		}
	}
}

//Removes and deletes all bots from teams.
function Slayer_TeamHandlerSG::deleteAllBots(%this)
{
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
		%this.getObject(%i).deleteAllBots();
}

//Removes all members from all teams.
//@param	string class	Only remove members of this class.
function Slayer_TeamHandlerSG::removeAllMembers(%this, %class)
{
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
		%this.getObject(%i).removeAllMembers(%class);
}

//Removes all teams.
function Slayer_TeamHandlerSG::removeAllTeams(%this)
{
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
		%this.removeTeam(%this.getObject(%i));
}

//Messages all teams of the selected color.
//@param	int color
//@param	string command
//@param	string msg
function Slayer_TeamHandlerSG::messageAllByColor(%this, %color, %cmd, %msg, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p)
{
	for(%idx = %this.getCount() - 1; %idx >= 0; %idx --)
	{
		%t = %this.getObject(%idx);
		if(%t.color == %color)
			%t.messageAll(%cmd, %msg, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p);
	}
}

//Messages the dead members of all teams of the selected color.
//@param	int color
//@param	string command
//@param	string msg
function Slayer_TeamHandlerSG::messageAllDeadByColor(%this, %color, %cmd, %msg, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p)
{
	for(%idx = %this.getCount() - 1; %idx >= 0; %idx --)
	{
		%t = %this.getObject(%idx);
		if(%t.color == %color)
			%t.messageAllDead(%cmd, %msg, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p);
	}
}

//Shuffles players between teams. This has two modes and is used between rounds to mix up teams.
//@param	int shuffleMode	0, players have chance of not changing to new team. 1, new team is guaranteed
//@param	bool doNotRespawn	When true, players will not be respawned.
function Slayer_TeamHandlerSG::shuffleTeams(%this, %shuffleMode, %doNotRespawn)
{
	%mini = %this.minigame;

	if(%this.getShuffleTeamCount() < 2)
		return;
	else if(%shuffleMode == 1 && %this.getAutoSortTeamCount() == 0)
	{
		Slayer_Support::Error("Slayer_TeamHandlerSG::shuffleTeams", "Teams full, fall back to method 0.");
		%shuffleMode = 0;
	}

	for(%i = %mini.numMembers["GameConnection"] - 1; %i >= 0; %i --)
	{
		%cl = %mini.member["GameConnection", %i];
		if(!isObject(%cl.slyrTeam))
			%noTeam[%cl] = true;
	}

	//disable team join messages
	%oldNotify = %mini.teams_notifyMemberChanges;
	%mini.teams_notifyMemberChanges = false;

	//set the flag
	%this.shufflingTeams = 1;

	if(%shuffleMode == 0) //remove everybody from their teams and then just autosort
	{
		for(%i = %this.getCount() - 1; %i >= 0; %i --)
		{
			%t = %this.getObject(%i);
			if(%t.sort && %t.sortWeight > 0 && %t.maxPlayers != 0)
				%t.removeAllMembers("GameConnection");
		}
		for(%i = %mini.numMembers["GameConnection"] - 1; %i >= 0; %i --)
		{
			%cl = %mini.member["GameConnection", %i];
			if(!isObject(%cl.slyrTeam) && !%noTeam[%cl])
			{
				%team = %this.pickTeam(%cl);
				if(isObject(%team))
					%team.addMember(%cl, "Team Shuffle", %doNotRespawn);
			}
		}
	}
	else if(%shuffleMode == 1) //autosort without removing people from teams
	{
		for(%i = %mini.numMembers["GameConnection"] - 1; %i >= 0; %i --)
		{
			%cl = %mini.member[%i];
			if(!%noTeam[%cl])
			{
				%team = %this.pickTeam(%cl, 1);
				if(isObject(%team))
					%team.addMember(%cl, "Team Shuffle", %doNotRespawn);
			}
		}
	}

	%this.balanceTeams(0);

	%this.shufflingTeams = 0;

	%mini.teams_notifyMemberChanges = %oldNotify;

	%mini.gameMode.callback("onTeamShuffle", %this, %shuffleMode, %doNotRespawn);
}

//Evenly distributes members across all teams, factoring in auto-sort weightings.
//@param	bool force	When true, all members will be immediately distributed. Otherwise, members will be balanced on death.
//@see	Slayer_TeamSO::balanceTeam
function Slayer_TeamHandlerSG::balanceTeams(%this, %force)
{
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%team = %this.getObject(%i);
		if(%team.sort && %team.sortWeight > 0)
		{
			while(!%team.isTeamBalanced())
			{
				%team.balanceTeam(%force);
				if(!%force)
					break;
			}
		}
	}
}

//Switches two clients between their teams.
//@param	GameConnection clientA
//@param	GameConnection clientB
//@see	Slayer_TeamHandlerSG::onTeamMemberSwap
function Slayer_TeamHandlerSG::swapTeamMembers(%this, %clientA, %clientB)
{
	%resetting = %this.minigame.isResetting();

	%teamA = %clientA.getTeam();
	%teamB = %clientB.getTeam();
	if(!isObject(%teamA) || !isObject(%teamB))
		return;

	if(%teamA.swapPending == %teamB)
	{
		cancel(%teamA.swapTime);
		%teamB.swapOffer = "";
		%teamA.swapPending = "";
		%teamA.swapClient = "";
	}
	else if(%teamB.swapPending == %teamA)
	{
		cancel(%teamB.swapTime);
		%teamA.swapOffer = "";
		%teamB.swapPending = "";
		%teamB.swapClient = "";
	}

	%teamA.addMember(%clientB, "Team Swap", %resetting);
	%teamB.addMember(%clientA, "Team Swap", %resetting);

	%this.onTeamMemberSwap(%this, %clientA, %clientB);
}

//Called when two players swap teams.
//@param	GameConnection clientA
//@param	GameConnection clientB
function Slayer_TeamHandlerSG::onTeamMemberSwap(%this, %clientA, %clientB)
{
	%this.minigame.gameMode.callback("onTeamSwap", %this, %clientA, %clientB);
}

//Finds the number of teams that are available for auto-sorting.
//@return	int	The number of available teams.
function Slayer_TeamHandlerSG::getAutosortTeamCount(%this)
{
	%count = 0;
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%t = %this.getObject(%i);
		if((%t.maxPlayers <= 0 || %t.numMembers["GameConnection"] < %t.maxPlayers) && %t.sort && %t.sortWeight > 0)
			%count ++;
	}
	return %count;
}

//Finds the number of teams that are available for team shuffling.
//@return	int	The number of available teams.
function Slayer_TeamHandlerSG::getShuffleTeamCount(%this)
{
	%count = 0;
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%t = %this.getObject(%i);
		if(%t.sort && %t.sortWeight > 0 && %t.maxPlayers != 0)
			%count ++;
	}
	return %count;
}

//Selects the best candidate team for a client to join.
//@param	GameConnection client	The client to select a team for.
//@param	bool skipCurrentTeam	If the client already has a team, don't consider it.
//@return	int	0, no suitable team was found; objID, the object ID of the team that was selected.
//@see	Slayer_TeamHandlerSG::autoSort
function Slayer_TeamHandlerSG::pickTeam(%this, %client, %skipCurrentTeam)
{
	%mini = %this.minigame;

	if(!isObject(%client))
		return 0;

	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%t = %this.getObject(%i);
//		if(%skipCurrentTeam && %t == %client.slyrTeam)
//			continue;
		if(!%t.sort || %t.sortWeight <= 0 || (%t.numMembers["GameConnection"] >= %t.maxPlayers && %t.maxPlayers >= 0))
			continue;

		%sumWeights += %t.sortWeight;
	}

	%sortRatio = %mini.numMembers["GameConnection"] / %sumWeights;

	%greatest = -1;
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%t = %this.getObject(%i);
		if(%skipCurrentTeam && %t == %client.slyrTeam)
			continue;
		if(!%t.sort || %t.sortWeight <= 0 || (%t.numMembers["GameConnection"] >= %t.maxPlayers && %t.maxPlayers >= 0))
			continue;

		%weight = (%t.sortWeight * %sortRatio) - %t.numMembers["GameConnection"];

		if(%weight > %greatest)
		{
			%greatest = %weight;
			%teams = %t;
		}
		else if(%weight >= %greatest)
		{
			%teams = setField(%teams, getFieldCount(%teams), %t);
		}
	}

	if(getFieldCount(%teams) <= 0)
		return 0;

	%r = getRandom(0, getFieldCount(%teams)-1);
	if(%r < 0)
		%r = 0;
	%team = getField(%teams, %r);

	if(!isObject(%team))
		return 0;

	if(%client.slyrTeam == %team)
		return 0;

	return %team;
}

//Selects a team for the client and causes them to join it automatically.
//@param	GameConnection client
//@param	bool doNotRespawn	If true, the client will not respawn during the process.
//@return	Slayer_TeamSO	The team that was selected.
//@see	Slayer_TeamHandlerSG::pickTeam
function Slayer_TeamHandlerSG::autoSort(%this, %client, %doNotRespawn)
{
	%team = %this.pickTeam(%client);
	if(!isObject(%team))
		return;
	%team.addMember(%client, "", %doNotRespawn);
	return %team;
}

//Determines whether two teams can damage each other.
//@param	Slayer_TeamSO teamA
//@param	Slayer_TeamSO teamB
//@return	bool
//@see minigameCanSlayerDamage
function Slayer_TeamHandlerSG::canDamage(%this, %teamA, %teamB)
{
	if(%teamA.spectate || %teamB.spectate)
		return false;
	if(!isObject(%teamA) || !isObject(%teamB))
		return false;
	if(!%this.minigame.teams_friendlyFire && %teamA.isAlliedTeam(%teamB))
		return false;
	return true;
}

//Auto-sorts all members of the minigame into teams.
//@param	bool doNotRespawn
//@see	Slayer_TeamHandlerSG::autoSort
function Slayer_TeamHandlerSG::autoSortAll(%this, %doNotRespawn)
{
	%mini = %this.minigame;
	for(%i = %mini.numMembers["GameConnection"] - 1; %i >= 0; %i --)
		%this.autoSort(%mini.member["GameConnection", %i], %doNotRespawn);
}

//@param	string name	The name of a team.
//@return	Slayer_TeamSO
//@see	Slayer_TeamHandlerSG::getTeamsFromColor
function Slayer_TeamHandlerSG::getTeamFromName(%this, %name)
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%t = %this.getObject(%i);
		if(%t.name $= %name)
			return %t;
	}

	return 0;
}

//Returns a list of teams that match a specified color.
//@param	int color	The colorID of team to search for.
//@param	string mode	The type of list to return, tab ("TAB") or comma ("COM") delimited.
//@param	bool returnColor	Whether TML color tags should be included in the returned string.
//@param	bool inverse	Return teams that do not match the color.
//@param	string exclude	A tab-delimited list of teams to exlude from the list.
//@return	string	A tab or comma delimited list, according to the mode paramater.
//@see	Slayer_TeamHandlerSG::getTeamFromName
function Slayer_TeamHandlerSG::getTeamsFromColor(%this, %color, %mode, %returnColor, %inverse, %exclude)
{
	if(%mode $= "")
		%mode = "TAB";

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%t = %this.getObject(%i);

		if((%t.color != %color && !%inverse) || (%t.color == %color && %inverse))
			continue;

		if(strPos(%exclude, %t) >= 0)
			continue;

		if(%mode $= "TAB")
		{
			if(%teams $= "")
				%teams = %t;
			else
				%teams = %teams TAB %t;
		}
		else
		{
			if(%returnColor)
			{
				if(%teams $= "")
					%teams = "<spush>" @ %t.getColoredName() @ "<spop>";
				else
					%teams = %teams @ ", " SPC "<spush>" @ %t.getColoredName() @ "<spop>";
			}
			else
			{
				if(%teams $= "")
					%teams = %t.name;
				else
					%teams = %teams @ ", " SPC %t.name;
			}
		}
	}

	return %teams;
}

//Generates a list of teams, sorted from greatest to least by score.
//@return	string	Tab-delimited list of teams.
function Slayer_TeamHandlerSG::getTeamListSortedScore(%this)
{
	%count = %this.getCount();
	for(%i = 0; %i < %count; %i ++)
		%list = setField(%list, getFieldCount(%list), %this.getObject(%i));
	while(!%done)
	{
		%done = 1;
		for(%i = 0; %i < %count; %i ++)
		{
			%e = %i + 1;
			if(%e >= %count)
				continue;
			%f1 = getField(%list, %i);
			%f2 = getField(%list, %e);
			if(%f1.getScore() < %f2.getScore())
			{
				%list = Slayer_Support::swapItemsInList(%list, %i, %e);
				%done = 0;
			}
		}
		%count --;
	}

	return %list;
}

//Determines if a color is used by any teams.
//@param	int color	The colorID to check.
//@return	bool
function Slayer_TeamHandlerSG::isNeutralColor(%this, %color)
{
	for(%i = 0; %i < %this.getCount(); %i ++)
	{
		if(%this.getObject(%i).color == %color)
			return 0;
	}

	return 1;
}

//Sends team data to clients using the GUI.
//@param	GameConnection client
//@see	Slayer::sendData
function Slayer_TeamHandlerSG::sendTeams(%this, %client)
{
	%mini = %this.minigame;
	Slayer_Support::Debug(2, "Sending Teams", %client.getSimpleName());

	//START THE TEAM TRANSFER
	commandToClient(%client, 'Slayer_getTeams_Start', %this.getCount());

	%count = %this.getCount();
	for(%i = 0; %i < %count; %i ++)
	{
		%team = %this.getObject(%i);
		if(%team.doNotSendToClients)
			continue;
		transferObjectToClient(%client, %team, false, "SlayerClient_TeamSO");
	}

	//END THE TEAM TRANSFER
	commandToClient(%client, 'Slayer_getTeams_End');
}

//Displays team information in the console.
function Slayer_TeamHandlerSG::dumpTeams(%this)
{
	%count = %this.getCount();
	for(%i = 0; %i < %count; %i ++)
	{
		%team = %this.getObject(%i);
		echo("\c5Team" SPC %team.name @ ":" NL
			"   Members:" SPC %team.numMembers NL
			"    \c3Humans:" SPC %team.numMembers["GameConnection"] NL
			"    \c3Bots:  " SPC %team.numMembers["AiController"] NL
			"   Score:  " SPC %team.getScore());
	}
}

function slayerTeamCmdAddMember(%client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(isSlayerMinigame(%client.editingMinigame))
		%mini = %client.editingMinigame;
	if(!%mini.canEdit(%client))
		return;
	if(%a $= "")
	{
		messageClient(%client, '', "\c5Incorrect usage. Please enter a player name, then a team name.");
		return;
	}
	if(isObject(%a) && %a.getClassName() $= "GameConnection")
		%victim = %a;
	else
		%victim = findClientByName(%a);
	if(!isObject(%victim))
	{
		messageClient(%client, '', "\c5Couldn't find player:\c3" SPC %a);
		return;
	}
	if(%victim.minigame != %mini)
	{
		messageClient(%client, '', "\c3" @ %victim.getPlayerName() SPC "\c5is not in the\c3" SPC %mini.Title SPC "\c5minigame.");
		return;
	}
	%name = trim(%b SPC %c SPC %d SPC %e SPC %f SPC %g SPC %h SPC %i SPC %j SPC %k SPC %l SPC %m);
	%team = %mini.Teams.getTeamFromName(%name);
	if(!isObject(%team))
	{
		messageClient(%client, '', "\c5Couldn't find team:\c3" SPC %name);
		return;
	}
	%tb = %mini.teams_balanceTeams;
	%mini.teams_balanceTeams = 0;
	%team.addMember(%victim);
	%mini.teams_balanceTeams = %tb;
}

function slayerTeamCmdRemoveMember(%client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(isSlayerMinigame(%client.editingMinigame))
		%mini = %client.editingMinigame;
	if(!%mini.canEdit(%client))
		return;
	if(%a $= "")
	{
		messageClient(%client, '', "\c5Incorrect usage. Please enter a player name, then a team name.");
		return;
	}
	if(isObject(%a) && %a.getClassName() $= "GameConnection")
		%victim = %a;
	else
		%victim = findClientByName(%a);
	if(!isObject(%victim))
	{
		messageClient(%client, '', "\c5Couldn't find player:\c3" SPC %a);
		return;
	}
	if(%victim.minigame != %mini)
	{
		messageClient(%client, '', "\c3" @ %victim.getPlayerName() SPC "\c5is not in the\c3" SPC %mini.Title SPC "\c5minigame.");
		return;
	}
	%name = trim(%b SPC %c SPC %d SPC %e SPC %f SPC %g SPC %h SPC %i SPC %j SPC %k SPC %l SPC %m);
	%team = %mini.Teams.getTeamFromName(%name);
	if(!isObject(%team))
	{
		messageClient(%client, '', "\c5Couldn't find team:\c3" SPC %name);
		return;
	}
	%team.removeMember(%victim, 1);
}

function slayerTeamCmdBalance(%client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(!%mini.canEdit(%client))
		return;
	%mini.Teams.balanceTeams(1);
	%mini.messageAll('', "\c3" @ %client.getPlayerName() SPC "\c5balanced \c3all \c5teams.");
}

function slayerTeamCmdJoin(%client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(%mini.teams_lock && isObject(%clTeam))
	{
		messageClient(%client, '', "\c5Teams are locked, you cannot change teams.");
		return;
	}
	%name = trim(%a SPC %b SPC %c SPC %d SPC %e SPC %f SPC %g SPC %h SPC %i SPC %j SPC %k SPC %l);
	if(%name $= "")
	{
		messageClient(%client, '', "\c5Please enter a team to join. Type \c3/joinTeam Team Name \c5 to join a team. Type \c3/listTeams \c5to view a list of teams.");
		return;
	}
	%team = %mini.Teams.getTeamFromName(%name);
	if(!isObject(%team))
	{
		messageClient(%client, '', "\c5Couldn't find team:\c3" SPC %name);
		return;
	}
	if(%clTeam.lock || %team.lock)
	{
		messageClient(%client, '', "\c5Team is locked.");
		return;
	}
	if(%clTeam == %team)
	{
		messageClient(%client, '', "\c5You're already on" SPC %team.getColoredName() @ "\c5.");
		return;
	}
	if(isObject(%clTeam.swapPending))
	{
		messageClient(%client, '', "\c5Someone on your team is already trying to swap teams.");
		return;
	}
	if(isObject(%team.swapOffer))
	{
		messageClient(%client, '', "\c5Someone is already trying to swap to that team.");
		return;
	}
	if(%team.maxPlayers == 0)
	{
		messageClient(%client, '', "\c5That team is not accepting any new members.");
		return;
	}
	if(isObject(%clTeam))
	{
		if(%team.swapPending == %clTeam)
		{
			%mini.Teams.swapTeamMembers(%client, %team.swapClient);
			return;
		}
		%teamMembers = %team.numMembers["GameConnection"];
		%clTeamMembers = %clTeam.numMembers["GameConnection"];
		if(%mini.teams_swapMode == 1 || (%clTeamMembers > %teamMembers && %team.maxPlayers == -1) || (%clTeamMembers > %teamMembers && %teamMembers < %team.maxPlayers && %team.maxPlayers != -1))
			%team.addMember(%client, "Team Swap", %mini.isResetting());
		else
		{
			%team.messageAll('', %clTeam.getColorHex() @ %client.name SPC "\c5wants to swap with someone on" SPC %team.getColorHex() @ %team.name @ "\c5. Type \c3/acceptSwap \c5to swap teams.");
			%team.swapOffer = %clTeam;
			messageClient(%client, '', "\c5The members of" SPC %team.getColorHex() @ %team.name SPC "\c5have been asked if they want to swap with you.");
			%clTeam.swapPending = %team;
			%clTeam.swapClient = %client;
			%clTeam.swapTime = schedule(20000, 0, serverCmdTeams, %client, "cancelSwap");
		}	
	}
	else if(%team.maxPlayers < 0 || %team.numMembers["GameConnection"] < %team.maxPlayers)
		%team.addMember(%client, "", %mini.isResetting());
	else
		messageClient(%client, '', "\c5That team is not accepting any new members.");
}

function slayerTeamCmdLeave(%client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(!isObject(%clTeam))
	{
		messageClient(%client, '', "\c5You aren't on a team.");
		return;
	}
	if((%mini.teams_lock || %mini.teams_autoSort) && !%mini.canEdit(%client))
	{
		messageClient(%client, '', "\c5You cannot leave your team.");
		return;
	}
	messageClient(%client, '', "\c5You have left" SPC %clTeam.getColoredName());
	if(%mini.teams_notifyMemberChanges)
		%mini.messageAllExcept(%client, '', "\c3" @ %client.getPlayerName() SPC "\c5has left" SPC %clTeam.getColoredName());
	%clTeam.removeMember(%client, 1);
}

function slayerTeamCmdAcceptSwap(%client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(!isObject(%clTeam))
		return;
	if(!isObject(%clTeam.swapOffer))
	{
		messageClient(%client, '', "\c5No one is trying to swap teams right now.");
		return;
	}
	%mini.Teams.swapTeamMembers(%client, %clTeam.swapOffer.swapClient);
}

function slayerTeamCmdCancelSwap(%client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(%client != %clTeam.swapClient)
		return;
	messageClient(%client, '', "\c5No one swapped teams.");
	%clTeam.swapPending.swapOffer = "";
	%clTeam.swapPending = "";
	%clTeam.swapClient = "";
}

function slayerTeamCmdList(%client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(%mini.Teams.getCount() <= 0)
	{
		messageClient(%client, '', "\c5There are no teams.");
		return;
	}
	for(%i=0; %i < %mini.Teams.getCount(); %i++)
	{
		%team = %mini.Teams.getObject(%i);
		messageClient(%client, '', %team.getColorHex() @ %team.name);
	}
}

function slayerTeamCmdCount(%client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(%mini.Teams.getCount() <= 0)
	{
		messageClient(%client, '', "\c5There are no teams.");
		return;
	}
	for(%i=0; %i < %mini.Teams.getCount(); %i++)
	{
		%team = %mini.Teams.getObject(%i);
		messageClient(%client, '', "\c3(" @ %team.numMembers @ ")" SPC %team.getColorHex() @ %team.name);
	}
}

function slayerTeamCmdScore(%client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(%mini.Teams.getCount() <= 0)
	{
		messageClient(%client, '', "\c5There are no teams.");
		return;
	}
	for(%i=0; %i < %mini.Teams.getCount(); %i++)
	{
		%team = %mini.Teams.getObject(%i);
		messageClient(%client, '', "\c3(" @ %team.getScore() @ ")" SPC %team.getColorHex() @ %team.name);
	}
}

function slayerTeamCmdLiving(%client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(%mini.Teams.getCount() <= 0)
	{
		messageClient(%client, '', "\c5There are no teams.");
		return;
	}
	for(%i=0; %i < %mini.Teams.getCount(); %i++)
	{
		%team = %mini.Teams.getObject(%i);
		messageClient(%client, '', "\c3(" @ %team.getLiving() @ ")" SPC %team.getColorHex() @ %team.name);
	}
}

function slayerTeamCmdListMembers(%client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	%name = trim(%a SPC %b SPC %c SPC %d SPC %e SPC %f SPC %g SPC %h SPC %i SPC %j SPC %k SPC %l);
	%team = %mini.Teams.getTeamFromName(%name);
	if(!isObject(%team))
	{
		messageClient(%client, '', "\c5Couldn't find team:\c3" SPC %name);
		return;
	}
	messageClient(%client, '', %team.getColoredName() SPC "\c3(" @ %team.numMembers @ ")\c5:");
	for(%i=0; %i < %team.numMembers["GameConnection"]; %i++)
	{
		%cl = %team.member["GameConnection", %i];
		messageClient(%client, '', " \c5+ \c3" @ %cl.getPlayerName());
	}
	if(%team.numMembers["AiController"] > 1)
		messageClient(%client, '', " \c5+ \c3" @ %team.numMembers["AiController"] SPC "\c5" @ (%team.numMembers["AiController"] == 1 ? "bot" : "bots"));
}

function serverCmdTeams(%client, %cmd, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(%client.isSpamming())
		return;
	%mini = getMinigameFromObject(%client);
	if(!isSlayerMinigame(%mini))
		return;
	if(!%mini.gameMode.template.useTeams)
		return;
	%clTeam = %client.getTeam();

	if(strLen(%cmd))
	{
		%func = "slayerTeamCmd" @ %cmd;
		if(isFunction(%func))
			call(%func, %client, %mini, %clTeam, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l);
		else
			messageClient(%client, '', "\c5Please enter a valid command.");
	}
	else if(%client.slayer)
	{
		%mini.teams.sendTeams(%client);
		commandToClient(%client, 'Slayer_ForceGUI', "Slayer_TeamList", true);
	}
	else
	{
		commandToClient(%client, 'MessageBoxOK', "Slayer",
				"This feature requires that you install Slayer.\n\n" @
				"<a:" @ $Slayer::URL::Information @ ">Download Now</a>");
	}
}

package Slayer_TeamHandlerSG
{
	function GameConnection::onDeath(%client, %obj, %killer, %type, %area)
	{
		parent::onDeath(%client, %obj, %killer, %type, %area);

		%mini = getMinigameFromObject(%client);
		if(!isSlayerMinigame(%mini))
			return;

		%team = %client.getTeam();
		if(!isObject(%team))
			return;

		if(	%mini.teams_balanceTeams &&
			isObject(%balanceTeam = %team.balanceTeam) &&
			%team.isBalanceCandidate(%client) )
		{
				
			%balanceTeam.addMember(%client, "Team Balance");
		}
	}

	function serverCmdTeamMessageSent(%client, %msg)
	{
		%mini = getMinigameFromObject(%client);
		if(!isSlayerMinigame(%mini))
			return parent::servercmdTeamMessageSent(%client, %msg);

		serverCmdStopTalking(%client);

		%team = %client.getTeam();

		if(!isObject(%team))
		{
			return parent::servercmdTeamMessageSent(%client, %msg);
		}
		else if(!%team.enableTeamChat || !%mini.chat_enableTeamChat)
		{
			messageClient(%client, '', "\c5Team chat disabled.");
			return;
		}

		%msg = stripMLControlChars(trim(%msg));

		%length = strLen(%msg);
		if(!%length)
			return;

		%time = getSimTime();

		if(!%client.isSpamming)
		{
			//did they repeat the same message recently?
			if(%msg $= %client.lastMsg && %time - %client.lastMsgTime < $SPAM_PROTECTION_PERIOD)
			{
				messageClient(%client, '', "\c5Do not repeat yourself.");
				if(!%client.isAdmin)
				{

					%client.isSpamming = true;
					%client.spamProtectStart = %time;
					%client.schedule($SPAM_PENALTY_PERIOD, spamReset);
				}
			}

			//are they sending messages too quickly?
			if(!%client.isAdmin)
			{
				if(%client.spamMessageCount >= $SPAM_MESSAGE_THRESHOLD)
				{
					%client.isSpamming = true;
					%client.spamProtectStart = %time;
					%client.schedule($SPAM_PENALTY_PERIOD, spamReset);
				}
				else
				{
					%client.spamMessageCount ++;
					%client.schedule($SPAM_PROTECTION_PERIOD, spamMessageTimeout);
				}
			}
		}

		//tell them they're spamming and block the message
		if(%client.isSpamming)
		{
			spamAlert(%client);
			return;
		}

		//eTard Filter, which I hate, but have to include
		//eTard Filter, which I hate, but have to include
		if(!chatFilter(%client, %msg, $Pref::Server::ETardList,
			'\c5This is a civilized game.  Please use full words.'))
		{
			return;
		}

		//URLs
		for(%i = getWordCount(%msg) - 1; %i >= 0; %i --)
		{
			%word = getWord(%msg, %i);
			%pos = strPos(%word, "://") + 3;
			%pro = getSubStr(%word, 0, %pos);
			%url = getSubStr(%word, %pos, strLen(%word));

			if((%pro $= "http://" || %pro $= "https://") && strPos(%url, ":") == -1)
			{
				%word = "<sPush><a:" @ %url @ ">" @ %url @ "</a><sPop>";
				%msg = setWord(%msg, %i, %word);
			}
		}

		//get the names and clan tags
		%name = %client.getPlayerName();
		%pre  = %client.clanPrefix;
		%suf  = %client.clanSuffix;

		//let people know what team they're on
		%tColor = %team.chatColor;
		if(%tColor $= "" || strLen(%tColor) != 6)
			%color = %team.getColorHex();
		else
			%color = "<color:" @ %tColor @ ">";

		switch(%mini.chat_teamDisplayMode)
		{
			case 1:
				%all  = '%5%1\c3%2%5%3%7: %4';

			case 2:
				%all  = '\c7%1%5%2\c7%3%7: %4';

			case 3:
				%all  = '\c7[%5%6\c7] %1\c3%2\c7%3%7: %4';
		}

		if(%all $= "")
			%all  = '\c7%1\c3%2\c7%3%7: %4';

		if(%client.dead() && (%mini.chat_deadChatMode == 0 || %mini.chat_deadChatMode == 2) && !%mini.isResetting())
		{
			if(%mini.teams_allySameColors)
			{
				%mini.Teams.messageAllDeadByColor(%team.color, 'chatMessage', %all, %pre, %name, %suf, %msg, %color, %team.name, " \c7[DEAD]<color:00ffff>");
			}
			else
			{
				%team.messageAllDead('chatMessage', %all, %pre, %name, %suf, %msg, %color, %team.name, " \c7[DEAD]<color:00ffff>");
			}
		}
		else
		{
			if(%mini.teams_allySameColors)
			{
				%mini.Teams.messageAllByColor(%team.color, 'chatMessage', %all, %pre, %name, %suf, %msg, %color, %team.name, "<color:00ffff>");
			}
			else
			{
				%team.messageAll('chatMessage', %all, %pre, %name, %suf, %msg, %color, %team.name, "<color:00ffff>");
			}
		}

		echo("(T" SPC %team.name @ ")" @ %client.getSimpleName() @ ":" SPC %msg);

		%client.lastMsg = %msg;
		%client.lastMsgTime = %time;

		if(isObject(%client.player))
		{
			%client.player.playThread(3, "talk");
			%client.player.schedule(%length * 50, playThread, 3, "root");
		}

		%mini.gameMode.callback("onTeamChat", %client, %msg);
	}

	function serverCmdJoinTeam(%client, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
	{
		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client, Join, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l);
		else
			parent::serverCmdJoinTeam(%client, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l);
	}

	function serverCmdSwapTeam(%client, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
	{
		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client, Join, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l);
		else
			parent::serverCmdSwapTeam(%client, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l);
	}

	function serverCmdSwapTeams(%client, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
	{
//		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client, Join, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l);
//		else
//			parent::serverCmdSwapTeams(%client, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l);
	}

	function serverCmdAcceptSwap(%client)
	{
		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client, AcceptSwap);
		else
			parent::serverCmdAcceptSwap(%client);
	}

	function serverCmdLeaveTeam(%client)
	{
		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client, Leave);
		else
			parent::serverCmdLeaveTeam(%client);
	}

	function serverCmdTeamList(%client)
	{
		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client, List);
		else
			parent::serverCmdTeamList(%client);
	}

	function serverCmdListTeams(%client)
	{
//		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client, List);
//		else
//			parent::serverCmdListTeams(%client);
	}

	function serverCmdTeamCount(%client)
	{
		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client, Count);
		else
			parent::serverCmdTeamCount(%client);
	}

	function serverCmdTeamScore(%client)
	{
//		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client, Score);
//		else
//			parent::serverCmdTeamScore(%client);
	}

	function serverCmdTeamLiving(%client)
	{
//		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client, Living);
//		else
//			parent::serverCmdTeamScore(%client);
	}
};
activatePackage(Slayer_TeamHandlerSG);