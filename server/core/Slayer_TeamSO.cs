// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_TeamSO::onAdd(%this)
{

}

function Slayer_TeamSO::onRemove(%this)
{
	%this.deleteAllBots();
}

//Called when the minigame is reset.
function Slayer_TeamSO::onMinigameReset(%this)
{
	%mini = %this.minigame;

	if(%mini.clearScores || %mini.points > 0)
		%this.setArtificialScore(0);

	if(%mini.gameMode.template.useTeams && !%mini.teams_shuffleTeams)
		%this.botFillTeam();
}

//Adds a minigame member to the team.
//@param	GameConnection client
//@param	string reason	A short description of why they were added. (example: "Team Swap")
//@param	bool doNotRespawn
//@param	bool noEquip	Whether or not to equip player with team items
//@return	bool	Whether the client was correctly added.
function Slayer_TeamSO::addMember(%this, %client, %reason, %doNotRespawn, %noEquip)
{
	%mini = getMinigameFromObject(%client);
	if(%mini != %this.minigame || %client.slyrTeam == %this)
		return false;

	if(isObject(%client.slyrTeam))
		%client.slyrTeam.removeMember(%client, 0, %noEquip);

	%class = %client.getClassName();
	if(%this.numMembers[%class] $= "")
		%this.numMembers[%class] = 0;

	%client.slyrTeam = %this;
	%client.slyrTeamJoinTime = getSimTime();

	%this.member[%this.numMembers] = %client;
	%this.numMembers ++;
	%this.member[%class, %this.numMembers[%class]] = %client;
	%this.numMembers[%class] ++;

	//ARE THEY HUMAN OR BOT?
	if(%class $= "GameConnection")
	{
		//HUMAN - ANNOUNCEMENT
		%msgA = '\c5You have joined %1 %2';
		%msgB = '\c3%1 \c5has joined %2 %3';
		%clName = %client.getPlayerName();
		%teamName = %this.getColoredName();
		%reason = (%reason $= "" ? "" : "\c5(\c3" @ %reason @ "\c5)");
		messageClient(%client, '', %msgA, %teamName, %reason);
		if(%mini.teams_notifyMemberChanges)
			%mini.messageAllExcept(%client, '', %msgB, %clName, %teamName, %reason);

		clearBottomPrint(%client);
		clearCenterPrint(%client);
	}

	//SPAWN
	if((isObject(%client.player) || %mini.isResetting() || !%client.hasSpawnedOnce) && !%doNotRespawn)
		%client.spawnPlayer();

	%this.onAddMember(%client);

	return true;
}

//Called after a client has been added to a team
//@param	GameConnection client
//@see	Slayer_TeamSO::addMember
function Slayer_TeamSO::onAddMember(%this, %client)
{
	%mini = %this.minigame;

	//SET LIVES
	if((%client.getLives() > %this.lives || %client.getLives() == %mini.lives) && %this.lives >= 0)
		%client.setLives(%this.lives);

	if(%client.getClassName() $= "GameConnection")
	{
		//TEAM BALANCE
		if(%mini.teams_balanceTeams)
			%this.balanceTeam();

		//TEAM FILLING
		if(%this.botFillLimit > 0)
			%this.botFillTeam();
	}

	%mini.gameMode.callback("onClientJoinTeam", %this, %client);
}

//Removes a client from a team.
//@param	GameConnection client
//@param	bool respawn	Whether to respawn the client after they leave.
//@return	bool	Whether the client was successfully removed.
function Slayer_TeamSO::removeMember(%this, %client, %respawn, %noEquip)
{
	%mini = %this.minigame;
	%teamHandler = %this.getGroup();

	if(%client.slyrTeam != %this)
	{
		Slayer_Support::Error("Member not on" SPC %this.name, %client.getSimpleName());
		return false;
	}

	//cancel any team swap requests
	if(%client == %this.swapClient)
	{
		%this.swapPending.swapOffer = "";
		%this.swapPending = "";
		%this.swapClient = "";
		cancel(%this.swapTime);
		%this.swapTime = "";
	}

	//are they the captain?
	if(%this.isCaptain(%client))
	{
		%this.captain = 0;
	}

	//REMOVE FROM MEMBER ARRAY
	%found = false;
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		if(%found)
		{
			%this.member[%i - 1] = %this.member[%i];
		}
		else
		{
			if(%this.member[%i] == %client)
			{
				%this.member[%i] = "";
				%found = true;
			}
		}
	}
	%this.numMembers --;

	//REMOVE FROM CLASS MEMBER ARRAY
	%class = %client.getClassName();
	%found = false;
	for(%i = 0; %i < %this.numMembers[%class]; %i ++)
	{
		if(%found)
		{
			%this.member[%class, %i - 1] = %this.member[%class, %i];
		}
		else
		{
			if(%this.member[%class, %i] == %client)
			{
				%this.member[%class, %i] = "";
				%found = true;
			}
		}
	}
	%this.numMembers[%class] --;

	%client.slyrTeam = "";

	if(%respawn)
	{
		%client.spawnPlayer();
	}
	else if(isObject(%client.player))
	{
		%client.applyBodyParts();
		%client.applyBodyColors();
		%client.player.setShapeNameColor(%mini.colorRGB);

		%playerDatablock = %mini.playerDatablock;
		%client.player.changeDatablock(%playerDatablock);
		if(!%noEquip)
			for(%i = 0; %i < %playerDatablock.maxTools; %i ++)
				%client.forceEquip(%i, %mini.startEquip[%i]);
	}
	if(%client.getLives() >= %this.lives)
	{
		%client.setLives(%mini.lives);
	}

	%this.onRemoveMember(%client);

	return true;
}

//Called before the client is removed from the team.
//@param	GameConnection client
function Slayer_TeamSO::onRemoveMember(%this, %client)
{
	%mini = %this.minigame;

	if(%client.getClassName() $= "GameConnection")
	{
		//TEAM BALANCE
		if(%mini.teams_balanceTeams)
			%this.balanceTeam();
	
		//TEAM FILLING
		if(%this.botFillLimit > 0)
			%this.botFillTeam();
	}

	%mini.gameMode.callback("onClientLeaveTeam", %this, %client);
}

//Removes all members from their teams.
//@param	string class	If specified, only members of that class will be removed.
//@see	Slayer_TeamSO::removeMember
function Slayer_TeamSO::removeAllMembers(%this, %class)
{
	if(%class $= "")
	{
		for(%i = %this.numMembers - 1; %i >= 0; %i --)
			%this.removeMember(%this.member[%i]);
	}
	else
	{
		for(%i = %this.numMembers[%class] - 1; %i >= 0; %i --)
			%this.removeMember(%this.member[%class, %i]);
	}
}

//Checks whether a client is on the team.
//@param	GameConnection client
//@return	bool
function Slayer_TeamSO::isMember(%this, %client)
{
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		if(%this.member[%i] == %client)
			return 1;
	}
	return 0;
}

//Removes and deletes all bots on the team.
function Slayer_TeamSO::deleteAllBots(%this)
{
	for(%i = %this.numMembers["AiController"] - 1; %i >= 0; %i --)
		%this.member["AiController", %i].delete();
}

//Checks whether a client is the team captain.
//@param	GameConnection client
//@return	bool
function Slayer_TeamSO::isCaptain(%this, %client)
{
	return %client == %this.captain;
}

//Represents the client's value to the team as an integer.
//@param	GameConnection client
//@param	bool includeScore	Whether the client's score is taken into consideration. Otherwise, only kills/deaths are considered.
//@return	int
function Slayer_TeamSO::getMemberValue(%this, %client, %includeScore)
{
	%value = 0;
	%value += %client.score;
	%value += %client.slyr_kills;
	%value -= %client.slyr_deaths;
	return %value;
}

//Finds the best candidate for balancing another team. Considers points, kills, deaths, and (optionally) time spent on the team.
//If there are multiple potential candidates, a random one will be chosen.
//@param	bool valueTime	Whether the amount of time spent on the team is taken into consideration. Less time equates to lower rank.
//@return	GameConnection
function Slayer_TeamSO::getBalanceCandidate(%this, %valueTime)
{
	for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
	{
		%cl = %this.member["GameConnection", %i];
		%value = %this.getMemberValue(%cl, true);
		if(%valueTime)
			%value += %i;
		if(%value < %lowestValue || %i == 0)
		{
			%lowestValue = %value;
			%candidate[0] = %cl;
			%candidateCount = 1;
		}
		else if(%value == %lowestValue)
		{
			%candidate[%candidateCount] = %cl;
			%candidateCount ++;
		}
	}
	if(%candidateCount > 0)
	{
		%index = getRandom(0, %candidateCount - 1);
		return %candidate[%index];
	}
	else
	{
		return 0;
	}
}

//Checks whether a client is a balance candidate.
//@param	GameConnection client
//@param	bool valueTime	Whether the amount of time spent on the team is taken into consideration. Less time equates to lower rank.
//@return	bool
function Slayer_TeamSO::isBalanceCandidate(%this, %client, %valueTime)
{
	%member = 0;
	for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
	{
		%cl = %this.member["GameConnection", %i];
		%value = %this.getMemberValue(%cl, true);
		if(%valueTime)
			%value += %i;
		if(%value < %lowestValue || %i == 0)
		{
			%lowestValue = %value;
			%candidate[0] = %cl;
			%candidateCount = 1;
		}
		else if(%value == %lowestValue)
		{
			%candidate[%candidateCount] = %cl;
			%candidateCount ++;
		}
	}
	for(%i = 0; %i < %candidateCount; %i ++)
	{
		if(%candidate[%i] == %client)
		{
			return true;
		}
	}
	return false;
}

//Pull clients from other teams to balance team members.
//@param	bool force	Whether to move players immediately (true) or wait until they die (false).
//@return	Slayer_TeamSO	The source team that players will come from.
function Slayer_TeamSO::balanceTeam(%this, %force)
{
	%mini = %this.minigame;
	%teamHandler = %this.getGroup();

	%this.balanceSource.balanceTeam = "";
	%this.balanceSource = "";

	if(!%this.sort || %this.sortWeight <= 0)
		return 0;
	if(%this.numMembers["GameConnection"] >= %this.maxPlayers && %this.maxPlayers >= 0)
		return 0;

	%ratio = %this.numMembers["GameConnection"] / %this.sortWeight;

	//find the most suitable source
	%highest = 0;
	for(%i = %teamHandler.getCount() - 1; %i >= 0; %i --)
	{
		%t = %teamHandler.getObject(%i);
		if(!%t.sort || %t.sortWeight <= 0 || %t.maxPlayers == 0)
			continue;
		%r = %t.numMembers["GameConnection"] / %t.sortWeight;
		%ratioSum += %r;
		%ratioCount ++;
		if(%t != %this && %r > %highest)
		{
			%highest = %r;
			%source = %t;
			%sourceRatio = %r;
		}
	}

	if(%ratioCount > 1)
	{
		%ratioAvg = %ratioSum / %ratioCount;

		if(%ratio < %ratioAvg)
		{
			//we can no longer give away members
			//the other team needs a new donor
			if(isObject(%this.balanceTeam))
			{
				%this.balanceTeam.balanceTeam(false);
			}
		}
	}

	if(!isObject(%source))
		return 0;

	%ratioDif = %sourceRatio - %ratio;

	if(%ratioDif > 1)
	{
		//is this team completely out of members?
		if(%force || (%this.numMembers["GameConnection"] < 1 && %teamHandler.getCount() == 2 && %mini.teams_swapMode != 1))
		{
			//yes - we need to move someone immediately
			%cl = %source.getBalanceCandidate(true);
			%this.addMember(%cl, "Team Balance");
		}
		else
		{
			//no - wait until someone dies
			%source.balanceTeam = %this;
			%this.balanceSource = %source;
		}

		return %source;
	}

	return 0;
}

//Checks if the team has a balanced number of players compared to other teams.
//@return	bool
function Slayer_TeamSO::isTeamBalanced(%this)
{
	%mini = %this.minigame;
	%teamHandler = %this.getGroup();

	if(!%this.sort || %this.sortWeight <= 0)
		return -1;
	if(%this.numMembers["GameConnection"] >= %this.maxPlayers && %this.maxPlayers >= 0)
		return 1;

	%ratio = %this.numMembers["GameConnection"] / %this.sortWeight;

	//find the most suitable source
	%highest = 0;
	for(%i = %teamHandler.getCount() - 1; %i >= 0; %i --)
	{
		%t = %teamHandler.getObject(%i);
		if(!%t.sort || %t.sortWeight <= 0 || %t.maxPlayers == 0)
			continue;

		%r = %t.numMembers["GameConnection"] / %t.sortWeight;
		%ratioSum += %r;
		%ratioCount ++;

		if(%t == %this)
			continue;

		if(%r > %highest)
		{
			%highest = %r;

			%source = %t;
			%sourceRatio = %r;
		}
	}

	%ratioDif = %sourceRatio - %ratio;

	return (%ratioDif <= 1);
}

//Adds or removes AIs to a team until it is at its "preferred playercount".
function Slayer_TeamSO::botFillTeam(%this)
{
	//we don't want to do anything if the minigame is paused
	if(%this.minigame.isSuspended)
		return;

	%limit = %this.botFillLimit;
	if(%limit $= "")
	{
		Slayer_Support::Error("Slayer_TeamSO::botFillTeam", "Limit undefined!");
		return;
	}

	//REMOVE UNNEEDED BOTS
	while(%this.numMembers > %limit && %this.numMembers["AiController"] > 0)
	{
		%bot = %this.member["AiController", 0];
		%last = %this.numMembers["AiController"];
		%bot.delete();
		if(%this.numMembers["AiController"] == %last)
		{
			Slayer_Support::Error("Slayer_TeamSO::botFillTeam", "Failed to delete bot" SPC %bot);
			break;
		}
	}

	//ADD EXTRA BOTS
	while(%this.numMembers < %limit)
	{
		%bot = %this.minigame.addBotToGame();
		if(!isObject(%bot))
		{
			Slayer_Support::Error("Slayer_TeamSO::botFillTeam", "Bot not created!");
			break;
		}
		%this.addMember(%bot);
	}
}

//@param	Slayer_TeamSO team
//@return	bool
function Slayer_TeamSO::isAlliedTeam(%this, %team)
{
	if(%this == %team)
		return true;

	if(%this.minigame.teams_allySameColors)
	{
		if(%this.color == %team.color)
			return true;
	}

	return false;
}

//Send message to all team members.
function Slayer_TeamSO::messageAll(%this, %cmd, %msg, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p, %q)
{
	for(%index = 0; %index < %this.numMembers["GameConnection"]; %index ++)
		messageClient(%this.member["GameConnection", %index], %cmd, %msg, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p, %q);
}

//Send message to all dead team members.
function Slayer_TeamSO::messageAllDead(%this, %cmd, %msg, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p, %q)
{
	for(%index = 0; %index < %this.numMembers["GameConnection"]; %index ++)
	{
		%cl = %this.member["GameConnection", %index];
		if(%cl.dead())
			messageClient(%cl, %cmd, %msg, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p, %q);
	}
}

//@param	string msg
//@param	GameConnection client
function Slayer_TeamSO::chatMsgAll(%this, %msg, %client)
{
	if(isObject(%client))
		%msg = strReplace(%msg, "%1", %client.getPlayerName());

	for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
		%this.member["GameConnection", %i].chatMessage(%msg);
}

//@param	string msg
//@param	int time
//@param	GameConnection client
function Slayer_TeamSO::centerPrintAll(%this, %msg, %time, %client)
{
	if(isObject(%client))
		%msg = strReplace(%msg, "%1", %client.getPlayerName());

	for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
		%this.member["GameConnection", %i].centerPrint(%msg, %time);
}

//@param	string msg
//@param	int time
//@param	bool hideBar
//@param	GameConnection client
function Slayer_TeamSO::bottomPrintAll(%this, %msg, %time, %hideBar, %client)
{
	if(isObject(%client))
		%msg = strReplace(%msg, "%1", %client.getPlayerName());

	for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
		%this.member["GameConnection", %i].bottomPrint(%msg, %time, %hideBar);
}

//@param	GameConnection client
function Slayer_TeamSO::respawnAll(%this, %client)
{
	for(%i = 0; %i < %this.numMembers; %i ++)
		%this.member[%i].spawnPlayer();
}

//@param	int flag
//@param	GameConnection client
function Slayer_TeamSO::incScore(%this, %flag, %client)
{
	%this.incArtificialScore(%flag);
}

//Increases the score of the team itself, without affecting scores of team members.
//@param	int flag
function Slayer_TeamSO::incArtificialScore(%this, %flag)
{
	%this.setArtificialScore(%this.score + %flag);

	return %this.score;
}

//Sets the score of the team itself, without affecting scores of team members.
//@param	int flag
function Slayer_TeamSO::setArtificialScore(%this, %flag)
{
	%this.score = %flag;

	%mini = %this.minigame;
	%winner = %mini.victoryCheck_Points();
	if(%winner >= 0)
		%mini.endRound(%winner);

	return %this.score;
}

//Gets the score of the team itself, not including member scores.
//@return	int
function Slayer_TeamSO::getArtificialScore(%this)
{
	return %this.score;
}

//Gets the team's total score, including member and artificial scores.
//@return	int
function Slayer_TeamSO::getScore(%this)
{
	%score = %this.getArtificialScore();
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%score += %this.member[%i].score;
	}
	return %score;
}

//@return	int
function Slayer_TeamSO::getKills(%this)
{
	%kills = 0;
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%kills += %this.member[%i].slyr_kills;
	}
	return %kills;
}

//@return	int
function Slayer_TeamSO::getDeaths(%this)
{
	%deaths = 0;
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%deaths += %this.member[%i].slyr_deaths;
	}
	return %deaths;
}

//@return	int
function Slayer_TeamSO::getSpawned(%this)
{
	%spawned = 0;
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		if(isObject(%this.member[%i].player))
			%spawned ++;
	}
	return %spawned;
}

//@return	int
function Slayer_TeamSO::getLiving(%this)
{
	%living = 0;
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		if(!%this.member[%i].dead())
			%living ++;
	}
	return %living;
}

//@return	bool	Whether the team has any living members remaining.
function Slayer_TeamSO::isTeamDead(%this)
{
	return (%this.getLiving() <= 0);
}

//Returns the team's color in the TML hex-color format: <color:######>
//@return	string
function Slayer_TeamSO::getColorHex(%this)
{
	return "<color:" @ %this.colorHex @ ">";
}

//Returns the team's name, preceded by the team's TML hex-color tag.
//@return	string
function Slayer_TeamSO::getColoredName(%this)
{
	return %this.getColorHex() @ %this.name;
}

//Forcibly equips each member of the team with a specific item.
//@param	int slot
//@param	ItemData item
function Slayer_TeamSO::forceEquip(%this, %slot, %item)
{
	for(%i = 0; %i < %this.numMembers; %i ++)
		%this.member[%i].forceEquip(%slot, %item);
}

//Equips each member of the team with a specific item, IF the player's item in %slot has not changed from %old.
//@param	int slot
//@param	ItemData new
//@param	ItemData old
function Slayer_TeamSO::updateEquip(%this, %slot, %new, %old)
{
	for(%i = 0; %i < %this.numMembers; %i ++)
		%this.member[%i].updateEquip(%slot, %new, %old);
}

//Causes all players on this team to change to the selected datablock.
//@param	PlayerData db
function Slayer_TeamSO::updateDatablock(%this, %db)
{
	if(!isObject(%db))
		return;
	if(%db.getClassName() !$= "PlayerData")
		return;

	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%cl = %this.member[%i];
		%pl = %cl.player;
		if(isObject(%pl))
			%pl.changeDatablock(%db);
	}
}

function Slayer_TeamSO::updateName(%this, %name)
{
	%this.name = "";

	%name = trim(%name);
	%name = getWords(%name, 0, 12); //names are limited to 12 words
	if(isObject(%this.getGroup().getTeamFromName(%name)))
		%name = %name SPC getRandom(0, 999999);
	%this.name = %name;
}

function Slayer_TeamSO::updateColor(%this, %color)
{
	%this.colorRGB = getColorIDTable(%color);
	%this.colorHex = Slayer_Support::RgbToHex(%this.colorRGB);

	%this.updateUniform();
}

function Slayer_TeamSO::updateUniform(%this)
{
	for(%i = 0; %i < %this.numMembers; %i ++)
		%this.member[%i].applyUniform();
}

function Slayer_TeamSO::updateBotFillLimit(%this, %flag)
{
	if(%this.minigame.isStarting)
		return;
	if(%flag < 1 || ($AddOnLoaded__["Bot_Hole"] && $AddOnLoaded__["Bot_Blockhead"]))
	{
		%this.botFillTeam();
	}
	else
	{
		%this.botFillLimit = -1;
		if(isObject(%client = %this.minigame.editClient))
			commandToClient(%client, 'MessageBoxOK', "Slayer | Error", "You must enable the Bot_Hole and Bot_Blockhead add-ons to use the Preferred Players setting.");
	}
}

function Slayer_TeamSO::updateRespawnTime(%this, %type, %flag, %old)
{
	%oldTime = %old * 1000;
	%time = %flag * 1000;
	if(%flag != -1)
		%time = mClampF(%time, 1000, 999999);

	switch$(%type)
	{
		case player:
			%this.respawnTime = %time;

			for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
			{
				%cl = %this.member["GameConnection", %i];
				if(%cl.respawnTime == %oldTime || %old == -1)
					%cl.setRespawnTime(%time);
			}
	}
}

function Slayer_TeamSO::updateLives(%this, %new, %old)
{
	%mini = %this.minigame;

	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%cl = %this.member[%i];

		if(%new == 0)
		{
			%cl.setLives(0);
			return;
		}
		else if(%new < 0)
		{
			%cl.setLives(%mini.lives);
			return;
		}

		if(%old $= "" || %old < 0)
			%cl.setLives(%new);
		else
			%cl.addLives(%new-%old);

		%lives = %cl.getLives();
		if(%lives <= 0 && !%cl.dead())
		{
			%cl.setDead(1);
			if(isObject(%cl.player))
			{
				%cl.camera.setMode(observer);
				%cl.setControlObject(%cl.camera);
				%cl.player.delete();
			}
			%cl.centerPrint("\c5The number of lives was reduced. You are now out of lives.", 5);

			%check = 1;
		}
		else if(%lives > 0 && %cl.dead())
		{
			%cl.setDead(0);
			if(!isObject(%cl.player))
				%cl.spawnPlayer();
		}
	}

	if(%check)
	{
		%win = %mini.victoryCheck_Lives();
		if(%win >= 0)
			%mini.endRound(%win);
	}
}

function Slayer_TeamSO::updatePlayerScale(%this, %flag)
{
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%cl = %this.member[%i];
		if(isObject(%cl.player))
			%cl.player.setScale(%flag SPC %flag SPC %flag);
	}
}

function Slayer_TeamSO::setPref(%this, %category, %title, %value)
{
	return Slayer.TeamPrefs.setPref(%this, %category, %title, %value);
}

function Slayer_TeamSO::getPref(%this, %category, %title)
{
	return Slayer.TeamPrefs.getPref(%this, %category, %title);
}

function Slayer_TeamSO::resetPrefs(%this)
{
	Slayer.TeamPrefs.resetPreferences(%this);
}