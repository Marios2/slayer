// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

//Finds the gamemode object belonging to the fName.
//@param	string flag	The fName to search for.
//@return	Slayer_GameModeTemplateSG The gamemode found.
//@DEPRECATED
function Slayer_GamemodeHandlerSG::getModeFromFName(%this, %flag)
{
	return %this.mode[%flag];
	// for(%i = 0; %i < %this.getCount(); %i ++)
	// {
		// %m = %this.getObject(%i);
		// if(%m.fName $= %flag)
			// return %m;
	// }

	// return 0;
}

//Finds the gamemode object belonging to the uiName.
//@param	string flag	The uiName to search for.
//@return	Slayer_GameModeTemplateSG The gamemode found.
//@DEPRECATED
function Slayer_GamemodeHandlerSG::getModeFromUIName(%this, %flag)
{
	for(%i = 0; %i < %this.getCount(); %i ++)
	{
		%m = %this.getObject(%i);
		if(%m.name $= %flag)
			return %m;
	}

	return 0;
}

//Finds the the uiName associated with a fName.
//@param	string flag	The fName to search for.
//@return	string	The uiName associated with the fName.
function Slayer_GamemodeHandlerSG::getFNameFromUIName(%this, %flag)
{
	return %this.getModeFromUIName(%flag).className;
}

//Finds the the fName associated with a uiName.
//@param	string flag	The uiName to search for.
//@return	string	The fName associated with the uiName.
function Slayer_GamemodeHandlerSG::getUINameFromFName(%this, %flag)
{
	return %this.getModeFromFName(%flag).name;
}

//Sends game mode data to a client.
//@param	GameConnection client	The client to send data to.
//@see	Slayer::sendInitialData
function Slayer_GamemodeHandlerSG::sendGameModes(%this, %client)
{
	//START THE GAMEMODE TRANSFER
	commandToClient(%client, 'Slayer_getGameModes_Start');

	%count = %this.getCount();
	for(%i = 0; %i < %count; %i ++)
	{
		transferObjectToClient(%client, %this.getObject(%i), true, "SlayerClient_GameModeTemplateSG");
	}

	//END THE GAMEMODE TRANSFER
	commandToClient(%client, 'Slayer_getGameModes_End');
}

//Adds a gamemode.
//@param	string uiName	The name that players will see.
//@param	string fName	This is the one-word name used in callbacks and functions.
//@param	bool teams	This gamemode uses default team functionality.
//@param	bool rounds	This gamemode uses default rounds functionality.
//@return	Slayer_GameModeTemplateSG	The newly created gamemode object.
//@DEPRECATED
function Slayer_GamemodeHandlerSG::addMode(%this, %uiName, %fname, %teams, %rounds)
{
	if(%uiName $= "" || %fName $= "" || %teams $= "")
		return;

	if(isObject(%this.getModeFromFName(%fName)) || isObject(%this.getModeFromUIName(%uiName)))
	{
		Slayer_Support::Error("Gamemode Already Exists", %fName TAB %uiName);
		return;
	}

	warn("\c2WARN: Slayer_GamemodeHandlerSG::addMode is deprecated! Use the new API.");

	%mode = new ScriptGroup(Slayer_GameModeTemplateSG)
	{
		className = %fName;
		uiName = %uiName;
		useTeams = %teams;
		compatibilityMode = true;
	};

	return %mode;
}