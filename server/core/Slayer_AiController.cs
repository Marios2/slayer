// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

//TO-DO
// - Check for new objectives once we run out.
// - Implement jump-search pathfinding

if(($AddOnLoaded__["Bot_Hole"] != 1 && $AddOn__["Bot_Hole"] != 1) ||
	($AddOnLoaded__["Bot_Blockhead"] != 1 && $AddOn__["Bot_Blockhead"] != 1))
{
	Slayer_Support::Debug(0, "\c2Failed to load Slayer_AiController.cs",
		"Bot_Hole and Bot_Blockhead must be enabled.");
	return;
}

$Slayer::Server::Bots::Priority["CP"] = 200;
$Slayer::Server::Bots::Priority["RN"] = 100;

$Slayer::Server::Bots::SkinColor[1] = "1 0.878431 0.611765 1";
$Slayer::Server::Bots::SkinColor[2] = "0.415686 0.305882 0.258823 1";
$Slayer::Server::Bots::SkinColor[3] = "0.257 0.135 0.07 1";
$Slayer::Server::Bots::SkinColor[4] = "0.677 0.56 0.42 1";
$Slayer::Server::Bots::SkinColorCount = 4;

if(isFile($Slayer::Server::ConfigDir @ "/botSkinColors.cs"))
	exec($Slayer::Server::ConfigDir @ "/botSkinColors.cs");
export("$Slayer::Server::Bots::SkinColor*", $Slayer::Server::ConfigDir @ "/botSkinColors.cs", false);

//Tasks the bot with a series of objectives to complete.
//Objectives can include rally nodes, capture points, CTF flags, etc.
//Default objective priorities: RN 100; CP 200;
//@return	bool	Whether any objectives were added.
//@see	Slayer_AiController::addObjective
function Slayer_AiController::assignObjectives(%this)
{
	// %numObjectives = %this.numObjectives;
	// %mini = %this.minigame;
	// %team = %this.slyrTeam;

	// %callback = %mini.gameMode.callback("assignBotObjectives", %this);
	// if(%callback == -1)
		// return (%this.numObjectives > %numObjectives);

	// //Rally nodes are now assigned every time the bot spawns.

	// if(isObject(%team))
	// {
		// //Capture Points
		// %count = Slayer.capturePoints.getCount();
		// for(%i = 0; %i < %count; %i ++)
		// {
			// %cp = Slayer.capturePoints.getObject(%i);

			// %blid = %cp.getGroup().bl_id;
			// if(%canUse[%blid] $= "")
				// %canUse[%blid] = miniGameCanUse(%this, %cp);

			// if(%canUse[%blid])
			// {
				// if(%cp.getTeamControl() != %team.color)
				// {
					// %this.addObjective(%cp, $Slayer::Server::Bots::Priority["CP"], "CP");
				// }
			// }
		// }
	// }

	// return (%this.numObjectives > %numObjectives);
}

//Adds an objective to the bot's objective queue.
//@param	Grid3D objective	This brick will be added to the queue.
//@param	int priority	The importance of this objective, 0 being the lowest priority.
//@param	string callbackID	The ID for the completion callback. If the callbackID is "test", the callback would be "Slayer_onBotObjectiveReached_test(%mini, %bot, %obj)"
//@return	bool	Whether the objective was successfully added.
//@see	Slayer_AiController::removeObjective
//@see	Slayer_AiController::isObjective
//@see	Slayer_AiController::assignObjectives
function Slayer_AiController::addObjective(%this, %objective, %priority, %callbackID)
{
	// if(!isObject(%objective))
		// return false;
	
	// //Allow bricks to be added as objectives.
	// if(%objective.getClassName() $= "FxDtsBrick")
	// {
		// %brick = %objective;
		// %db = %brick.getDatablock();
		// if(%db.specialBrickType $= "SpawnPoint")
			// %pos = %brick.getSpawnPoint();
		// else
		// {
			// %z = %db.brickSizeZ * $Slayer::Paths::BrickGridZ;
			// %pos = vectorAdd(%brick.position, "0 0" SPC %z);
		// }
		// %objective = Slayer.pathHandler.findClosestNode(%pos);
	// }
	// else
	// {
		// %brick = 0;
	// }
	
	// if(%this.isObjective(%objective))
		// return false;

	// if(!%this.numObjectives)
		// %this.numObjectives = 0;

	// %this.objectiveID[%objective] = %this.numObjectives;
	// %this.objective[%this.numObjectives] = %objective;
	// %this.objectivePriority[%this.numObjectives] = %priority;
	// %this.objectiveBrick[%this.numObjectives] = %brick;
	// %this.objectiveCallbackID[%this.numObjectives] = %callbackID;
	// %this.numObjectives ++;

	// return true;
}

//Checks whether a bot has an object in its objective array.
//@param	fxDtsBrick objective	The objective to check.
//@return	bool	Whether the bot has the objective.
//@see	Slayer_AiController::addObjective
//@see	Slayer_AiController::removeObjective
function Slayer_AiController::isObjective(%this, %objective)
{
	for(%i = 0; %i < %this.numObjectives; %i ++)
	{
		if(nameToID(%this.objective[%i]) == nameToID(%objective))
			return true;
	}
	return false;
}

//Removes an objective from the bot's objective array.
//@param	fxDtsBrick objective	The objective to remove.
//@return	bool	Whether the objective was successfully removed.
//@see	Slayer_AiController::addObjective
//@see	Slayer_AiController::removeAllObjectives
//@see	Slayer_AiController::isObjective
function Slayer_AiController::removeObjective(%this, %objective)
{
	%found = false;

	for(%i = 0; %i < %this.numObjectives; %i++)
	{
		if(%found)
		{
			%this.objectiveID[%this.objective[%i]] --;
			%this.objective[%i] = %this.objective[%i + 1];
			%this.objectivePriority[%i] = %this.objectivePriority[%i + 1];
			%this.objectiveBrick[%i] = %this.objectiveBrick[%i + 1];
			%this.objectiveCallbackID[%i] = %this.objectiveCallbackID[%i + 1];
		}
		else if(nameToID(%this.objective[%i]) == nameToID(%objective))
		{
			%found = true;
			%i --;
			continue;
		}
	}

	if(%found)
	{
		%this.numObjectives --;
		%this.objectiveID[%objective] = "";
		%this.objective[%this.numObjectives] = "";
		%this.objectivePriority[%this.numObjectives] = "";
		%this.objectiveBrick[%this.numObjectives] = "";
		%this.objectiveCallbackID[%this.numObjectives] = "";
	}
	else
	{
		Slayer_Support::Error("Slayer_AiController::removeObjective", "Objective does not exist.");
	}

	return %found;
}

//Removes all objectives
//@see	Slayer_AiController::removeObjective
function Slayer_AiController::removeAllObjectives(%this)
{
	for(%i = %this.numObjectives - 1; %i >= 0; %i --)
		%this.removeObjective(%this.objective[%i]);
}

//Finds the current objective that the bot is trying to reach.
//@return	fxDtsBrick	The objective.
function Slayer_AiController::getCurrentObjective(%this)
{
	if(!isObject(%this.player))
		return 0;
	if(!isObject(%this.player.objective))
		return 0;
	return %this.player.objective;
}

//Finds the next objective for the bot to complete.
//@param	bool closestFirst	If true, will find the closest, highest-priority objective rather than highest-priority in order of addition.
//@return	fxDtsBrick	Returns the object ID of the objective that was found.
//@see	Slayer_AiController::goToNextObjective
function Slayer_AiController::getNextObjective(%this, %closestFirst)
{
	%priority = -1;
	%objective = 0;
	// if(%closestFirst && isObject(%this.player))
	// {
		// %distance = 999999;
		// for(%i = 0; %i < %this.numObjectives; %i ++)
		// {
			// if(%this.objectivePriority[%i] > %priority)
			// {
				// %priority = %this.objectivePriority[%i];
				// %distance = pathDistance(%this, %this.objective[%i]);
				// %objective = %this.objective[%i];
			// }
			// else if(%this.objectivePriority[%i] == %priority)
			// {
				// %dist = pathDistance(%this, %this.objective[%i]);
				// if(%dist > %distance)
				// {
					// %distance = %dist;
					// %objective = %this.objective[%i];
				// }
			// }
		// }
	// }
	// else
	// {
		for(%i = %this.numObjectives - 1; %i >= 0; %i --)
		{
			if(%this.objectivePriority[%i] >= %priority)
			{
				%priority = %this.objectivePriority[%i];
				%objective = %this.objective[%i];
			}
		}
	// }
	return %objective;
}

//Sends the bot to its next objective.
//@param	bool closestFirst	Please see Slayer_AiController::getNextObjective for more information.
//@see	Slayer_AiController::getNextObjective
//@see	AiPlayer::goToObjective
function Slayer_AiController::goToNextObjective(%this, %closestFirst)
{
	if(!isObject(%this.player))
		return;
	%objective = %this.getNextObjective(%closestFirst);
	if(!isObject(%objective))
		return;
	%this.player.goToObjective(%objective);
}

//Called when a bot reaches its objective.
//If a callback exists (Slayer_onBotObjectiveReached_callbackID), it will be called.
//If the callback returns true, or if there is no callback, the bot will proceed to the next objective.
//@param	fxDtsBrick objective	The objective that was reached.
//@see	Slayer_AiController::onObjectiveFailed
//@see	Slayer_NodeTriggerData::onEnterTrigger
function Slayer_AiController::onObjectiveReached(%this, %objective)
{
	Slayer_Support::Debug(2, "Slayer_AiController::onObjectiveReached", %this TAB %objective);

	//restore bot properties
	%this.player.hWander = %this.player.hOldWander;

	%callbackID = %this.objectiveCallbackID[%this.objectiveID[%objective]];
	%callback = "Slayer_onBotObjectiveReached_" @ %callbackID;
	if(isFunction(%callback))
	{
		%brick = %this.objectiveBrick[%this.objectiveID[%objective]];
		%callbackReturn = call(%callback, %this.minigame, %this, %objective, %brick);
	}

	if(!%this.removeObjective(%objective))
	{
		Slayer_Support::Error("Slayer_AiController::onObjectiveReached", "Cannot remove objective, stopping!");
		return;
	}

	if(%callbackReturn != -1)
	{
		%this.player.objective = "";
		%this.player.objectiveBrick = "";
		%this.goToNextObjective(true);
	}
}

//Called when a bot is unable to reach its objective.
//If a callback exists (Slayer_onBotObjectiveFailed_callbackID), it will be called.
//If the callback returns true, or if there is no callback, the bot will proceed to the next objective.
//@param	fxDtsBrick objective	The objective that was reached.
//@see	Slayer_AiController::onObjectiveReached
//@see	Slayer_NodeTriggerData::onEnterTrigger
function Slayer_AiController::onObjectiveFailed(%this, %objective)
{
	Slayer_Support::Debug(2, "Slayer_AiController::onObjectiveFailed", %this TAB %objective);

	%callbackID = %this.objectiveCallbackID[%this.objectiveID[%objective]];
	%callback = "Slayer_onBotObjectiveFailed_" @ %callbackID;
	if(isFunction(%callback))
	{
		%brick = %this.objectiveBrick[%this.objectiveID[%objective]];
		%callbackReturn = call(%callback, %this.minigame, %this, %objective, %brick);
	}

	if(%callbackReturn != -1)
	{
		if(!%this.removeObjective(%objective))
		{
			Slayer_Support::Error("Slayer_AiController::onObjectiveFailed", "Cannot remove objective, stopping!");
			return;
		}
		%this.player.objective = "";
		%this.goToNextObjective(true);
	}
}

//Creates an AiPlayer object for the Slayer_AiController.
//@param	transform transform	The initial transform for the new player.
//@return	AiPlayer	The AiPlayer object that was created.
//@see	Slayer_AiController::spawnPlayer
//@private
function Slayer_AiController::createPlayer(%this, %transform)
{
	%mini = getMinigameFromObject(%this);
	if(isSlayerMinigame(%mini) && %this.isHoleBot)
	{
		if(isObject(%this.lastSpawnBrick))
		{
			%spawnBrick = %this.lastSpawnBrick;
		}
		else
		{
			//This is a hack to allow Hole Bots to spawn without a spawnbrick.
			if(!isObject(SlayerFakeBrick))
			{
				new FxDtsBrick(SlayerFakeBrick)
				{
					datablock = brick1x1Data;
					isPlanted = false;
					itemPosition = 1;
					position = "0 0 -2000";
				};
				Slayer.add(SlayerFakeBrick);
			}
			%spawnBrick = SlayerFakeBrick;
			%useFakeBrick = true;
		}

		%this.hName = "";

		%team = %this.getTeam();
		%handler = (isObject(%team) ? %team : %mini);

		%pos = getWords(%transform, 0, 2);
		%rot = getWords(%transform, 3, 6);
		%hBotType = BlockheadHoleBot; //this should be changeable
		%hDamageType = %hBotType.hMeleeCI;

		%player = new AiPlayer()
		{
			client = %this;
			dataBlock = %handler.playerDatablock;
			isHoleBot = 1;
			isSlayerBot = 1;
			path = "";
			spawnBrick = %spawnBrick;

			position = %pos;
			hGridPosition = %pos;
			rotation = %rot;
					
			//Apply attributes to Bot
			Name = %hBotType.hName;
			hType = "mercenary"; //%hBotType.hType;
			hDamageType = (strLen(%damageType) > 0 ? $DamageType["::" @ %damageType] : $DamageType::HoleMelee);
			hSearchRadius = %hBotType.hSearchRadius;
			hSearch = %hBotType.hSearch;
			hSight = %hBotType.hSight;
			hWander = %hBotType.hWander;
			hGridWander = %hBotType.hGridWander;
			hReturnToSpawn = !%useFakeBrick; //%hBotType.hReturnToSpawn;
			hSpawnDist = %hBotType.hSpawnDist;
			hMelee = %hBotType.hMelee;
			hAttackDamage = %hBotType.hAttackDamage;
			hSpazJump = %hBotType.hSpazJump;
			hSearchFOV = %hBotType.hSearchFOV;
			hFOVRadius = %hBotType.hFOVRadius;
			hTooCloseRange = %hBotType.hTooCloseRange;
			hAvoidCloseRange = %hBotType.hAvoidCloseRange;
			hShoot = %hBotType.hShoot;
			hMaxShootRange = %hBotType.hMaxShootRange;
			hStrafe = %hBotType.hStrafe;
			hAlertOtherBots = %hBotType.hAlertOtherBots;
			hIdleAnimation = %hBotType.hIdleAnimation;
			hSpasticLook = %hBotType.hSpasticLook;
			hAvoidObstacles = %hBotType.hAvoidObstacles;
			hIdleLookAtOthers = %hBotType.hIdleLookAtOthers;
			hIdleSpam = false; //%hBotType.hIdleSpam;
			hAFKOmeter = %hBotType.hAFKOmeter + getRandom( 0, 2 );
			hHearing = %hBotType.hHearing;
			hIdle = %hBotType.hIdle;
			hSmoothWander = %hBotType.hSmoothWander;
			hEmote = %hBotType.hEmote;
			hSuperStacker = %hBotType.hSuperStacker;
			hNeutralAttackChance = %hBotType.hNeutralAttackChance;
			hFOVRange = %hBotType.hFOVRange;
			hMoveSlowdown = %hBotType.hMoveSlowdown;
			hMaxMoveSpeed = 1.0;
			hActivateDirection = %hBotType.hActivateDirection;
		};
		%this.player = %player;
		missionCleanup.add(%player);

		for(%i = 0; %i < %handler.playerDatablock.maxTools; %i ++)
			%this.forceEquip(%i, %handler.startEquip[%i]);

		if(%handler == %team)
		{
			%ps = %team.playerScale;
			if(%ps != 1)
				%player.setScale(%ps SPC %ps SPC %ps);

			%this.applyUniform();
		}

		%player.setShapeNameColor(%handler.colorRGB);

		%spawnBrick.hBot = %player;
		%spawnBrick.onBotSpawn();

		return %player;
	}
	else
		return parent::createPlayer(%this, %transform);
}

//Called after an AiPlayer has been spawned.
function Slayer_AiController::onSpawn(%this)
{
	if(%this.isSlayerBot)
	{
		%mini = %this.minigame;
		%team = %this.slyrTeam;

		%this.player.useRandomTool();

		//Add rally nodes to the bot's objective queue.
		// for(%i = Slayer.pathHandler.rallyNodes.getCount() - 1; %i >= 0; %i --)
		// {
			// %node = Slayer.pathHandler.rallyNodes.getObject(%i);

			// %blid = %node.getGroup().bl_id;
			// if(%canUse[%blid] $= "")
				// %canUse[%blid] = miniGameCanUse(%this, %node);

			// if(%canUse[%blid])
			// {
				// %control = %node.getTeamControl();
				// if(%control == %team.color || %mini.teams.isNeutralColor(%control))
				// {
					// %this.addObjective(%node, $Slayer::Server::Bots::Priority["RN"], "RN");
				// }
			// }
		// }

		//We need to direct bots to their objectives.
		//Let's make sure that we don't have to
		//direct all the bots at once.
		%time = 100 + getRandom(900);
		%this.scheduleNoQuota(%time, "goToNextObjective", true);
	}
	else
		return parent::onSpawn(%this);
}

//Called when a Slayer_AiController's AiPlayer is killed.
function Slayer_AiController::onDeath(%this, %obj, %killer, %type, %area)
{
	parent::onDeath(%this, %obj, %killer, %type, %area);
	%mini = getMinigameFromObject(%this);
	if(isSlayerMinigame(%mini) && %this.isSlayerBot && !isObject(%this.spawnBrick))
	{
		if(!%this.dead())
		{
			%spawnTime = %mini.botRespawnTime;
			%this.respawnSchedule = %this.scheduleNoQuota(%spawnTime, "spawnPlayer");
		}
	}
}

function Slayer_AiController::applyBodyColors(%this)
{
	parent::applyBodyColors(%this);
	if(%this.isSlayerBot && isObject(%player = %this.player))
	{
		%index = getRandom($Slayer::Server::Bots::SkinColorCount);
		%color = $Slayer::Server::Bots::SkinColor[%index];
		if(%color !$= "")
		{
			%player.setNodeColor(headSkin, %color);
			%player.setNodeColor(lHand, %color);
			%player.setNodeColor(rHand, %color);
		}
	}
}

function Slayer_AiController::applyUniform(%this)
{
	return GameConnection::applyUniform(%this);
}

function Slayer_AiController::getTeam(%this)
{
	return GameConnection::getTeam(%this);
}

function Slayer_AiController::getLives(%this)
{
	return GameConnection::getLives(%this);
}

function Slayer_AiController::setLives(%this, %flag)
{
	return GameConnection::setLives(%this, %flag);
}

function Slayer_AiController::addLives(%this, %flag)
{
	return GameConnection::addLives(%this, %flag);
}

function Slayer_AiController::dead(%this)
{
	return GameConnection::dead(%this);
}

function Slayer_AiController::setDead(%this, %flag)
{
	return GameConnection::setDead(%this, %flag);
}

function Slayer_AiController::setKills(%this, %flag)
{
	return GameConnection::setKills(%this, %flag);
}

function Slayer_AiController::setDeaths(%this, %flag)
{
	return GameConnection::setDeaths(%this, %flag);
}

function Slayer_AiController::getKills(%this, %flag)
{
	return GameConnection::getKills(%this, %flag);
}

function Slayer_AiController::addKills(%this, %flag)
{
	return GameConnection::addKills(%this, %flag);
}

function Slayer_AiController::getDeaths(%this, %flag)
{
	return GameConnection::getDeaths(%this, %flag);
}

function Slayer_AiController::addDeaths(%this, %flag)
{
	return GameConnection::addDeaths(%this, %flag);
}

function Slayer_AiController::forceEquip(%this, %slot, %item)
{
	return GameConnection::forceEquip(%this, %slot, %item);
}

function Slayer_AiController::updateEquip(%this, %slot, %new, %old)
{
	return GameConnection::updateEquip(%this, %slot, %new, %old);
}

function Slayer_AiController::instantRespawn(%this)
{
	return GameConnection::instantRespawn(%this);
}

function Slayer_AiController::resetRespawnTime(%this)
{
	return GameConnection::resetRespawnTime(%this);
}

function Slayer_AiController::setRespawnTime(%this, %flag)
{
	return GameConnection::setRespawnTime(%this, %flag);
}

function Slayer_AiController::incRespawnTime(%this, %flag)
{
	return GameConnection::incRespawnTime(%this, %flag);
}

//This controls what bots do when they reach a capture point.
//@param	Slayer_MinigameSO mini
//@param	AiController ai
//@param	Grid3D objective	The objective that was reached.
//@param	FxDtsBrick brick	The capture point brick.
function Slayer_onBotObjectiveReached_CP(%mini, %ai, %objective, %brick)
{
	%bot = %ai.player;
	if(!isObject(%bot))
		return;

	%team = %ai.getTeam();

	if(!isObject(%team) || %brick.getTeamControl() == %team)
		return;
	else
	{
		//Let's make sure there are only 3 or less people capturing a CP.
		//If more, redirect them.
		%trigger = %brick.cpTrigger;
		%count = 0;
		for(%i =0; %i < %trigger.getNumObjects(); %i ++)
		{
			%obj = %trigger.getObject(%i);
			if(%obj.client.slyrTeam == %team)
				%count ++;
		}

		if(%count <= 3)
		{
			%bot.hOldWander = %bot.hWander;
			%bot.hWander = false;
			%bot.hOldAvoidObstacles = %bot.hAvoidObstacles;
			%bot.hAvoidObstacles = false;

			%bot.hClearMovement();

			if(getRandom(0, 2))
				%bot.hCrouch(1500 + getRandom(1000, 6000));

			//returning -1 prevents the bot from
			//continuing to the next objective
			return -1;
		}
	}
}

//This controls what bots do when they reach a rally node.
//@param	Slayer_MinigameSO mini
//@param	AiController ai
//@param	ScriptObject objective	The objective that was reached.
//@param	FxDtsBrick brick	The rally node brick.
function Slayer_onBotObjectiveReached_RN(%mini, %bot, %objective, %brick)
{

}