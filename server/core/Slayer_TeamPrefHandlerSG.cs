// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_TeamPrefHandlerSG::onAdd(%this)
{
	
}

//Returns the Slayer_TeamPrefSO preference object.
//@param	string category
//@param	string title
//@return	Slayer_PrefSO
function Slayer_TeamPrefHandlerSG::getPrefSO(%this, %category, %title)
{
	return Slayer_PrefHandlerSG::getPrefSO(%this, %category, %title);
}

//Resets all preferences to their default values.
//@param	Slayer_TeamSO team
function Slayer_TeamPrefHandlerSG::resetPreferences(%this, %team)
{
	if(isObject(%team))
		Slayer_PrefHandlerSG::resetPreferences(%this, %team);
}

//Exports all team preference settings from a specific minigame to a file.
//@param	string path
//@param	Slayer_MinigameSO mini
function Slayer_TeamPrefHandlerSG::exportTeamPreferences(%this, %path, %mini)
{
	if(!isWriteableFileName(%path))
	{
		Slayer_Support::error("Slayer_TeamPrefHandlerSG::exportTeamPreferences", "Invalid file path" SPC %path);
		return;
	}
	if(!isSlayerMiniGame(%mini) || !isObject(%mini.teams))
	{
		Slayer_Support::error("Slayer_TeamPrefHandlerSG::exportTeamPreferences", "Invalid minigame" SPC %mini);
		return;
	}
	
	%file = new FileObject();
	%file.openForWrite(%path);
	%file.writeLine("SLAYERTEAMCONFIG 1.0");
	
	%teamCount = %mini.teams.getCount();
	for(%i = 0; %i < %teamCount; %i ++)
	{
		%team = %mini.teams.getObject(%i);
		%file.writeLine("");
		for(%j = %this.getCount() - 1; %j >= 0; %j --)
		{
			%pref = %this.getObject(%j);
			%value = %pref.getValue(%team);
			if(%pref.type $= "object")
				%value = isObject(%value) ? %value.uiName : "";
			%file.writeLine(
				"\"" @ expandEscape(%pref.category) @ "\"," @
				"\"" @ expandEscape(%pref.title) @ "\"," @
				"\"" @ expandEscape(%value) @ "\"");
		}
	}
	
	%file.close();
	%file.delete();
}

//Imports team preference settings from a file.
//@param	string path
//@param	Slayer_MinigameSO mini
function Slayer_TeamPrefHandlerSG::importTeamPreferences(%this, %path, %mini)
{
	if(!isFile(%path))
	{
		Slayer_Support::error("Slayer_TeamPrefHandlerSG::importTeamPreferences", "Invalid file path" SPC %path);
		return;
	}
	if(!isSlayerMiniGame(%mini))
	{
		Slayer_Support::error("Slayer_TeamPrefHandlerSG::importTeamPreferences", "Invalid minigame" SPC %mini);
		return;
	}
	
	%file = new FileObject();
	%file.openForRead(%path);
	
	if(%file.readLine() $= "SLAYERTEAMCONFIG 1.0")
	{
		%csv = CSVReader(",");
		while(!%file.isEOF())
		{
			%line = %file.readLine();
			if(!strLen(%line))
			{
				%lastLineBlank = true;
				continue;
			}
			if(%lastLineBlank)
			{
				%team = %mini.teams.addTeam();
				%lastLineBlank = false;
			}
			
			%csv.setDataString(%line);
			%category = collapseEscape(%csv.readNextValue());
			%title = collapseEscape(%csv.readNextValue());
			%value = collapseEscape(%csv.readNextValue());
			%pref = %this.getPrefSO(%category, %title);
			if(isObject(%pref) && !%csv.hasNextValue())
			{
				if(%pref.type $= "object")
				{
					if(strLen(%value))
						%value = Slayer_Support::getIDFromUiName(%value, %pref.object_class);
					else
						%value = 0;
				}
				%pref.setValue(%value, %team);
			}
			else
				Slayer_Support::error("Invalid config file line", %line);
		}
	}
	else
	{
		Slayer_Support::error("Slayer_TeamPrefHandlerSG::importTeamPreferences", "Incompatible preference file" SPC %path);	
	}
	
	%file.close();
	%file.delete();
}

//Sends preference data to a client.
//@param	GameConnection client
function Slayer_TeamPrefHandlerSG::sendPreferenceData(%this, %client)
{
	//START THE TEAM PREF TRANSFER
	commandToClient(%client, 'Slayer_getTeamPrefs_Start', %this.getCount());

	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %this.getObject(%i);
		if(%pref.doNotSendToClients)
			continue;
		transferObjectToClient(%client, %pref, false, "SlayerClient_TeamPrefSO");
	}

	//END THE TEAM PREF TRANSFER
	commandToClient(%client, 'Slayer_getTeamPrefs_End');
}

//Sends preference values to a client.
//@param	GameConnection client
//@param	Slayer_MinigameSO mini
function Slayer_TeamPrefHandlerSG::sendPreferenceValues(%this, %client, %mini)
{
	%container = new ScriptObject();
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %this.getObject(%i);
		if(isObject(%mini))
		{
			if(!%mini.canEdit(%client, %pref.permissionLevel))
			{
				%container.noEdit[%pref.variable] = true;
			}
		}
		else
		{
			if(%pref.permissionLevel <= 2 && %client.getAdminLvl() > %pref.permissionLevel)
			{
				%container.noEdit[%pref.variable] = true;
			}
		}
	}
	transferObjectToClient(%client, %container, false, "Slayer_TeamPrefPermissionsSO");
	%container.delete();
}

function serverCmdSlayer_removeTeam(%client, %team)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);

	if(!isSlayerMinigame(%mini))
		return;
	if(!%mini.canEdit(%client))
		return;

	if(isObject(%team) && %team.class $= "Slayer_TeamSO"
		&& %mini.teams.isMember(%team) && !%team.template.disable_delete)
	{
		%mini.teams.removeTeam(%team);
	}
}

function serverCmdSlayer_getTeamPrefs_Start(%client, %team, %template, %count)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);
	if(!isSlayerMinigame(%mini) || !%mini.canEdit(%client))
		return;

	//check if it's a new team
	if(%team < 1)
	{
		if(isObject(%mini.teams.newTeam))
		{
			Slayer_Support::Error("Cannot create teams concurrently!");
			return;
		}
		if(%mini.gameMode.template.disable_teamCreation && !isObject(%template))
		{
			messageClient(%client, '', "You may not create teams in this game mode.");
			return;
		}
		%team = %mini.teams.addTeam();
		if(!isObject(%team))
		{
			messageClient(%client, '', "Team creation failed.");
			return;
		}
		if(isObject(%template))
		{
			%team.template = %template;
			%mini.gameMode.applyDefaultTeamPreferences(%team, %template);
		}
		%mini.teams.newTeam = %team;
	}
	else if(isObject(%team) && %count > 0 && %mini.notifyOnChange)
	{
		%message = '\c3 + \c5Updated %1 \c5(team).';
		%mini.messageAll('', %message, %team.getColoredName());
	}
}

function serverCmdSlayer_getTeamPrefs_Tick(%client, %team, %pref, %value)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);
	if(!isSlayerMinigame(%mini) || !%mini.canEdit(%client))
		return;

	if(%team < 1)
		%team = %mini.teams.newTeam;
	if(!isObject(%team) || !%mini.teams.isMember(%team))
		return;
	if(!isObject(%pref) || !%mini.teamPrefs.isMember(%pref))
		return;
	if(!%mini.canEdit(%client, %pref.permissionLevel))
		return;

	Slayer_Support::Debug(2, "Received Team Pref", %client SPC %team SPC %pref SPC %value);

	if(%pref.getValue(%team) $= %value)
		return;
	%success = %pref.setValue(%value, %team);
	if(%success)
	{
		if(%team != %mini.Teams.newTeam && %pref.notifyPlayersOnChange && %mini.notifyOnChange)
		{
			%val = %pref.getDisplayValue(%value);
			%message = '\c3 + + \c5[\c3%1\c5|\c3%2\c5] is now \c3%3';
			%mini.messageAll('', %message, %pref.category, %pref.title, %val);
		}
	}
}

function serverCmdSlayer_getTeamPrefs_End(%client, %team)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);
	if(!isSlayerMinigame(%mini) || !%mini.canEdit(%client))
		return;
	if(%team < 1)
	{
		%mini.teams.onNewTeam(%mini.teams.newTeam);
		%mini.teams.newTeam = 0;
		%mini.notifyNewTeam ++;
	}
}

//----------------------------------------------------------------------
// Deprecated Functions - DO NOT USE
//----------------------------------------------------------------------

//@DEPRECATED
function Slayer_TeamPrefHandlerSG::addPref(%this, %category, %title, %variable,
	%type, %defaultValue, %notify, %editRights, %list, %callback)
{
	warn("\c2WARN: Slayer_TeamPrefHandlerSG::addPref is deprecated! Use the new API.");

	%typeVals = getWords(%type, 1);
	%type = getWord(%type, 0);

	%pref = new ScriptObject(Slayer_TeamPrefSO)
	{
		category = %category;
		title = %title;
		defaultValue = %defaultValue;
		permissionLevel = %editRights;
		variable = %variable;
		type = %type; //need to retain the rest of the type info
		callback = %callback;
		guiTag = %list;
		notifyPlayersOnChange = %notify;
	};
	
	switch$(%type)
	{
		case "list": %pref.list_items = strReplace(%typeVals, "\t", "\n");
		case "int":
			%pref.int_minValue = getWord(%typeVals, 0);
			%pref.int_maxValue = getWord(%typeVals, 1);
		case "string": %pref.string_maxLength = getWord(%typeVals, 0);
		case "slide":
			%pref.slide_minValue = getWord(%typeVals, 0);
			%pref.slide_maxValue = getWord(%typeVals, 1);
			%pref.slide_numTicks = getWord(%typeVals, 2);
			%pref.slide_snapToTicks = getWord(%typeVals, 3);
	}
	
	return %pref;
}

//@DEPRECATED
function Slayer_TeamPrefHandlerSG::setPref(%this, %team, %category, %title, %value)
{
	warn("\c2WARN: Slayer_TeamPrefHandlerSG::setPref is deprecated! Use the new API.");

	if(!isObject(%team))
		return;

	%pref = %this.getPrefSO(%category, %title);
	if(!isObject(%pref))
		return;

	return %pref.setValue(%value, %team);
}

//@DEPRECATED
function Slayer_TeamPrefHandlerSG::getPref(%this, %team, %category, %title) //get the value of a pref
{
	warn("\c2WARN: Slayer_TeamPrefHandlerSG::getPref is deprecated! Use the new API.");

	if(!isObject(%team))
		return;

	%pref = %this.getPrefSO(%category, %title);
	if(!isObject(%pref))
		return;

	return %pref.getValue(%team);
}

//@DEPRECATED
//Resets all preferences to their default values.
function Slayer_TeamPrefHandlerSG::resetPrefs(%this, %mini)
{
	warn("\c2WARN: Slayer_TeamPrefHandlerSG::resetPrefs is deprecated! Use the new API.");
	%this.resetPreferences(%mini);
}