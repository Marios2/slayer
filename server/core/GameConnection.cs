// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

$Slayer::Server::spamTimeout = 0.5; //time, in seconds

$Slayer::Server::Spectate::Mode["None"] = 0;
$Slayer::Server::Spectate::Mode["Free"] = 1;
$Slayer::Server::Spectate::Mode["Orbit"] = 2;
$Slayer::Server::Spectate::Mode["Auto"] = 3;

//Adds a client to a team. Used for events.
//@param	string flag	The name of the team.
//@see Slayer_TeamSO::addMember
function GameConnection::joinTeam(%this, %flag, %reason, %noRespawn)
{
	%mini = getMinigameFromObject(%this);
	if(!isSlayerMinigame(%mini))
		return;

	%team = %mini.Teams.getTeamFromName(%flag);
	if(isObject(%team))
	{
		%team.addMember(%this, %reason, %noRespawn, %noRespawn); //noRespawn auto-disables equipping with team items
	}
}

//@return	Slayer_TeamSO	The client's team.
function GameConnection::getTeam(%this)
{
	%mini = getMinigameFromObject(%this);
	if(!isSlayerMinigame(%mini) ||
		!%mini.gameMode.template.useTeams ||
		!isObject(%this.SlyrTeam))
	{
		return 0;
	}
	return %this.SlyrTeam;
}

//@return	int
function GameConnection::getKills(%this)
{
	return %this.Slyr_Kills;
}

//@param	int flag
function GameConnection::setKills(%this, %flag)
{
	%this.Slyr_Kills = %flag;
}

//@param	int flag
function GameConnection::addKills(%this, %flag)
{
	%this.setKills(%this.Slyr_Kills + %flag);
}

//@return	int
function GameConnection::getDeaths(%this)
{
	return %this.Slyr_Deaths;
}

//@param	int flag
function GameConnection::setDeaths(%this, %flag)
{
	%this.Slyr_Deaths = %flag;
}

//@param	int flag
function GameConnection::addDeaths(%this, %flag)
{
	%this.setDeaths(%this.Slyr_Deaths + %flag);
}

//@return	int	The number of lives that this client has left.
function GameConnection::getLives(%this)
{
	return %this.lives;
}

//@param	int flag
function GameConnection::setLives(%this, %flag)
{
	%this.lives = %flag;
}

//@param	int flag
function GameConnection::addLives(%this, %flag)
{
	%this.setLives(%this.lives + %flag);
}

//@return	bool	Whether this client is dead.
function GameConnection::Dead(%this)
{
	return %this.Dead;
}

//Sets whether the client is dead.
//@param	int flag
function GameConnection::setDead(%this, %flag)
{
	%this.Dead = %flag;
}

//@return	int
function GameConnection::getScore(%this)
{
	return %this.score;
}

//Applies the client's team uniform to their player.
function GameConnection::applyUniform(%this)
{
	%player = %this.player;
	if(!isObject(%player))
		return;
	%team = %this.getTeam();
	if(!isObject(%team))
		return;

	%this.applyingUniform = true;
	
	%color = %team.colorRGB;
	%player.setShapeNameColor(%color);
	
	%db = %player.getDatablock();
	%shapeFile = fileName(%db.shapeFile);

	//check if this playertype uses the default model
	if(%shapeFile $= "m.dts" || %db.uniformCompatible || %team.uniform == 0)
	{
		switch(%team.uniform)
		{
			case 0: //no uniform
				%this.applyBodyParts();
				%this.applyBodyColors();

			case 1: //shirt only
				%this.applyBodyParts();
				%this.applyBodyColors();

				%player.setNodeColor(chest, %color);
				%player.setNodeColor(femChest, %color);
				%player.setNodeColor(armor, %color);
				%player.setNodeColor(bucket, %color);
				%player.setNodeColor(cape, %color);
				%player.setNodeColor(pack, %color);
				%player.setNodeColor(quiver, %color);
				%player.setNodeColor(tank, %color);

			case 2: //full uniform
				%this.applyBodyColors();
				hideAllNodes(%player);

				%player.unHideNode(copHat);
				%player.unHideNode(headSkin);
				%player.unHideNode(chest);
				%player.unHideNode(pants);
				%player.unHideNode(lShoe);
				%player.unHideNode(rShoe);
				%player.unHideNode(lArm);
				%player.unHideNode(rArm);
				%player.unHideNode(lHand);
				%player.unHideNode(rHand);

				%player.setNodeColor(copHat, %color);
				%player.setNodeColor(chest, %color);
				%player.setNodeColor(pants, %color);
				%player.setNodeColor(lShoe, %color);
				%player.setNodeColor(rShoe, %color);
				%player.setNodeColor(larm, %color);
				%player.setNodeColor(rarm, %color);
				
				// skin color
				if(!strLen(%player.skinColor))
				{
					%index = getRandom($Slayer::Server::Bots::SkinColorCount);
					%player.skinColor = $Slayer::Server::Bots::SkinColor[%index];
					if(!strLen(%player.skinColor))
						%player.skinColor = "1 0.878431 0.611765 1";
				}
				%player.setNodeColor(lHand, %player.skinColor);
				%player.setNodeColor(rHand, %player.skinColor);
				%player.setNodeColor(headSkin, %player.skinColor);

			case 3: //custom uniform
				for(%i = 0; %i < Slayer.TeamPrefs.getCount(); %i ++)
				{
					%p = Slayer.TeamPrefs.getObject(%i);
					if(%p.category !$= "Uniform")
						continue;

					%val = %team.uni_[%p.title];
					if(%val $= "TEAMCOLOR")
						%val = %color;

					%uni_[%p.title] = %val;
				}

				%this.applyBodyColors();
				hideAllNodes(%player);

				//head
				%player.unHideNode(headSkin);
				%player.setNodeColor(headSkin, %uni_headColor);

				//chest
				switch(%uni_chest)
				{
					case 0:
						%player.unHideNode(chest);
						%player.setNodeColor(chest, %uni_torsoColor);
					case 1:
						%player.unHideNode(femChest);
						%player.setNodeColor(femChest, %uni_torsoColor);
				}

				//arms
				switch(%uni_lArm)
				{
					case 0:
						%player.unHideNode(lArm);
						%player.setNodeColor(lArm, %uni_lArmColor);
					case 1:
						%player.unHideNode(lArmSlim);
						%player.setNodeColor(lArmSlim, %uni_lArmColor);
				}
				switch(%uni_rArm)
				{
					case 0:
						%player.unHideNode(rArm);
						%player.setNodeColor(rArm, %uni_rArmColor);
					case 1:
						%player.unHideNode(rArmSlim);
						%player.setNodeColor(rArmSlim, %uni_rArmColor);
				}

				//hands
				switch(%uni_lHand)
				{
					case 0:
						%player.unHideNode(lHand);
						%player.setNodeColor(lHand, %uni_lHandColor);
					case 1:
						%player.unHideNode(lHook);
						%player.setNodeColor(lHook, %uni_lHandColor);
				}
				switch(%uni_rHand)
				{
					case 0:
						%player.unHideNode(rHand);
						%player.setNodeColor(rHand, %uni_rHandColor);
					case 1:
						%player.unHideNode(rHook);
						%player.setNodeColor(rHook, %uni_rHandColor);
				}

				//legs and pants/hips
				switch(%uni_hip)
				{
					case 0:
						%player.unHideNode(pants);
						%player.setNodeColor(pants, %uni_hipColor);
						switch(%uni_lLeg)
						{
							case 0:
								%player.unHideNode(lShoe);
								%player.setNodeColor(lShoe, %uni_lLegColor);
							case 1:
								%player.unHideNode(lPeg);
								%player.setNodeColor(lPeg, %uni_lLegColor);
						}
						switch(%uni_rLeg)
						{
							case 0:
								%player.unHideNode(rShoe);
								%player.setNodeColor(rShoe, %uni_rLegColor);
							case 1:
								%player.unHideNode(rPeg);
								%player.setNodeColor(rPeg, %uni_rLegColor);
						}
					case 1:
						%player.unHideNode(skirtHip);
						%player.setNodeColor(skirtHip, %uni_hipColor);
						%player.unHideNode(skirtTrimLeft);
						%player.setNodeColor(skirtTrimLeft, %uni_lLegColor);
						%player.unHideNode(skirtTrimRight);
						%player.setNodeColor(skirtTrimRight, %uni_rLegColor);
				}

				//hat
				switch(%uni_hat)
				{
					case 1:
						%player.unHideNode(helmet);
						%player.setNodeColor(helmet, %uni_hatColor);
					case 2:
						%player.unHideNode(pointyHelmet);
						%player.setNodeColor(pointyHelmet, %uni_hatColor);
					case 3:
						%player.unHideNode(flareHelmet);
						%player.setNodeColor(flareHelmet, %uni_hatColor);
					case 4:
						%player.unHideNode(scoutHat);
						%player.setNodeColor(scoutHat, %uni_hatColor);
					case 5:
						%player.unHideNode(bicorn);
						%player.setNodeColor(bicorn, %uni_hatColor);
					case 6:
						%player.unHideNode(copHat);
						%player.setNodeColor(copHat, %uni_hatColor);
					case 7:
						%player.unHideNode(knitHat);
						%player.setNodeColor(knitHat, %uni_hatColor);
				}

				//pack
				switch(%uni_pack)
				{
					case 1:
						%player.unHideNode(armor);
						%player.setNodeColor(armor, %uni_packColor);
					case 2:
						%player.unHideNode(bucket);
						%player.setNodeColor(bucket, %uni_packColor);
					case 3:
						%player.unHideNode(cape);
						%player.setNodeColor(cape, %uni_packColor);
					case 4:
						%player.unHideNode(pack);
						%player.setNodeColor(pack, %uni_packColor);
					case 5:
						%player.unHideNode(quiver);
						%player.setNodeColor(quiver, %uni_packColor);
					case 6:
						%player.unHideNode(tank);
						%player.setNodeColor(tank, %uni_packColor);
				}
				%player.setHeadUp(%uni_pack > 0);

				//secondPack
				switch(%uni_secondPack)
				{
					case 1:
						%player.unHideNode(epaulets);
						%player.setNodeColor(epaulets, %uni_secondPackColor);
					case 2:
						%player.unHideNode(epauletsRankA);
						%player.setNodeColor(epauletsRankA, %uni_secondPackColor);
					case 3:
						%player.unHideNode(epauletsRankB);
						%player.setNodeColor(epauletsRankB, %uni_secondPackColor);
					case 4:
						%player.unHideNode(epauletsRankC);
						%player.setNodeColor(epauletsRankC, %uni_secondPackColor);
					case 5:
						%player.unHideNode(epauletsRankD);
						%player.setNodeColor(epauletsRankD, %uni_secondPackColor);
					case 6:
						%player.unHideNode(shoulderPads);
						%player.setNodeColor(shoulderPads, %uni_secondPackColor);
				}

				//accent
				switch(%uni_hat)
				{
					case 1:
						switch(%uni_accent)
						{
							case 1:
								%player.unHideNode(visor);
								%player.setNodeColor(visor, %uni_accentColor);
						}
					default:
						switch(%uni_accent)
						{
							case 1:
								%player.unHideNode(plume);
								%player.setNodeColor(plume, %uni_accentColor);
							case 2:
								%player.unHideNode(triPlume);
								%player.setNodeColor(triPlume, %uni_accentColor);
							case 3:
								%player.unHideNode(septPlume);
								%player.setNodeColor(septPlume, %uni_accentColor);
						}
				}

				//decals
				%player.setFaceName(fileBase(%uni_faceName));
				%player.setDecalName(fileBase(%uni_decalName));
		}
	}
	else //using a custom model, like the Horse
	{
		%player.setNodeColor("ALL", %color);
	}

	%this.applyingUniform = false;

	Slayer_Support::Debug(2, "GameConnection::applyUniform", %this);
}

//Gives the client's player an item.
//@param	int slot	An integer from 0-maxitems depicting the item's location in the inventory.
//@param	ItemData item	The item to equip.
//@see	GameConnection::updateEquip
function GameConnection::forceEquip(%this, %slot, %item)
{
	%player = %this.player;
	if(!isObject(%player))
		return;

	%item = (isObject(%item) ? %item.getID() : 0);

	%oldTool = %player.tool[%slot];
	%player.tool[%slot] = %item;

	messageClient(%this, 'MsgItemPickup', '', %slot, %item);

	if(!isObject(%oldTool))
		%player.weaponCount ++;
}

//Gives the client's player an item if their current one is the same as %old.
//@param	int slot	An integer from 0-maxitems depicting the item's location in the inventory.
//@param	ItemData new	The item to equip.
//@param	ItemData old	The previous, required item.
//@see	GameConnection::forceEquip
function GameConnection::updateEquip(%this, %slot, %new, %old)
{
	%player = %this.player;
	if(!isObject(%player))
		return;

	if(%player.tool[%slot] != %old && (isObject(%player.tool[%slot]) || isObject(%old)))
		return;

	if(%player.tool[%slot] == %new)
		return;

	%this.forceEquip(%slot, %new);
}

//Places a client in spectator mode.
function GameConnection::spectateInit(%this)
{
	%this.isSpectator = true;
	// if(%this.minigame.spectateAutoCam)
		// %target = %this.spectateAutoCam();
	// if(!isObject(%target))
	// {
		%target = %this.spectateNextTarget();
		if(!isObject(%target))
		{
			%this.spectateFree();
		}
	// }
	return %target;
}

//Removes a client from spectator mode.
function GameConnection::spectateEnd(%this)
{
	%this.spectateMode = $Slayer::Server::Spectate::Mode["None"];
	%this.isSpectator = false;
	if(isObject(%this.pathCamera))
		%this.pathCamera.setState("stop");
	if(isEventPending(%this.spectateStartSched))
		cancel(%this.spectateStartSched);
	if(isEventPending(%this.autoCamRetrySched))
		cancel(%this.autoCamRetrySched);
	if(isObject(%this.player))
		%this.setControlObject(%this.player);
	clearBottomPrint(%this);
}

//Changes to the next spectate mode.
function GameConnection::spectateChangeMode(%this)
{
	switch(%this.spectateMode)
	{
		case $Slayer::Server::Spectate::Mode["Free"]:
			if(%this.minigame.spectateAutoCam)
			{
				%this.spectateAutoCam();
			}
			else
			{
				%this.spectateNextTarget();
			}	

		case $Slayer::Server::Spectate::Mode["Orbit"]:
			if(!%this.minigame.teams_teamOnlyDeadCam || !isObject(%this.getTeam()))
			{
				%this.spectateFree();
			}
			else if(%this.minigame.spectateAutoCam)
			{
				%this.spectateAutoCam();
			}
			else
			{
				%this.centerPrint("<div:1>\n<div:1>\c0No other camera modes available.\n<div:1>", 5);
			}

		case $Slayer::Server::Spectate::Mode["Auto"]:
			if(isObject(%this.pathCamera))
			{
				%this.pathCamera.setState("stop");
			}
			if(isEventPending(%this.autoCamRetrySched))
			{
				cancel(%this.autoCamRetrySched);
			}
			%this.spectateNextTarget();
	}
}

//Enters free-camera-spectate mode.
function GameConnection::spectateFree(%this)
{
	%cam = %this.camera;
	%mini = getMinigameFromObject(%this);
	if(!isObject(%cam) || !isSlayerMinigame(%mini))
		return;

	%this.spectateMode = $Slayer::Server::Spectate::Mode["Free"];
	%this.camera.setMode("observer");
	%this.setControlObject(%this.camera);
	if(!%mini.isResetting())
	{
		%this.bottomPrint("<just:right>\c3Free Camera "
			NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Use Light\c5] ", -1, 1);
	}
	%mini.gameMode.callback("onClientSpectateFree", %this);
}

//Goes to the next target in target-spectate mode.
function GameConnection::spectateNextTarget(%this)
{
	%camera = %this.camera;
	%mini = getMinigameFromObject(%this);
	%team = %this.getTeam();
	if(!isObject(%camera) || !isSlayerMinigame(%mini))
		return;

	%this.spectateMode = $Slayer::Server::Spectate::Mode["Orbit"];
	%restart = false;
	%targetCount = %mini.spectateTargets.getCount();
	%i = strLen(%this.camTargetIdx) ? %this.camTargetIdx + 1 : 0;
	if(%i >= %targetCount)
		%i = 0;
	while(%i < %targetCount)
	{
		%obj = %mini.spectateTargets.getObject(%i);
		%class = %obj.getClassName();
		if(%class $= "GameConnection" || %class $= "AiController")
		{
			%objTeam = %obj.getTeam();
			if(isObject(%obj.player) && (!%mini.teams_teamOnlyDeadCam || !%team || %team == %objTeam))
			{
				if(!%mini.isResetting())
				{
					%color = isObject(%objTeam) ? %objTeam.getColorHex() : "\c3";
					%name = %color @ (%class $= "GameConnection" ? %obj.getPlayerName() : "Bot");
					%this.bottomPrint("<just:right>\c3Spectating: " @ %name @ " "
						NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Use Light\c5] "
						NL "\c5[\c3Next\c5|\c3Fire\c5] [\c3Previous\c5|\c3Alt-Fire\c5] ", -1, 1);
				}
				%camera.setMode("corpse", %obj.player);
				%this.setControlObject(%camera);
				%this.camTargetIdx = %i;
				%mini.gameMode.callback("onClientSpectateTarget", %this, %obj);
				return %obj;

			}
		}
		else if(%mini.spectateCapturePoints && %class $= "FxDtsBrick" && minigameCanUse(%this, %obj))
		{
			%control = %obj.getTeamControl();
			if(!%mini.teams_teamOnlyDeadCam || !%team || %team.color == %control)
			{
				if(!%mini.isResetting())
				{
					%color = "<color:" @ Slayer_Support::RgbToHex(getColorIDTable(%control)) @ ">";
					%objName = trim(strReplace(%obj.getName(), "_", " "));
					%name = %color @ (strLen(%objName) ? %objName : "Capture Point");
					%this.bottomPrint("<just:right>\c3Spectating: " @ %name @ " "
						NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Use Light\c5] "
						NL "\c5[\c3Next\c5|\c3Fire\c5] [\c3Previous\c5|\c3Alt-Fire\c5] ", -1, 1);
				}
				%pos = vectorAdd(%obj.position, "0 0 1.2");
				%camera.setOrbitPointMode(%pos, 4.5);
				%this.setControlObject(%camera);
				%this.camTargetIdx = %i;
				%mini.gameMode.callback("onClientSpectateTarget", %this, %obj);
				return %obj;
			}
		}
		
		%i ++;
		if(!%restart && %i >= %targetCount)
		{
			%restart = true;
			%i = 0;
		}
	}
	
	if(!%mini.isResetting())
	{
		%this.bottomPrint("<just:right>\c3No Target "
			NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Use Light\c5] "
			NL "\c5[\c3Next\c5|\c3Fire\c5] [\c3Previous\c5|\c3Alt-Fire\c5] ", -1, 1);
	}
	%mini.gameMode.callback("onClientSpectateTarget", %this, 0);
	return 0;
}

//Goes to the previous target in target-spectate mode.
function GameConnection::spectatePrevTarget(%this)
{
	%camera = %this.camera;
	%mini = getMinigameFromObject(%this);
	%team = %this.getTeam();
	if(!isObject(%camera) || !isSlayerMinigame(%mini))
		return;

	%this.spectateMode = $Slayer::Server::Spectate::Mode["Orbit"];
	%restart = false;
	%targetCount = %mini.spectateTargets.getCount();
	%i = strLen(%this.camTargetIdx) ? %this.camTargetIdx - 1 : %targetCount - 1;
	if(%i < 0)
		%i = %targetCount - 1;
	while(%i >= 0)
	{
		%obj = %mini.spectateTargets.getObject(%i);
		%class = %obj.getClassName();
		if(%class $= "GameConnection" || %class $= "AiController")
		{
			%objTeam = %obj.getTeam();
			if(isObject(%obj.player) && (!%mini.teams_teamOnlyDeadCam || !%team || %team == %objTeam))
			{
				if(!%mini.isResetting())
				{
					%color = isObject(%objTeam) ? %objTeam.getColorHex() : "\c3";
					%name = %color @ (%class $= "GameConnection" ? %obj.getPlayerName() : "Bot");
					%this.bottomPrint("<just:right>\c3Spectating: " @ %name @ " "
						NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Use Light\c5] "
						NL "\c5[\c3Next\c5|\c3Fire\c5] [\c3Previous\c5|\c3Alt-Fire\c5] ", -1, 1);
				}
				%camera.setMode("corpse", %obj.player);
				%this.setControlObject(%camera);
				%this.camTargetIdx = %i;
				%mini.gameMode.callback("onClientSpectateTarget", %this, %obj);
				return %obj;

			}
		}
		else if(%mini.spectateCapturePoints && %class $= "FxDtsBrick" && minigameCanUse(%this, %obj))
		{
			%control = %obj.getTeamControl();
			if(!%mini.teams_teamOnlyDeadCam || !%team || %team.color == %control)
			{
				if(!%mini.isResetting())
				{
					%color = "<color:" @ Slayer_Support::RgbToHex(getColorIDTable(%control)) @ ">";
					%objName = trim(strReplace(%obj.getName(), "_", " "));
					%name = %color @ (strLen(%objName) ? %objName : "Capture Point");
					%this.bottomPrint("<just:right>\c3Spectating: " @ %name @ " "
						NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Use Light\c5] "
						NL "\c5[\c3Next\c5|\c3Fire\c5] [\c3Previous\c5|\c3Alt-Fire\c5] ", -1, 1);
				}
				%pos = vectorAdd(%obj.position, "0 0 1.2");
				%camera.setOrbitPointMode(%pos, 4.5);
				%this.setControlObject(%camera);
				%this.camTargetIdx = %i;
				%mini.gameMode.callback("onClientSpectateTarget", %this, %obj);
				return %obj;
			}
		}
		
		%i --;
		if(!%restart && %i < 0)
		{
			%restart = true;
			%i = %targetCount - 1;
		}
	}
	
	if(!%mini.isResetting())
	{
		%this.bottomPrint("<just:right>\c3No Target "
			NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Use Light\c5] "
			NL "\c5[\c3Next\c5|\c3Fire\c5] [\c3Previous\c5|\c3Alt-Fire\c5] ", -1, 1);
	}
	%mini.gameMode.callback("onClientSpectateTarget", %this, 0);
	return 0;
}

//Automatic path-camera spectating.
function GameConnection::spectateAutoCam(%this)
{
	%camera = %this.pathCamera;
	%mini = getMinigameFromObject(%this);
	%team = %this.getTeam();
	if(!isObject(%camera) || !isSlayerMinigame(%mini))
		return;

	%this.spectateMode = $Slayer::Server::Spectate::Mode["Auto"];
	%restart = false;
	%targetCount = %mini.spectateTargets.getCount();
	%i = strLen(%this.camTargetIdx) ? %this.camTargetIdx + 1 : 0;
	if(%i >= %targetCount)
		%i = 0;
	while(%i < %targetCount)
	{
		%obj = %mini.spectateTargets.getObject(%i);
		%class = %obj.getClassName();
		if(%class $= "GameConnection" || %class $= "AiController")
		{
			%objTeam = %obj.getTeam();
			if(isObject(%obj.player) && (!%mini.teams_teamOnlyDeadCam || !%team || %team == %objTeam))
			{
				//find start and end points
				%transform = %obj.player.getTransform();
				if(getRandom(1))
				{
					%relStartTransformZ = "0 0 2.8";
					%relEndTransformZ = "0 0 2.2";
				}
				else
				{
					%relStartTransformZ = "0 0 2.2";
					%relEndTransformZ = "0 0 2.8";
				}
				if(getRandom(1))
				{
					%rot = rotFromTransform(%transform);
					%startTransform = vectorAdd(%transform, %relStartTransformZ);
					%startTransform = vectorAdd(vectorScale(%obj.player.getForwardVector(), -5), %startTransform) SPC %rot;
					%endTransform = vectorAdd(%transform, %relEndTransformZ);
					%endTransform = vectorAdd(vectorScale(%obj.player.getForwardVector(), -0.5), %endTransform) SPC %rot;
				}
				else
				{
					%rot = rotFromTransform(%transform);
					%rot = setWord(%rot, 3, getWord(%rot, 3) + $Slayer::PI);
					%startTransform = vectorAdd(%transform, %relStartTransformZ);
					%startTransform = vectorAdd(vectorScale(%obj.player.getForwardVector(), 5), %startTransform) SPC %rot;
					%endTransform = vectorAdd(%transform, %relEndTransformZ);
					%endTransform = vectorAdd(vectorScale(%obj.player.getForwardVector(), 0.5), %endTransform) SPC %rot;
				}

				//check that path is clear
				%mask = $TypeMasks::PlayerObjectType | $TypeMasks::FxBrickObjectType;
				%ray = containerRayCast(%startTransform, %endTransform, %mask, %obj.player);
				if(!isObject(firstWord(%ray)))
				{
					if(!%mini.isResetting())
					{
						%color = isObject(%objTeam) ? %objTeam.getColorHex() : "\c3";
						%name = %color @ (%class $= "GameConnection" ? %obj.getPlayerName() : "Bot");
						%this.bottomPrint("<just:right>\c3Spectating: " @ %name @ " "
							NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Use Light\c5] ", -1, 1);
					}
					
					//clear all nodes
					for(%j = 0; %j < 20; %j ++)
						%camera.popFront();
					%camera.pushBack(%startTransform, 1, "Normal", "Linear");
					%camera.setPosition(1.0);
					%camera.pushBack(%endTransform, 1, "Normal", "Linear");
					%camera.setTarget(1.0);
					%this.setControlObject(%camera);
					%this.setControlCameraFOV(120);
					%camera.setState("forward");
					
					%this.camTargetIdx = %i;
					%mini.gameMode.callback("onClientSpectateTarget", %this, %obj);
					return %obj;
				}
			}
		}
		
		%i ++;
		if(!%restart && %i >= %targetCount)
		{
			%restart = true;
			%i = 0;
		}
	}
	
	if(!%mini.isResetting())
	{
		%this.bottomPrint("<just:right>\c3Waiting for target... "
			NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Use Light\c5] ", -1, 1);
		cancel(%this.autoCamRetrySched);
		%this.autoCamRetrySched = %this.scheduleNoQuota(1000, "spectateAutoCam");
	}
	%mini.gameMode.callback("onClientSpectateTarget", %this, 0);
	return 0;
}

//@return	int	0=host, 1=super_admin, 2=admin
function GameConnection::getAdminLvl(%this)
{
	if(%this.isHost)
		return 0;
	if(%this.isSuperAdmin)
		return 1;
	if(%this.isAdmin)
		return 2;

	return 3;
}

//Determines whether the client can edit or create a minigame.
//This is used for the GUI.
//@return	int	Corresponds to $Slayer::MinigameState[Edit|Create|None].
function GameConnection::getMinigameState(%this)
{
	if(isObject(%this.minigame) && %this.minigame.canEdit(%this))
	{
		return $Slayer::MinigameState["Edit"];
	}
	else if(Slayer.Minigames.canCreateMinigame(%this))// &&
		// !isObject(Slayer.Minigames.getMinigameFromBLID(%this.getBLID())))
	{
		return $Slayer::MinigameState["Create"];
	}
	return $Slayer::MinigameState["None"];
}

//Use this at the beginning of server commands to prevent spam.
// if(%client.isSpamming()) { return; } else { echo("No spam!"); }
//@return	bool	Whether the client is spamming.
function GameConnection::isSpamming(%this)
{
	%time = $Sim::Time;
	%spam = (%time - %this.lastSlyrCmdTime < $Slayer::Server::spamTimeout);

	%this.lastSlyrCmdTime = %time;

	if(%this.ignoreSpam)
		%spam = 0;
	%this.ignoreSpam = 0;

	return %spam;
}

function Slayer_SpectatePathCamData::onNode(%this, %camera, %node)
{
	if(%node == 2)
	{
		%camera.client.spectateAutoCam();
	}
}

//@private
function isSpecialKill_Slayer_Teams(%client, %sourceObject, %killer, %mini)
{
	if(isSlayerMinigame(%mini))
	{
		if(!%mini.deathMsgMode)
			return -1; //this hides the death msg
		else if(%mini.gameMode.template.useTeams)
		{
			%returnMode = 2;

			%clientClass = %client.getClassName();
			if((%clientClass $= "AiController" || %clientClass $= "AiPlayer") && %client.hName $= "")
				return -1;

			%clientTeam = %client.getTeam();
			if(isObject(%clientTeam))
				%clientColor = (%mini.deathMsgMode == 1 ? %clientTeam.getColorHex() : "<color:ff0040>");

				
				//OK SO I GET THIS HALF-WORKING: When bots kill you, name displays.
				//HOWEVER: This is very hacky and should be removed.
				
			%so = %sourceObject.sourceObject;
			if(!isObject(%killer) && %so.isHoleBot && !isObject(%so.client))
				%killer = %so;
			if(isObject(%killer) && %killer != %client)
			{
				%killerClass = %killer.getClassName();
				if((%killerClass $= "AiController" || %killerClass $= "AiPlayer") && %killer.hName $= "")
					return -1;

				%killerTeam = %killer.getTeam();
				if(isObject(%killerTeam))
				{
					%killerColor = (%mini.deathMsgMode == 1 ? %killerTeam.getColorHex() : "<color:ff0040>");
					if(%killerTeam == %clientTeam && %mini.teams_punishFF)
					{
						%friendlyFire = "\c3(Teamkill)";
					}
					else if(%killerTeam.hideKillMsgs)
						%returnMode = 4;
				}
				else
					%killerColor = "<color:ff0040>"; //default color
			}

			%value = %returnMode TAB %clientColor TAB %killerColor TAB %friendlyFire;

			%special = %mini.gameMode.callback("onCreateDeathMessage", %client, %killer, %value);
			if(getField(%special, 0) == true)
				%value = getFields(%special, 1);

			Slayer_Support::Debug(2, "isSpecialKill_Slayer_Teams", %value);

			return %value;
		}
	}

	return 0;
}
addSpecialDamageMsg("Slayer_Teams", "%5%2%3%4%1 %6", "%3%4%1");

//@private
function serverCmdAddLives(%client, %flag, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l)
{
	if(%client.isSpamming())
		return;

	%mini = getMinigameFromObject(%client);

	if(!%mini.canEdit(%client))
		return;

	if(!Slayer_Support::isFloat(%flag))
	{
		messageClient(%client, '', "\c5Please use the following format: \c3/addLives Amount Player Name");
		return;
	}

	%name = trim(%a SPC %b SPC %c SPC %d SPC %e SPC %f SPC %g SPC %h SPC %i SPC %j SPC %k SPC %l);
	%target = findClientByName(%name);

	if(!isObject(%target) || getMinigameFromObject(%target) != %mini)
	{
		messageClient(%client, '', "\c5Invalid target.");
		return;
	}

	if(%mini.lives <= 0)
		return;

	%flag = mClampF(%flag, -99, 99);

	%target.addLives(%flag);
	%mini.messageAll('', "\c3" @ %client.getPlayerName() SPC "\c5gave\c3" SPC %flag SPC "\c5lives to\c3" SPC %target.getPlayerName() @ "\c5.");

	%lives = %target.getLives();
	if(%lives < 0)
		%target.setLives(0);

	if(%lives > 0 && %target.dead())
	{
		%target.setDead(0);
		if(!isObject(%target.player))
			%target.spawnPlayer();
	}
	if(%lives <= 0 && !%target.dead())
	{
		%target.setDead(1);
		if(isObject(%target.player))
		{
			%target.camera.setMode(observer);
			%target.setControlObject(%target.camera);
			%target.player.delete();
		}
		%target.bottomPrint("<just:center>\c5The number of lives was reduced. You are now out of lives.", 5);
	}
}

//overwriting the default chat function, nbd
//@private
function serverCmdMessageSent(%client, %msg)
{
	serverCmdStopTalking(%client);

	%msg = stripMLControlChars(trim(%msg));

	%length = strLen(%msg);
	if(!%length)
		return;

	%time = getSimTime();

	if(!%client.isSpamming)
	{
		//did they repeat the same message recently?
		if(%msg $= %client.lastMsg && %time - %client.lastMsgTime < $SPAM_PROTECTION_PERIOD)
		{
			messageClient(%client, '', "\c5Do not repeat yourself.");
			if(!%client.isAdmin)
			{

				%client.isSpamming = true;
				%client.spamProtectStart = %time;
				%client.schedule($SPAM_PENALTY_PERIOD, spamReset);
			}
		}

		//are they sending messages too quickly?
		if(!%client.isAdmin)
		{
			if(%client.spamMessageCount >= $SPAM_MESSAGE_THRESHOLD)
			{
				%client.isSpamming = true;
				%client.spamProtectStart = %time;
				%client.schedule($SPAM_PENALTY_PERIOD, spamReset);
			}
			else
			{
				%client.spamMessageCount ++;
				%client.schedule($SPAM_PROTECTION_PERIOD, spamMessageTimeout);
			}
		}
	}

	//tell them they're spamming and block the message
	if(%client.isSpamming)
	{
		spamAlert(%client);
		return;
	}

	//eTard Filter, which I hate, but have to include
	if(!chatFilter(%client, %msg, $Pref::Server::ETardList,
		'\c5This is a civilized game.  Please use full words.'))
	{
		return;
	}

	//URLs
	for(%i = getWordCount(%msg) - 1; %i >= 0; %i --)
	{
		%word = getWord(%msg, %i);
		%pos = strPos(%word, "://") + 3;
		%pro = getSubStr(%word, 0, %pos);
		%url = getSubStr(%word, %pos, strLen(%word));

		if((%pro $= "http://" || %pro $= "https://" || %pro $= "ftp://") &&
			strPos(%url, ":") == -1)
		{
			%word = "<sPush><a:" @ %url @ ">" @ %url @ "</a><sPop>";
			%msg = setWord(%msg, %i, %word);
		}
	}

	%mini = getMinigameFromObject(%client);
	%slayer = isSlayerMinigame(%mini);

	//MESSAGE FORMAT
	if(%slayer)
	{
		%team = %client.getTeam();

		if(isObject(%team))
		{
			%tColor = %team.chatColor;
			if(%tColor $= "" || strLen(%tColor) != 6)
				%color = %team.getColorHex();
			else
				%color = "<color:" @ %tColor @ ">";

			switch(%mini.chat_teamDisplayMode)
			{
				case 1:
					%all  = '%5%1\c3%2%5%3%7: %4';
	
				case 2:
					%all  = '\c7%1%5%2\c7%3%7: %4';
	
				case 3:
					%all  = '\c7[%5%6\c7] %1\c3%2\c7%3%7: %4';
			}
		}
	}
	if(%all $= "")
		%all  = '\c7%1\c3%2\c7%3%7: %4';

	%name = %client.getPlayerName();
	%pre  = %client.clanPrefix;
	%suf  = %client.clanSuffix;

	if(%slayer && %client.dead() && (%mini.chat_deadChatMode == 0 || %mini.chat_deadChatMode == 3) && !%mini.isResetting())
		%mini.messageAllDead('chatMessage', %all, %pre, %name, %suf, %msg, %color, %team.name, " \c7[DEAD]<color:c0c0c0>");
	else
		commandToAll('chatMessage', %client, '', '', %all, %pre, %name, %suf, %msg, %color, %team.name, "<color:ffffff>");

	echo(%client.getSimpleName() @ ":" SPC %msg);

	%client.lastMsg = %msg;
	%client.lastMsgTime = %time;

	if(isObject(%client.player))
	{
		%client.player.playThread(3, "talk");
		%client.player.schedule(%length * 50, playThread, 3, "root");
	}

	if(%slayer)
	{
		%mini.gameMode.callback("onClientChat", %client, %msg);
	}
}


package Slayer_GameConnection
{
	//@private
	function GameConnection::autoAdminCheck(%this)
	{
		%this.isHost = (%this.isLocalConnection() || %this.getBLID() == Slayer_Support::getBlocklandID());

		Slayer.schedule(0, initClientAuth, %this);

		return parent::autoAdminCheck(%this);
	}

	//@private
	function GameConnection::onClientEnterGame(%this)
	{
		//PREVENT VEHICLE RESPAWN
		%oldClearVehicles = %this.doNotResetVehicles;

		%blid = %this.getBLID();
		for(%i=0; %i < Slayer.Minigames.getCount(); %i ++)
		{
			%mini = Slayer.Minigames.getObject(%i);
			if(%mini.useAllPlayersBricks || %mini.creatorBLID == %blid)
			{
				%this.doNotResetVehicles = 1; //hack to make vehicles not respawn
				break;
			}
		}

		%parent = parent::onClientEnterGame(%this);

		%this.doNotResetVehicles = %oldClearVehicles;

		//TRANSMIT MINIGAME LIST
		Slayer.Minigames.broadcastAllMinigames(%this);
		
		//PATH CAMERA CREATION
		%this.pathCamera = new PathCamera()
		{
			datablock = Slayer_SpectatePathCamData;
			client = %this;
		};
		%this.pathCamera.scopeToClient(%this);

		//DEFAULT MINIGAME
		%defaultMini = Slayer.Minigames.defaultMinigame;
		if(isObject(%defaultMini))
			%defaultMini.addMember(%this);

		return %parent;
	}

	//@private
	function GameConnection::onClientLeaveGame(%this)
	{
		%oldClearVehicles = %this.doNotResetVehicles;
		
		if(isObject(%this.pathCamera))
			%this.pathCamera.delete();

		if(isObject(Slayer.Minigames))
		{
			%blid = %this.getBLID();

			for(%i=0; %i < Slayer.Minigames.getCount(); %i ++)
			{
				%mini = Slayer.Minigames.getObject(%i);
				if(%mini.useAllPlayersBricks || %mini.creatorBLID == %blid)
				{
					%this.doNotResetVehicles = 1; //hack to make vehicles not respawn
					break;
				}
			}
		}

		%parent = parent::onClientLeaveGame(%this);

		%this.doNotResetVehicles = %oldClearVehicles;

		return %parent;
	}

	//@private
	function GameConnection::createPlayer(%this, %pos)
	{
		%mini = getMinigameFromObject(%this);

		if(isSlayerMinigame(%mini))
		{
			%parent = parent::createPlayer(%this, %pos);

			//get them out of spectate mode
			%this.spectateEnd();

			//apply team settings
			if(isObject(%team = %this.getTeam()))
			{
				%this.player.setShapeNameColor(%team.colorRGB);
				if(%team.isCaptain(%this))
				{
					%this.player.changeDatablock(%team.captain_playerDatablock);
					for(%i = 0; %i < %team.captain_playerDatablock.maxTools; %i ++)
						%this.forceEquip(%i, %team.captain_startEquip[%i]);
				}
				else
				{
					%this.player.changeDatablock(%team.playerDatablock);
					for(%i=0; %i < %team.playerDatablock.maxTools; %i++)
						%this.forceEquip(%i, %team.startEquip[%i]);
				}
				if((%ps = %team.playerScale) != 1)
					%this.player.setScale(%ps SPC %ps SPC %ps);
			}

			//Name distance
			%this.player.setShapeNameDistance(%mini.nameDistance);

			//display remaining lives
			%lives = %this.getLives();
			if(%lives > 0 && !%mini.isResetting())
			{
				%msg = "\c5You have \c3" @ %lives SPC (%lives == 1 ? "\c5life" : "\c5lives") SPC "left.";
				%this.schedule(100, centerPrint, %msg, 3);
				messageClient(%this, '', %msg);
			}

			//dynamic respawn time
			if(isObject(%team) && %team.respawnTime >= 0)
				%this.setRespawnTime(%team.respawnTime);
			else
				%this.resetRespawnTime();

			%mini.gameMode.callback("onPlayerSpawn", %this);

			return %parent;
		}
		else
		{
			return parent::createPlayer(%this, %pos);
		}
	}

	//@private
	function GameConnection::applyBodyParts(%this)
	{
		if(%this.applyingUniform ||
			%this.getClassName() !$= "GameConnection" ||
			!isObject(%this.getTeam()))
		{
			return parent::applyBodyParts(%this);
		}
	}
	
	//@private
	function GameConnection::applyBodyColors(%this)
	{
		if(!%this.applyingUniform &&
			%this.getClassName() $= "GameConnection" &&
			isObject(%this.getTeam()))
		{
			%this.applyUniform();
		}
		else
		{
			return parent::applyBodyColors(%this);
		}
	}

	//@private
	function GameConnection::onDeath(%this, %obj, %killer, %type, %area)
	{
		%mini = getMinigameFromObject(%this);

		if(isSlayerMinigame(%mini))
		{
			Slayer_Support::Debug(2, "GameConnection::onDeath", %this TAB %killer);
			//friendly fire, kill and death counts
			if(isObject(%killer))
			{
				if(%killer.isSlayerBot)
				{
					if(%this.isSlayerBot)
						%killer.incScore(%mini.points_killBot);
					else
						%killer.incScore(%mini.points_killPlayer);
				}
				else if(%this.isSlayerBot) //correcting for double bot scoring
					%killer.incScore(-%mini.points_killPlayer);

				%killerClass = %killer.getClassName();
				//work around Rotondo's hacky bots
				if(isFunction(%killerClass, getTeam))
					%killTeam = %killer.getTeam();
				%clientTeam = %this.getTeam();
	
				if(%killTeam == %clientTeam && isObject(%killTeam) && %killer != %this && %mini.teams_punishFF)
				{
					//This is friendly fire. Punishment needs to be dished out.

					//fix scores
					%killer.incScore(%mini.points_friendlyFire);
					%killer.incScore(-%mini.points_killPlayer);
					%this.incScore(-%mini.points_die);

					//increase killer respawn time.
					%killer.friendlyFireCount ++;
					if(%mini.friendlyFireProgressivePenalty)
						%timeInc = %killer.friendlyFireCount * %mini.friendlyFireRespawnPenalty;
					else
						%timeInc = %mini.friendlyFireRespawnPenalty;
					%killer.incRespawnTime(%timeInc);

					//warning
					if(!%killer.isBot && %mini.friendlyFireDisplayWarning)
					{
						%killer.centerPrint("<just:center><div:1>\n<div:1>\c2You just killed a team-mate!" NL
							"<div:1>\c2Be more careful.\n<div:1>\c2Your respawn time is now\c0" SPC
							mCeil(%killer.respawnTime / 1000) SPC "\c2seconds.\n<div:1>", 7);
					}
				}
				else
				{
					%this.addDeaths(1);
					//work around Rotondo's hacky bots
					if(%killer != %this && isFunction(%killerClass, addKills))
						%killer.addKills(1);
				}
			}
			else
			{
				%this.addDeaths(1);
			}

			%special = %mini.gameMode.callback("prePlayerDeath", %this, %obj, %killer, %type, %area);
			if(getField(%special, 0) == 1)
				%type = getField(%special, 1);

			//lives
			if(%this.getLives() > 0)
			{
				%this.addLives(-1);
				if(%this.getLives() <= 0)
					%this.setDead(1);
			}

			parent::onDeath(%this, %obj, %killer, %type, %area);

			//onMinigameDeath input event
			$InputTarget_["Client"] = %this;
			$InputTarget_["Player(Killer)"] = %killer.player;
			$InputTarget_["Client(Killer)"] = %killer;
			$InputTarget_["Minigame"] = %mini;
			processMultiSourceInputEvent("onMinigameDeath", %this, %mini);

			%special = %mini.gameMode.callback("onPlayerDeath", %this, %obj, %killer, %type, %area);

			if(%special != -1 && %this.dead())
			{
				//remove the "Click to Respawn" message and replace it with a lives message
				messageClient(%this, 'MsgYourSpawn');
				%this.centerPrint("\c5You have \c30 \c5lives left.", 3);

				//check to see if anyone won
				%winner = %mini.victoryCheck_Lives();
				if(%winner >= 0)
					%mini.endRound(%winner);
				else
				{
					%this.spectateStartSched = %this.scheduleNoQuota(5000, "spectateInit");
				}
			}
		}
		else
		{
			parent::onDeath(%this, %obj, %killer, %type, %area);
		}
	}

	//@private
	function GameConnection::setScore(%this, %flag)
	{
		%mini = getMinigameFromObject(%this);
		if(isSlayerMinigame(%mini))
		{
			if(!(%mini.isResetting() && !%mini.clearScores && %this.points <= 0))
			{
				%parent = parent::setScore(%this, %flag);

				%mini.gameMode.callback("onClientSetScore", %this, %flag);
	
				%winner = %mini.victoryCheck_Points();
				if(%winner >= 0)
					%mini.endRound(%winner);
			}
		}
		else
			%parent = parent::setScore(%this, %flag);

		return %parent;
	}

	//@private
	function GameConnection::resetVehicles(%this) //hack
	{
		if(!%this.doNotResetVehicles)
			parent::resetVehicles(%this);
	}

	//@private
	function GameConnection::clearEventObjects(%this, %mask)
	{
		if(!%this.doNotClearEventObjects)
		{
			%mask = %mask ^ ($TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType);
			return parent::clearEventObjects(%this, %mask);
		}
	}

	//@private
	function Observer::onTrigger(%this, %camera, %button, %state)
	{
		if(!%state || !isObject(%camera))
			return parent::onTrigger(%this, %camera, %button, %state);

		%client = %camera.getControllingClient();
		if(!isObject(%client))
			return parent::onTrigger(%this, %camera, %button, %state);

		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini))
		{
			if(getSimTime() - %client.lastDeathTime < 1000)
			{
				return;
			}
			
			if(!%mini.isResetting() && !%client.dead() && !%client.isSpectator)
			{
				return parent::onTrigger(%this, %camera, %button, %state);
			}

			if(%button == 0 && %camera.isOrbitMode())
			{
				%client.spectateNextTarget();
			}
			else if(%button == 4 && %camera.isOrbitMode())
			{
				%client.spectatePrevTarget();
			}
			else if(%button == 2)
			{
				%client.spectateChangeMode();
			}
			%mini.gameMode.callback("onObserverTrigger", %this, %camera, %button, %state, %client);
		}
		else
		{
			return parent::onTrigger(%this, %camera, %button, %state);
		}
	}

	function serverCmdLight(%client)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini))
		{
			if(%client.isSpectator)
			{
				%client.spectateChangeMode();
			}
			else if(!%mini.enablePlayerLights)
			{
				messageClient(%client, '', "\c5Lights are disabled in this minigame.");
			}
			else
			{
				parent::serverCmdLight(%client);
			}
		}
		else
		{
			parent::serverCmdLight(%client);
		}
	}
	
	function serverCmdSuicide(%client)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini) && !%mini.enablePlayerSuicide)
			messageClient(%client, '', "\c5Suicide is disabled in this minigame.");
		else
			parent::serverCmdSuicide(%client);
	}

	//@private
	function serverCmdDropPlayerAtCamera(%client)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini))
		{
			if(%client.dead())
			{
				if(!%mini.canEdit(%client))
				{
					%client.centerPrint("\c5You're out of lives! Don't cheat!", 3);
					return;
				}
			}
		}

		parent::serverCmdDropPlayerAtCamera(%client);
	}

	//@private
	function serverCmdDropCameraAtPlayer(%client)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini))
		{
			if(%client.dead())
			{
				if(!%mini.canEdit(%client))
				{
					%client.centerPrint("\c5You're out of lives! Don't cheat!", 3);
					return;
				}
			}
		}

		parent::serverCmdDropCameraAtPlayer(%client);
	}
};
activatePackage(Slayer_GameConnection);