// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_PrefHandlerSG::onAdd(%this)
{
	//%this.schedule(0, "loadInitialPrefs");
}

//Returns the Slayer_PrefSO preference object.
//@param	string category
//@param	string title
//@return	Slayer_PrefSO
function Slayer_PrefHandlerSG::getPrefSO(%this, %category, %title)
{
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %this.getObject(%i);
		if(%pref.category $= %category && %pref.title $= %title)
		{
			return %pref;
		}
	}
	return false;
}

//Resets all preferences to their default values.
//@param	Slayer_MinigameSO mini
function Slayer_PrefHandlerSG::resetPreferences(%this, %mini)
{
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %this.getObject(%i);
		if(!%pref.doNotReset)
			%pref.setValue(%pref.defaultValue, %mini);
	}
}

//Exports all minigame preference settings to a file.
//@param	string path
//@param	Slayer_MinigameSO mini
function Slayer_PrefHandlerSG::exportMinigamePreferences(%this, %path, %mini)
{
	if(!isWriteableFileName(%path))
	{
		Slayer_Support::error("Slayer_PrefHandlerSG::exportMinigamePreferences", "Invalid file path" SPC %path);
		return;
	}
	if(!isSlayerMiniGame(%mini))
	{
		Slayer_Support::error("Slayer_PrefHandlerSG::exportMinigamePreferences", "Invalid minigame" SPC %mini);
		return;
	}
	
	%file = new FileObject();
	%file.openForWrite(%path);
	%file.writeLine("SLAYERMINIGAMECONFIG 1.0");
	%file.writeLine("");
	
	%heap = HeapQueue("", $COMPARE::SCORES);
	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %this.getObject(%i);
		%heap.push(%pref, %pref.priority);
	}
	while(isObject(%pref = %heap.pop()))
	{
		if(!%pref.isMinigameVar)
			continue;
		%value = %pref.getValue(%mini);
		if(%pref.type $= "object")
			%value = isObject(%value) ? %value.uiName : "";
		%file.writeLine(
			"\"" @ expandEscape(%pref.category) @ "\"," @
			"\"" @ expandEscape(%pref.title) @ "\"," @
			"\"" @ expandEscape(%value) @ "\"");
	}
	%heap.delete();
	
	%file.close();
	%file.delete();
}

//Imports minigame preference settings from a file.
//@param	string path
//@param	Slayer_MinigameSO mini
function Slayer_PrefHandlerSG::importMinigamePreferences(%this, %path, %mini)
{
	if(!isFile(%path))
	{
		Slayer_Support::error("Slayer_PrefHandlerSG::importMinigamePreferences", "Invalid file path" SPC %path);
		return;
	}
	if(!isSlayerMiniGame(%mini))
	{
		Slayer_Support::error("Slayer_PrefHandlerSG::importMinigamePreferences", "Invalid minigame" SPC %mini);
		return;
	}
	
	%file = new FileObject();
	%file.openForRead(%path);
	
	if(%file.readLine() $= "SLAYERMINIGAMECONFIG 1.0")
	{
		%csv = CSVReader(",");
		%file.readLine(); //blank line
		while(!%file.isEOF())
		{
			%line = %file.readLine();
			%csv.setDataString(%line);
			%category = collapseEscape(%csv.readNextValue());
			%title = collapseEscape(%csv.readNextValue());
			%value = collapseEscape(%csv.readNextValue());
			%pref = %this.getPrefSO(%category, %title);
			if(isObject(%pref) && !%csv.hasNextValue())
			{
				if(%pref.type $= "object")
				{
					if(strLen(%value))
						%value = Slayer_Support::getIDFromUiName(%value, %pref.object_class);
					else
						%value = 0;
				}
				%pref.setValue(%value, %mini);
			}
			else
				Slayer_Support::error("Invalid config file line", %line);
		}
	}
	else
	{
		Slayer_Support::error("Slayer_PrefHandlerSG::importMinigamePreferences", "Incompatible preference file" SPC %path);	
	}
	
	%file.close();
	%file.delete();
}

//Sends preference data to a client.
//@param	GameConnection client
function Slayer_PrefHandlerSG::sendPreferenceData(%this, %client)
{
	Slayer_Support::Debug(2, "Slayer_PrefHandlerSG::sendPrefData", %client.getSimpleName());

	//START THE PREF TRANSFER
	commandToClient(%client, 'Slayer_getPrefData_Start', %this.getCount());

	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %this.getObject(%i);
		if(%pref.doNotSendToClients)
			continue;
		transferObjectToClient(%client, %pref, false, "SlayerClient_ServerPrefSO");
	}

	//END THE PREF TRANSFER
	commandToClient(%client, 'Slayer_getPrefData_End');
}

//Sends preference values to a client.
//@param	GameConnection client
//@param	Slayer_MinigameSO mini
function Slayer_PrefHandlerSG::sendPreferenceValues(%this, %client, %mini)
{
	if(!isObject(%mini) && %mini != -1)
		%mini = getMinigameFromObject(%client);
	if(isObject(%mini) && !isSlayerMinigame(%mini))
		return;

	Slayer_Support::Debug(2, "Slayer_PrefHandlerSG::sendPreferenceValues", %client.getSimpleName());

	//START THE PREF TRANSFER
	commandToClient(%client, 'Slayer_getPrefValues_Start', %this.getCount());

	for(%i = %this.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %this.getObject(%i);
		if(%pref.doNotSendToClients)
			continue;

		if(isObject(%mini))
			%canEdit = %mini.canEdit(%client, %pref.permissionLevel);
		else
		{
			if(%pref.permissionLevel <= 2)
				%canEdit = (%pref.permissionLevel <= %client.getAdminLvl());
			else
				%canEdit = 1;
		}
		%value = %pref.getValue(%mini);

		commandToClient(%client, 'Slayer_getPrefValues_Tick', %pref, %value, %canEdit);

		Slayer_Support::Debug(2, "Sending preference value", %pref TAB %value);
	}

	//END THE PREF TRANSFER
	commandToClient(%client, 'Slayer_getPrefs_End', %this.getCount());
}

function serverCmdSlayer_getPrefs_Start(%client, %reset, %count, %notify)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);

	if(isSlayerMinigame(%mini) && !%mini.canEdit(%client))
	{
		return;
	}
	else if(!isObject(%mini)) //minigame creation
	{
		if(Slayer.Minigames.canCreateMinigame(%client))
		{
			%mini = Slayer.Minigames.addMinigame(%client);
			if(!isObject(%mini))
				return;
			%client.editingMinigame = %mini;
		}
		else
		{
			return;
		}
	}

	%mini.editClient = %client;
	%mini.requiresReset = %reset;
	%mini.notifyOnChange = %notify;

	if(%count > 0)
	{
		%message = '\c3%1 \c5updated the \c3%2 \c5minigame.';
		%mini.messageAll('', %message, %client.getPlayerName(), %mini.title);
	}
}

function serverCmdSlayer_getPrefs_Tick(%client, %pref, %value)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);
	if(!isSlayerMinigame(%mini))
		return;

	if(!Slayer.Prefs.isMember(%pref))
		return;
	if(%pref.doNotSendToClients)
		return;
	
	//Make sure that this preference isn't locked by the game mode
	if(%pref.isMinigameVar)
	{
		if(%mini.gameMode.template.locked_[%pref.variable] !$= "")
			return;
	}

	//make sure that nobody can change things without permission
	if(!%mini.canEdit(%client, %pref.permissionLevel))
		return;

	Slayer_Support::Debug(2, "Preference Received", %pref.category TAB %pref.title TAB %value);

	if(%pref.getValue(%mini) $= %value)
		return;

	%success = %pref.setValue(%value, %mini);

	if(%success)
	{
		//does the minigame need to be reset once all prefs are loaded?
		if(%pref.requiresMiniGameReset)
			%mini.requiresReset = true;

		//tell players that the pref was updated
		if(%pref.notifyPlayersOnChange && %mini.notifyOnChange)
		{
			%val = %pref.getDisplayValue(%value);
			%message = '\c3 + \c5[\c3%1\c5|\c3%2\c5] is now \c3%3';
			%mini.messageAll('', %message, %pref.category, %pref.title, %val);
		}
	}
}

function serverCmdSlayer_getPrefs_End(%client)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);
	if(!isSlayerMinigame(%mini))
		return;
	if(!%mini.canEdit(%client))
		return;

	//new teams
	if(%mini.notifyOnChange && %mini.notifyNewTeam > 0)
	{
		%newTeams = (%mini.notifyNewTeam == 1 ? "team" : "teams");
		%mini.messageAll('', "\c3 + \c5Added\c3" SPC %mini.notifyNewTeam SPC "\c5new" SPC %newTeams @ ".");
		%mini.messageAll('', "\c3 + + \c5Type \c3/listTeams \c5to view a list of teams and \c3/joinTeam [Team Name] \c5to join a team.");
		if(%mini.teams_balanceOnNew)
		{
			%mini.messageAll('', "\c3 + + \c5Teams have been \c3balanced\c5.");
		}
	}

	//check if the minigame should be reset
	if(%mini.requiresReset)
		%mini.reset(%client);

	//Add the client to the minigame they created
	if(%mini.autoAddCreator)
	{
		%mini.addMember(%client);
		%mini.autoAddCreator = "";
	}

	%mini.editClient = "";
	%mini.requiresReset = 0;
	%mini.notifyNewTeam = 0;
	%mini.notifyOnChange = 0;

	//update the Join Minigame menu
	%mini.broadcastMinigame();

	%client.editingMinigame = "";

	//save settings?
	if($Pref::Slayer::Server::SavePrefsOnUpdate)
	{
		%blid = Slayer_Support::getBlocklandID();
		if(%mini.creatorBLID == %blid && ($Slayer::Server::CurGameModeArg $= "" || $Slayer::Server::CurGameModeArg $= $Slayer::Server::GameModeArg))
		{
			Slayer.Prefs.exportMiniGamePreferences($Slayer::Server::ConfigDir @ "/last.mgame.csv", %mini);
			Slayer.TeamPrefs.exportTeamPreferences($Slayer::Server::ConfigDir @ "/last.teams.csv", %mini);
		}
	}
}

function serverCmdSlayer_saveConfig(%client, %fileName)
{
	if(%client.isSpamming())
		return;

	%fileName = strReplace(%fileName, " ", "");
	if(%fileName $= "")
		return;

	%mini = getMinigameFromObject(%client);
	if(!isSlayerMinigame(%mini))
		return;
	if(!%mini.canEdit(%client))
		return;

	%path = $Slayer::Server::ConfigDir @ "/config_saved/config_" @ %fileName;
	Slayer.Prefs.exportMiniGamePreferences(%path @ ".mgame.csv", %mini);
	Slayer.TeamPrefs.exportTeamPreferences(%path @ ".teams.csv", %mini);

	messageClient(%client, '', "Config saved as \"" @ %fileName @ "\"");
}

function serverCmdSlayer_loadConfig(%client, %fileName)
{
	if(%client.isSpamming())
		return;

	%mini = getMinigameFromObject(%client);
	if(!isSlayerMinigame(%mini))
		return;
	if(!%mini.canEdit(%client))
		return;

	%miniGameConfig = %fileName @ ".mgame.csv";
	if(isFile(%miniGameConfig))
	{
		Slayer.Prefs.importMiniGamePreferences(%miniGameConfig, %mini);
		%teamConfig = %fileName @ ".teams.csv";
		if(isFile(%teamConfig) && isObject(%this.teams))
		{
			Slayer.TeamPrefs.importTeamPreferences(%teamConfig, %this);
		}
	}

	//hide the GUI
	commandToClient(%client, 'Slayer_forceGUI', "ALL", 0);

	%mini.reset(%client);
	%mini.messageAll('', %client.getPlayerName() SPC "loaded a minigame config file.");
}

//----------------------------------------------------------------------
// Deprecated Functions - DO NOT USE
//----------------------------------------------------------------------

//@DEPRECATED
//Adds a preference to the preference handler. Call using Slayer.Prefs.addPref(...);
//@param	string category	The preference category.
//@param	string title	The display name of the preference.
//@param	string variable	The variable name. Use %mini.varName if this is a minigame member field.
//@param	string type	The type of variable. bool|string|int|list|slide|object
//@param	string defaultValue	The initial value of the variable.
//@param	bool requiresReset	Whether the minigame must be reset after this is updated.
//@param	bool notify	Whether minigame members should be notified when this preference is updated.
//@param	int editRights	Who can change this preference. 0=host, 1=superAdmin, 2=admin, 3=mini-owner, 4=owner-full-trust, 5=owner-build-trust
//@param	string list	Optional, the GUI list to add this to. For example, "advanced" would place this in the Advanced list.
//@param	string callback	The optional function called when this preference is updated.
//@return	Slayer_PrefSO	The new preference object.
function Slayer_PrefHandlerSG::addPref(%this, %category, %title, %variable, %type, %defaultValue, %requiresReset, %notify, %editRights, %list, %callback)
{
	warn("\c2WARN: Slayer_PrefHandlerSG::addPref is deprecated! Use the new API.");

	%typeVals = getWords(%type, 1);
	%type = getWord(%type, 0);

	%pref = new ScriptObject(Slayer_PrefSO : Slayer_DefaultPrefSO)
	{
		title = %title;
		category = %category;
		type = %type;
		variable = %variable;
		defaultValue = %defaultValue;
		requiresMiniGameReset = %requiresReset;
		notifyPlayersOnChange = %notify;
		permissionLevel = %editRights;
		guiTag = %list;
		callback = %callback;
	};
	
	switch$(%type)
	{
		case "list": %pref.list_items = strReplace(%typeVals, "\t", "\n");
		case "int":
			%pref.int_minValue = getWord(%typeVals, 0);
			%pref.int_maxValue = getWord(%typeVals, 1);
		case "string": %pref.string_maxLength = getWord(%typeVals, 0);
		case "slide":
			%pref.slide_minValue = getWord(%typeVals, 0);
			%pref.slide_maxValue = getWord(%typeVals, 1);
			%pref.slide_numTicks = getWord(%typeVals, 2);
			%pref.slide_snapToTicks = getWord(%typeVals, 3);
	}

	return %pref;
}

//@DEPRECATED
//Same as Slayer_PrefHandlerSG::addPref, but the preference is never sent to clients and cannot be modified by them.
//@param	string category
//@param	string title
//@param	string variable
//@param	string type
//@param	string defaultValue
//@param	string callback
//@return	Slayer_PrefSO
//@see	Slayer_PrefHandlerSG::addPref
function Slayer_PrefHandlerSG::addNonNetworkedPref(%this, %category, %title, %variable, %type, %defaultValue, %callback)
{
	%pref = %this.addPref(%category, %title, %variable, %type, %defaultValue, "", "", "", "", %callback);
	%pref.doNotSentToClients = true;
	return %pref;
}

//@DEPRECATED
//Same as Slayer_PrefHandlerSG::addPref, but is not managed by Slayer's preference-saving system.
//@param	string category
//@param	string title
//@param	string variable
//@param	string type
//@param	string defaultValue
//@param	bool requiresReset
//@param	bool notify
//@param	int editRights
//@param	string list
//@param	string callback
//@return	Slayer_PrefSO
//@see	Slayer_PrefHandlerSG::addPref
function Slayer_PrefHandlerSG::addTransientPref(%this, %category, %title, %variable, %type, %defaultValue, %requiresReset, %notify, %editRights, %list, %callback)
{
	%pref = %this.addPref(%category, %title, %variable, %type, %defaultValue, %requiresReset, %notify, %editRights, %list, %callback);
	%pref.doNotSave = 1;
	%pref.doNotReset = 1;
	return %pref;
}

//@DEPRECATED
//Sets a preference to a specified value.
//@param	string category
//@param	string title
//@param	string value
//@param	Slayer_MinigameSO mini
//@return	bool	The value was set successfully.
function Slayer_PrefHandlerSG::setPref(%this, %category, %title, %value, %mini)
{
	warn("\c2WARN: Slayer_PrefHandlerSG::setPref is deprecated! Use the new API.");
	//compatibility
	if(%category $= "Minigame" && %title $= "Gamemode")
		%title = "Game Mode";
	%pref = %this.getPrefSO(%category, %title);
	if(!isObject(%pref))
		return false;
	//compatibility
	if(%category $= "Minigame" && %title $= "Game Mode")
	{
		if(isObject(Slayer.gameModes.template[%value]))
			%value = Slayer.gameModes.template[%value];
		else if(isObject(Slayer.gameModes.template[$Slayer::GameModes::NewClass[%value]]))
		{
			warn("\c2WARN: Game mode class" SPC %value SPC "is deprecated! Use" SPC $Slayer::GameModes::NewClass[%value] @ ".");
			%value = Slayer.gameModes.template[$Slayer::GameModes::NewClass[%value]];
		}
	}
	return %pref.setValue(%value, %mini);
}

//@DEPRECATED
//@param	string category
//@param	string title
//@param	Slayer_MinigameSO mini
//@return	string
function Slayer_PrefHandlerSG::getPref(%this, %category, %title, %mini)
{
	warn("\c2WARN: Slayer_PrefHandlerSG::getPref is deprecated! Use the new API.");

	%pref = %this.getPrefSO(%category, %title);
	if(!isObject(%pref))
		return false;

	return %pref.getValue(%mini);
}

//@DEPRECATED
//Resets all preferences to their default values.
function Slayer_PrefHandlerSG::resetPrefs(%this, %mini)
{
	warn("\c2WARN: Slayer_PrefHandlerSG::resetPrefs is deprecated! Use the new API.");
	%this.resetPreferences(%mini);
}

//@DEPRECATED
//Exports all preference settings to a file.
//@param	string path
//@param	Slayer_MinigameSO mini
function Slayer_PrefHandlerSG::exportPrefs(%this, %path, %mini)
{
	warn("\c2WARN: Slayer_PrefHandlerSG::exportPrefs is deprecated! Use the new API.");

	%isSlayerMinigame = isSlayerMinigame(%mini);

	%file = new fileObject();
	%file.openForWrite(%path);

	%file.writeLine("//Slayer -----" SPC getDateTime());
	%file.writeLine("//By Greek2me, Blockland ID 11902");
	%file.writeLine("");
	%file.writeLine("//Version:" SPC $Slayer::Server::Version);
	%file.writeLine("//Debug Mode:" SPC $Slayer::Server::Debug);
	%file.writeLine("//Core Directory:" SPC $Slayer::Server::Path);
	%file.writeLine("//Config Directory:" SPC $Slayer::Server::ConfigDir);
	%file.writeLine("");
	%file.writeLine("if(isObject(%mini))");
	%file.writeLine("\t%mini.preConfigLoad(\"" @ %path @ "\");");
	%file.writeLine("");

	%file.writeLine("//PREFERENCES //USAGE: (category, title, value)");

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%p = %this.getObject(%i);
		if(%p.doNotSave)
			continue;
		if(!%isSlayerMinigame && %p.isMinigameVar)
			continue;

		%v = %p.getValue(%mini);

		if(%p.isObject)
		{
			if(isObject(%v))
				%var = "nameToID(" @ %v.getName() @ ")";
			else
				%var = 0;
			%file.writeLine("Slayer.Prefs.setPref(\"" @ %p.category @ "\", \"" @ %p.title @ "\", " @ %var @ ", %mini);");
		}
		else
		{
			%file.writeLine("Slayer.Prefs.setPref(\"" @ %p.category @ "\", \"" @ %p.title @ "\", \"" @ %v @ "\", %mini);");
		}
	}

	%teams = %mini.Teams;
	if(isObject(%teams))
	{
		if(%teams.getCount() > 0)
		{
			%file.writeLine("");
			%file.writeLine("//TEAMS");
			%file.writeLine("if(isObject(%mini.Teams))");
			%file.writeLine("{");
			for(%i=0; %i < %teams.getCount(); %i++)
			{
				%t = %teams.getObject(%i);
	
				if(%t.isSpecialTeam)
					continue;
	
				%file.writeLine("\t%team = %mini.Teams.addTeam();");
				for(%e=0; %e < %teams.Prefs.getCount(); %e++)
				{
					%p = %teams.Prefs.getObject(%e);
					%v = %p.getValue(%t);
	
					if(%p.isObject)
					{
						if(isObject(%v))
							%var = "nameToID(" @ %v.getName() @ ")";
						else
							%var = 0;
						%file.writeLine("\t\t%mini.TeamPrefs.setPref(%team, \"" @ %p.category @ "\", \"" @ %p.title @ "\", " @ %var @ ");");
					}
					else
						%file.writeLine("\t\t%mini.TeamPrefs.setPref(%team, \"" @ %p.category @ "\", \"" @ %p.title @ "\", \"" @ %v @ "\");");
				}
			}
			%file.writeLine("}");
		}
	}

	%file.writeLine("");
	%file.writeLine("if(isObject(%mini))");
	%file.writeLine("\t%mini.postConfigLoad(\"" @ %path @ "\");");

	%file.close();
	%file.delete();

	for(%i=0; %i < Slayer.Gamemodes.getCount(); %i++)
	{
		%m = Slayer.Gamemodes.getObject(%i);
		if(isFunction("Slayer_" @ %m.fName @ "_onExportPrefs"))
			call("Slayer_" @ %m.fName @ "_onExportPrefs", %path);
	}

	Slayer_Support::Debug(1, "Exporting Preferences", %path);
}

//@DEPRECATED
//Loads previous settings from file.
function Slayer_PrefHandlerSG::loadInitialPrefs(%this)
{
	warn("\c2WARN: Slayer_PrefHandlerSG::exportPrefs is deprecated!");

	//LOAD ANY SAVED NON-MINIGAME PREFS
	if(isFile($Slayer::Server::ConfigDir @ "/last.cs"))
		exec($Slayer::Server::ConfigDir @ "/last.cs");
	%this.setNonMinigamePrefs = 1;
}