// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

//Restricted Events
//KEY: -1=anyone with edit rights; 0=host; 1=super admin; 2=admin; 3=creator; 4=full trust; 5=build trust
$Slayer::Server::Events::RestrictedEvent__["Minigame", "BottomPrintAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame", "CenterPrintAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame", "ChatMsgAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame", "incTimeRemaining"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame", "Reset"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame", "RespawnAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame", "setTimeRemaining"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame", "Slayer_setPref"] = 1;
$Slayer::Server::Events::RestrictedEvent__["Minigame", "Win"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Slayer_TeamSO", "BottomPrintAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Slayer_TeamSO", "CenterPrintAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Slayer_TeamSO", "ChatMsgAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Slayer_TeamSO", "RespawnAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Slayer_TeamSO", "IncScore"] = -1;

if(isFile($Slayer::Server::ConfigDir @ "/restrictedEvents.cs"))
	exec($Slayer::Server::ConfigDir @ "/restrictedEvents.cs");
export("$Slayer::Server::Events::RestrictedEvent__*", $Slayer::Server::ConfigDir @ "/restrictedEvents.cs", false);


//Sets up Slayer-related bricks, like Capture Points.
//@param	string isLoadPlant	Whether this was called when the brick was loaded.
function FxDtsBrick::slayerPrepareBrick(%this, %isLoadPlant)
{
	if(!%this.isPlanted)
		return;

	%datablock = %this.getDatablock();
	switch$(%datablock.specialBrickType)
	{
		case "SpawnPoint":
			Slayer.spawns.add(%this);

		case "vehicleSpawn":
			Slayer.vehicleSpawns.add(%this);
	}
	
	if(%datablock.isSlyrBrick)
	{
		%this.isSlyrBrick = true;
		
		Slayer.slayerBricks.add(%this);

		%type = %datablock.slyrType;
		%color = %this.getColorID();
		%mini = getMinigameFromObject(%this);

		switch$(%type)
		{
			case "TeamSpawn" or "TeamVehicle":
				if(isSlayerMinigame(%mini))
					%teams = %mini.Teams.getTeamsFromColor(%color, "COM", 1);

			case "TeamBotHole":
				if(isSlayerMinigame(%mini))
					%teams = %mini.Teams.getTeamsFromColor(%color, "COM", 1);

				%this.spawnHoleBot();

			case "CP":
				if(isObject(%this.cpTrigger))
					%this.cpTrigger.delete();

				Slayer.capturePoints.add(%this);
				for(%i = Slayer.Minigames.getCount() - 1; %i >= 0; %i --)
				{
					%m = Slayer.Minigames.getObject(%i);
					%m.spectateTargets.add(%this);
				}

				%this.cpTrigger = %this.createTrigger(Slayer_CPTriggerData);
				%this.origColor = %color;
				%this.controlColor = %color;

				if(isSlayerMinigame(%mini))
				{
					%teams = %mini.Teams.getTeamsFromColor(%color, "COM", 1);
				}

			case "RegionBoundary":
				Slayer.boundaryBricks.add(%this);
				for(%i = Slayer.minigames.getCount() - 1; %i >= 0; %i --)
				{
					Slayer.minigames.getObject(%i).calculateRegionBox();
				}

			case "BotPathNode":
				if(!%isLoadPlant)
				{
					%this.setRendering(false);
					%this.setColliding(false);
					%this.setRayCasting(false);
				}

				if(isSlayerMinigame(%mini) && %datablock.isRallyNode)
					%teams = %mini.Teams.getTeamsFromColor(%color, "COM", 1);
		}
		
		for(%i = Slayer.Minigames.getCount() - 1; %i >= 0; %i --)
		{
			%m = Slayer.Minigames.getObject(%i);
			%customMsg = %m.gameMode.callback("onMiniGameBrickAdded", %this, %type);
			if(%customMsg !$= "" && %customMsg !$= 0)
				%msg = %customMsg;
		}

		if(!%isLoadPlant && isObject(%client = Slayer_Support::getClientFromObject(%this)))
		{
			if(%msg $= "")
			{
				if(%teams $= "")
					%msg = "<just:center><color:ff00ff>" @ %datablock.uiName SPC "set.";
				else
					%msg = "<just:center><color:ff00ff>" @ %datablock.uiName SPC "set for" SPC %teams @ ".";
			}

			%client.bottomPrint(%msg, 4);
		}
	}
}

//Called when the Slayer minigame that this brick belongs to is reset.
//@param	Slayer_MinigameSO mini
function FxDtsBrick::onSlayerMinigameReset(%this, %mini)
{
	if(%this.isFakeDead()) 
		%this.respawn();
	if(!%mini.useAllPlayersBricks && %this.numEvents > 0 && %this.numEvents !$= "")
		%this.onMiniGameReset();
}

//Sets the brick's alliance to a specific team color.
//@param	GameConnection client	Optional, checks whether client can use the brick first.
function FxDtsBrick::setTeamControl(%this, %color, %client)
{
	if(isObject(%client))
	{
		%mini = getMinigameFromObject(%client);
		if(!isSlayerMinigame(%mini))
			return;
		if(!minigameCanUse(%client, %this))
			return;
	}

	%db = %this.getDatablock();
	if(!%db.isSlyrBrick)
		return;

	switch$(%db.slyrType)
	{
		case "TeamSpawn" or "TeamVehicle":
			%this.controlColor = %color;

		case "CP":
			%this.setCPControl(%color, 0, %client);
	}
}

//@return	int	The color ID of the teams that control this brick.
function FxDtsBrick::getTeamControl(%this)
{
	if(%this.controlColor $= "")
		return %this.getColorID();
	else
		return %this.controlColor;
}

//@return	string	Tab-delimited list of teams that control this brick.
function FxDtsBrick::getTeamControlList(%this)
{
	%mini = getMinigameFromObject(%this);
	if(isObject(%mini.Teams))
		return %mini.Teams.getTeamsFromColor(%this.getTeamControl(), "TAB");
}

//Used for events.
//Sets whether the brick is "locked", meaning that a team can't use it.
//@param	int mode	0=trigger_team_only, 1=team_color, 2=everyone
//@param	int color
//@param	boolean flag
//@param	GameConnection client
function FxDtsBrick::setTeamControlLocked(%this, %mode, %color, %flag, %client)
{
	if(isObject(%client))
	{
		if(!minigameCanUse(%client, %this))
			return;
	}

	%brColor = %this.getTeamControl();

	switch(%mode)
	{
		case 0: //TriggerTeam
			if(isObject(%client))
			{
				%team = %client.getTeam();
				if(isObject(%team))
				{
					if(%this.isLocked[%team.color] != %flag)
					{
						%this.isLocked[%team.color] = %flag;
						if(%team.color != %brColor)
							%this.capture[%team.color] = 0;
					}
				}
			}

		case 1: //TeamColor
			if(%color !$= "")
			{
				if(%this.isLocked[%color] != %flag)
				{
					%this.isLocked[%color] = %flag;
					if(%color != %brColor)
						%this.capture[%color] = 0;
				}
			}

		case 2: //ALL
			for(%i = 0; %i < 64; %i ++)
			{
				if(%this.isLocked[%i] !=  %flag)
				{
					%this.isLocked[%i] = %flag;
					if(%i != %brColor)
						%this.capture[%i] = 0;
				}
			}
	}
}

//returns a comma separated list of teams controlling the brick - for VCE vars
//@param	bool returnColor	Whether names will be colored.
//@return	string	A human-readable list of team names.
function FxDtsBrick::getControllingTeam(%this, %returnColor) 
{
	%mini = getMinigameFromObject(%this);
	if(!isSlayerMinigame(%mini))
		return;

	%color = %this.getTeamControl();
	%teams = %mini.Teams.getTeamsFromColor(%color, "COM", %returnColor);

	return (%teams $= "" ? "None" : %teams);
}

//team checks useful for events
function fxDTSBrick::checkTeam(%this, %type, %string, %eventnums, %client)
{
	if(!isObject(%client))
		return;

	if(!isFunction(%client.getClassName(), "getTeam"))
		return;

	if(!isObject(%miniGame = getMiniGameFromObject(%client)) || !isObject(%miniGame.Teams))
		return;

	%team = %client.getTeam();
	%teamColor = %team.getColorHex();

	%checkTeam = %miniGame.Teams.getTeamFromName(%string);

	switch(%type)
	{
		case 0:
			%pass = %team == %checkTeam;
		case 1:
			%pass = %team != %checkTeam;
		case 2:
			%pass = %team.isAlliedTeam(%checkTeam);
	}

	if(getWordCount(%eventnums) != 2) {
		if(%pass) {
			%this.onTeamCheckTrue(%client);
		}
		else {
			%this.onTeamCheckFalse(%client);
		}
		return;
	}

	%eventmin = getWord(%eventnums, 0);
	%eventmax = getWord(%eventnums, 1);
	%max = %this.numEvents;
	for(%i = 0; %i < %max; %i++) {
		%input = %this.eventInput[%i];
		
		if(%this.eventInput[%i] $= "onTeamCheckTrue" || %this.eventInput[%i] $= "onTeamCheckFalse") {
			if(%i < %eventmin || %i > %eventmax) {
				%old[%i] = %this.eventEnabled[%i];
				%this.eventEnabled[%i] = 0;
			}
		}
	}

	if(%pass) {
		%this.onTeamCheckTrue(%client);
	}
	else {
		%this.onTeamCheckFalse(%client);
	}
	for(%i = 0; %i < %max; %i++) {
		if(%old[%i] !$= "") {
			%this.eventEnabled[%i] = %old[%i];
		}
	}
}

function fxDTSBrick::checkTeamCount(%this, %string, %operator, %check, %eventnums, %client)
{
	if(!isObject(%miniGame = getMiniGameFromObject(%this)) || !isObject(%miniGame.Teams))
		return;

	%checkTeam = %miniGame.Teams.getTeamFromName(%string);
	switch(%operator)
	{
		case 0:
			%pass = %checkteam.numMembers >= %check;
		case 1:
			%pass = %checkteam.numMembers <= %check;
		case 2:
			%pass = %checkteam.numMembers == %check;
		case 3:
			%pass = %checkteam.numMembers != %check;
	}

	if(getWordCount(%eventnums) != 2) {
		if(%pass) {
			%this.onTeamCheckTrue(%client);
		}
		else {
			%this.onTeamCheckFalse(%client);
		}
		return;
	}

	%eventmin = getWord(%eventnums, 0);
	%eventmax = getWord(%eventnums, 1);
	%max = %this.numEvents;
	for(%i = 0; %i < %max; %i++) {
		%input = %this.eventInput[%i];
		
		if(%this.eventInput[%i] $= "onTeamCheckTrue" || %this.eventInput[%i] $= "onTeamCheckFalse") {
			if(%i < %eventmin || %i > %eventmax) {
				%old[%i] = %this.eventEnabled[%i];
				%this.eventEnabled[%i] = 0;
			}
		}
	}

	if(%pass) {
		%this.onTeamCheckTrue(%client);
	}
	else {
		%this.onTeamCheckFalse(%client);
	}
	for(%i = 0; %i < %max; %i++) {
		if(%old[%i] !$= "") {
			%this.eventEnabled[%i] = %old[%i];
		}
	}
}

function fxDTSBrick::onTeamCheckTrue(%this, %client)
{
	//setup targets
	$InputTarget_["Self"]   = %this;
	$InputTarget_["Player"] = %client.player;
	$InputTarget_["Client"] = %client;

	if($Server::LAN) {
		$InputTarget_["MiniGame"] = getMiniGameFromObject(%client);
	}
	else {
		if(getMiniGameFromObject(%this) == getMiniGameFromObject(%client)) {
			$InputTarget_["MiniGame"] = getMiniGameFromObject(%this);
		}
		else {
			$InputTarget_["MiniGame"] = 0;
		}
		if(isObject(%this.getGroup().client))
		{
			$InputTarget_["OwnerPlayer"] = %this.getGroup().client.Player;
			$InputTarget_["OwnerClient"] = %this.getGroup().client;
		}
		else
		{
			$InputTarget_["OwnerPlayer"] = 0;
			$InputTarget_["OwnerClient"] = 0;
		}
	}
	//process the event
	%this.processInputEvent("onTeamCheckTrue", %client);
}

function fxDTSBrick::onTeamCheckFalse(%this, %client)
{
	//setup targets
	$InputTarget_["Self"]   = %this;
	$InputTarget_["Player"] = %client.player;
	$InputTarget_["Client"] = %client;

	if($Server::LAN) {
		$InputTarget_["MiniGame"] = getMiniGameFromObject(%client);
	}
	else {
		if(getMiniGameFromObject(%this) == getMiniGameFromObject(%client)) {
			$InputTarget_["MiniGame"] = getMiniGameFromObject(%this);
		}
		else {
			$InputTarget_["MiniGame"] = 0;
		}
		if(isObject(%this.getGroup().client))
		{
			$InputTarget_["OwnerPlayer"] = %this.getGroup().client.Player;
			$InputTarget_["OwnerClient"] = %this.getGroup().client;
		}
		else
		{
			$InputTarget_["OwnerPlayer"] = 0;
			$InputTarget_["OwnerClient"] = 0;
		}
	}
	//process the event
	%this.processInputEvent("onTeamCheckFalse", %client);
}

//@return	Point3F	A point centered on the top of the brick.
function FxDtsBrick::getPositionTop(%this)
{
	%box = %this.getWorldBox();
	%pos = %this.getPosition();
	return getWords(%pos, 0, 1) SPC getWord(%box, 5);
}

//Creates a trigger tied to a specific brick.
//@param	datablock data
//@param	string polyhedron	Defines the shape of the trigger. Defaults to "0 0 0 1 0 0 0 -1 0 0 0 1".
function FxDtsBrick::createTrigger(%this, %data, %polyhedron)
{
	if(!isObject(%data))
	{
		Slayer_Support::Error("FxDtsBrick::createTrigger", "Trigger datablock not found.");
		return 0;
	}

	if(%polyhedron $= "")
		%polyhedron = "0 0 0 1 0 0 0 -1 0 0 0 1";

	%trigger = new Trigger()
	{
		brick = %this;
		datablock = %data;
		polyhedron = %polyhedron;
	};
	missionCleanup.add(%trigger);

	%boxMax = getWords(%this.getWorldBox(), 3, 5);
	%boxMin = getWords(%this.getWorldBox(), 0, 2);
	%boxDiff = vectorSub(%boxMax, %boxMin);
	%boxDiff = vectorAdd(%boxDiff, "0 0 0.2"); 
	%trigger.setScale(%boxDiff);

	%posA = %this.getWorldBoxCenter();
	%posB = %trigger.getWorldBoxCenter();
	%posDiff = vectorSub(%posA, %posB);
	%posDiff = vectorAdd(%posDiff, "0 0 0.1");
	%trigger.setTransform(%posDiff);

	return %trigger;
}

//Sets a capture point to the control of a specific team color.
//@param	int color
//@param	bool reset	Whether the CP is being reset to defaults.
//@param	GameConnection client	The client that captured the CP.
function FxDtsBrick::setCPControl(%this, %color, %reset, %client)
{
	%mini = getMinigameFromObject(%this);
	if(!isSlayerMinigame(%mini))
		return;
	if(isObject(%client))
	{
		if(!minigameCanUse(%client, %this))
			return;
		if(!%reset)
			%client.incScore(%mini.points_cp);
	}

	%oldColor = %this.getTeamControl();
	%this.setColor(%color);
	%this.controlColor = %color;

	for(%i = 0; %i < 64; %i ++)
	{
		if(%i != %color)
			%this.capture[%i] = "";
	}

	//onCapture Input Event
	$InputTarget_["Self"] = %this;
	$InputTarget_["Player"] = %client.player;
	$InputTarget_["Client"] = %client;
	$InputTarget_["MiniGame"] = %mini;
	if(%reset)
	{
		%mini.gameMode.callback("onCPReset", %this, %color, %oldColor, %client);
		%this.processInputEvent("onCPReset", %client);
	}
	else
	{
		for(%i = 0; %i < %mini.numMembers["AiController"]; %i ++)
		{
			%ai = %mini.member["AiController", %i];
			%team = %ai.getTeam();
			if(isObject(%team))
			{
				if(%team.color == %color)
				{
					if(%ai.isObjective(%this))
					{
						if(%ai.getCurrentObjective() == %this)
						{
							%ai.removeObjective(%this);
							%ai.goToNextObjective();
						}
						else
						{
							%ai.removeObjective(%this);
						}
					}
				}
				else
				{
					if(!%ai.isObjective(%this))
					{
						%ai.addObjective(%this);
						if(!isObject(%ai.getCurrentObjective()))
							%ai.goToNextObjective(true);
					}
				}
			}
		}

		%mini.gameMode.callback("onCPCapture", %this, %color, %oldColor, %client);
		%this.processInputEvent("onCPCapture", %client);

		for(%i = 0; %i < %mini.Teams.getCount(); %i ++)
		{
			%t = %mini.Teams.getObject(%i);
			if(%t.color == %color)
				%this.processInputEvent("onCPCapture(Team" @ %i + 1 @ ")", %client);
		}
	}
}

//Called when an object enters a capture point.
//@param	Trigger trigger
//@param	Player player
function Slayer_CPTriggerData::onEnterTrigger(%this, %trigger, %player)
{
	if(!(%player.getType() & $TypeMasks::PlayerObjectType) || %player.dataBlock.rideable)
		return;

	%client = %player.client;
	if(!isObject(%client))
		return;

	%mini = getMinigameFromObject(%client);
	%team = %client.getTeam();

	if(!isSlayerMinigame(%mini) || !isObject(%team))
		return;

	Slayer_Support::Debug(3, "CP Enter", %trigger TAB %player TAB %client);

	%client.doNotDisplay = 1; //compatibility with Boom's DM+ script
}

//Called every Slayer_CPTriggerData.tickMS milliseconds that an object is on the capture point.
//@param	Trigger trigger
//@param	Player player
function Slayer_CPTriggerData::onTickTrigger(%this, %trigger, %player)
{
	%brick = %trigger.brick;
	if(!%brick.isRendering())
		return;

	if(!(%player.getType() & $TypeMasks::PlayerObjectType) || %player.dataBlock.rideable)
		return;

	%client = %player.client;
	if(!isObject(%client))
		return;

	%mini = getMinigameFromObject(%client);
	%team = %client.getTeam();

	if(!isSlayerMinigame(%mini) || !isObject(%team) || %mini.isResetting())
		return;

	Slayer_Support::Debug(3, "CP Tick", %trigger TAB %player TAB %client);

	%db = %brick.getDatablock();
	%attCol = %team.color;
	%defCol = %brick.controlColor;
	%maxTicks = %db.CPMaxTicks;

	cancel(%trigger.decreaseTimer[%attCol]);

	if(%attCol != %defCol)
	{
		if(%brick.capture[%attCol] < %maxTicks)
		{
			if(%brick.isLocked[%attCol])
				%client.bottomPrint("<just:center>\c5This capture point is locked. You cannot capture it yet.", 1);
			else
			{
				%trigger.decreaseTimer[%attCol] = %this.scheduleNoQuota(%this.tickPeriodMS + 1000, decreaseCapture, %trigger, %attCol);

				%brick.capture[%attCol] ++;

				for(%i = 0; %i < %brick.capture[%attCol]; %i ++)
					%a = "_" @ %a;
				for(%i = 0; %i < %maxTicks-%brick.capture[%attCol]; %i ++)
					%d = "_" @ %d;

				%rgb = getColorIDTable(%defCol);
				%hex = "<color:" @ Slayer_Support::RgbToHex(%rgb) @ ">";

				%a = %team.getColorHex() @ %a;
				%d = %hex @ %d;

				%client.bottomPrint("<just:center>" @ %a @ %d, 1, true);

				if(%mini.CPTransitionColors)
				{
					%avgColor = Slayer_Support::getAverageColor(%rgb, %team.colorRGB);
					%avgColor = Slayer_Support::getClosestPaintColor(%avgColor);

					%brick.setColor(%avgColor);
				}
			}
		}
		else
		{
			%brick.setCPControl(%attCol, 0, %client);

			for(%i = 0; %i < %brick.capture[%attCol]; %i ++)
				%a = "_" @ %a;
			%client.bottomPrint("<just:center>" @ %team.getColorHex() @ %a, 1, true);

			//bot objectives
			%count = %trigger.getNumObjects();
			for(%i = 0; %i < %count; %i ++)
			{
				%obj = %trigger.getObject(%i);
				if(%obj.isSlayerBot && %obj.objectiveBrick == %brick)
				{
					%obj.hWander = %obj.hOldWander;
					%obj.hAvoidObstacles = %obj.hOldAvoidObstacles;

					if(isObject(%obj.client))
						%obj.client.goToNextObjective(true);
				}
			}
		}
	}
	else
	{
		for(%i = 0; %i < %maxTicks; %i ++)
			%a = "_" @ %a;
		%client.bottomPrint("<just:center>" @ %team.getColorHex() @ %a, 1, true);
	}
}

//Called when an object leaves a capture point.
//@param	Trigger trigger
//@param	Player player
function Slayer_CPTriggerData::onLeaveTrigger(%this, %trigger, %player)
{
	if(!(%player.getType() & $TypeMasks::PlayerObjectType) || %player.dataBlock.rideable)
		return;

	%client = %player.client;
	if(!isObject(%client))
		return;

	%brick = %trigger.brick;
	%mini = getMinigameFromObject(%client);
	%team = %client.getTeam();

	if(!isSlayerMinigame(%mini) || !isObject(%team))
		return;

	Slayer_Support::Debug(3, "CP Leave", %trigger TAB %player TAB %client);

	%client.doNotDisplay = 0; //compatibility with Boom's DM+ script

	if(%player.isSlayerBot)
	{
		if(%player.objective == %brick)
			%player.hPathBrick = %brick;
	}	
}

//Runs recursively after all players leave a CP until capture percentages are 0.
//@param	Trigger trigger
//@param	int color
function Slayer_CPTriggerData::decreaseCapture(%this, %trigger, %color)
{
	cancel(%trigger.decreaseTimer[%color]);

	%brick = %trigger.brick;
	%defCol = %brick.controlColor;

	if(%color == %defCol)
		return;

	Slayer_Support::Debug(3, "CP Decrease Tick", %trigger TAB %color);

	//Check if anyone of that color is still on the CP
	for(%i = 0; %i < %trigger.getNumObjects(); %i ++)
	{
		%pl = %trigger.getObject(%i);
		if(%pl.getType() & $TypeMasks::PlayerObjectType && !%player.dataBlock.rideable)
		{
			%cl = %pl.client;
			if(%cl != %client && %cl.slyrTeam.color == %color)
				return;
		}
	}

	%brick.capture[%color] --;

	if(%brick.capture[%color] <= 0)
		%brick.setColor(%defCol);
	else
		%trigger.decreaseTimer[%color] = %this.scheduleNoQuota(%this.tickPeriodMS, decreaseCapture, %trigger, %color);
}

package Slayer_FxDtsBrick
{
	function FxDtsBrick::onPlant(%this) //this only is called on manual plant
	{
		parent::onPlant(%this);
		%db = %this.getDatablock();
		if(%db.isSlyrBrick || %db.specialBrickType !$= "")
			%this.scheduleNoQuota(15, slayerPrepareBrick, false);
	}

	function FxDtsBrick::onLoadPlant(%this) //called on load plant
	{
		parent::onLoadPlant(%this);
		%db = %this.getDatablock();
		if(%db.isSlyrBrick || %db.specialBrickType !$= "")
			%this.scheduleNoQuota(15, slayerPrepareBrick, true);
	}

	function FxDtsBrick::onRemove(%this)
	{
		if(%this.isPlanted)
		{
			%db = %this.getDatablock();
			%type = %db.slyrType;
			if(%db.isSlyrBrick)
			{
				switch$(%type)
				{
					case "CP":
						if(isObject(%this.cpTrigger))
							%this.cpTrigger.delete();

					case "RegionBoundary":
						if(isObject(Slayer.boundaryBricks))
						{
							Slayer.boundaryBricks.remove(%this);
							for(%i = Slayer.minigames.getCount() - 1; %i >= 0; %i --)
							{
								Slayer.minigames.getObject(%i).calculateRegionBox();
							}
						}
				}
				
				if(isObject(Slayer.minigames))
				{
					for(%i = Slayer.minigames.getCount() - 1; %i >= 0; %i --)
					{
						%m = Slayer.minigames.getObject(%i);
						%m.gameMode.callback("onMiniGameBrickRemoved", %this, %type);
					}
				}
			}
		}

		parent::onRemove(%this);
	}

	function serverCmdAddEvent(%client, %enabled, %inputEventIdx, %delay, %targetIdx,
		%namedTargetNameIdx, %outputEventIdx, %par1, %par2, %par3, %par4)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini) && %mini.restrictOutputEvents)
		{
			%target = inputEvent_getTargetClass("FxDtsBrick", %inputEventIdx, %targetIdx);
			if(%target $= "")
				%target = "FxDtsBrick";
			%outputEvent = outputEvent_getOutputName(%target, %outputEventIdx);

			if($Slayer::Server::Events::RestrictedEvent__[%target, %outputEvent] !$= "")
			{
				if(!%mini.canEdit(%client, $Slayer::Server::Events::RestrictedEvent__[%target, %outputEvent]))
				{
					messageClient(%client, '', "You do not have permission to use the [" @
						%target @ ", " SPC %outputEvent @ "] event.");
					Slayer_Support::Debug(2, "serverCmdAddEvent", %client.getSimpleName() SPC
						"does not have permission to use [" @ %target @ ", " SPC %outputEvent @ "]");
					return;
				}
			}
		}

		parent::serverCmdAddEvent(%client, %enabled, %inputEventIdx, %delay, %targetIdx,
			%NamedTargetNameIdx, %outputEventIdx, %par1, %par2, %par3, %par4);
	}

	function FxDtsBrick::onColorChange(%this)
	{
		%parent = parent::onColorChange(%this);
		if(%this.isPlanted && %this.isSlyrBrick && !%this.getDatablock().setOnceOnly)
			%this.slayerPrepareBrick(false);
		return %parent;
	}

	function FxDtsBrick::onPlayerTouch(%this, %player)
	{
		parent::onPlayerTouch(%this, %player);
		if(%player.getClassName() !$= "Player")
			return;
		%client = %player.client;
		%mini = getMinigameFromObject(%client);
		if(!isSlayerMinigame(%mini))
			return;
		%team = %client.getTeam();
		if(!isObject(%team))
			return;
		$InputTarget_["Self"] = %this;
		$InputTarget_["Player"] = %player;
		$InputTarget_["Client"] = %client;
		$InputTarget_["MiniGame"] = %mini;
		%this.processInputEvent("onPlayerTouch(Team" @ %team.getGroup().indexOf(%team) + 1 @ ")", %client);
	}

	function FxDtsBrick::onActivate(%this, %player, %client)
	{
		parent::onActivate(%this, %player, %client);
		if(%player.getClassName() !$= "Player")
			return;
		%client = %player.client;
		%mini = getMinigameFromObject(%client);
		if(!isSlayerMinigame(%mini))
			return;
		%team = %client.getTeam();
		if(!isObject(%team))
			return;
		$InputTarget_["Self"] = %this;
		$InputTarget_["Player"] = %player;
		$InputTarget_["Client"] = %client;
		$InputTarget_["MiniGame"] = %mini;
		%this.processInputEvent("onActivate(Team" @ %team.getGroup().indexOf(%team) + 1 @ ")", %client);
	}

	function FxDtsBrick::setNTObjectName(%this, %flag)
	{
		%name = lTrim(strReplace(%flag, "_", " "));
		%pre = getWord(%name, 0);
		%oldName = lTrim(strReplace(%this.getName(), "_", " "));
		%oldPre = getWord(%oldName, 0);
		%prefixList = "Team\tTeamVehicle";
		switch$(%pre)
		{
			case "Team" or "TeamVehicle":
				%value = getWords(%name, 1);
				%this.setTeamControl(%value);

			default:
				if(%this.controlColor !$= "" && Slayer_Support::isItemInList(%prefixList, %oldPre))
					%this.setTeamControl("");
		}
		return parent::setNTObjectName(%this, %flag);
	}

	function paintProjectile::onCollision(%this, %obj, %brick, %fade, %pos, %normal)
	{
		if(%brick.getType() & $TypeMasks::fxBrickAlwaysObjectType)
		{
			%oldColor = %brick.getColorID();
			%parent = parent::onCollision(%this, %obj, %brick, %fade, %pos, %normal);
			%color = %brick.getColorID();
			if(%color != %oldColor || getTrustLevel(%obj, %brick) >= $TrustLevel::FXPaint)
			{
				%db = %brick.getDatablock();
				if(%db.isSlyrBrick && %db.slyrType $= "CP")
				{
					%brick.origColor = %color;
					%brick.setCPControl(%color, 1, %obj.client);
				}
			}
			return %parent;
		}
		return parent::onCollision(%this, %obj, %brick, %fade, %pos, %normal);
	}

	function FxDtsBrick::contentTDMTeamAllowUse(%this, %action, %direction, %text, %client)
	{
		%id = %this.contentTypeID();
		if(%id >= 0)
		{
			%mini = getMinigameFromObject(%this.getGroup());

			if(isSlayerMinigame(%mini))
			{
				if(!miniGameCanUse(%client, %this))
					return 0;

				%team = %client.getTeam();

				for(%i = 0; %i < getWordCount(%text); %i ++)
				{
					%w = getWord(%text, %i);

					if(%w == 0 && isObject(%team))
						return 1;
					if(%w == -1 && !isObject(%team))
						return 1;
					else if(isObject(%team) && %team.getGroup().indexOf(%team) + 1 == %w)
						return 1;
				}

				return 0;
			}
			else if(isObject(%mini) && $AddOn__Gamemode_TeamDeathmatch == 1 && isObject(GameModeStorerSO))
			{
				//use the original TDM functionality
				return parent::contentTDMTeamAllowUse(%this, %action, %direction, %text, %client);
			}
			else
			{
				for(%i = 0; %i < getWordCount(%text); %i ++)
				{
					%w = getWord(%text, %i);

					if(%w == -1)
						return 1;
				}

				return 0;
			}
		}
		else
		{
			return -1;
		}
	}

	function FxDtsBrick::contentTDMTeamDenyUse(%this, %action, %direction, %text, %client)
	{
		%id = %this.contentTypeID();
		if(%id >= 0)
		{
			%mini = getMinigameFromObject(%this.getGroup());

			if(isSlayerMinigame(%mini))
			{
				if(!miniGameCanUse(%client, %this))
					return 0;

				%team = %client.getTeam();

				for(%i = 0; %i < getWordCount(%text); %i ++)
				{
					%w = getWord(%text, %i);

					if(%w == 0 && isObject(%team))
						return 0;
					if(%w == -1 && !isObject(%team))
						return 0;
					else if(isObject(%team) && %team.getGroup().indexOf(%team) + 1 == %w)
						return 0;
				}

				return 1;
			}
			else if(isObject(%mini) && $AddOn__Gamemode_TeamDeathmatch == 1 && isObject(GameModeStorerSO))
			{
				//use the original TDM functionality
				return parent::contentTDMTeamDenyUse(%this, %action, %direction, %text, %client);
			}
			else
			{
				for(%i = 0; %i < getWordCount(%text); %i ++)
				{
					%w = getWord(%text, %i);

					if(%w == -1)
						return 0;
				}

				return 1;
			}
		}
		else
		{
			return -1;
		}
	}

	function onMissionLoaded()
	{
		//Slayer.scheduleNoQuota(0, createBrickEvents);
		%parent = parent::onMissionLoaded();
		Slayer.createBrickEvents();
		return %parent;
	}

	function Slayer::createBrickEvents(%this)
	{
		// +--------------+
		// | Input Events |
		// +--------------+
		registerInputEvent(FxDtsBrick, onCPReset, "Self FxDtsBrick\tMiniGame MiniGame");
		registerInputEvent(FxDtsBrick, onCPCapture, "Self FxDtsBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
		for(%i=1; %i <= $Pref::Slayer::Server::Teams::maxEvents; %i ++)
		{
			registerInputEvent(FxDtsBrick, "onCPCapture(Team" @ %i @ ")", "Self FxDtsBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
			registerInputEvent(FxDtsBrick, "onPlayerTouch(Team" @ %i @ ")", "Self FxDtsBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
			registerInputEvent(FxDtsBrick, "onActivate(Team" @ %i @ ")", "Self FxDtsBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
		}

		registerInputEvent("fxDTSBrick", "onTeamCheckTrue", "Self fxDTSBrick" TAB "Player Player" TAB "Client GameConnection" TAB "MiniGame MiniGame" TAB "OwnerPlayer Player" TAB "OwnerClient GameConnection", 1);
		registerInputEvent("fxDTSBrick", "onTeamCheckFalse", "Self fxDTSBrick" TAB "Player Player" TAB "Client GameConnection" TAB "MiniGame MiniGame" TAB "OwnerPlayer Player" TAB "OwnerClient GameConnection", 1);

		registerMultiSourceInputEvent(FxDtsBrick, "onMinigameDeath", 
			"Self FxDtsBrick\tClient GameConnection\tPlayer(Killer) Player\tClient(Killer) GameConnection\tMiniGame MiniGame");
		registerMultiSourceInputEvent(FxDtsBrick, "onMinigameJoin", "Self FxDtsBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
		registerMultiSourceInputEvent(FxDtsBrick, "onMinigameLeave", "Self FxDtsBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
		registerMultiSourceInputEvent(FxDtsBrick, "onMinigameRoundStart", "Self FxDtsBrick\tMiniGame MiniGame");
		registerMultiSourceInputEvent(FxDtsBrick, "onMinigameRoundEnd", "Self FxDtsBrick\tMiniGame MiniGame");
		// +---------------+
		// | Output Events |
		// +---------------+
		registerOutputEvent(FxDtsBrick, "setTeamControl", "paintColor 0", 1);
		registerOutputEvent(FxDtsBrick, "setTeamControlLocked", "list TriggerTeam 0 TeamColor 1 ALL 2" TAB "paintColor 0" TAB "bool", 1);
		//registerOutputEvent(FxDtsBrick, setTeamSpawn, "paintColor 0", 1);
		registerOutputEvent(GameConnection, "addLives", "int 0 200 1");
		registerOutputEvent(GameConnection, "addDeaths", "int 0 200 1");
		registerOutputEvent(GameConnection, "addKills", "int 0 200 1");
		registerOutputEvent(GameConnection, "setLives", "int 0 200 1");
		registerOutputEvent(GameConnection, "setDeaths", "int 0 200 1");
		registerOutputEvent(GameConnection, "setKills", "int 0 200 1");
		registerOutputEvent(GameConnection, "joinTeam", "string 50 80" TAB "string 50 80" TAB "bool 0");
		registerOutputEvent(Minigame, "incTimeRemaining", "int -999 999 1" TAB "bool 1");
		registerOutputEvent(Minigame, "setTimeRemaining", "int 1 999 1" TAB "bool 1");
		registerOutputEvent(Minigame, "Win", "list TriggerPlayer 0 TriggerTeam 1 CustomPlayer 2 CustomTeam 3 CustomString 4 NONE 5" TAB "string 80 100", 1);

		registerOutputEvent("fxDTSBrick", "checkTeam", "list IsMember 0 IsNotMember 1 IsAllied 2" TAB "string 50 75" TAB "string 8 30", 1);
		registerOutputEvent("fxDTSBrick", "checkTeamCount", "string 50 75" TAB "list >= 0 <= 1 == 2 != 3" TAB "string 8 30" TAB "string 8 30", 1);

		registerEventTarget("Team(Client) Slayer_TeamSO", "GameConnection", "%client.slyrTeam");
		registerEventTarget("Team(Brick) Slayer_TeamSO", "FxDtsBrick", "%this.getTeamControlList()");
		registerOutputEvent(Slayer_TeamSO, "BottomPrintAll", "string 200 156" TAB "int 1 10 3" TAB "bool 0", 1);
		registerOutputEvent(Slayer_TeamSO, "CenterPrintAll", "string 200 156" TAB "int 1 10 3", 1);
		registerOutputEvent(Slayer_TeamSO, "ChatMsgAll", "string 200 176", 1);
		registerOutputEvent(Slayer_TeamSO, "RespawnAll", "", 1);
		registerOutputEvent(Slayer_TeamSO, "IncScore", "int -999999 999999 1", 1);

		// registerEventTarget("TeamCaptain GameConnection", "GameConnection", "%client.slyrTeam.captain");

		if(isFile($Slayer::Server::ConfigDir @ "/.advancedEvents.txt"))
		{
			Slayer_Support::Debug(0, "\c2WARNING: Registering Advanced Events - INSECURE");

			registerOutputEvent(Minigame, "Slayer_setPref", "string 50 80" TAB "string 50 80" TAB "string 50 80", 1);
		}

		// +--------------------------+
		// | JVS Content Restrictions |
		// +--------------------------+
		if($AddOn__JVS_Content == 1 && isFunction(ContentRestrictionsSO, addRestriction))
		{
			//we are using the exact same restrictions as Space Guy's TDM mod
			//this makes things easier for users
			ContentRestrictionsSO.addRestriction("TeamAllow", "contentTDMTeamAllowUse", "10100130");
			ContentRestrictionsSO.addRestriction("TeamDeny", "contentTDMTeamDenyUse", "10200130");
		}

		// +-----------------------+
		// | VCE Special Variables |
		// +-----------------------+
		if($AddOn__Event_Variables == 1 && isFunction(registerSpecialVar))
		{
			Slayer_Support::Debug(2, "VCE", "Registering variables");

			registerSpecialVar(FxDtsBrick, "teamName", "%this.getControllingTeam(0)");
			registerSpecialVar(FxDtsBrick, "teamNameColored", "%this.getControllingTeam(1)");

			registerSpecialVar(GameConnection, "lives", "%this.getLives()");
			registerSpecialVar(GameConnection, "kills", "%this.getKills()");
			registerSpecialVar(GameConnection, "deaths", "%this.getDeaths()");
			registerSpecialVar(GameConnection, "team", "%this.getTeam().getGroup().indexOf(%this.getTeam()) + 1");
			registerSpecialVar(GameConnection, "teamName", "%this.getTeam().name");
			registerSpecialVar(GameConnection, "teamNameColored", "\"<sPush>\" @ %this.getTeam().getColorHex() @ %this.getTeam().name @ \"<sPop>\"");
			registerSpecialVar(GameConnection, "teamColor", "%this.getTeam().getColorHex()");
			registerSpecialVar(GameConnection, "teamColorID", "%this.getTeam().color");
			registerSpecialVar(GameConnection, "teamScore", "%this.getTeam().getScore()");
			registerSpecialVar(GameConnection, "teamMembers", "%this.getTeam().numMembers");

			registerSpecialVar(MinigameSO, "gamemode", "%this.mode");
			registerSpecialVar(MinigameSO, "resetting", "%this.isResetting()");
			registerSpecialVar(MinigameSO, "useTeams", "%this.gameMode.template.useTeams");
			registerSpecialVar(MinigameSO, "lives", "%this.lives");
			registerSpecialVar(MinigameSO, "points", "%this.points");
			registerSpecialVar(MinigameSO, "time", "%this.time");
			registerSpecialVar(MinigameSO, "teamCount", "%this.Teams.getCount()");
			registerSpecialVar(MinigameSO, "numLiving", "%this.getLiving()");
			registerSpecialVar(MinigameSO, "numSpawned", "%this.getSpawned()");
			registerSpecialVar(MinigameSO, "numMembers", "%this.numMembers");

			for(%i = 1; %i <= $Pref::Slayer::Server::Teams::maxEvents; %i ++)
			{
				registerSpecialVar(MinigameSO, "teamScore" @ %i, "%this.Teams.getObject(" @ %i - 1 @ ").getScore()");
				registerSpecialVar(MinigameSO, "teamLiving" @ %i, "%this.Teams.getObject(" @ %i - 1 @ ").getLiving()");
				registerSpecialVar(MinigameSO, "teamSpawned" @ %i, "%this.Teams.getObject(" @ %i - 1 @ ").getSpawned()");
				registerSpecialVar(MinigameSO, "teamIsDead" @ %i, "%this.Teams.getObject(" @ %i - 1 @ ").isTeamDead()");
				registerSpecialVar(MinigameSO, "teamKills" @ %i, "%this.Teams.getObject(" @ %i - 1 @ ").getKills()");
				registerSpecialVar(MinigameSO, "teamDeaths" @ %i, "%this.Teams.getObject(" @ %i - 1 @ ").getDeaths()");
				registerSpecialVar(MinigameSO, "teamMembers" @ %i, "%this.Teams.getObject(" @ %i - 1 @ ").numMembers");
			}
		}
	}
};
activatePackage(Slayer_FxDtsBrick);


//----------------------------------------------------------------------
// Deprecated Functions - DO NOT USE
//----------------------------------------------------------------------

//@DEPRECATED
function FxDtsBrick::getControl(%this)
{
	warn("\c2WARN: FxDtsBrick::getControl is deprecated! Use the new API.");
	return %this.getTeamControl();
}

//@DEPRECATED
function FxDtsBrick::getControlTeams(%this)
{
	warn("\c2WARN: FxDtsBrick::getControlTeams is deprecated! Use the new API.");
	return %this.getTeamControlList();
}