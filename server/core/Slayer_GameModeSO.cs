// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

//Translate old game modes classes
//Add a similar line to your game mode if the class has changed.
$Slayer::GameModes::NewClass["Slyr"] = "Slayer_Deathmatch";
$Slayer::GameModes::NewClass["TSlyr"] = "Slayer_TeamDeathmatch";

//Translate old callback names
$Slayer::GameModes::OldCallback["miniGameCanDamage"] = "canDamage";
$Slayer::GameModes::OldCallback["miniGameCanUse"] = "canUse";
$Slayer::GameModes::OldCallback["miniGameClientCanDamage"] = "clientCanDamage";
$Slayer::GameModes::OldCallback["onClientChat"] = "onChat";
$Slayer::GameModes::OldCallback["onClientDisplayRules"] = "onRules";
$Slayer::GameModes::OldCallback["onClientJoinGame"] = "onJoin";
$Slayer::GameModes::OldCallback["onClientJoinTeam"] = "Teams_onJoin";
$Slayer::GameModes::OldCallback["onClientLeaveGame"] = "onLeave";
$Slayer::GameModes::OldCallback["onClientLeaveTeam"] = "Teams_onLeave";
$Slayer::GameModes::OldCallback["onClientSetScore"] = "onScore";
$Slayer::GameModes::OldCallback["onCreateDeathMessage"] = "onDeathMessage";
$Slayer::GameModes::OldCallback["onGameModeEnd"] = "onModeEnd";
$Slayer::GameModes::OldCallback["onGameModeStart"] = "onModeStart";
$Slayer::GameModes::OldCallback["onMiniGameBrickAdded"] = "onSlyrBrickAdd";
$Slayer::GameModes::OldCallback["onMiniGameBrickRemoved"] = "onSlyrBrickRemove";
$Slayer::GameModes::OldCallback["onMiniGameReset"] = "postReset";
$Slayer::GameModes::OldCallback["onPlayerDeath"] = "postDeath";
$Slayer::GameModes::OldCallback["onPlayerSpawn"] = "onSpawn";
$Slayer::GameModes::OldCallback["onRoundEnd"] = "postVictory";
$Slayer::GameModes::OldCallback["onTeamAdd"] = "Teams_onAdd";
$Slayer::GameModes::OldCallback["onTeamChat"] = "Teams_onChat";
$Slayer::GameModes::OldCallback["onTeamRemove"] = "Teams_onRemove";
$Slayer::GameModes::OldCallback["onTeamShuffle"] = "Teams_onShuffle";
$Slayer::GameModes::OldCallback["onTeamSwap"] = "Teams_onTeamMemberSwap";
$Slayer::GameModes::OldCallback["pickPlayerSpawnPoint"] = "pickSpawnPoint";
$Slayer::GameModes::OldCallback["preMiniGameReset"] = "preReset";
$Slayer::GameModes::OldCallback["prePlayerDeath"] = "preDeath";
$Slayer::GameModes::OldCallback["preRoundEnd"] = "preVictory";


//Calls a game mode callback.
//@param	string method
//@return	string	Returns from game mode callback.
function Slayer_GameModeSO::callback(%this, %method, %v0, %v1, %v2, %v3, %v4,
	%v5, %v6, %v7, %v8, %v9, %v10, %v11, %v12, %v13, %v14, %v15, %v16, %v17)
{
	if(isFunction(%this.class, %method))
	{
		return %this.call(%method, %v0, %v1, %v2, %v3, %v4, %v5, %v6, %v7, %v8,
			%v9, %v10, %v11, %v12, %v13, %v14, %v15, %v16, %v17);
	}
	else if(isFunction(%fn = "Slayer_" @ %this.class @ "_" @ %method))
	{
		%new = %this.class @ "::" @ %method;
		warn("WARN:" SPC %fn SPC "is deprecated! Use" SPC %new @ ".");
		return call(%fn, %this.miniGame, %v0, %v1, %v2, %v3, %v4, %v5, %v6, %v7, %v8,
			%v9, %v10, %v11, %v12, %v13, %v14, %v15, %v16, %v17);
	}
	else if(strLen(%old = $Slayer::GameModes::OldCallback[%method]))
	{
		if(isFunction(%fn = "Slayer_" @ %this.class @ "_" @ %old))
		{
			%new = %this.class @ "::" @ %method;
			warn("WARN:" SPC %fn SPC "is deprecated! Use" SPC %new @ ".");
			return call(%fn, %this.miniGame, %v0, %v1, %v2, %v3, %v4, %v5, %v6, %v7,
				%v8, %v9, %v10, %v11, %v12, %v13, %v14, %v15, %v16, %v17);
		}
		else if(isFunction(%this.class, %old))
		{
			%oldFn = %this.class @ "::" @ %old;
			%new = %this.class @ "::" @ %method;
			warn("WARN:" SPC %oldFn SPC "is deprecated! Use" SPC %new @ ".");
			return %this.call(%old, %v0, %v1, %v2, %v3, %v4, %v5, %v6, %v7,
				%v8, %v9, %v10, %v11, %v12, %v13, %v14, %v15, %v16, %v17);
		}
	}
	return "";
}

//Applies preferences from the game mode template.
function Slayer_GameModeSO::applyDefaultPreferences(%this)
{
	%prefs = %this.miniGame.prefs;
	for(%i = %prefs.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %prefs.getObject(%i);
		if(%pref.isMinigameVar)
		{
			%default = (strLen(%this.template.locked_[%pref.variable]) ?
				%this.template.locked_[%pref.variable] : %this.template.default_[%pref.variable]);
			if(strLen(%default))
			{
				%pref.setValue(%default, %this.miniGame);
			}
		}
	}
}

//Applies preferences from the game mode template to a specific team.
//@param	Slayer_TeamSO team
//@param	ScriptObject teamTemplate
function Slayer_GameModeSO::applyDefaultTeamPreferences(%this, %team, %teamTemplate)
{
	%prefs = %team.getGroup().prefs;
	for(%i = %prefs.getCount() - 1; %i >= 0; %i --)
	{
		%pref = %prefs.getObject(%i);
		%default = (strLen(%teamTemplate.locked_[%pref.variable]) ?
			%teamTemplate.locked_[%pref.variable] : %teamTemplate.default_[%pref.variable]);
		if(strLen(%default))
		{
			%pref.setValue(%default, %team);
		}
	}
}

//Create built-in teams for the game mode.
function Slayer_GameModeSO::createDefaultTeams(%this)
{
	%count = %this.template.getCount();
	for(%i = 0; %i < %count; %i ++)
	{
		%template = %this.template.getObject(%i);
		%team = %this.miniGame.teams.addTeam();
		%team.template = %template;
		%this.applyDefaultTeamPreferences(%team, %template);
	}
}

//Prepare bricks which will be used by the game-mode.
function Slayer_GameModeSO::prepareSlayerBricks(%this)
{
	for(%i = Slayer.slayerBricks.getCount() - 1; %i >= 0; %i --)
	{
		%brick = Slayer.slayerBricks.getObject(%i);
		%type = %brick.getDatablock().slyrType;
		%this.callback("onMiniGameBrickAdded", %brick, %type);
	}
}