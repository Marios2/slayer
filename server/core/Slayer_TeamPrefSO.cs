// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_TeamPrefSO::onAdd(%this)
{
	%p = Slayer.TeamPrefs.getPrefSO(%this.category, %this.title);
	if(isObject(%p))
	{
		%p.delete();
		Slayer_Support::Debug(1, "Overwriting team preference", %this.category TAB %this.title);
	}
	else
	{
		Slayer_Support::Debug(2, "Slayer_TeamPrefSO::onAdd", %this.category TAB %this.title);
	}
	%this.schedule(0, onAdded, %this);
}

function Slayer_TeamPrefSO::onAdded(%this)
{
	Slayer.TeamPrefs.add(%this);
	if(isObject(missionCleanup) && missionCleanup.isMember(%this))
		missionCleanup.remove(%this);
}

//Sets preference to a specified value.
//@param	string value
//@param	Slayer_TeamSO team
//@return	bool	The value was set successfully.
function Slayer_TeamPrefSO::setValue(%this, %value, %team)
{
	if(!isObject(%team))
		return false;

	%proof = %this.idiotProof(%value);
	if(getField(%proof, 0))
		%value = getField(%proof, 1);
	else
		return false;

	if(%this.callback !$= "")
	{
		%callback = strReplace(%this.callback, "%1", "\"" @ %value @ "\"");
		%callback = strReplace(%callback, "%2", "\"" @ %this.getValue(%team) @ "\"");
		%callback = striReplace(%callback, "%team", %team);
		%callback = striReplace(%callback, "%mini", %team.minigame);
	}

	Slayer_Support::setDynamicVariable(%team @ "." @ %this.variable, %value);

	Slayer_Support::Debug(2, "Team Preference Set", %team TAB %this.category TAB %this.title TAB %value);

	if(%callback !$= "")
		eval(%callback);

	return true;
}

//@param	Slayer_TeamSO team
//@return	string	The value of this preference.
function Slayer_TeamPrefSO::getValue(%this, %team)
{
	return Slayer_Support::getDynamicVariable(%team @ "." @ %this.variable);
}

//Checks that %value is within acceptable limits.
//@param	string value
//@return	bool
//@see	Slayer_PrefSO::idiotProof
function Slayer_TeamPrefSO::idiotProof(%this, %value)
{
	return Slayer_PrefSO::idiotProof(%this, %value);
}

//Returns a human-readable version of the value.
//@param	Slayer_TeamSO team
//@return	string
//@see	Slayer_PrefSO::getDisplayValue
function Slayer_TeamPrefSO::getDisplayValue(%this, %value)
{
	return Slayer_PrefSO::getDisplayValue(%this, %value);
}