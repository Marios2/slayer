// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

function Slayer_MiniGameHandlerSG::onAdd(%this)
{
	
}

function Slayer_MiniGameHandlerSG::onRemove(%this)
{
	%this.endAllMinigames();
}

function Slayer_MiniGameHandlerSG::addMinigame(%this, %client, %configFilePrefix)
{
	if(isObject(%client))
	{
		if(!%this.canCreateMinigame(%client))
			return;

		%blid = %client.getBLID();
	}
	else
	{
		%blid = Slayer_Support::getBlocklandID();
	}

	Slayer_Support::Debug(1, "Minigame", "Starting...");
		
	%mini = new ScriptObject()
	{
		class = Slayer_MinigameSO;
		superClass = MinigameSO;

		creatorBLID = %blid;
		owner = 0;

		isDedicatedStart = !isObject(%client);

		configFilePrefix = %configFilePrefix;
	};

	if(isObject(%client))
		%mini.autoAddCreator = true;

	return %mini;
}

function Slayer_MiniGameHandlerSG::endAllMinigames(%this)
{
	for(%i = 0; %i < %this.getCount(); %i ++)
		%this.getObject(%i).endGame();
}

function Slayer_MiniGameHandlerSG::getMinigameFromBLID(%this, %blid)
{
	if(!Slayer_Support::isFloat(%blid))
		return 0;

	for(%i = 0; %i < %this.getCount(); %i ++)
	{
		%mini = %this.getObject(%i);
		if(%mini.creatorBLID == %blid)
		{
			return %mini;
		}
	}

	return 0;
}

function Slayer_MiniGameHandlerSG::getHostMinigame(%this)
{
	%blid = Slayer_Support::getBlocklandID();

	return %this.getMinigameFromBLID(%blid);
}

function Slayer_MiniGameHandlerSG::getHighestPriorityMinigame(%this, %blid)
{
	%m = %this.getMinigameFromBLID(%blid);
	if(isObject(%m))
	{
		%mini = %m; //owner has #1 priority
	}
	else
	{
		%hostID = Slayer_Support::getBlocklandID();

		for(%i = 0; %i < %this.getCount(); %i ++)
		{
			%m = %this.getObject(%i);
			if(!%m.useAllPlayersBricks)
				continue;

			if(%m.creatorBLID == %hostID) //host has #2 priority
				return %m;

			%mini = %m; //this is a crappy way of doing things
		}
	}

	return (isObject(%mini) ? %mini : -1);
}

function Slayer_MiniGameHandlerSG::setDefaultMinigame(%this, %mini)
{
	%this.defaultMinigame = %mini;

	for(%i = 0; %i < %this.getCount(); %i ++)
	{
		%m = %this.getObject(%i);
		%m.isDefaultMinigame = (%mini == %m);
		%m.broadcastMinigame();
	}

	if(isObject(%mini))
	{
		for(%i = 0; %i < clientGroup.getCount(); %i ++)
		{
			%cl = clientGroup.getObject(%i);
			if(%cl.hasSpawnedOnce)
			{
				%m = getMinigameFromObject(%cl);
				if(!isObject(%m))
					%mini.addMember(%cl);
			}
		}
	}
}

function Slayer_MiniGameHandlerSG::broadcastAllMinigames(%this, %client, %onlyChanged)
{
	for(%i = 0; %i < %this.getCount(); %i ++)
		%this.getObject(%i).broadcastMinigame(%client, %onlyChanged);
}

function Slayer_MiniGameHandlerSG::canCreateMinigame(%this, %client, %overRide)
{
	if(%client.isHost)
		return true;

	%mini = getMinigameFromObject(%client);
	if(isSlayerMinigame(%mini))
	{
		if(!%mini.canLeave(%client))
		{
			return false;
		}
	}

	%create = $Pref::Slayer::Server::MiniGameCreationRights;
	%level = %client.getAdminLvl();

	if(%level <= %create || (%level <= %overRide && %overRide !$= "" && %overRide != -1))
		return true;

	return false;
}

package Slayer_MiniGameHandlerSG
{
	function onMissionLoaded()
	{
		%parent = parent::onMissionLoaded();

		// +-------------------------+
		// | Minigame Initialization |
		// +-------------------------+
		if(strLen($Slayer::Server::CurGameModeArg))
		{
			%gameModePath = filePath($Slayer::Server::CurGameModeArg);
			%configFilePrefix = %gameModePath @ "/slayer";
			if(isFile(%configFilePrefix @ ".mgame.csv"))
				Slayer.Minigames.schedule(0, addMinigame, 0, %configFilePrefix);
			else if(isFile(%oldConfig = %gameModePath @ "/config_slayer.cs"))
			{
				//legacy mode
				Slayer.Minigames.schedule(0, addMinigame, 0, %oldConfig);
			}
		}
		else if($Pref::Slayer::Server::AutoStartMiniGame)
			Slayer.Minigames.schedule(0, addMinigame, 0, $Slayer::Server::ConfigDir @ "/last");
		
		return %parent;
	}
};
activatePackage(Slayer_MiniGameHandlerSG);
