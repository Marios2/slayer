// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

$Slayer::Server::Minigames::ResetTimeOut = 5000;

function Slayer_MiniGameSO::onAdd(%this)
{
	%this.isStarting = true;
	%this.isSlayerMinigame = true;
	%this.isResetting = false;
	%this.numMembers = 0;

	%this.prefs = Slayer.Prefs;
	%this.teamPrefs = Slayer.TeamPrefs;
	%this.teams = new ScriptGroup()
	{
		class = Slayer_TeamHandlerSG;
		minigame = %this;
	};
	
	%this.spectateTargets = new SimSet();
	for(%i = Slayer.capturePoints.getCount() - 1; %i >= 0; %i --)
		%this.spectateTargets.add(Slayer.capturePoints.getObject(%i));

	%this.onStart();
}

function Slayer_MiniGameSO::onRemove(%this)
{
	%blid = Slayer_Support::getBlocklandID();
	if(%this.creatorBLID == %blid && ($Slayer::Server::CurGameModeArg $= "" || $Slayer::Server::CurGameModeArg $= $Slayer::Server::GameModeArg))
	{
		Slayer.Prefs.exportMiniGamePreferences($Slayer::Server::ConfigDir @ "/last.mgame.csv", %this);
		Slayer.TeamPrefs.exportTeamPreferences($Slayer::Server::ConfigDir @ "/last.teams.csv", %this);
	}

	if(isObject(%this.teams))
		%this.teams.delete();
	if(isObject(%this.spectateTargets))
		%this.spectateTargets.delete();
}

function Slayer_MiniGameSO::onStart(%this)
{
	Slayer.Minigames.add(%this);

	//PREFERENCES
	Slayer.Prefs.resetPreferences(%this);

	//Host name
	%cl = findClientByBL_ID(%this.creatorBLID);
	if(isObject(%cl))
		%this.hostName = %cl.getPlayerName();

	//AUTOMATIC STARTUP
	if(%this.isDedicatedStart)
	{
		//Load configuration files.
		if(strLen(%this.configFilePrefix))
		{
			if(fileExt(%this.configFilePrefix) $= ".cs")
			{
				warn("\c2WARN: Using legacy compatibility mode for config file ", %this.configFilePrefix);
				%mini = %this;
				exec(%this.configFilePrefix);
			}
			else
			{
				%miniGameConfig = %this.configFilePrefix @ ".mgame.csv";
				if(isFile(%miniGameConfig))
				{
					Slayer.Prefs.importMiniGamePreferences(%miniGameConfig, %this);
					%teamConfig = %this.configFilePrefix @ ".teams.csv";
					if(isFile(%teamConfig) && isObject(%this.teams))
					{
						Slayer.TeamPrefs.importTeamPreferences(%teamConfig, %this);
					}
				}
			}
		}

		//Are we using the BL Slayer game mode? (from the main menu)
		if($Slayer::Server::CurGameModeArg $= $Slayer::Server::GameModeArg)
		{
			%this.isDefaultMinigame = true;
		}
		else if($Slayer::Server::CurGameModeArg !$= "")
		{
			//"I reject your minigame and substitute my own" - replace the BL default minigame
			if(isObject($DefaultMiniGame))
				$DefaultMiniGame.delete();
		}
	}

	//add to the join minigame GUI
	%this.broadcastMinigame();

	%this.isStarting = false;

	%this.updateDefaultMinigame(%this.isDefaultMinigame);

	//callback for teams - must be called afterwards
	%this.teams.onMinigameStart();

	%this.reset(0);
	%this.lastResetTime = 0;

	Slayer_Support::Debug(1, "Slayer_MiniGameSO::onStart");
}

//@DEPRECATED
function Slayer_MiniGameSO::preConfigLoad(%this, %file)
{
	Slayer_Support::Debug(2, "Loading config file", %file);

	if(isObject(%this.Teams))
		%this.Teams.deleteAll();
}

//@DEPRECATED
function Slayer_MiniGameSO::postConfigLoad(%this, %file)
{
	Slayer_Support::Debug(2, "Config file loaded", %file);

	if(!isObject(%this.gamemode)) //in case the gamemode they were using before was disabled
	{
		%this.setPref("Minigame", "Gamemode", "Slyr");
	}
	else if(%this.gameMode.template.useTeams && isObject(%this.Teams) && %this.Teams.getAutoSortTeamCount() > 0)
	{
		%this.Teams.autoSortAll(1);
	}

	//corruption protection
	//if this prefs isn't set, something went wrong with the config file
	//reset to defaults
	if(%this.getPref("Permissions", "Edit Rights") $= "")
		Slayer.Prefs.resetPreferences(%this);
}

function Slayer_MiniGameSO::broadcastMinigame(%this, %client, %onlyIfChanged)
{
	%blid = %this.creatorBLID;
	%defaultMini = (Slayer.Minigames.defaultMinigame == %this ? "Yes" : "No");

	if(isObject(%client))
	{
		%canEdit = %this.canEdit(%client);
		%inviteOnly = (%this.inviteOnly && !%canEdit);
		%data = %this.hostName TAB %blid TAB %this.title TAB %inviteOnly TAB %defaultMini TAB %canEdit;

		if(%onlyIfChanged && %client.lastMinigameBroadcast[%this] $= %data)
			return;

		%client.lastMinigameBroadcast[%this] = %data;

		commandToClient(%client, 'addMinigameLine', %data, %this.getID(), %this.colorIDX);
	}
	else
	{
		for(%i = 0; %i < clientGroup.getCount(); %i ++)
		{
			%cl = clientGroup.getObject(%i);

			%canEdit = %this.canEdit(%cl);
			%inviteOnly = (%this.inviteOnly && !%canEdit);
			%data = %this.hostName TAB %blid TAB %this.title TAB %inviteOnly TAB %defaultMini TAB %canEdit;

			if(%onlyIfChanged && %cl.lastMinigameBroadcast[%this] $= %data)
				continue;

			%cl.lastMinigameBroadcast[%this] = %data;

			commandToClient(%cl, 'addMinigameLine', %data, %this.getID(), %this.colorIDX);
		}
	}
}

function Slayer_MiniGameSO::victoryCheck_Lives(%this)
{
	if(%this.isResetting() || %this.isEnding)
		return -1;

	%special = %this.gameMode.callback("victoryCheck_Lives");
	if(%special !$= "")
		return %special;

	//check if any teams have remaining players
	%teamCount = %this.Teams.getCount();
	if(%this.gameMode.template.useTeams && %teamCount > 0)
	{
		%winnerColor = -1;
		for(%i = 0; %i < %teamCount; %i ++)
		{
			%t = %this.Teams.getObject(%i);
			if(%t.spectate)
				continue;
			if(!%t.isTeamDead())
			{
				if(%this.teams_allySameColors && %t.color == %winnerColor)
				{
					%winner = setField(%winner, getFieldCount(%winner), %t);
				}
				else
				{
					%winner = %t;
					%winnerColor = %t.color;
					%count ++;
				}
			}
		}
		if(%count > 1)
			return -1;
	}

	//check if anyone without a team is still alive
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%cl = %this.member[%i];
		if(!%cl.Dead() && !isObject(%cl.getTeam()))
		{
			%winner = %cl;
			%count ++;
		}
	}

	if(%count == 1)
		return %winner;
	else if(%count > 1)
		return -1;
	else
		return 0;
}

function Slayer_MiniGameSO::victoryCheck_Points(%this)
{
	if(%this.isResetting())
		return -1;

	if(%this.points <= 0)
		return -1;

	%special = %this.gameMode.callback("victoryCheck_Points");

	if(%special >= 0 && %special !$= "")
		return %special;

	if(%this.gameMode.template.useTeams && %this.Teams.getCount() > 0)
	{
		for(%i = 0; %i < %this.Teams.getCount(); %i ++)
		{
			%t = %this.Teams.getObject(%i);
			if(%t.spectate)
				continue;
			if(%t.getScore() >= %this.points)
			{
				%winner = %t;
				break;
			}
		}
	}

	if(!isObject(%winner))
	{
		for(%i = 0; %i < %this.numMembers; %i ++)
		{
			%cl = %this.member[%i];
			if(!isObject(%cl.slyrTeam) && %cl.score >= %this.points)
			{
				%winner = %cl;
				break;
			}
		}
	}

	if(isObject(%winner))
		return %winner;
	else
		return -1;
}

function Slayer_MiniGameSO::victoryCheck_Time(%this, %ticks)
{
	if(%this.time <= 0 || %this.isResetting())
		return;

	//round time - total
	%time = %this.time * 60000;
	%this.timeTicks = %ticks;

	//round time - tick size
	%this.timeTickLength = (%ticks <= 0 ? 60000 : %this.timeTickLength);
	%timeTickLength = %this.timeTickLength;

	//round time - remaining
	%this.timeRemaining = (%ticks <= 0 ? %time : %this.timeRemaining - %timeTickLength);
	%timeRemaining = %this.timeRemaining;

	//round time - elapsed
	%this.timeElapsed = %time - %this.timeRemaining;
	%timeElapsed = %this.timeElapsed;

	Slayer_Support::Debug(2, "Slayer_MiniGameSO::victoryCheck_Time", "Time remaining:" SPC %timeRemaining);

	//CHECK IF THE GAMEMODE HAS ANYTHING TO SAY ABOUT THIS
	%special = %this.gameMode.callback("victoryCheck_Time", %ticks);
	if(%special > 0 && %special !$= "")
	{
		%this.endRound(%special);
		return;
	}
	if(%special < 0 && %special !$= "")
		return;

	if(%timeElapsed >= %time)
	{
		%least = 0;
		%count = 0;
		if(%this.gameMode.template.useTeams && %this.Teams.getCount() > 0)
		{
			for(%i = 0; %i < %this.Teams.getCount(); %i ++)
			{
				%t = %this.Teams.getObject(%i);
				if(%t.spectate)
					continue;

				%sc = %t.getScore();
				if((%sc > %least || %t.winOnTimeUp) && !%winOnTimeUp)
				{
					%winner = %t;
					%least = %sc;
					%count = 1;

					if(%t.winOnTimeUp)
						%winOnTimeUp = 1;
				}
				else if((%sc == %least && %least > 0 && !%winOnTimeUp) || (%winOnTimeUp && %t.winOnTimeUp))
				{
					%winner = setField(%winner, %count, %t);
					%count ++;
				}
			}
		}
		for(%i = 0; %i < %this.numMembers; %i ++)
		{
			%cl = %this.member[%i];
			%sc = %cl.score;

			if(isObject(%cl.getTeam()))
				continue;

			if(%sc > %least)
			{
				%winner = %cl;
				%least = %sc;
				%count = 1;
			}
			else if(%sc == %least && %least > 0)
			{
				%winner = setField(%winner, %count, %cl);
				%count ++;
			}
		}

		%this.endRound(%winner);
		return;
	}
	else
	{
		if(%timeRemaining % 60000 == 0) //only on the minute
		{
			if(%timeRemaining <= 60000) //start ticking every 10 seconds
				%this.timeTickLength = 30000;

			if(%timeRemaining % 600000 == 0) //every 10 minutes
				%announce = 1;
			else if(%timeRemaining <= 300000) // <= 5 minutes remaining
				%announce = 1;

			%minutes = %timeRemaining / 60000;
			%remain = %minutes SPC "\c5" @ (%minutes == 1 ? "minute" : "minutes");
		}
		else
		{
			if(%timeRemaining % 10000 == 0 && %timeRemaining <= 30000) //30, 20, 10 seconds remaining
			{
				if(%timeRemaining <= 10000) // <= 10 seconds remaining
					%this.timeTickLength = 5000;
				else
					%this.timeTickLength = 10000;
				%announce = 1;
			}
			else if(%timeRemaining <= 5000) // <= 5 seconds remaining
			{
				%this.timeTickLength = 1000;
				%announce = 1;
			}

			%seconds = %timeRemaining / 1000;
			%remain = %seconds SPC "\c5" @ (%seconds == 1 ? "second" : "seconds");
		}

		//This should be independent of the timescale
		%this.timeTickLength *= getTimeScale();

		if(%announce && %ticks > 0)
			%this.messageAll('', '\c3%1 \c5remaining.', %remain);

		cancel(%this.timeSchedule);
		%this.timeSchedule = %this.scheduleNoQuota(%this.timeTickLength, victoryCheck_Time, %ticks++);
	}
}

function Slayer_MiniGameSO::onReset(%this)
{
	//START THE ROUND
	if(%this.preRoundSeconds > 0)
		%this.preRoundCountdownTick(0);
	else
		%this.startRound();
}

function Slayer_MiniGameSO::preRoundCountdownTick(%this, %ticks)
{
	cancel(%this.preRoundCountdownTimer);

	if(%this.isResetting())
		return;

	%remain = %this.preRoundSeconds - %ticks;

	Slayer_Support::Debug(2, "Slayer_MiniGameSO::preRoundCountdownTick", %remain);
	
	%this.gameMode.callback("onPreRoundCountdownTick", %ticks, %remain);

	if(%remain <= 0)
	{
		%this.startRound();

		%this.play2dAll(Slayer_Begin_Sound);
		%this.centerPrintAll("<font:Arial Bold:100><color:ff00ff>GO!", 2);
	}
	else
	{
		if(%ticks == 0)
		{
			for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
			{
				%cl = %this.member["GameConnection", %i];
				if(isObject(%cl.player))
					%cl.player.changeDatablock(PlayerFrozenArmor);
			}
			for(%i = 0; %i < %this.numMembers["AiController"]; %i ++)
			{
				%ai = %this.member["AiController", %i];
				if(isObject(%ai.player) && %ai.player.isHoleBot)
					%ai.player.stopHoleLoop();
			}
		}

		%sound = "Slayer_" @ %remain @ "_Seconds_Sound";
		if(isObject(%sound))
			%this.play2dAll(%sound);
		%this.centerPrintAll("<font:Arial Bold:100><color:ff00ff>" @ %remain);

		%this.preRoundCountdownTimer = %this.scheduleNoQuota(1000, preRoundCountdownTick, %ticks ++);
	}
}

function Slayer_MiniGameSO::startRound(%this)
{
	Slayer_Support::Debug(1, "Slayer_MiniGameSO::startRound");
	%this.roundStarted = $Sim::Time;

	for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
	{
		%cl = %this.member["GameConnection", %i];
		%cl.roundWon = "";
		if(isObject(%cl.player))
		{
			if(%cl.player.getDatablock().getID() == PlayerFrozenArmor.getID())
			{
				%t = %cl.getTeam();
				%db = (isObject(%t) ? %t.playerDatablock : %this.playerDatablock);
				%cl.player.changeDatablock(%db);
			}
		}
		else if(!%cl.dead())
			%cl.spawnPlayer();
	}
	for(%i = 0; %i < %this.numMembers["AiController"]; %i ++)
	{
		%ai = %this.member["AiController", %i];
		if(isObject(%ai.player) && %ai.player.isHoleBot)
			%ai.player.resetHoleLoop();
	}

	if(%this.time > 0)
		%this.victoryCheck_Time(0);

	//event
	$InputTarget_["MiniGame"] = %this;
	processMultiSourceInputEvent("onMinigameRoundStart", 0, %this);
	
	%this.gameMode.callback("onRoundStart");
}

function Slayer_MiniGameSO::endRound(%this, %winner, %resetTime)
{
	if(%this.isResetting())
		return;
	%this.setResetting(1);

	Slayer_Support::Debug(1, "Slayer_MiniGameSO::endRound", "winner:" SPC %winner TAB "resetTime:" SPC %resetTime);

	if(getField(%winner, 0) $= "CUSTOM")
	{
		%count = 1;
		%nameList = getField(%winner, 1);
	}
	else //this generates the list of winner names
	{
		%count = 0;
		for(%i = 0; %i < getFieldCount(%winner); %i ++)
		{
			%w = getField(%winner, %i);
			if(!isObject(%w))
				continue;
			if(getMinigameFromObject(%w) != %this)
				continue;

			if(%w.class $= "Slayer_TeamSO")
			{
				%name = "<sPush>" @ %w.getColoredName() @ "<sPop>";

				for(%e=0; %e < %w.numMembers; %e++)
				{
					%cl = %w.member[%e];

					if(isObject(%cl.player))
						%cl.player.emote(winStarProjectile);

					%cl.roundWon = true;
				}
			}
			else if(%w.getClassName() $= "GameConnection")
			{
				%name = "<sPush><color:ffff00>" @ %w.getPlayerName() @ "<sPop>";

				if(isObject(%w.player))
					%w.player.emote(winStarProjectile);

				%w.roundWon = true;
			}
			else
				continue;

			%w.wins ++;

			%winner[%count] = %w;
			%count ++;

			if(%nameList $= "")
				%nameList = %name;
			else
				%nameList = %nameList @ ", " SPC %name;
		}
	}

	%this.gameMode.callback("preRoundEnd", %winner, %nameList);

	if(%count <= 0)
		%msg = '\c5Nobody won this round. Resetting in %4 seconds.';
	else
	{
		if(%count > 1)
			%msg = '<color:ff00ff>%1 tied this round. Resetting in %4 seconds.';
		else if(isObject(%winner))
		{
			%score = %winner.getScore();
			%wins = (%winner.wins == 1 ? "once" : %winner.wins SPC "\c5times");
			if(%score > 0)
			{
				%points = %score SPC (%score == 1 ? "\c5point" : "\c5points");
				%msg = '<color:ff00ff>%1 won this round with a score of \c3%2\c5. %1 has won \c3%3\c5. Resetting in %4 seconds.';
			}
			else
				%msg = '<color:ff00ff>%1 won this round. %1 has won \c3%3\c5. Resetting in %4 seconds.';

			if(isObject(%winner.player) && %winner.player.getType() & $TypeMasks::PlayerObjectType)
				%winnerCam = true;
		}
		else
			%msg = '<color:ffff00>%1 \c5won this round. Resetting in %4 seconds.';
	}

	for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
	{
		%cl = %this.member["GameConnection", %i];

		%cl.setDead(true);

		if(!%this.allowMoveWhileResetting)
		{
			if(%winnerCam)
				%cl.camera.setMode(corpse, %winner.player);
			else
			{
				if(isObject(%cl.player))
					%cl.camera.setMode(corpse, %cl.player);
			}
			%cl.setControlObject(%cl.camera);
		}

		//END OF ROUND REPORT
		if(%this.eorrEnable)
		{
			if(%cl.slayer)
				commandToClient(%cl, 'Slayer_ForceGUI', "Slayer_CtrDisplay", 1);
			else
				messageClient(%cl, '', "\c3You cannot see the \"End of Round Report\" because you don't have <a:" @ $Slayer::URL::Information @ ">Slayer</a> \c3installed.");
		}
	}
	for(%i = 0; %i < %this.numMembers["AiController"]; %i ++)
	{
		%ai = %this.member["AiController", %i];

		%ai.setDead(true);

		if(!%this.allowMoveWhileResetting)
		{
			if(isObject(%ai.player) && %ai.player.isHoleBot)
				%ai.player.stopHoleLoop();
		}
	}

	if(%resetTime $= "")
		%resetTime = %this.timeBetweenRounds * 1000;
	%seconds = mCeil(%resetTime / 1000);

	//tell everybody who won
	%this.messageAll('', %msg, %nameList, %points, %wins, %seconds);

	//populate end of round report
	if(%this.eorrEnable)
		%this.sendScoreListAll();

	//reset countdown
	if(%resetTime != -1)
	{
		for(%i = 0; %i < %seconds; %i ++)
		{
			%remaining = %seconds - %i;
			%timeLeft = %remaining SPC (%remaining == 1 ? "second" : "seconds");
			%this.resetCountdownTimer[%i] = %this.schedule(%i * 1000, "bottomPrintAll", "<just:center>\c5Resetting in" SPC %timeLeft @ ".", 2, 1);
		}
		%this.resetCountdownTimerCount = %seconds;

		%this.resetTimer = %this.scheduleNoQuota(%resetTime, reset, 0);
	}

	//event
	$InputTarget_["MiniGame"] = %this;
	processMultiSourceInputEvent("onMinigameRoundEnd", 0, %this);

	%this.teams.onMinigameRoundEnd();

	%this.gameMode.callback("onRoundEnd", %winner, %nameList);
	
	//Is the game paused?
	if(%resetTime == -1)
	{
		%this.gameMode.callback("onMiniGameSuspend");
	}
}

function Slayer_MiniGameSO::sendScoreListAll(%this)
{
	//INIT LISTS ALL
	%this.commandToAllSlayerClients('Slayer_ctrDisplayInit');

	//SETUP HEADER
	%header = '<color:ffffff><tab:150, 250, 350, 450><font:arial:16>%1<h2>%2\t%3\t%4\t%5\t%6</h2><br>';

	%var1 = "&victoryStatus";
	%var2 = "Name";
	%var3 = "Score";
	%var4 = "Kills";
	%var5 = "Deaths";
	%var6 = "Rounds Won";

	%custom = %this.gameMode.callback("scoreListInit", %header, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9);
	if(getField(%custom, 0))
	{
		%header = getField(%custom, 1);
		for(%e = 2; %e < 11; %e ++)
			%var[%e - 1] = getField(%custom, %e);
	}

	if(%this.eorrDisplayVictory) //check whether a gamemode replaced this
	{
		if(%var1 $= "&victoryStatus")
			%victoryStatus = 1;
	}
	else if(%var1 $= "&victoryStatus")
		%victoryStatus = 2;

	//SEND HEADER ALL
	for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
	{
		%cl = %this.member["GameConnection", %i];
		if(%cl.slayer)
		{
			if(%victoryStatus == 1)
			{
				if(!%cl.slyrTeam.spectate)
					%var1 = "<just:center><h1>" @ (%cl.roundWon ? "VICTORY" : "DEFEAT") @ "</h1></just>";
				else
					%var1 = "";
			}
			else if(%victoryStatus == 2)
				%var1 = "";

			commandToClient(%cl, 'Slayer_ctrDisplayAdd', %header, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9);
		}
	}

	%sendTeams = %this.gameMode.callback("scoreListCheckSendTeams");
	%sendTeams = (%sendTeams $= "" || %sendTeams);

	//SEND TEAMS ALL
	if(%sendTeams && %this.gameMode.template.useTeams && %this.Teams.getCount() > 0 && %this.eorrDisplayTeamScores)
	{
		%this.commandToAllSlayerClients('Slayer_ctrDisplayAdd', "<b>Teams:</b><br>");

		%teamList = %this.Teams.getTeamListSortedScore();

		for(%i = 0; %i < %this.Teams.getCount(); %i ++)
		{
			%t = getField(%teamList, %i);
			if(%t.spectate)
				continue;

			%var1 = %t.colorHex;
			%var2 = %t.name;
			%var3 = %t.getScore();
			%var4 = %t.getKills();
			%var5 = %t.getDeaths();
			%var6 = %t.wins;

			%line = '<color:%1><b>%2</b></color>\t%3\t%4\t%5\t%6<br>';

			%custom = %this.gameMode.callback("scoreListAdd", %t, %line, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9);
			if(getField(%custom, 0))
			{
				%line = getField(%custom, 1);
				for(%e = 2; %e < 11; %e ++)
					%var[%e - 1] = getField(%custom, %e);
			}

			%this.commandToAllSlayerClients('Slayer_ctrDisplayAdd', %line, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9);
		}

		%teamsSent = true;
	}

	%sendPlayers = %this.gameMode.callback("scoreListCheckSendPlayers");
	%sendPlayers = (%sendPlayers $= "" || %sendPlayers);

	//SEND PLAYERS ALL
	if(%sendPlayers)
	{
		if(%teamsSent)
			%this.commandToAllSlayerClients('Slayer_ctrDisplayAdd', "<br><b>Players:</b><br>");

		%memberList = %this.getMemberListSortedScore();
		%count = getFieldCount(%memberList);
		for(%i = 0; %i < %count; %i ++)
		{
			%cl = getField(%memberList, %i);
			%class = %cl.getClassName();
			if(%class $= "AiController" && %cl.hName $= "")
				continue;

			%team = %cl.getTeam();
			if(%team.spectate)
				continue;

			%var1 = (%team > 0 ? %team.colorHex : "ffffff");
			%var2 = (%class $= "GameConnection" ? %cl.getPlayerName() : %cl.hName);
			%var3 = %cl.getScore();
			%var4 = %cl.getKills();
			%var5 = %cl.getDeaths();
			%var6 = (%team > 0 ? "" : %cl.wins);

			%line = '<color:%1><b>%2</b></color>\t%3\t%4\t%5\t%6<br>';

			%custom = %this.gameMode.callback("scoreListAdd", %cl, %line, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9);
			if(getField(%custom, 0))
			{
				%line = getField(%custom, 1);
				for(%e = 2; %e < 11; %e ++)
					%var[%e - 1] = getField(%custom, %e);
			}

			%this.commandToAllSlayerClients('Slayer_ctrDisplayAdd', %line, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9);
		}
	}
}

function Slayer_MiniGameSO::roundStarted(%this)
{
	return %this.roundStarted;
}

function Slayer_MiniGameSO::isResetting(%this)
{
	return %this.isResetting;
}

function Slayer_MiniGameSO::setResetting(%this, %flag)
{
	%this.isResetting = %flag;

	if(!%flag)
		%this.roundStarted = $Sim::Time;
}

//Changes to a different game mode. A reset may be required after the change.
//@param	Slayer_GameModeTemplateSG template	The game mode template.
//@param	bool noCreateTeams	Whether to not create the game mode's default teams. Used when client has created/set up teams.
//@return	Slayer_GameModeSO	The game mode object.
function Slayer_MiniGameSO::setGameMode(%this, %template, %noCreateTeams)
{
	if(isObject(%this.gameMode))
	{
		%this.gameMode.callback("onGameModeEnd");
		%this.gameMode.delete();
	}

	Slayer_Support::Debug(1, "Setting game mode with template", %template);

	%this.gameMode = new ScriptObject()
	{
		class = %template.className;
		superClass = Slayer_GameModeSO;
		miniGame = %this;
		template = %template;
	};

	%this.gameMode.applyDefaultPreferences();
	%this.gameMode.prepareSlayerBricks();
	
	// Compatibility Mode.
	if(%template.compatibilityMode)
	{
		%this.mode = %template.className;
	}
	else
	{
		%this.mode = "";
	}
	
	if(%template.getCount() > 0)
	{
		%this.teams.removeAllTeams();
		if(!%noCreateTeams)
			%this.gameMode.createDefaultTeams();
	}
	else
	{
		for(%i = %this.teams.getCount() - 1; %i >= 0; %i --)
		{
			%team = %this.teams.getObject(%i);
			if(isObject(%team.template))
				%this.teams.removeTeam(%team);
		}
	}
	if(%this.teams.getCount() > 0)
	{
		if(%template.useTeams)
		{
			for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
			{
				%cl = %this.member["GameConnection", %i];
				if(!isObject(%cl.slyrTeam))
					%this.teams.autoSort(%cl);
			}
		}
		else
		{
			%this.teams.removeAllMembers("GameConnection");
			%this.teams.deleteAllBots();
		}
	}
	
	%this.gameMode.callback("onGameModeStart");

	return %gameMode;
}

//Creates an AiController and adds it to the minigame.
//The bot is not tied to a specific brick and is instead tied to the minigame.
//@return	Slayer_AiController	The newly created bot.
function Slayer_MiniGameSO::addBotToGame(%this)
{
	%bot = new ScriptObject()
	{
		class = Slayer_AiController;
		superClass = AiController;
		hasSpawnedOnce = true;
		isHoleBot = true;
		isSlayerBot = true;
	};
	%this.addMember(%bot);

	Slayer_Support::Debug(2, "Slayer_MiniGameSO::addBotToGame", %bot);

	return %bot;
}

function Slayer_MiniGameSO::resetVehicles(%this)
{
	for(%i = Slayer.vehicleSpawns.getCount() - 1; %i >= 0; %i --)
	{
		%br = Slayer.vehicleSpawns.getObject(%i);
		if(minigameCanUse(%this, %br))
			%br.respawnVehicle();
	}
}

function Slayer_MiniGameSO::resetBots(%this)
{
	if(!isObject(mainBrickGroup))
		return;

	for(%i = 0; %i < mainBrickGroup.getCount(); %i ++)
	{
		%bg = mainBrickGroup.getObject(%i);
		if(miniGameCanUse(%this, %bg))
		{
			hResetBots(%bg);
		}
	}
}

function Slayer_MiniGameSO::resetCapturePoints(%this, %client)
{
	for(%i = 0; %i < Slayer.capturePoints.getCount(); %i ++)
	{
		%cp = Slayer.capturePoints.getObject(%i);

		if(minigameCanUse(%this, %cp))
			%cp.setCPControl(%cp.origColor, 1, %client);
	}
}

function Slayer_MiniGameSO::resetBricks(%this)
{
	if(!isObject(mainBrickGroup))
		return;

	for(%i = 0; %i < mainBrickGroup.getCount(); %i ++)
	{
		%bg = mainBrickGroup.getObject(%i);
		if(%bg.bl_id == 888888 || %bg.bl_id == 999999)
			continue;
		if(miniGameCanUse(%this, %bg))
		{
			%bg.chain_batchSize = 600; //decrease this number to reduce lag but increase reload time
			%bg.chain_timeOut = 32;
			%bg.schedule(0, "chainMethodCall", "onSlayerMinigameReset", %this); //schedule reduces lag
		}
	}
}

function Slayer_MiniGameSO::getMemberListSortedScore(%this, %class)
{
	if(%class $= "")
	{
		%count = %this.numMembers;
		for(%i = 0; %i < %count; %i ++)
			%list = setField(%list, getFieldCount(%list), %this.member[%i]);
	}
	else
	{
		%count = %this.numMembers[%class];
		for(%i = 0; %i < %count; %i ++)
			%list = setField(%list, getFieldCount(%list), %this.member[%class, %i]);
	}

	while(!%done)
	{
		%done = 1;
		for(%i = 0; %i < %count; %i ++)
		{
			%e = %i + 1;
			if(%e >= %count)
				continue;
			%f1 = getField(%list, %i);
			%f2 = getField(%list, %e);
			if(%f1.score < %f2.score)
			{
				%list = Slayer_Support::swapItemsInList(%list, %i, %e);
				%done = 0;
			}
		}
		%count --;
	}
	return %list;
}

function Slayer_MiniGameSO::getLiving(%this)
{
	%living = 0;
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		if(!%this.member[%i].dead())
			%living ++;
	}
	return %living;
}

function Slayer_MiniGameSO::getSpawned(%this)
{
	%spawned = 0;
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		if(isObject(%this.member[%i].player))
			%spawned ++;
	}
	return %spawned;
}

function Slayer_MiniGameSO::canDamage(%this, %objA, %classA, %objB, %classB)
{
	Slayer_Support::Debug(3, "Slayer_MiniGameSO::canDamage", "A:" SPC %objA SPC %classA SPC "B:" SPC %objB SPC %classB);

	if(%classA $= "fxDtsBrick")
		return %this.brickDamage;
	else if(%classA $= "Player")
	{
		if(!%this.weaponDamage && Slayer_Support::isVehicle(%objB))
			return %this.vehicleDamage;
		return %this.weaponDamage;
	}
	else if(%classA $= "AiPlayer")
	{
		if(Slayer_Support::isVehicle(%objA))
			return %this.vehicleDamage;
		return %this.botDamage;
	}
	else if(%classA $= "GameConnection" || %classA $= "AiController")
	{
		if(%classB $= "fxDtsBrick")
			return %this.brickDamage;
		return %this.weaponDamage;
	}
 	else if(Slayer_Support::isVehicle(%objA))
	{
		return %this.vehicleDamage;
	}

	return 1;
}

//Checks whether a specific point is within the minigame region.
//@param	Point3F position
//@return	bool	True if in region or region is undefined. False if outside region.
function Slayer_MiniGameSO::isPointInRegion(%this, %position)
{
	%region = %this.region;
	%x = getWord(%position, 0);
	%y = getWord(%position, 1);
	// %z = getWord(%position, 2);
	return (%x >= getWord(%region, 0) && %x <= getWord(%region, 3) &&
		%y >= getWord(%region, 1) && %y <= getWord(%region, 4));
}

//Re-calculates the minigame region.
//@return	Vector6F	The region box.
function Slayer_MinigameSO::calculateRegionBox(%this)
{
	%count = Slayer.boundaryBricks.getCount();
	if(%count > 1)
	{
		%start = %count;
		%boundaryBrickCount = 0;
		for(%i = 0; %i < %count; %i ++)
		{
			%brick = Slayer.boundaryBricks.getObject(%i);
			if(%brick.getColorID() == %this.colorIdx)
			{
				%start = %i + 1;
				%minX = getWord(%brick.position, 0);
				%maxX = %minX;
				%minY = getWord(%brick.position, 1);
				%maxY = %minY;
				%minZ = getWord(%brick.position, 2);
				%maxZ = %minZ;
				%boundaryBrickCount = 1;
				break;
			}
		}
		for(%i = %start; %i < %count; %i ++)
		{
			%brick = Slayer.boundaryBricks.getObject(%i);
			if(%brick.getColorID() == %this.colorIdx)
			{
				%x = getWord(%brick.position, 0);
				%y = getWord(%brick.position, 1);
				%z = getWord(%brick.position, 2);
				if(%x < %minX) %minX = %x;
				if(%x > %maxX) %maxX = %x;
				if(%y < %minY) %minY = %y;
				if(%y > %maxY) %maxY = %y;
				if(%z < %minZ) %minZ = %Z;
				if(%z > %maxZ) %maxZ = %Z;
				%boundaryBrickCount ++;
			}
		}
		if(%boundaryBrickCount > 1)
		{
			%this.region = %minX SPC %minY SPC %minZ SPC %maxX SPC %maxY SPC %maxZ;
		}
		else
		{
			%this.region = "";
		}
	}
	else
	{
		%this.region = "";
	}
	return %this.region;
}

function Slayer_MiniGameSO::updateColor(%this, %new, %old)
{
	%this.calculateRegionBox();

	if(Slayer.minigames.colorTaken[%new])
		%this.colorIdx = %old;
	else
	{
		Slayer.minigames.colorTaken[%new] = 1;
		Slayer.minigames.colorTaken[%old] = 0;
		%this.colorRGB = getColorIDTable(%new);

		for(%i = 0; %i < %this.numMembers; %i ++)
		{
			%cl = %this.member[%i];
			if(!isObject(%cl.slyrTeam) && isObject(%cl.player))
				%cl.player.setShapeNameColor(%this.colorRGB);
		}
	}
}

function Slayer_MiniGameSO::updateGameMode(%this, %newTemplate, %oldTemplate)
{
	if(!isObject(%newTemplate))
	{
		Slayer_Support::Error("Invalid game mode template", %newTemplate);
		%this.gameModeTemplate = %oldTemplate;
		return;
	}
	%this.setGameMode(%newTemplate, true);
}

function Slayer_MiniGameSO::updateLives(%this, %new, %old)
{
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%cl = %this.member[%i];

		%t = %cl.getTeam();
		if(isObject(%t) && %t.lives >= 0)
			continue;

		if(%new <= 0)
		{
			%cl.setLives(0);
			if(%cl.dead())
			{
				%cl.setDead(0);
				if(!isObject(%cl.player))
					%cl.spawnPlayer();
			}
			continue;
		}

		if(%old $= "")
			%cl.setLives(%new);
		else
			%cl.addLives(%new-%old);

		%lives = %cl.getLives();
		if(%lives <= 0 && !%cl.dead())
		{
			%cl.setDead(1);
			if(isObject(%cl.player))
			{
				%cl.camera.setMode(observer);
				%cl.setControlObject(%cl.camera);
				%cl.player.delete();
			}
			%cl.centerPrint("\c5The number of lives was reduced. You are now out of lives.", 5);

			%check = 1;
		}
		else if(%lives > 0 && %cl.dead())
		{
			%cl.setDead(0);
			if(!isObject(%cl.player))
				%cl.spawnPlayer();
		}
	}

	if(%check)
	{
		%win = %this.victoryCheck_Lives();
		if(%win >= 0)
			%this.endRound(%win);
	}
}

function Slayer_MiniGameSO::updatePoints(%this, %new, %old)
{
	%this.victoryCheck_Points();
}

function Slayer_MiniGameSO::updateTime(%this, %new, %old)
{
	if(%new <= 0 && %old > 0)
		cancel(%this.timeSchedule);
	else if(%new > 0 && %old <= 0)
	{
		cancel(%this.timeSchedule);
		%this.victoryCheck_Time(0);
	}
}

function Slayer_MiniGameSO::updateRespawnTime(%this, %type, %flag, %old)
{
	%oldTime = %old * 1000;

	if(%flag == -1)
		%time = -1;
	else
	{
		%time = %flag * 1000;
		%time = mClampF(%time, -999999, 999999);
	}

	switch$(%type)
	{
		case "player":
			%this.respawnTime = %time;

			for(%i = 0; %i < %this.numMembers; %i ++)
			{
				%cl = %this.member[%i];
				%t = %cl.getTeam();
				if(isObject(%t) && %t.lives >= 0)
					continue;
				if(%cl.respawnTime == %oldTime)
					%cl.setRespawnTime(%time);
			}

		case "bot":
			%this.botRespawnTime = %time;

		case "brick":
			%this.brickRespawnTime = %time;

		case "vehicle":
			%this.vehicleRespawnTime = %time;

		case "penalty_FF":
			%this.friendlyFireRespawnPenalty = %time;

		default:
			if(%type !$= "")
			{
				%this.respawnTime_[%type] = %time;
			}
	}
}

function Slayer_MiniGameSO::updateNameDistance(%this, %flag)
{
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%cl = %this.member[%i];
		if(isObject(%cl.player))
			%cl.player.setShapeNameDistance(%flag);
	}
}

function Slayer_MiniGameSO::updateDefaultMinigame(%this, %flag)
{
	if(%this.isStarting)
		return;

	if(%flag)
		Slayer.Minigames.setDefaultMinigame(%this);
	else if(%this == Slayer.Minigames.defaultMinigame)
		Slayer.Minigames.setDefaultMinigame(0);
}

//different than the default forceEquip
//because it only forces the item if the player hasn't changed it
function Slayer_MiniGameSO::updateEquip(%this, %slot, %new, %old)
{
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%cl = %this.member[%i];
		if(!isObject(%cl.getTeam()))
		{
			%cl.updateEquip(%slot, %new, %old);
		}
	}
}

function Slayer_MiniGameSO::pickSpawnPoint(%this, %client)
{
	%special = %this.gameMode.callback("pickPlayerSpawnPoint", %client);
	if(strLen(%special))
		return %special;

	%numSpawns = Slayer.Spawns.getCount();
	if(!%this.useSpawnBricks || %numSpawns < 1)
		return pickSpawnPoint();

	%team = %client.getTeam();
	%count = 0;
	%openCount = 0;

	if(isObject(%team))
	{
		for(%i = 0; %i < %numSpawns; %i ++)
		{
			%sp = Slayer.Spawns.getObject(%i);
			%type = %sp.getDatablock().slyrType;
			%color = %sp.getTeamControl();

			if(%type !$= "TeamSpawn"
				|| !minigameCanUse(%client, %sp)
				|| %color != %team.color)
				continue;

			if(!%sp.isBlocked())
			{
				%openSpawn[%openCount] = %sp;
				%openCount ++;
			}
			%spawn[%count] = %sp;
			%count ++;
		}
	}

	if(%count < 1)
	{
		for(%i = 0; %i < %numSpawns; %i ++)
		{
			%sp = Slayer.Spawns.getObject(%i);
			%db = %sp.getDatablock();

			if(%db.getName() !$= "brickSpawnPointData"
				|| !minigameCanUse(%client, %sp)
				|| (%this.spawnBrickColor >= 0 && %sp.getColorID() != %this.spawnBrickColor))
				continue;

			if(!%sp.isBlocked())
			{
				%openSpawn[%openCount] = %sp;
				%openCount ++;
			}
			%spawn[%count] = %sp;
			%count ++;
		}
	}

	Slayer_Support::Debug(2, "Slayer_MiniGameSO::pickSpawnPoint", "open:" SPC %openCount SPC "total:" SPC %count);

	if(%count < 1)
		return pickSpawnPoint();
	else if(%openCount > 0)
	{
		%r = getRandom(0, %openCount - 1);
		%spawn = %openSpawn[%r];
	}
	else
	{
		%r = getRandom(0, %count - 1);
		%spawn = %spawn[%r];
	}

	if(%client.isHoleBot)
		%client.lastSpawnBrick = %spawn;
	if(isObject(%spawn))
		return %spawn.getSpawnPoint();
	else
		return pickSpawnPoint();
}

function Slayer_MiniGameSO::messageAllDead(%this, %cmd, %msg, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p, %q)
{
	for(%index=0; %index < %this.numMembers; %index ++)
	{
		%cl = %this.member[%index];
		if(%cl.dead())
			messageClient(%cl, %cmd, %msg, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p, %q);
	}
}

function Slayer_MiniGameSO::commandToAll(%this, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p, %q, %r, %s)
{
	for(%index = 0; %index < %this.numMembers["GameConnection"]; %index ++)
		commandToClient(%this.member["GameConnection", %index], %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p, %q, %r, %s);
}

function Slayer_MiniGameSO::commandToAllSlayerClients(%this, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p, %q, %r, %s)
{
	for(%index = 0; %index < %this.numMembers["GameConnection"]; %index ++)
	{
		%cl = %this.member["GameConnection", %index];
		if(%cl.slayer)
			commandToClient(%cl, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p, %q, %r, %s);
	}
}

function Slayer_MiniGameSO::play2dAll(%this, %sound)
{
	for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
		%this.member["GameConnection", %i].play2d(%sound);
}

//for events
function Slayer_MiniGameSO::centerPrintAll(%this, %msg, %time, %client)
{
	if(isObject(%client))
		%msg = strReplace(%msg, "%1", %client.getPlayerName());

	for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
		%this.member["GameConnection", %i].centerPrint(%msg, %time);
}

//for events
function Slayer_MiniGameSO::bottomPrintAll(%this, %msg, %time, %hideBar, %client)
{
	if(isObject(%client))
		%msg = strReplace(%msg, "%1", %client.getPlayerName());

	for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
		%this.member["GameConnection", %i].bottomPrint(%msg, %time, %hideBar);
}

//for events
function Slayer_MiniGameSO::chatMsgAll(%this, %msg, %client)
{
	if(isObject(%client))
		%msg = strReplace(%msg, "%1", %client.getPlayerName());

	for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
		%this.member["GameConnection", %i].chatMessage(%msg);
}

//for events
function Slayer_MiniGameSO::respawnAll(%this, %client)
{
	for(%i = 0; %i < %this.numMembers; %i ++)
		%this.member[%i].spawnPlayer();
}

//for events
function Slayer_MiniGameSO::incTimeRemaining(%this, %flag, %display)
{
	%inc = %flag * 60000;

	//round to the nearest 30 seconds
	%rmndr = %this.timeRemaining % 30000;
	if(%rmndr >= 15000)
	{
		%this.timeTickLength = 60000;
		%remain = %this.timeRemaining + (30000 - %rmndr);
	}
	else
	{
		%this.timeTickLength = 30000;
		%remain = %this.timeRemaining - %rmndr;
	}

	if(%display)
	{
		%min = (%flag == 1 ? "minute" : "minutes");
		%this.messageAll('', "\c5The round has been extended by\c3" SPC %flag SPC "\c5" @ %min @ ".");
	}

	%this.timeRemaining = %remain + %inc + %this.timeTickLength;
	%this.victoryCheck_time(%this.timeTicks + 1);
}

//for events
function Slayer_MiniGameSO::setTimeRemaining(%this, %flag, %display)
{
	%time = %flag * 60000;

	if(%time > 1)
		%this.timeTickLength = 60000;
	else
		%this.timeTickLength = 30000;

	if(%display)
	{
		%min = (%flag == 1 ? "minute" : "minutes");
		%this.messageAll('', "\c5The round has been set to\c3" SPC %flag SPC "\c5" @ %min @ ".");
	}

	%this.timeRemaining = %time + %this.timeTickLength;
	%this.victoryCheck_time(%this.timeTicks + 1);
}

//for events
function MiniGameSO::Slayer_setPref(%this, %category, %title, %value, %client)
{
	if(!isSlayerMinigame(%this))
		return;

	%pref = Slayer.Prefs.getPrefSO(%category, %title);
	if(!isObject(%pref))
		return;

	if((!isObject(%client) && %pref.permissionLevel == $Slayer::PermissionLevel["Any"]) ||
		%this.canEdit(%client, %pref.permissionLevel))
		%pref.setValue(%value, %this);
}

//for events
function MiniGameSO::Win(%this, %mode, %flag, %client)
{
	if(isObject(%client) && getMinigameFromObject(%client) != %this)
		return;

	if(!isSlayerMinigame(%this))
		return;

	switch(%mode)
	{
		case 0: //TRIGGERPLAYER
			if(isObject(%client))
				%this.endRound(%client);

		case 1: //TRIGGERTEAM
			if(isObject(%client))
			{
				%team = %client.getTeam();
				if(isObject(%team))
					%this.endRound(%team);
			}

		case 2: //CUSTOMPLAYER
			%cl = findClientByName(%flag);
			if(isObject(%cl))
				%this.endRound(%cl);

		case 3: //CUSTOMTEAM
			%team = %this.Teams.getTeamFromName(%flag);
			if(isObject(%team))
				%this.endRound(%team);

		case 4: //CUSTOMSTRING
			%this.endRound("CUSTOM" TAB %flag);

		case 5: //NONE
			%this.endRound();
	}
}

function MiniGameSO::canEdit(%this, %client, %overRide)
{
	if(!isObject(%client))
		return 0;

	if(!isSlayerMinigame(%this))
	{
		Slayer_Support::Debug(3, "MiniGameSO::canEdit", "False - Not a Slayer minigame");
		return 0;
	}

	if(%client.isHost)
	{
		Slayer_Support::Debug(3, "MiniGameSO::canEdit", "True - Client is host");
		return 1;
	}

	%brickgroup = "Brickgroup_" @ %this.creatorBLID;

	switch(%this.editRights)
	{
		case 0:
			%canEdit = %client.isHost;

		case 1:
			%canEdit = %client.isSuperAdmin;

		case 2:
			%canEdit = %client.isAdmin;

		case 3:
			%canEdit = (getTrustLevel(%client, %brickgroup) >= 3 || %client.getBLID() == %this.creatorBLID);

		case 4:
			%canEdit = (getTrustLevel(%client, %brickgroup) >= 2);

		case 5:
			%canEdit = (getTrustLevel(%client, %brickgroup) >= 1);

		default:
			Slayer_Support::Error("MiniGameSO::canEdit", "[Permissions | Edit Rights] is invalid.");
			%canEdit = 0;
	}

	if(%canEdit && %overRide !$= "")
	{
		switch(%overRide)
		{
			case 0:
				%canEdit = %client.isHost;

			case 1:
				%canEdit = %client.isSuperAdmin;

			case 2:
				%canEdit = %client.isAdmin;

			case 3:
				%canEdit = (getTrustLevel(%client, %brickgroup) >= 3 || %client.getBLID() == %this.creatorBLID);

			case 4:
				%canEdit = (getTrustLevel(%client, %brickgroup) >= 2);

			case 5:
				%canEdit = (getTrustLevel(%client, %brickgroup) >= 1);
		}
	}

	Slayer_Support::Debug(3, "MiniGameSO::canEdit", %canEdit);

	return %canEdit;
}

function MiniGameSO::canReset(%this, %client, %overRide)
{
	if(!isObject(%client))
		return 0;

	if(!isSlayerMinigame(%this))
		return 0;

	if(%client.isHost)
		return 1;

	if(%client.getBLID() == %this.creatorBLID)
		return 1;

	%brickgroup = "Brickgroup_" @ %this.creatorBLID;

	switch(%this.resetRights)
	{
		case 0:
			%canReset = %client.isHost;

		case 1:
			%canReset = %client.isSuperAdmin;

		case 2:
			%canReset = %client.isAdmin;

		case 3:
			%canReset = (getTrustLevel(%client, %brickgroup) >= 3);

		case 4:
			%canReset = (getTrustLevel(%client, %brickgroup) >= 2);

		case 5:
			%canReset = (getTrustLevel(%client, %brickgroup) >= 1);

		default:
			Slayer_Support::Error("MiniGameSO::canReset", "[Permissions | Reset Rights] is invalid.");
			%canReset = 0;
	}

	if(%canReset && %overRide !$= "")
	{
		switch(%overRide)
		{
			case 0:
				%canReset = %client.isHost;

			case 1:
				%canReset = %client.isSuperAdmin;

			case 2:
				%canReset = %client.isAdmin;

			case 3:
				%canReset = (getTrustLevel(%client, %brickgroup) >= 3);

			case 4:
				%canReset = (getTrustLevel(%client, %brickgroup) >= 2);

			case 5:
				%canReset = (getTrustLevel(%client, %brickgroup) >= 1);
		}
	}

	return %canReset;
}

function MiniGameSO::canLeave(%this, %client, %overRide)
{
	if(!isObject(%client))
		return 0;

	if(!isSlayerMinigame(%this))
		return 0;

	if(!%this.isDefaultMinigame)
		return 1;

	if(%client.isHost)
		return 1;

	if(%client.getBLID() == %this.creatorBLID)
		return 1;

	%brickgroup = "Brickgroup_" @ %this.creatorBLID;

	switch(%this.leaveRights)
	{
		case 0:
			%canLeave = %client.isHost;

		case 1:
			%canLeave = %client.isSuperAdmin;

		case 2:
			%canLeave = %client.isAdmin;

		case 3:
			%canLeave = (getTrustLevel(%client, %brickgroup) >= 3);

		case 4:
			%canLeave = (getTrustLevel(%client, %brickgroup) >= 2);

		case 5:
			%canLeave = (getTrustLevel(%client, %brickgroup) >= 1);

		default:
			Slayer_Support::Error("MiniGameSO::canLeave", "[Permissions | Leave Rights] is invalid.");
			%canLeave = 0;
	}

	if(%canLeave && %overRide !$= "")
	{
		switch(%overRide)
		{
			case 0:
				%canLeave = %client.isHost;

			case 1:
				%canLeave = %client.isSuperAdmin;

			case 2:
				%canLeave = %client.isAdmin;

			case 3:
				%canLeave = (getTrustLevel(%client, %brickgroup) >= 3);

			case 4:
				%canLeave = (getTrustLevel(%client, %brickgroup) >= 2);

			case 5:
				%canLeave = (getTrustLevel(%client, %brickgroup) >= 1);
		}
	}

	return %canLeave;
}

function isSlayerMinigame(%mini)
{
	if(!isObject(%mini) || !%mini.isSlayerMinigame)
		return false;

	if(!isObject(Slayer.Minigames) || !Slayer.Minigames.isMember(%mini))
		return false;

	return true;
}

function serverCmdSlayer(%client, %cmd, %a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p, %q, %r, %s, %t)
{
	if(%client.isSpamming())
		return;

	%mini = getMinigameFromObject(%client);

	switch$(%cmd)
	{
		case "edit":
			if(isObject(%a) || %a == -1)
				%minigame = %a;
			else
				%minigame = %mini;

			if(isSlayerMinigame(%minigame))
			{
				if(!%minigame.canEdit(%client))
				{
					messageClient(%client, '', "\c5You don't have permission to edit that minigame!");
					return;
				}
			}
			else
			{
				if(!Slayer.Minigames.canCreateMinigame(%client))
				{
					messageClient(%client, '', "\c5You don't have permission to create a minigame!");
					return;
				}
			}

			if(!%client.slayer)
			{
				if(%client.slayerVersion !$= "")
					commandToClient(%client, 'messageBoxOK', "Slayer | Error", "Version Mismatch!\n\nYou appear to be running a different version of Slayer than the server. Make sure you have the latest version.\n\n<a:" @ $Slayer::URL::Download @ ">Download Latest Version</a>\n\nYour Version:" SPC %client.slayerVersion NL "Server:" SPC $Slayer::Server::Version);
				else
					commandToClient(%client, 'messageBoxOK', "Slayer | Error", "Missing GUI\n\nThis server is running the Slayer gamemode. To change the settings for Slayer, you need to download and install the Slayer GUI.\n\n<a:" @ $Slayer::URL::Information @ ">Download Slayer</a>\n\nServer Version:" SPC $Slayer::Server::Version);
				return;
			}

			Slayer.sendData(%client, %minigame, true);

		case "reset":
			if(isObject(%mini) && %mini.canReset(%client) && !%mini.isResetting)
				%mini.reset(%client);

		case "end":
			if(isObject(%client.editingMinigame) || %client.editingMinigame == -1)
			{
				commandToClient(%client, 'Slayer_ForceGUI', "ALL", 0);
				%minigame = %client.editingMinigame;
			}
			else
				%minigame = %mini;

			if(isObject(%minigame) && %minigame.canEdit(%client))
				%minigame.endGame();

		case "rules":
			if(isSlayerMinigame(%mini))
			{
				%team = %client.slyrTeam;
				%lifeCount = ((isObject(%team) && %team.lives >= 0) ? %team.lives : %mini.lives);
				%lives = "\c3" @ %lifeCount SPC "\c5" @ (%lifeCount == 1 ? "life" : "lives");
				%points = "\c3" @ %mini.points SPC "\c5" @ (%mini.points == 1 ? "point" : "points");
				%time = "\c3" @ %mini.time SPC "\c5" @ (%mini.time == 1 ? "minute" : "minutes");

				%person = (%mini.gameMode.template.useTeams ? "team" : "man");

				messageClient(%client, '', "\c3" @ %mini.gameMode.template.uiName);
				if(%lifeCount > 0)
					messageClient(%client, '', '\c5 + %1 - The last %2 standing wins.', %lives, %person);
				if(%mini.points > 0)
					messageClient(%client, '', '\c5 + %1 - The first %2 to reach %1 wins.', %points, %person);
				if(%mini.time > 0)
					messageClient(%client, '', '\c5 + %1 - The %2 with the highest score after %1 wins.', %time, %person);
				if(%mini.customRule !$= "")
					messageClient(%client, '', "\c5 +" SPC %mini.customRule);

				%mini.gameMode.callback("onClientDisplayRules", %client);
			}

		case "dump":
			%msg = '\c5[\c3%1\c5|\c3%2\c5] \c3%3';
			%count = Slayer.Prefs.getCount();
			for(%i = 0; %i < %count;%i ++)
			{
				%pref = Slayer.Prefs.getObject(%i);
				if(%pref.isMinigameVar && !isObject(%mini))
					%val = "";
				else
					%val = %pref.getValue(%mini);
				messageClient(%client, '', %msg, %pref.category, %pref.title, %val);
			}

		default:
			messageClient(%client, '', "\c5This server is running <a:" @ $Slayer::URL::Information @ ">Slayer</a> \c3v" @ $Slayer::Server::Version @ "\c5, by Greek2me (Blockland ID 11902).");
	}
}

function serverCmdRequestSlayerMiniGameColorList(%client)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);

	for(%i = 0; %i < 64; %i ++)
	{
		if(!Slayer.minigames.colorTaken[%i] || (isObject(%mini) && %mini.colorIdx == %i))
		{
			commandToClient(%client, 'addSlayerMiniGameColor', %i, "", getColorI(getColorIDTable(%i)));
		}
	}
}

function serverCmdSlayer_SendPlayerList(%client)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);
	if(!isObject(%mini) || !%mini.canEdit(%client))
		return;
	for(%i = 0; %i < %mini.numMembers["GameConnection"]; %i ++)
	{
		%cl = %mini.member["GameConnection", %i];
		commandToClient(%client, 'SlayerClient_getPlayerListItem', %cl, %cl.getPlayerName(), %cl.getTeam());
	}
}

package Slayer_MiniGameSO
{
	function serverCmdCreateMinigame(%client, %a, %b, %c, %d, %e, %f, %g)
	{
		%mini = getMinigameFromObject(%client);

		if(isSlayerMinigame(%mini))
		{
			if(%mini.canLeave(%client))
			{
				%mini.removeMember(%client);
				parent::serverCmdCreateMinigame(%client, %a, %b, %c, %d, %e, %f, %g);
			}
			else
				messageClient(%client, '', "\c5You don't have permission to do that.");
		}
		else
			parent::serverCmdCreateMinigame(%client, %a, %b, %c, %d, %e, %f, %g);
	}

	function serverCmdLeaveMinigame(%client)
	{
		%mini = getMinigameFromObject(%client);

		if(!isSlayerMinigame(%mini) || (%can = %mini.canLeave(%client)))
			parent::serverCmdLeaveMinigame(%client);
		else if(!%can)
		{
			messageClient(%client, '', "\c5You don't have permission to do that.");
			return;
		}
	}

	function serverCmdJoinMinigame(%client, %minigame)
	{
		%mini = getMinigameFromObject(%client);

		if(isSlayerMinigame(%mini))
		{
			if(!%mini.canLeave(%client))
				return;
		}

		if(isSlayerMinigame(%minigame))
		{
			if(%mini == %minigame)
			{
				messageClient(%client, '', 'You\'re already in that mini-game.');
				return;
			}
			else if(%minigame.inviteOnly && %client.minigameInvitePending != %minigame && %mini.creatorBLID == %client.getBLID())
			{
				messageClient(%client, '', 'That mini-game is invite-only.');
				return;
			}
			else
				%minigame.addMember(%client);
		}
		else
			parent::serverCmdJoinMinigame(%client, %minigame);
	}

	function serverCmdResetMinigame(%client)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini) && %mini.canReset(%client) && !%mini.isResetting)
			%mini.reset(%client);
		else
			parent::serverCmdResetMinigame(%client);
	}

	function serverCmdEndMinigame(%client)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini) && %mini.canEdit(%client))
			%mini.endGame();
		else
			parent::serverCmdEndMinigame(%client);
	}

	function serverCmdSetMiniGameData(%client, %data1, %data2, %data3, %data4)
	{
		if(!isSlayerMinigame(getMinigameFromObject(%client)))
			parent::serverCmdSetMiniGameData(%client, %data1, %data2, %data3, %data4);
	}

	function serverCmdInviteToMinigame(%client, %victim)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini))
		{
			if(%mini.canEdit(%client))
			{
				if(isObject(%victim))
				{
					%pend = %victim.minigameInvitePending;

					if(isObject(%pend))
					{
						if(%pend == %mini)
							commandToClient(%client, 'messageBoxOK', 'Mini-Game Invite Error', 'This person hasn\'t responded to your first invite yet.');
						else
							commandToClient(%client, 'messageBoxOK', 'Mini-Game Invite Error', 'This person is considering another invite right now.');
					}
					else if(%mini.isMember(%victim))
					{
						commandToClient(%client, 'messageBoxOK', 'Mini-Game Invite Error', 'This person is already in the mini-game.');
					}
					else
					{
						%victim.minigameInvitePending = %mini;
						commandToClient(%victim, 'minigameInvite', %mini.title, %mini.hostName, %mini.creatorBLID, %mini);
					}
				}
			}
			else
			{
				commandToClient(%client, 'messageBoxOK', 'Mini-Game Invite Error', 'You do not have permission to send invites.');
			}
		}
		else
			parent::serverCmdInviteToMinigame(%client, %victim);
	}

	function serverCmdAcceptMinigameInvite(%client, %mini)
	{
		if(isSlayerMinigame(%mini))
		{
			if(!%mini.isMember(%client))
			{
				%mini.addMember(%client);
			}
		}
		else
			parent::serverCmdAcceptMinigameInvite(%client, %mini);
	}

	function serverCmdRejectMinigameInvite(%client, %mini)
	{
		if(isSlayerMinigame(%mini))
		{
			%creator = findClientByBL_ID(%mini.creatorBLID);
			if(isObject(%creator))
				messageClient(%creator, '', '%1 rejected your mini-game invitation', %client.getPlayerName());

			%client.minigameInvitePending = 0;
		}
		else
			parent::serverCmdRejectMinigameInvite(%client, %mini);
	}

	function serverCmdIgnoreMinigameInvite(%client, %mini)
	{
		if(isSlayerMinigame(%mini))
		{
			%creator = findClientByBL_ID(%mini.creatorBLID);
			if(isObject(%creator))
				messageClient(%creator, '', '%1 rejected your mini-game invitation (ignoring)', %client.getPlayerName());

			%client.minigameInvitePending = 0;
		}
		else
			parent::serverCmdRejectMinigameInvite(%client, %mini);
	}

	function serverCmdRemoveFromMinigame(%client, %victim)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini))
		{
			if(%mini.canEdit(%client))
			{
				if(isObject(%victim))
				{
					if(%mini.isMember(%victim))
					{
						%clName = %client.getPlayerName();
						%vName = %victim.getPlayerName();

						%mini.removeMember(%victim);
						messageClient(%victim, '', '%1 kicked you from the minigame', %clName);
						%mini.messageAll('', '%1 kicked %2 from the minigame', %clName, %vname);
					}
				}
			}
		}
		else
			parent::serverCmdRemoveFromMinigame(%client, %victim);
	}

	function Slayer_MiniGameSO::addMember(%this, %client)
	{
		if(%this.isStarting)
			return;
		if(!isObject(%client))
			return;

		if(isObject(%client.minigame))
		{
			if(%client.minigame == %this || %this.isMember(%client))
				return;

			%client.minigame.removeMember(%client);
		}

		%class = %client.getClassName();
		if(%this.numMembers[%class] $= "")
			%this.numMembers[%class] = 0;

		if(%this.resetOnEmpty && %this.numMembers[%class] < 1 && %this.isResetting() && %class $= "GameConnection")
			%this.reset(0);

		%client.minigame = %this;
		%client.minigameJoinTime = getSimTime();
		%client.minigameInvitePending = 0;

		%this.member[%this.numMembers] = %client;
		%this.numMembers ++;
		%this.member[%class, %this.numMembers[%class]] = %client;
		%this.numMembers[%class] ++;

		//reset their stats
		%client.setScore(0);
		%client.setDeaths(0);
		%client.setKills(0);
		%client.setLives(%this.lives);
		%client.wins = 0;
		
		%this.spectateTargets.add(%client);

		if(%class $= "GameConnection")
		{
			clearCenterPrint(%client);
			clearBottomPrint(%client);

			commandToClient(%client, 'SetBuildingDisabled', !%this.enableBuilding);
			commandToClient(%client, 'SetPaintingDisabled', !%this.enablePainting);
			commandToClient(%client, 'SetPlayingMinigame', 1); //enables the Leave button in the join minigame GUI

			//notify members
			%joinMessage = "\c1" @ %client.getPlayerName() SPC "joined the\c3" SPC %this.title SPC "\c1mini-game.";
			%this.messageAll('MsgClientInYourMiniGame', %joinMessage, %client, 1);
			for(%i = 0; %i < %this.numMembers; %i ++)
			{
				%cl = %this.member[%i];
				if(%cl.getClassName() $= "GameConnection" && %cl != %client)
					messageClient(%client, 'MsgClientInYourMiniGame', '', %cl, 1);
			}

			//tell them the gamemode, lives, points, time
			%client.ignoreSpam = 1;
			serverCmdSlayer(%client, rules);
			if(%this.gameMode.template.useTeams && !%this.teams_lock)
				messageClient(%client, '', "\c5 + Type \c3/joinTeam [Team Name] \c5to join a team. Type \c3/listTeams \c5to view a list of teams.");

			//autosort into team
			if(%this.gameMode.template.useTeams && %this.teams_autoSort)
				%this.Teams.autoSort(%client, 1);

			//late join time
			%ljt = %this.lateJoinTime;
			if(%this.getLiving()-1 > 1 && %ljt >= 0 && ($Sim::Time-%this.roundStarted())/60 > %ljt)
			{
				%client.setDead(1);
				%client.spectateInit();

				messageClient(%client, '', "\c5You will spawn when the next round starts.");
				%client.centerPrint("\c5You will spawn when the next round starts.", 6);

				if(isObject(%client.player))
					%client.player.delete();
			}
			else
				%client.spawnPlayer();

			//onMinigameJoin input event
			$InputTarget_["Client"] = %client;
			$InputTarget_["Player"] = %client.player;
			$InputTarget_["Minigame"] = %this;
			processMultiSourceInputEvent("onMinigameJoin", %client, %this);
		}
		else if(%class $= "AiController")
		{
			%client.removeAllObjectives();
			%client.assignObjectives();
			%client.spawnPlayer();
		}

		Slayer_Support::Debug(1, "Slayer_MiniGameSO::addMember", %client.getSimpleName());

		%this.gameMode.callback("onClientJoinGame", %client);

		//call this for compatibility
		if(%class $= "GameConnection")
			parent::addMember(%this, %client);
	}

	function Slayer_MiniGameSO::removeMember(%this, %client)
	{
		if(!isObject(%client) || !%this.isMember(%client))
			return;

		%class = %client.getClassName();

		if(%class $= "GameConnection")
		{
			//onMinigameLeave input event
			$InputTarget_["Client"] = %client;
			$InputTarget_["Player"] = %client.player;
			$InputTarget_["Minigame"] = %this;
			processMultiSourceInputEvent("onMinigameLeave", %client, %this);
		}

		//GAMEMODE STUFF
		%this.gameMode.callback("onClientLeaveGame", %client);

		//REMOVE FROM TEAM
		%team = %client.slyrTeam;
		if(isObject(%team))
			%team.removeMember(%client);

		//CLEAR GUI
		clearCenterPrint(%client);
		clearBottomPrint(%client);
		if(%client.slayer)
			commandToClient(%client, 'Slayer_ForceGUI', "Slayer_CtrDisplay", 0);

		//CLEAR ATTRIBUTES
		%client.resetRespawnTime();
		%client.setDead(0);
		%client.setDeaths(0);
		%client.setLives(0);
		%client.setKills(0);
		
		%this.spectateTargets.remove(%client);

		//REMOVE FROM CLASS MEMBER ARRAY
		%start = -1;
		for(%i = 0; %i < %this.numMembers[%class]; %i ++)
		{
			if(%this.member[%class, %i] == %client) //find the member to be removed
			{
				%this.member[%class, %i] = "";
				%start = %i;
				break;
			}
		}
		if(%start >= 0)	
		{
			for(%i = %start + 1; %i < %this.numMembers[%class]; %i ++)
				%this.member[%class, %i - 1] = %this.member[%class, %i];

			%this.member[%class, %this.numMembers[%class] - 1] = "";
			%this.numMembers[%class] --;
		}

		if(%class $= "GameConnection")
		{
			//PREVENT OUR OWN DELETION
			if(%this.numMembers <= 1)
			{
				%subtract = 1;
				%this.numMembers ++; //hack!
			}

			//PREVENT VEHICLE DELETION
			%oldClearEventObjects = %client.doNotClearEventObjects;
			%oldClearVehicles = %client.doNotResetVehicles;
			%client.doNotClearEventObjects = 1;
			%client.doNotResetVehicles = 1;

			//CALL PARENT FOR COMPATIBILITY
			parent::removeMember(%this, %client);

			//RESTORE EVERYTHING
			%client.doNotClearEventObjects = %oldClearEventObjects;
			%client.doNotResetVehicles = %oldClearVehicles;
			if(%subtract)
				%this.numMembers --;

			//PAUSE GAME IF EMPTY
			if(%this.resetOnEmpty && %this.numMembers[%class] < 1)
			{
				for(%i = 0; %i < %this.numMembers["AiController"]; %i ++)
				{
					%ai = %this.member["AiController", %i];
					cancel(%ai.respawnSchedule);
					if(isObject(%ai.player))
						%ai.player.delete();
				}
				%this.isSuspended = true;
				%this.endRound(0, -1);
				cancel(%this.resetTimer); //in case it was resetting already
			}
		}
		else
		{
			//REMOVE FROM MEMBER ARRAY
			%start = -1;
			for(%i = 0; %i < %this.numMembers; %i ++)
			{
				if(%this.member[%i] == %client) //find the member to be removed
				{
					%this.member[%i] = "";
					%start = %i;
					break;
				}
			}
			if(%start >= 0)	
			{
				for(%i = %start + 1; %i < %this.numMembers; %i ++)
					%this.member[%i - 1] = %this.member[%i];

				%this.member[%this.numMembers - 1] = "";
				%this.numMembers --;
			}

			%client.miniGame = -1;
		}

		//CHECK IF ANYONE WON
		if(!%this.isSuspended)
		{
			if(%this.lives > 0 || %team.lives > 0)
			{
				%winner = %this.victoryCheck_Lives();
				if(%winner >= 0)
					%this.endRound(%winner);
			}
		}

		Slayer_Support::Debug(1, "Slayer_MiniGameSO::removeMember", %client.getSimpleName());
	}

	function Slayer_MiniGameSO::Reset(%this, %client)
	{
		if(getSimTime() - %this.lastResetTime < $Slayer::Server::Minigames::ResetTimeOut)
		{
			Slayer_Support::Error("Slayer_MiniGameSO::Reset", "Minigame being reset too quickly!");
			return;
		}

		//make sure the schedules stop
		cancel(%this.timeSchedule);
		cancel(%this.resetTimer);
		for(%i = 0; %i < %this.resetCountdownTimerCount; %i ++)
			cancel(%this.resetCountdownTimer[%i]);

		//no longer suspended
		%this.isSuspended = false;

		//the minigame is resetting
		%this.setResetting(1);

		%this.gameMode.callback("preMiniGameReset", %client);

		%this.resetBricks();
		%this.resetCapturePoints(%client);
		%this.Teams.onMinigameReset();
		if($AddOnLoaded__Bot_Hole)
			%this.resetBots();

		if(!isObject(%client))
			%client = 0;

		for(%i = 0; %i < %this.numMembers["GameConnection"]; %i ++)
		{
			%cl = %this.member["GameConnection", %i];
			%t = %cl.slyrTeam;

			%cl.setDead(0);
			%cl.setLives((isObject(%t) && %t.lives >= 0) ? %t.lives : %this.lives);
			if(%this.clearStats)
			{
				%cl.setKills(0);
				%cl.setDeaths(0);
			}

			clearCenterPrint(%cl);
			clearBottomPrint(%cl);
			commandToClient(%cl, 'Slayer_ForceGUI', "Slayer_CtrDisplay", 0);

			if(!%this.disableSlayerMessages)
			{
				if(%client)
					messageClient(%cl, '', '\c3%1 \c5reset the \c3%2 \c5minigame.', %client.name, %this.title);
				else
					messageClient(%cl, '', '\c5The \c3%1 \c5minigame was reset.', %this.title);

				//let them know the rules
				%cl.ignoreSpam = 1;
				serverCmdSlayer(%cl, "rules");
			}
		}
		for(%i = 0; %i < %this.numMembers["AiController"]; %i ++)
		{
			%ai = %this.member["AiController", %i];
			%t = %ai.slyrTeam;

			%ai.setDead(0);
			%ai.setLives((isObject(%t) && %t.lives >= 0) ? %t.lives : %this.lives);
			if(%this.clearStats)
			{
				%ai.setKills(0);
				%ai.setDeaths(0);
			}

			//Bot Objectives
			%ai.removeAllObjectives();
			%ai.assignObjectives();
		}

		%parent = parent::reset(%this, %client);

		//vehicles must be reset after the parent
		%this.resetVehicles();

		%this.gameMode.callback("onMiniGameReset", %client);
		%this.setResetting(0); //the minigame is no longer resetting

		Slayer_Support::Debug(1, "Slayer_MiniGameSO::Reset");

		//HANDLE STARTING THE NEW ROUND
		%this.onReset();

		return %parent;
	}

	function Slayer_MiniGameSO::endGame(%this)
	{
		Slayer_Support::Debug(1, "Minigame", "Ending");

		%this.isEnding = true;

		%this.gameMode.callback("onGameModeEnd");

		//remove all of our bots
		for(%i = %this.numMembers["AiController"] - 1; %i >= 0; %i --)
			%this.member["AiController", %i].delete();

		for(%i = 0; %i < %this.numMembers; %i ++)
		{
			%cl = %this.member[%i];

			%cl.setDead(0);
			%cl.setLives(0);
			%cl.setKills(0);
			%cl.setDeaths(0);
			%cl.wins = 0;

			%cl.spawnPlayer();

			clearBottomPrint(%cl);
			clearCenterPrint(%cl);
			commandToClient(%cl, 'Slayer_ForceGUI', "ALL", 0);
		}

		%this.resetBricks();
		%this.resetVehicles();
		%this.resetCapturePoints();

		%parent = parent::endGame(%this);

		%this.delete();

		return %parent;
	}

	function miniGameCanUse(%objA, %objB)
	{
		Slayer_Support::Debug(3, "minigameCanUse", %objA TAB %objB);

		%miniA = getMinigameFromObject(%objA);
		%miniB = getMinigameFromObject(%objB);

		if(isSlayerMinigame(%miniA))
		{
			return minigameCanSlayerUse(%objA, %objB, %miniA, %miniB);
		}
		else if(isSlayerMinigame(%miniB))
		{
			return minigameCanSlayerUse(%objB, %objA, %miniB, %miniA);
		}

		return parent::miniGameCanUse(%objA, %objB);
	}

	//Determines if two objects are in the same or compatible Slayer minigames.
	//@see	miniGameCanUse
	//@private
	function minigameCanSlayerUse(%objA, %objB, %miniA, %miniB)
	{
		%special = %miniA.gameMode.callback("miniGameCanUse", %objA, %objB);
		if(%special !$= "" && %special != -1)
			return %special;

		%blidA = Slayer_Support::getBLIDFromObject(%objA);
		%blidB = Slayer_Support::getBLIDFromObject(%objB);

		//bricks that we planted online need to work in LAN
		if((%blidA != "999999" || %blidB != getNumKeyID()) &&
			%blidA != "888888" && %blidB != "888888"
		)
		{
			if(%miniA.playersUseOwnBricks)
			{
				%trust = getTrustLevel(%objA, %objB);

				if((%trust !$= "" && %trust < $TrustLevel::Build) || %blidA != %blidB)
					return false;
			}

			if(%miniA.useAllPlayersBricks)
			{
				if(isObject(%miniB) && %miniA != %miniB)
					return false;
			}
			else if(%blidB != %miniA.creatorBLID)
			{
				if(%miniA != %objB.minigame)
				{
					if(%blidA != -1 && %blidB != -1)
						return false;
				}
			}
		}
		
		//minigame regions
		if(strLen(%miniA.region))
		{
			%typeA = %objA.getType();
			if(%typeA & $TypeMasks::FxBrickAlwaysObjectType)
			{
				if(!%miniA.isPointInRegion(%objA.position))
				{
					return false;
				}
			}
			%typeB = %objB.getType();
			if(%typeB & $TypeMasks::FxBrickAlwaysObjectType)
			{
				if(!%miniA.isPointInRegion(%objB.position))
				{
					return false;
				}
			}
		}

		//team vehicles
		if(!%objB.doNotCheckTeamVehicle &&
			Slayer_Support::isVehicle(%objB) && 
			isObject(%spawn = %objB.spawnBrick) &&
			%spawn.getDatablock().slyrType $= "TeamVehicle" &&
			isObject(%clientA = Slayer_Support::getClientFromObject(%objA)) &&
			isObject(%team = %clientA.getTeam()) &&
			(%color = %spawn.getTeamControl()) != %team.color && 
			(%teams = %miniA.teams.getTeamsFromColor(%color, "COM", 1)) !$= ""
		)
		{
			%clientA.schedule(0, centerPrint, "<color:ff00ff>Only members of"
				SPC %teams SPC "\c5may use this vehicle.", 2);
			return false;
		}

		return true;
	}

	//Determines whether two objects can damage each other.
	function minigameCanDamage(%objA, %objB)
	{
		if(!isObject(%objA) || !isObject(%objB))
			return parent::minigameCanDamage(%objA, %objB);

		//get minigames
		%miniA = getMinigameFromObject(%objA);
		%miniB = getMinigameFromObject(%objB);

		//figure out which minigame to use
		%isA = isObject(%miniA);
		%isB = isObject(%miniB);
		if(%isA && !%isB)
			%mini = %miniA;
		else if(!%isA && %isB)
			%mini = %miniB;
		else if(%miniA == %miniB)
			%mini = %miniA;
		else
			return parent::minigameCanDamage(%objA, %objB);

		//bad minigame
		if(!isSlayerMinigame(%mini))
			return parent::minigameCanDamage(%objA, %objB);

		Slayer_Support::Debug(3, "minigameCanDamage", %objA TAB %objB);

		//get classes
		%classA = %objA.getClassName();
		%classB = %objB.getClassName();

		//prevent player damage outside minigame
		if(%classB $= "Player" || (%classB $= "AiPlayer" && !Slayer_Support::isVehicle(%objB)))
		{
			if(%miniA != %miniB)
			{
				Slayer_Support::Debug(3, "minigameCanDamage", "False - Player not in minigame");
				return false;
			}
		}
		else //prevent damaging stuff not in the minigame
		{
			%objB.doNotCheckTeamVehicle = 1;
			%canUse = minigameCanUse(%objA, %objB);
			%objB.doNotCheckTeamVehicle = "";

			if(!%canUse)
			{
				Slayer_Support::Debug(3, "minigameCanDamage", "False - Object not in minigame");
				return false;
			}
		}

		//run a basic gamemode canDamage check
		%special = %mini.gameMode.callback("miniGameCanDamage", %objA, %objB);
		if(%special != -1 && %special !$= "")
		{
			Slayer_Support::Debug(3, "minigameCanDamage", %special SPC "- Gamemode is in charge");
			return %special;
		}

		//check if we can damage them, according to minigame prefs
		if(!%mini.canDamage(%objA, %classA, %objB, %classB) || !%mini.canDamage(%objB, %classB, %objA, %classA))
		{
			Slayer_Support::Debug(3, "minigameCanDamage", "False - Denied by preferences");
			return false;
		}

		//get clients
		%clientA = Slayer_Support::getClientFromObject(%objA);
		%clientB = Slayer_Support::getClientFromObject(%objB);

		//gamemode clientCanDamage check
		%special = %mini.gameMode.callback("miniGameClientCanDamage", %clientA, %clientB);
		if(%special !$= "" && %special != -1)
		{
			Slayer_Support::Debug(3, "minigameCanDamage", %special SPC "- Gamemode client check");
			return %special;
		}

		if(%clientA != %clientB && isObject(%clientA) && isObject(%clientB))
		{
			%teamA = %clientA.slyrTeam;
			%teamB = %clientB.slyrTeam;
			if(isObject(%teamA) && isObject(%teamB) && !%mini.Teams.canDamage(%teamA, %teamB))
			{
				Slayer_Support::Debug(3, "minigameCanDamage", "False - Friendly fire");
				return false;
			}
		}

		return true;
	}

	//why do minigames return -1 when checked with this...?
	function getMinigameFromObject(%obj)
	{
		%parent = parent::getMinigameFromObject(%obj);

		if(!isObject(%parent) && isObject(%obj) && !%obj.doNotCheckSlayerMinigame)
		{
			%className = %obj.getClassName();

			if(striPos("fxDtsBrick SimGroup", %className) >= 0)
			{
				%blid = Slayer_Support::getBLIDFromObject(%obj);
				if(%blid >= 0)
				{
					%mini = Slayer.Minigames.getHighestPriorityMinigame(%blid);
					if(isObject(%mini))
						return %mini;
				}
			}

			//MINIGAME
			if(%obj.class $= "MiniGameSO" || %obj.superClass $= "MiniGameSO")
				return %obj;
		}

		return %parent;
	}

	function getBrickGroupFromObject(%obj)
	{
		if(%obj.isSlayerFakeClient && isObject(%obj.brickgroup))
			return %obj.brickgroup;

		return parent::getBrickGroupFromObject(%obj);
	}
};
activatePackage(Slayer_MiniGameSO);

//----------------------------------------------------------------------
// Deprecated Functions - DO NOT USE
//----------------------------------------------------------------------

//@DEPRECATED
function Slayer_MiniGameSO::setPref(%this, %category, %title, %value)
{
	warn("\c2WARN: Slayer_MiniGameSO::setPref is deprecated! Use the new API.");
	//compatibility
	if(%category $= "Minigame" && %title $= "Gamemode")
		%title = "Game Mode";
	%pref = Slayer.Prefs.getPrefSO(%category, %title);
	if(!isObject(%pref))
		return false;
	//compatibility
	if(%category $= "Minigame" && %title $= "Game Mode")
	{
		if(isObject(Slayer.gameModes.template[%value]))
			%value = Slayer.gameModes.template[%value];
		else if(isObject(Slayer.gameModes.template[$Slayer::GameModes::NewClass[%value]]))
		{
			warn("\c2WARN: Game mode class" SPC %value SPC "is deprecated! Use" SPC $Slayer::GameModes::NewClass[%value] @ ".");
			%value = Slayer.gameModes.template[$Slayer::GameModes::NewClass[%value]];
		}
	}
	return %pref.setValue(%value, %this);
}

//@DEPRECATED
function Slayer_MiniGameSO::getPref(%this, %category, %title)
{
	warn("\c2WARN: Slayer_MiniGameSO::getPref is deprecated! Use the new API.");
	%pref = Slayer.Prefs.getPrefSO(%category, %title);
	if(!isObject(%pref))
		return false;
	return %pref.getValue(%this);
}
